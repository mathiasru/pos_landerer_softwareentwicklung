- [OBJEKTORIENTIERTE ANALYSE](#objektorientierte-analyse)
  - [Requirements-Engineering](#requirements-engineering)
  - [Strukturelle Modellierung](#strukturelle-modellierung)
  - [**Dynamische Modellierung**](#dynamische-modellierung)
- [ANFORDERUNGSANALYSE](#anforderungsanalyse)
    - [USE-CASE](#use-case)
    - [MOCKUPS](#mockups)
- [Diagramme](#diagramme)
  - [USE-CASE DIAGRAMME](#use-case-diagramme)
    - [USE-CASE-DIAGRAMM 1](#use-case-diagramm-1)
    - [USE-CASE-DIAGRAMM 2](#use-case-diagramm-2)
    - [USE-CASE-DETAILBESCHREIBUNGEN](#use-case-detailbeschreibungen)
  - [KLASSENDIAGRAMME](#klassendiagramme)
    - [KLASSENDIAGRAMME 1](#klassendiagramme-1)
    - [KLASSENDIAGRAMME 2](#klassendiagramme-2)
    - [KLASSENDIAGRAMME 3](#klassendiagramme-3)
    - [KLASSENDIAGRAMME 4](#klassendiagramme-4)
  - [SEQUENZDIAGRAMM](#sequenzdiagramm)
    - [SEQUENZDIAGRAMM 1](#sequenzdiagramm-1)
  - [AKTIVITÄTSDIAGRAMM](#aktivitätsdiagramm)
    - [AKTIVITÄTSDIAGRAMM 1](#aktivitätsdiagramm-1)
  - [ZUSTANDSDIAGRAMM](#zustandsdiagramm)
    - [ZUSTANDSDIAGRAMM 1](#zustandsdiagramm-1)

# OBJEKTORIENTIERTE ANALYSE

Das Ziel der Analyse ist es, die Anforderungen eines Auftraggebers an ein neues Softwaresystem zu ermitteln und zu beschreiben. Die objektorientierte Analyse geht von Objekten aus, die sich in der realen Welt befinden. Das zu realisierende Problem soll verstanden und in einem OOA-Modell beschrieben werden.

## Requirements-Engineering

Ziel des Requirements Engineering ist es, die Anforderungen an ein neues Softwareprodukt zu ermitteln, zu spezifizieren, zu analysieren, zu validieren und daraus eine fachliche Lösung abzuleiten beziehungsweise ein Produktmodell zu entwickeln.

## Strukturelle Modellierung

Zur Modellierung der inneren Struktur eines Systems werden Strukturdiagramme herangezogen. Die beiden wichtigsten Diagrammtypen des objektorientierten Designs sind das Klassendiagramm (UML) und Objektdiagramm.

## **Dynamische Modellierung**

Das dynamische Modell beschreibt die Funktionsabläufe. Anwendungsfälle beschreiben die durchgeführten Aufgaben auf einem hohen Abstraktionsniveau. Diese können durch Aktivitätsdiagramme oder Zustandsdiagramme noch verfeinert werden. (Use-Cases, Sequenzdiagramm, Kommunikationsdiagramm, Zustandsdiagramm)

# ANFORDERUNGSANALYSE

Um das Ergebnis eines Projektes möglichst nah an die Vorstellungen des Auftraggebers heranzubringen, sollten benötigte Anforderungen analysiert und ausgewertet werden. Um vorzubeugen, dass keine Probleme bei der Projektentwicklung auftreten, ist die laufende Kommunikation zwischen Auftragnehmer und -geber essenziell. Sobald Anforderungen konkret definiert und aufeinander abgestimmt wurden, kann mit der tatsächlichen Produktentwicklung begonnen werden.

Die Anforderungen eines Projektes lassen sich in funktionale und nichtfunktionale Anforderungen unterteilen. Zusätzlich können sie in ihrer Notwendigkeit kategorisiert werden (kann/muss). Während funktionale Anforderungen ausschließlich die Funktionalität des Produktes beschreiben, stellen nichtfunktionale die Leistungen dar, die über den Systemzweck hinauslaufen.

### USE-CASE

Mithilfe von User Stories können Funktionalitäten, textuell, aus der Sicht des Anwenders, beschrieben werden. Der Kunde kann sich dadurch besser vorstellen welche Anforderungen von der Applikation erfüllt werden sollen. Besonders bei umfangreichen Systemen ist dies ein essentieller Vorgang, damit bei der Kommunikation zwischen Auftraggeber und Auftragnehmer keine Differenzen entstehen.

Use Cases beschreiben Szenarien aus der Sicht des Anwenders. Im Sinne der Anforderungsanalyse konzentriert man sich bei der sogenannten “Anwendungsfallanalyse” auf das Finden funktionaler Anforderungen. Die Beschreibung, wie das System für den Benutzer aussieht, hilft auch beim Erkennen von Vorteilen und Konsequenzen, die infolgedessen gezogen werden müssen.

In einem System existieren meistens mehrere Anwendungsfälle und mehrere sogenannte Akteure. Ein Akteur entspricht einer Anwendergruppe, der mehrere Use Cases ausführen kann.

Mehrere Use Cases können in einem Use Case Diagramm übersichtlich dargestellt werden. Wichtig ist, dass es sich dabei nicht um ein dynamisches Modell handelt. Für besonders wichtige Anwendungsfälle sollte auch eine textuelle, tabellarische Auflistung erfolgen. Aus der detaillierten Aufschlüsselung eines Use Cases können weitere Maßnahmen für den funktionalen Ablauf abgeleitet werden.

### MOCKUPS

Mockups können als Vorführmodell genutzt werden, um das Design des geplanten Produkts zu präsentieren und einen guten Eindruck der Funktionalität zu vermitteln. Die Gestaltung der Benutzeroberfläche kann sich im Zuge der Arbeit in geringem Maße verändern.

# Diagramme
## USE-CASE DIAGRAMME

Hierbei soll gezeigt werden:

- wo die Systemgrenzen liegen, um systeminterne systemexterne Elemente voneinander unterscheiden
zu können
- welcher Akteur welche Prozesse anstoßen kann
- und welche Prozessschritte wie zusammenhängen und ggf. mehrfach verwendet werden können
- Das Use Case Diagramm wird relativ früh im Prozess der Softwareentwicklung erstellt.

### USE-CASE-DIAGRAMM 1

In einem Kino kann ein Gast Kinokarten an der Kasse kaufen, die vorbestellt sein könnten. Außerdem ist es möglich Popcorn und Getränke zu bestellen. Danach bezahlt der Kunde beim Kassierer die Rechnung. Es ist auch möglich mit Kreditkarte zu bezahlen, welche bei Bedarf einer automatischen Prüfung unterzogen werden kann.

![Use_Case_Kino_01.png](01_Bericht_Objektorientierte_Analyse_Bilder/Use_Case_Kino_01.png)

### USE-CASE-DIAGRAMM 2

Es soll ein Anwendungssystem zur Unterstützung der Geschäftsprozesse in einem Krankenhaus
entwickelt werden. 

- Herr Müller und Herr Maier seien in der Verwaltung angestellt.
- Herr Müller soll Mitarbeiter einstellen und entlassen können. Sowohl Herr Müller als auch Herr Maier
    
    kann Patienten aufnehmen und entlassen.
    
- Sowohl bei der Einstellung von Mitarbeitern, als auch bei der Aufnahme von Patienten müssen Name
    
    und Adresse erfasst werden. Um redundante Anwendungsfall-Beschreibungen zu verhindern wird
    
    diese Tätigkeit in einen gesonderten Anwendungsfall ausgelagert.
    
- Falls der einzustellende Mitarbeiter bzw. der aufzunehmende Patient seinen Wohnsitz am Ort des
    
    Krankenhauses hat, wird geprüft, ob die angegebene Adresse am Wohnort existiert. Lagern Sie auch
    diesen Anwendungsfall aus.
    
![Use_Case_Krankenhaus_02.png](01_Bericht_Objektorientierte_Analyse_Bilder/Use_Case_Krankenhaus_02.png)

### USE-CASE-DETAILBESCHREIBUNGEN

Priorisieren Sie die aus den vorherigen beiden Use-Case-Übungen und definieren Sie die Use-Case-Details
(level, complexity, status, pre-conditions, post-conditions and assumptions, event flow etc.) für die vorherigen
beiden Use-Case-Übungen.
	

| Name | Kino besuchen |
| --- | --- |
| Kurzbeschreibung | Der Gast kann eine Kinokarte an der Kassa bestellen oder kann diese vorbestellen. Zudem können Popcorn und Getränke bestellt werden. Die Bestellung kann Bar oder mit Kreditkarte bezahlt werden. Diese wird dann vom System noch überprüft. |
| Akteure | Gast |
| Trigger | Gast besucht das Kino |
| Essenzielle Schritte | 1. Der Gast besucht das Kino.
|   | 2. Der Gast bestellt die Kinokarte an der Kasse.
|   | 3. Der Gast bezahlt die Kinokarte bar an der Kasse. |
| Erweiterungen | 2a. Der Gast bestellt die Karte im Vorfeld.
|   | 2b. Der Gast bestellt zusätzlich Popcorn und Getränke.
|   | 3a. Der Gast bezahlt die Bestellung an der Kasse mit Kreditkarte.
|   | 3a1. Es wird überprüft ob die Kreditkarte gültig ist. |

| Name | Patient aufnehmen |
| --- | --- |
| Ziel im Kontext | Patient wid im Krankenhaus aufgenommen |
| Akteure | Verwaltungsmitarbeiter, Patient |
| Trigger | Patient besucht das Krankenhaus |
| Essenzielle Schritte | 1. Der Patient besucht das Krankenhaus um aufgenommen zu werden.
|   | 2. Der Name und Adresse werden von einem Verwaltungsmitarbeiter aufgenommen. |
| Erweiterungen | 2a. Der Verwaltungsmitarbeiter stellt fest, dass der Wohnsitz dem selben Ort wie des Krankenhauses entspricht.
|   | 2a1. Es wird überprüft ob die angegebene Adresse existiert. |

## KLASSENDIAGRAMME

Das in der Praxis wohl am meisten genutzte UML Diagramm ist das Klassendiagramm. Hier werden die Beziehungen einzelner Klassen (und ggf. Methoden und Eigenschaften) übersichtlich dargestellt.

### KLASSENDIAGRAMME 1

![UML_Bestellung_01.png](01_Bericht_Objektorientierte_Analyse_Bilder/UML_Bestellung_01.png)

Bestimmen Sie, ob die folgenden Aussagen zum Klassendiagramm richtig oder falsch sind.

- Es kann im System Kunden geben die nie eine Bestellung durchgeführt haben. (richtig, da die Kardinalität 0..* gegeben ist)
- Die Klasse Einzahlung ist die Oberklasse der Klasse Bestellung. (falsch, sie wäre die Mutterklasse von Barzahlung und Einzahlung mit Kreditkarte)
- Jedes Objekt der Klasse *Bestellung_Detail* besitzt genau einen Artikel. (richtig, da die Kardinalität 1 gegeben ist)
- Alle Einzahlungen mit Kreditkarte haben einen Betrag. (richtig, ist über die Vererbungshirarchie gegeben)
- Es ist möglich, dass ein Artikel keine Assoziation mit einem *Bestellung_Detail* besitzt. (richtig, da die Kardinalität 0..* gegeben ist)
- Jedes *Bestellung_Detail*, das Teil einer Bestellung ist, hat seinen eigenen Status und sein eigenes
Datum. (falsch, da es sich hier nicht um eine Vererbung handelt, sondern um eine Aggregation(ist ein Teil von))

### KLASSENDIAGRAMME 2

Jede Person hat einen Namen, eine Telefonnummer und E-Mail. Jede Wohnadresse wird von nur einer Person bewohnt. Es kann aber sein, dass einige Wohnadressen nichtbewohnt sind. Den Wohnadressen sind je eine Strasse, eine Stadt, eine PLZ und ein Land zugeteilt. Alle Wohnadressen können bestätigt werden und als Beschriftung (für Postversand) gedruckt werden. Es gibt zwei Sorten von Personen: Student, welcher sich für ein Modul einschreiben kann und Professor, welcher einen Lohn hat. Der Student besitzt eine Matrikelnummer und eine Durchschnittsnote.

![UML_Person_02.png](01_Bericht_Objektorientierte_Analyse_Bilder/UML_Person_02.png)

### KLASSENDIAGRAMME 3

Sie haben den Auftrag, eine Online-Videothek zu realisieren. Sie haben dazu folgende Angaben erhalten:

- Die Videothek unterstützt das Ausleihen von Filmen für registrierte Kunden. Dazu müssen
- Kunden sich zunächst mit ihrer Kundennummer und ihrem Passwort anmelden.
- Kunden werden zusammen mit ihrem Guthaben verwaltet.
- Filme besitzen einen individuellen Namen und Preis.
- Ein Film wird über einen Streaming-Server bereitgestellt. Der Server kann hierzu einen
kundenspezifischen Link generieren.

Modellieren Sie diesen Sachverhalt anhand eines Klassendiagramms. Wählen Sie sinnvolle Operationen (mit
möglichst vollständigen Signaturen) und Attribute für Ihre Klassen. Ergänzen Sie die Klassen um sinnvolle
Beziehungen und deren Kardinalitäten.

![UML_Videothek_03.png](01_Bericht_Objektorientierte_Analyse_Bilder/UML_Videothek_03.png)

### KLASSENDIAGRAMME 4

Beschreiben Sie den in dem Klassendiagramm dargestellten Sachverhalt in Worten.

![UML_Raumschiff_04.png](01_Bericht_Objektorientierte_Analyse_Bilder/UML_Raumschiff_04.png)

- Vererbungshierarchie - (Vererbung aller Datenfelder und Methoden)
    - Die Abstrakte Klasse Spielobjekt(kann nicht instanziert werden) ist die Oberklasse von der Klasse Raumschiff und der Abstrakten Klasse Laserschuss.
    - Die Abstrakte Klasse Laserschuss(kann nicht instanziert werden) ist die Oberklasse von der Klasse LaserschussGross und der Klasse LaserschussKlein.
    - Die Abstrakte Klasse Schussvorrichtung(kann nicht instanziert werden) ist die Oberklasse von der Klasse VorrichtungGross und der Klasse VorrichtungKlein.
- Beziehungen
    - Ein Raumschiff hat einhe kann beliebig viele LaserschussGross und LaserschussKlein Beziehung. (Kann Beziehung *)
    - Ein Raumschiff hat eine kann 0-10  VorrichtungGross und Vorrichtungklein Beziehung. (Kann Beziehung 0-10)
- Zugriffsmodifikatoren
    - Alle Datenfelder sind private. (durch - gekennzeichnet)
    - Alle Methoden und Konstruktoren sind public. (durch + gekennzeichnet)
- Rückgabetypen und Typen
    - Rückgabetypen stehen nach den : der Methode und sind in unserem Fall (void, int, Laserschuss).

## SEQUENZDIAGRAMM

Sequenzdiagramme fokussieren sich auf:

- einen bestimmten Prozessschritt („Szene“) in der gesamten Verarbeitung und die dabei benötigten
Objekte bzw. Teilnehmer
- Sequenzdiagramme beschreiben den Austausch von Nachrichten zwischen Objekten mittels Lebenslinien.
- die Methoden und Signale, welche hierbei für die Kommunikation benötigt werden
- den zeitlichen Ablauf der Aufrufe bzw. des Nachrichtenaustauschs
- Instanziierung und Zerstörung der Objekte im Zeitablauf

### SEQUENZDIAGRAMM 1

Modellieren Sie für die Online-Videothek (siehe Aufgabe 3) die Film Ausleihen Funktion. Erstellen Sie dazu ein
Sequenzdiagramm für folgenden Ablauf der Ausleihe:

- Die Videothek berechnet zuerst, ob das Guthaben des Kunden reicht um den Film zu bezahlen.
- Reicht das Guthaben nicht aus, wird stattdessen eine Aufforderung zum Auffüllen des Guthabens
    
    angezeigt.
    
- Falls das aktuelle Guthaben des Mitglieds ausreicht, veranlasst die Videothek einen Streaming-
    
    Server einen Link für den Film zu generieren.
    
- Die Videothek zeigt dem Benutzer den Link an, unter dem der Film zugreifbar ist.

Gehen Sie davon aus, dass sich das Mitglied bereits auf der Seite des gewünschten Films befindet. Wählen Sie
geeignete Namen für die Elemente Ihres Diagramms. Wichtig: Achten Sie darauf, dass Ihre Diagramme aus
Aufgabe 3 und 5 konsistent sind. Nutzen Sie im Sequenzdiagramm nur Klassen, Operationen, etc. die im
Klassendiagram aus Aufgabe 3 enthalten sind. Falls Sie im Sequenzdiagramm zusätzliche Operationen,
Parameter, etc. brauchen, ergänzen Sie das Klassendiagramm entsprechend!

![Sequenz_Videothek_01.png](01_Bericht_Objektorientierte_Analyse_Bilder/Sequenz_Videothek_01.png)

## AKTIVITÄTSDIAGRAMM

Das Aktivitätsdiagramm ist eines von fünf Diagrammen in UML, welches dynamische Zusammenhänge von Systemen modelliert.

Aktivitätsdiagramme legen das Hauptaugenmerk auf:

- was ein einzelner Schritt tun soll (also die Aktionen der Aktivität)
- in welcher Reihenfolge die Aktionen durchgeführt werden sollen
- wer für welche Aktionen verantwortlich ist

### AKTIVITÄTSDIAGRAMM 1

Ein Fluggast ist am Flughafen angekommen. Zur Überprüfung seines Tickets begibt er sich zum Schalterseiner Fluggesellschaft. Falls das Ticket in Ordnung ist, übergibt er am Schalter sein Gepäck. Falls mit dem Ticket etwas nicht stimmt, muss der Fluggast den Kundendienst konsultieren und er kann nichtmitfliegen. Das Gepäck wird zudem auf Übergewicht überprüft. Falls dem so ist, muss der Fluggast zusätzliche Kostenübernehmen. Falls aber das Gewicht in Ordnung ist, wird die Bordkarte ausgestellt.

![Aktivität_Flughafen_01.png](01_Bericht_Objektorientierte_Analyse_Bilder/Aktivitat_Flughafen_01.png)

## ZUSTANDSDIAGRAMM

Der Grundgedanke ist, das Verhalten eines endlichen Zustandsautomaten grafisch zu modellieren. Bei Zustandsautomaten gelten folgende Grundregeln:

- der Zustandsautomat befindet sich zu jeder Zeit in einem der definierten Zustände
- der Zustandsautomat kann niemals in zwei Zuständen gleichzeitig sein
- es gibt eine endliche Menge an Zuständen
- es gibt bedingte Verzweigungen (Entscheidungen)
- jeder Zustand kann eingenommen werden

Aus der Sichtweise eines Programmierers ist ein Zustand nichts anderes als die Belegung einer Zustands- variable mit fest definierten Werten

### ZUSTANDSDIAGRAMM 1

Erklären Sie folgendes Zustandsmodell zum Thema „Booten eines Bankomaten“.

![Zustandsdiagramm_Bankautomat_01.png](01_Bericht_Objektorientierte_Analyse_Bilder/Zustandsdiagramm_Bankautomat_01.png)

- Beim Booten des Bankautomaten wird ein Selbsttest durchgeführt.
- Sofern der Selbsttest problemlos (Result = true) durchgeführt wurde, wird die Grundstellung erreicht.
- Im Fehlerfall (Result = false) wird in einen Störungszustand gewechselt und von dort aus kann ein "ResetBefehl" durchgeführt werden.
- Wenn der Selbsttest die Dauer von 3 min überschreitet, wird auch in den Storungszustand gewechselt.
- Nach Einschieben der Karte wird in den Karte lesen Zustand gewechselt und die Karte geprüft.
- Nach erfolgreicher Überprüfung (Result = true) wird man zur PIN-Abfrage weitergeleitet, ansonsten (Result = false) wird zum Zustand Kartenrückgabe gewechselt.
- Nun hat der Kunde 30sec Zeit die Karte zu mitzunehmen, ansonsten wird in den Karte Ablegen Zustand gewechselt, von dort kann dann wieder mittels "Karte abgelegt" in die Grundstellung gewechselt werden.
- Die fortsetzenden Zustände wie Betrag wählen, Geld Mitnahme, Kontostand anzeigen usw., werden hier nicht dargestellt.