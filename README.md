# POS_Landerer_Softwareentwicklung

Hier werden wöchentlich die Aufgaben zum Unterrichtsgegenstand POS-Softwareentwicklung hochgeladen.

## Semster 3
- [x] Aufgabe 1 IDEs und JavaDoc
- [x] Aufgabe 2 Version Control System
- [x] Aufgabe 3 Build Tool Maven
- [x] Aufgabe 4 Fullstack Spring Boot
  - [x] Git
  - [x] User Management
  - [x] Department Management
  - [x] Spring Data JPA 
  - [x] Implementierung einer weiteren Entity
- [x] Aufgabe 5 Objektorientierte Analyse
- [x] Aufgabe 6 Mikroarchitektur

## Semester 4
- [x] Aufgabe 7 Datenpersistenz
- [x] Aufgabe 8 Test-Driven-Developement
- [x] Aufgabe 9 Prozessmanagement
- [ ] Aufgabe 10 Makroarchitektur
