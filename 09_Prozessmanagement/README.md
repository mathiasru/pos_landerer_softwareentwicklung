# SCRUM_FSE_myKids

Das Repo, in dem der SCRUM-Prozess durgeführt wurde, ist über folgenden Link erreichbar: https://gitlab.com/mathiasru/scrum_fse_mykids

* Product Owner - Mathias Rudig
* Scrum Master - Tobias Tilg
* Entwickler 1 - Dario Skocibusic (Backend)
* Entwickler 2 - Clemens Kerber (Frontend)

## Branches
* main
* Developer
* Backend
* Frontend 
