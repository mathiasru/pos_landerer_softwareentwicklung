package domain;

import exception.InvalidValueException;

import java.sql.Date;

public class Course extends BaseEntity {

    private String name;
    private String description;
    private int hours;
    private Date beginDate;
    private Date endDate;
    private CourseType courseType;

    //Konstruktor beim Auslesen aus Datenbank
    public Course(Long id, String name, String description, int hours, Date beginDate, Date endDate, CourseType courseType) throws InvalidValueException {
        super(id);
        this.setName(name);
        this.setDescription(description);
        this.setHours(hours);
        this.setBeginDate(beginDate);
        this.setEndDate(endDate);
        this.setCourseType(courseType);
    }

    //Konstruktor zum Erstellen, da die ID Autoincrement ist
    public Course(String name, String description, int hours, Date beginDate, Date endDate, CourseType courseType) throws InvalidValueException {
        super(null);
        this.setName(name);
        this.setDescription(description);
        this.setHours(hours);
        this.setBeginDate(beginDate);
        this.setEndDate(endDate);
        this.setCourseType(courseType);
    }

    public void setName(String name) throws InvalidValueException {
        if (name != null && name.length() > 1) {
            this.name = name;
        } else {
            throw new InvalidValueException("Der Kursname muss muss midestens 2 Zeichen lang sein!");
        }
    }

    public void setDescription(String description) throws InvalidValueException {
        if (description != null && description.length() > 10) {
            this.description = description;
        } else {
            throw new InvalidValueException("Die Kursbeschreibung muss muss midestens 10 Zeichen lang sein!");
        }
    }

    public void setHours(int hours) throws InvalidValueException {
        if (hours > 0 && hours <= 10) {
            this.hours = hours;
        } else {
            throw new InvalidValueException("Die Stundenanzahl darf nur zwischen 1-10 liegen!");
        }
    }

    public void setBeginDate(Date beginDate) throws InvalidValueException {
        if (beginDate != null) {
            if (this.endDate != null) {
                if (beginDate.before(this.endDate)) {
                    this.beginDate = beginDate;
                } else {
                    throw new InvalidValueException("Kursbeginn muss vor dem Kursende liegen!");
                }
            } else {
                this.beginDate = beginDate;
            }
        } else {
            throw new InvalidValueException("Kursbeginn darf nicht null / leer sein!");
        }
    }

    public void setEndDate(Date endDate) throws InvalidValueException {
        if (endDate != null) {
            if (beginDate != null) {
                if (endDate.after(beginDate)) {
                    this.endDate = endDate;
                } else {
                    throw new InvalidValueException("Kursende muss nach dem Kurbeginn liegen!");
                }
            } else {
                this.endDate = endDate;
            }
        } else {
            throw new InvalidValueException("Kursende darf nicht null / leer sein!");
        }
    }

    public void setCourseType(CourseType courseType) throws InvalidValueException {
        if (courseType != null) {
            this.courseType = courseType;
        } else {
            throw new InvalidValueException("Kurstyp darf nicht null / leer sein!");
        }
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public int getHours() {
        return hours;
    }

    public Date getBeginDate() {
        return beginDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public CourseType getCourseType() {
        return courseType;
    }

    @Override
    public String toString() {
        return "Course{" +
                "id='" + this.getId() + '\'' +
                ", name='" + this.name + '\'' +
                ", description='" + this.description + '\'' +
                ", hours=" + this.hours +
                ", beginDate=" + this.beginDate +
                ", endDate=" + this.endDate +
                ", courseType=" + this.courseType +
                '}';
    }
}
