package domain;

import exception.InvalidDateException;
import exception.InvalidValueException;

import java.sql.Date;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class Student extends BaseEntity {

    private String firstname;
    private String lastname;
    private Date birthdate;

    public Student(Long id, String firstname, String lastname, Date birthdate) throws InvalidValueException {
        super(id);
        this.setFirstname(firstname);
        this.setLastname(lastname);
        this.setBirthdate(birthdate);
    }

    public Student(String firstname, String lastname, Date birthdate) throws InvalidValueException {
        super(null);
        this.setFirstname(firstname);
        this.setLastname(lastname);
        this.setBirthdate(birthdate);
    }

    public void setFirstname(String firstname) throws InvalidValueException {
        if (firstname != null && firstname.length() > 1) {
            this.firstname = firstname;
        } else {
            throw new InvalidValueException("Der Vorname muss muss midestens 2 Zeichen lang sein!");
        }
    }

    public void setLastname(String lastname) throws InvalidValueException {
        if (lastname != null && lastname.length() > 1) {
            this.lastname = lastname;
        } else {
            throw new InvalidValueException("Der Nachname muss muss midestens 2 Zeichen lang sein!");
        }
    }

    public void setBirthdate(Date birthdate) throws InvalidDateException {
        if (birthdate != null) {
            if (this.isAdult(birthdate)) {
                this.birthdate = birthdate;
            } else {
                throw new InvalidDateException("Der Student muss mindesten 17 Jahre alt sein!");
            }
        } else {
            throw new InvalidDateException("Das Geburtsdatum darf nicht leer sein!");
        }
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    private boolean isAdult(Date birthdate) {
        LocalDate localDate = birthdate.toLocalDate();
        int day = localDate.getDayOfMonth();
        int month = localDate.getMonthValue();
        int year = localDate.getYear();
        Calendar aktDatum = Calendar.getInstance();
        Calendar gebDatum = new GregorianCalendar(year, month, day);
        int alter = aktDatum.get(Calendar.YEAR) - gebDatum.get(Calendar.YEAR);
        if (alter >= 17) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString() {
        return "Student{" +
                "id='" + super.getId() + '\'' +
                ", firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", birthdate=" + birthdate +
                '}';
    }
}
