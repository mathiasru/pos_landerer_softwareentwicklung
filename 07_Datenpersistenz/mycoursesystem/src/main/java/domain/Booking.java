package domain;

import java.sql.Date;

public class Booking extends BaseEntity{
    private Date createDate;
    private Long courseId;
    private Long studentId;

    public Booking(Long id, Date createAtDate, Long courseId, Long studentId) {
        super(id);
        this.setCreateAtDate(createAtDate);
        this.setCourseId(courseId);
        this.setStudentId(studentId);
    }

    public Booking(Long courseId, Long studentId) {
        super(null);
        this.setCreateAtDate(null);
        this.setCourseId(courseId);
        this.setStudentId(studentId);
    }

    public void setCreateAtDate(Date createAtDate) {
        this.createDate = createAtDate;
    }

    public void setCourseId(Long courseId) {
        this.courseId = courseId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public Date getCreateAtDate() {
        return createDate;
    }

    public Long getCourseId() {
        return courseId;
    }

    public Long getStudentId() {
        return studentId;
    }

    @Override
    public String toString() {
        return "Booking{" +
                "id=" + super.getId() +
                ",createAtDate=" + createDate +
                ", courseId=" + courseId +
                ", studentId=" + studentId +
                '}';
    }
}
