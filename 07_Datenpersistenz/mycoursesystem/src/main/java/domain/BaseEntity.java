package domain;

import exception.InvalidValueException;

/**
 * Basisklasse für alle Entitys
 */
public abstract class BaseEntity {
    private Long id;

    public BaseEntity(Long id) {
        this.setId(id);
    }

    /**
     * ID muss eine positive Zahl sein
     * @param id übergebene ID vom Typ Long
     */
    public void setId(Long id) {
        if (id == null || id >= 0){
            this.id = id;
        } else {
            throw new InvalidValueException("Kurs-ID muss größer gleich 0 sein"); //Unhandled Exception
        }
    }

    public Long getId() {
        return this.id;
    }

    @Override
    public String toString() {
        return "BaseEntity{" +
                "id=" + this.id +
                '}';
    }
}
