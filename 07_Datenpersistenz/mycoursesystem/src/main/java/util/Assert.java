package util;

public class Assert {

    public static void notNull(Object object) {
        if (object == null){
            throw new IllegalArgumentException("Reference must not be null!");
        }
    }
}
