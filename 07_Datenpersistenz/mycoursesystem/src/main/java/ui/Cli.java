package ui;

import dataaccess.booking.BookingRepositoryDAO;
import dataaccess.course.CourseRepositoryDAO;
import dataaccess.student.StudentRepositoryDAO;
import domain.Booking;
import domain.Course;
import domain.CourseType;
import domain.Student;
import exception.DatabaseException;
import exception.InvalidDateException;
import exception.InvalidValueException;

import java.sql.Date;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;

public class Cli {

    Scanner scanner;
    CourseRepositoryDAO courseRepositoryDAO;
    StudentRepositoryDAO studentRepositoryDAO;
    BookingRepositoryDAO bookingRepositoryDAO;

    public Cli(CourseRepositoryDAO courseRepositoryDAO, StudentRepositoryDAO studentRepositoryDAO, BookingRepositoryDAO bookingRepositoryDAO) {
        this.scanner = new Scanner(System.in);
        this.courseRepositoryDAO = courseRepositoryDAO;
        this.studentRepositoryDAO = studentRepositoryDAO;
        this.bookingRepositoryDAO = bookingRepositoryDAO;
    }

    public void start() {
        String input = "-";
        while (!input.equals("x")) {
            showMenue();
            input = scanner.nextLine();
            switch (input) {
                case "1" -> courseMenue();
                case "2" -> studentMenue();
                case "3" -> bookingMenue();
                case "x" -> System.out.println("Auf Wiedersehen");
                default -> inputError();
            }
        }
        scanner.close();
    }

    public void courseMenue() {
        String input = "-";
        while (!input.equals("x")) {
            showCourseMenue();
            input = scanner.nextLine();
            switch (input) {
                case "1" -> addCourse();
                case "2" -> showAllCourses();
                case "3" -> showCourseById();
                case "4" -> updateCourse();
                case "5" -> deleteCourseById();
                case "6" -> courseSearch();
                case "7" -> showRunningCourses();
                case "8" -> showAllStudents();
                case "x" -> System.out.println("zurück ins Hauptmenü");
                default -> inputError();
            }
        }
    }

    public void studentMenue() {
        String input = "-";
        while (!input.equals("x")) {
            showStudentMenue();
            input = scanner.nextLine();
            switch (input) {
                case "1" -> addStudent();
                case "2" -> showAllStudents();
                case "3" -> showStudentById();
                case "4" -> updateStudent();
                case "5" -> deleteStudentById();
                case "6" -> studentSearchByName();
                case "7" -> showStudentByBirthdateBetweenDate();
                case "x" -> System.out.println("zurück ins Hauptmenü");
                default -> inputError();
            }
        }
    }
    public void bookingMenue() {
        String input = "-";
        while (!input.equals("x")) {
            showBookingMenue();
            input = scanner.nextLine();
            switch (input) {
                case "1" -> addBooking();
                case "2" -> showAllBookings();
                case "x" -> System.out.println("zurück ins Hauptmenü");
                default -> inputError();
            }
        }
    }

    private void showMenue() {
        System.out.println("-----------HAUPTMENÜ-------------");
        System.out.println("(1) Kursmanagement \t (2) Studentmanagement \t (3) Buchungsmanagement");
        System.out.println("(x) Ende");

    }
    private void showCourseMenue() {
        System.out.println("-----------KURSMANAGEMENT-------------");
        System.out.println("(1) Kurs anlegen \t (2) Alle Kurse anzeigen \t (3) Kursdetails anzeigen \t");
        System.out.println("(4) Kurs ändern \t (5) Kurs löschen \t (6) Kurse filtern per Name und Beschreibung \t");
        System.out.println("(7) laufende Kurse anzeigen \t (8) Alle Studenten anzeigen \t (-) --- \t");
        System.out.println("(x) Ende");
    }
    private void showStudentMenue() {
        System.out.println("-----------STUDENTMANAGEMENT-------------");
        System.out.println("(1) Student anlegen \t (2) Alle Studenten anzeigen \t (3) Studentendetails anzeigen \t");
        System.out.println("(4) Student ändern \t (5) Student löschen \t (6) Student filtern per Vorname und Nachname \t");
        System.out.println("(7) Student filtern anhand Geburtsdatum");
        System.out.println("(x) Ende");
    }
    private void showBookingMenue() {
        System.out.println("-----------STUDENTMANAGEMENT-------------");
        System.out.println("(1) Buchung anlegen \t (2) Alle Buchungen anzeigen \t (3) Buchungsdetails anzeigen \t");
        System.out.println("(4) Buchung ändern \t (5) Buchung löschen \t (6) Alle Buchungen eines Studenten anzeigen \t");
        System.out.println("(7) Alle Buchungen eines Kurses anzeigen");
        System.out.println("(x) Ende");
    }

    private void addCourse() {
        String name, description;
        int hours;
        Date beginDate, endDate;
        CourseType courseType;

        try {
            System.out.println("Bitte alle Kursdaten angeben:");

            System.out.println("Name: ");
            name = scanner.nextLine();
            if (name.equals("")) {
                throw new IllegalArgumentException("Eingabe darf nicht leer sein");
            }

            System.out.println("Beschreibung: ");
            description = scanner.nextLine();
            if (description.equals("")) {
                throw new IllegalArgumentException("Eingabe darf nicht leer sein");
            }

            System.out.println("Stunden: ");
            hours = Integer.parseInt(scanner.nextLine()); //hier wird in der Domänenklasse validiert

            System.out.println("Startdatum (YYYY-MM-DD): ");
            beginDate = Date.valueOf(scanner.nextLine());

            System.out.println("Enddatum (YYYY-MM-DD): ");
            endDate = Date.valueOf(scanner.nextLine());

            System.out.println("Kurytyp (OE/BF/ZA/FF): ");
            courseType = CourseType.valueOf(scanner.nextLine());

            Optional<Course> optionalCourse = courseRepositoryDAO.insert(new Course(name, description, hours, beginDate, endDate, courseType));


            if (optionalCourse.isPresent()) {
                System.out.println("Kurs wurde angelegt: " + optionalCourse.get());
            } else {
                System.out.println("Kurs konnte nicht angelegt werden!");
            }

        } catch (IllegalArgumentException illegalArgumentException) {
            System.out.println("Eingabefehler: " + illegalArgumentException.getMessage());
        } catch (InvalidValueException invalidValueException) {
            System.out.println("Kursdaten nicht korrekt eingegeben: " + invalidValueException.getMessage());
        } catch (DatabaseException databaseException) {
            System.out.println("Datenbankfehler beim Einfügen: " + databaseException.getMessage());
        } catch (Exception exception) {
            System.out.println("Unbekannter Fehler beim Einfügen: " + exception.getMessage());
        }

    }

    private void showCourseById() {
        System.out.println("Eingabe Kurs-ID: ");

        try {
            Long courseId = Long.parseLong(scanner.nextLine());
            Optional<Course> courseOptional = courseRepositoryDAO.getById(courseId);
            if (courseOptional.isPresent()) {
                System.out.println(courseOptional.get());
            } else {
                System.out.println("Kurs mit der ID: " + courseId + " nicht gefunden!");
            }
        } catch (DatabaseException databaseException) { //RunntimeException müsste nicht behandelt werden
            System.out.println("Datenbankfehler: " + databaseException.getMessage());
        } catch (Exception exception) {
            System.out.println("Unbekannter Fehler bei Anzeige aller Kurse: " + exception.getMessage());
        }
    }

    private void showAllCourses() {
        System.out.println("Alle Kurse:");
        try {
            List<Course> courseList = courseRepositoryDAO.getAll();
            if (courseList.size() > 0) {
                for (Course course : courseList) {
                    System.out.println(course);
                }
            }
        } catch (DatabaseException databaseException) { //RunntimeException müsste nicht behandelt werden
            System.out.println("Datenbankfehler: " + databaseException.getMessage());
        } catch (Exception exception) {
            System.out.println("Unbekannter Fehler bei Anzeige aller Kurse: " + exception.getMessage());
        }
    }

    private void updateCourse() {
        System.out.println("Für welchen Kurs möchten Sie anhand der Kurs-ID ändern:");

        try {
            Long courseId = Long.parseLong(scanner.nextLine());
            Optional<Course> courseOptional = courseRepositoryDAO.getById(courseId);
            if (courseOptional.isEmpty()) {
                System.out.println("Kurs mit der ID: " + courseId + " nicht gefunden!");
            } else {
                Course course = courseOptional.get(); //brauchen wir um später die ID zu verwenden

                System.out.println("Änderungen für folgenden Kurs:");
                System.out.println(course);

                String name, description, hours, beginDate, endDate, courseType;

                System.out.println("Bitte neu Kursdaten angeben (Enter, falls keine Änderungen gewünscht ist):");
                System.out.println("Name: ");
                name = scanner.nextLine();
                System.out.println("Beschreibung: ");
                description = scanner.nextLine();
                System.out.println("Stunden: ");
                hours = scanner.nextLine(); //hier wird in der Domänenklasse validiert
                System.out.println("Startdatum (YYYY-MM-DD): ");
                beginDate = scanner.nextLine();
                System.out.println("Enddatum (YYYY-MM-DD): ");
                endDate = scanner.nextLine();
                System.out.println("Kurytyp (OE/BF/ZA/FF): ");
                courseType = scanner.nextLine();

                Optional<Course> optionalCourseUpdated = courseRepositoryDAO.update(
                        new Course(
                                course.getId(),
                                name.equals("") ? course.getName() : name,
                                description.equals("") ? course.getDescription() : description,
                                hours.equals("") ? course.getHours() : Integer.parseInt(hours),
                                beginDate.equals("") ? course.getBeginDate() : Date.valueOf(beginDate),
                                endDate.equals("") ? course.getEndDate() : Date.valueOf(endDate),
                                courseType.equals("") ? course.getCourseType() : CourseType.valueOf(courseType)
                        )
                );
                //Funktionale Schreibweise (IF/Else)
                optionalCourseUpdated.ifPresentOrElse(
                        (c) -> System.out.println("Kurs aktualisiert: " + c),
                        () -> System.out.println("Kurs konnte nicht angelegt werden.")
                );
            }
        } catch (IllegalArgumentException illegalArgumentException) {
            System.out.println("Eingabefehler: " + illegalArgumentException.getMessage());
        } catch (InvalidValueException invalidValueException) {
            System.out.println("Kursdaten nicht korrekt eingegeben: " + invalidValueException.getMessage());
        } catch (DatabaseException databaseException) {
            System.out.println("Datenbankfehler beim Updaten: " + databaseException.getMessage());
        } catch (Exception exception) {
            System.out.println("Unbekannter Fehler beim Update: " + exception.getMessage());
        }
    }

    private void deleteCourseById() {
        System.out.println("Welchen Kurs möchten Sie löschen? Bitte geben Sie die Kurs-ID ein:");

        try {
            Long courseId = Long.parseLong(scanner.nextLine());
            if (courseRepositoryDAO.deleteById(courseId)) {
                System.out.println("Kurs wurde gelöscht.");
            } else {
                System.out.println("Kurs mit der ID: " + courseId + " nicht gefunden!");
            }
        } catch (DatabaseException databaseException) {
            System.out.println("Datenbankfehler beim Löschen: " + databaseException.getMessage());
        } catch (IllegalArgumentException illegalArgumentException) {
            System.out.println("Datenbankfehler beim Illegal: " + illegalArgumentException.getMessage());
        } catch (Exception exception) {
            System.out.println("Unbekannter Fehler beim Löschen: " + exception.getMessage());
        }
    }

    private void courseSearch() {
        System.out.println("Bitte geben Sie einen Suchbegriff ein:");

        try {
            String searchString = scanner.nextLine();
            List<Course> courseList = courseRepositoryDAO.findAllCoursesByNameOrDescription(searchString);
            courseList.forEach(course -> {
                System.out.println(course);
            });

        } catch (DatabaseException databaseException) { //RunntimeException müsste nicht behandelt werden
            System.out.println("Datenbankfehler bei der Kurssuche: " + databaseException.getMessage());
        } catch (Exception exception) {
            System.out.println("Unbekannter Fehler bei der Kurssuche: " + exception.getMessage());
        }
    }

    private void showRunningCourses(){
        try {
            List<Course> courseList = courseRepositoryDAO.findAllRunningCourses();
            courseList.forEach(course -> {
                System.out.println(course);
            });

        } catch (DatabaseException databaseException) { //RunntimeException müsste nicht behandelt werden
            System.out.println("Datenbankfehler bei Anzeige der laufenden Kurse: " + databaseException.getMessage());
        } catch (Exception exception) {
            System.out.println("Unbekannter Fehler bei Anzeige der laufenden Kurse: " + exception.getMessage());
        }
    }

    //Students
    private void addStudent() {
        String firstname, lastname;
        Date birthdate;

        try {
            System.out.println("Bitte alle Studentendaten angeben:");

            System.out.println("Vorname: ");
            firstname = scanner.nextLine();
            if (firstname.equals("")) {
                throw new IllegalArgumentException("Eingabe darf nicht leer sein");
            }

            System.out.println("Nachname: ");
            lastname = scanner.nextLine();
            if (lastname.equals("")) {
                throw new IllegalArgumentException("Eingabe darf nicht leer sein");
            }

            System.out.println("Geburtsdatum (YYYY-MM-DD): ");
            birthdate = Date.valueOf(scanner.nextLine());

            Optional<Student> optionalStudent = studentRepositoryDAO.insert(new Student(firstname, lastname, birthdate));

            if (optionalStudent.isPresent()) {
                System.out.println("Student wurde angelegt: " + optionalStudent.get());
            } else {
                System.out.println("Student konnte nicht angelegt werden!");
            }

        } catch (IllegalArgumentException illegalArgumentException) {
            System.out.println("Eingabefehler: " + illegalArgumentException.getMessage());
        } catch (InvalidDateException invalidDateException) {
            System.out.println("Datumsfehler: " + invalidDateException.getMessage());
        } catch (InvalidValueException invalidValueException) {
            System.out.println("Studentendaten nicht korrekt eingegeben: " + invalidValueException.getMessage());
        } catch (DatabaseException databaseException) {
            System.out.println("Datenbankfehler beim Einfügen: " + databaseException.getMessage());
        } catch (Exception exception) {
            System.out.println("Unbekannter Fehler beim Einfügen: " + exception.getMessage());
        }

    }

    private void showStudentById() {
        System.out.println("Eingabe Student-ID: ");

        try {
            Long studentId = Long.parseLong(scanner.nextLine());
            Optional<Student> studentOptional = studentRepositoryDAO.getById(studentId);
            if (studentOptional.isPresent()) {
                System.out.println(studentOptional.get());
            } else {
                System.out.println("Student mit der ID: " + studentId + " nicht gefunden!");
            }
        } catch (DatabaseException databaseException) { //RunntimeException müsste nicht behandelt werden
            System.out.println("Datenbankfehler: " + databaseException.getMessage());
        } catch (Exception exception) {
            System.out.println("Unbekannter Fehler bei Anzeigen des Studenten: " + exception.getMessage());
        }
    }

    private void showAllStudents() {
        System.out.println("Alle Studenten:");
        try {
            List<Student> studentList = studentRepositoryDAO.getAll();
            if (studentList.size() > 0) {
                for (Student student : studentList) {
                    System.out.println(student);
                }
            }
        } catch (DatabaseException databaseException) { //RunntimeException müsste nicht behandelt werden
            System.out.println("Datenbankfehler: " + databaseException.getMessage());
        } catch (Exception exception) {
            System.out.println("Unbekannter Fehler bei Anzeige aller Studenten: " + exception.getMessage());
        }
    }

    private void updateStudent() {
        System.out.println("Für welchen Studenten möchten Sie anhand der Student-ID ändern:");

        try {
            Long studentId = Long.parseLong(scanner.nextLine());
            Optional<Student> studentOptional = studentRepositoryDAO.getById(studentId);
            if (studentOptional.isEmpty()) {
                System.out.println("Student mit der ID: " + studentId + " nicht gefunden!");
            } else {
                Student student = studentOptional.get(); //brauchen wir um später die ID zu verwenden

                System.out.println("Änderungen für folgenden Studenten:");
                System.out.println(student);

                String firstname, lastname, birthdate;

                System.out.println("Bitte neu Studentendaten angeben (Enter, falls keine Änderungen gewünscht ist):");
                System.out.println("Vorname: ");
                firstname = scanner.nextLine();
                System.out.println("Nachname: ");
                lastname = scanner.nextLine();
                System.out.println("Geburtsdatum (YYYY-MM-DD): ");
                birthdate = scanner.nextLine();

                Optional<Student> optionalStudentUpdated = studentRepositoryDAO.update(
                        new Student(
                                student.getId(),
                                firstname.equals("") ? student.getFirstname() : firstname,
                                lastname.equals("") ? student.getLastname() : lastname,
                                birthdate.equals("") ? student.getBirthdate() : Date.valueOf(birthdate)
                        )
                );
                //Funktionale Schreibweise (IF/Else)
                optionalStudentUpdated.ifPresentOrElse(
                        (c) -> System.out.println("Student aktualisiert: " + c),
                        () -> System.out.println("Student konnte nicht angelegt werden.")
                );
            }
        } catch (IllegalArgumentException illegalArgumentException) {
            System.out.println("Eingabefehler: " + illegalArgumentException.getMessage());
        } catch (InvalidDateException invalidDateException) {
            System.out.println("Datumsfehler: " + invalidDateException.getMessage());
        } catch (InvalidValueException invalidValueException) {
            System.out.println("Studentendaten nicht korrekt eingegeben: " + invalidValueException.getMessage());
        } catch (DatabaseException databaseException) {
            System.out.println("Datenbankfehler beim Updaten: " + databaseException.getMessage());
        } catch (Exception exception) {
            System.out.println("Unbekannter Fehler beim Update: " + exception.getMessage());
        }
    }

    private void deleteStudentById() {
        System.out.println("Welchen Studenten möchten Sie löschen? Bitte geben Sie die Student-ID ein:");

        try {
            Long studentId = Long.parseLong(scanner.nextLine());
            if (studentRepositoryDAO.deleteById(studentId)) {
                System.out.println("Student wurde gelöscht.");
            } else {
                System.out.println("Student mit der ID: " + studentId + " nicht gefunden!");
            }
        } catch (DatabaseException databaseException) {
            System.out.println("Datenbankfehler beim Löschen: " + databaseException.getMessage());
        } catch (IllegalArgumentException illegalArgumentException) {
            System.out.println("Datenbankfehler beim Illegal: " + illegalArgumentException.getMessage());
        } catch (Exception exception) {
            System.out.println("Unbekannter Fehler beim Löschen: " + exception.getMessage());
        }
    }

    private void studentSearchByName() {
        System.out.println("Bitte geben Sie den gesuchten vor- oder Nachnamen ein:");

        try {
            String name = scanner.nextLine();
            List<Student> studentList = studentRepositoryDAO.findAllStudentsByFirstnameOrLastname(name);
            studentList.forEach(student -> {
                System.out.println(student);
            });

        } catch (DatabaseException databaseException) { //RunntimeException müsste nicht behandelt werden
            System.out.println("Datenbankfehler bei der Studentensuche: " + databaseException.getMessage());
        } catch (Exception exception) {
            System.out.println("Unbekannter Fehler bei der Studentensuche: " + exception.getMessage());
        }
    }

    private void showStudentByBirthdateBetweenDate(){
        System.out.println("Bitte geben Sie Start- und Enddatum zum Suchen ein:");
        Date startdate, enddate;

        try {
            System.out.println("Startdatum (YYYY-MM-DD): ");
            startdate = Date.valueOf(scanner.nextLine());
            System.out.println("Enddatum (YYYY-MM-DD): ");
            enddate = Date.valueOf(scanner.nextLine());

            List<Student> studentList = studentRepositoryDAO.findAllStudentsByBirthdateBetweenDate(startdate, enddate);
            studentList.forEach(student -> {
                System.out.println(student);
            });

        } catch (DatabaseException databaseException) { //RunntimeException müsste nicht behandelt werden
            System.out.println("Datenbankfehler bei Anzeige der gesuchten Studenten: " + databaseException.getMessage());
        } catch (Exception exception) {
            System.out.println("Unbekannter Fehler bei Anzeige der gesuchten Studenten: " + exception.getMessage());
        }
    }

    //Bookings
    private void addBooking() {
        Long courseId, studentId;

        try {
            System.out.println("Bitte alle weisen Sie der Buchung einen Kurs und einen Studenten zu:");

            showAllCourses();
            System.out.println("Kurs-ID: ");
            courseId = Long.parseLong(scanner.nextLine());

            showAllStudents();
            System.out.println("Student-ID: ");
            studentId = Long.parseLong(scanner.nextLine());

            Optional<Booking> optionalBooking = bookingRepositoryDAO.insert(new Booking(courseId, studentId));
            if (optionalBooking.isPresent()) {
                System.out.println("Buchung wurde angelegt: " + optionalBooking.get());
            } else {
                System.out.println("Buchung konnte nicht angelegt werden!");
            }

        } catch (IllegalArgumentException illegalArgumentException) {
            System.out.println("Eingabefehler: " + illegalArgumentException.getMessage());
        } catch (InvalidValueException invalidValueException) {
            System.out.println("Buchungsdaten nicht korrekt eingegeben: " + invalidValueException.getMessage());
        } catch (DatabaseException databaseException) {
            System.out.println("Datenbankfehler beim Einfügen: " + databaseException.getMessage());
        } catch (Exception exception) {
            System.out.println("Unbekannter Fehler beim Einfügen: " + exception.getMessage());
        }

    }

    private void showAllBookings() {
        System.out.println("Alle Buchungen:");
        try {
            List<Booking> bookingList = bookingRepositoryDAO.getAll();
            if (bookingList.size() > 0) {
                for (Booking booking : bookingList) {
                    System.out.println(booking);
                }
            }
        } catch (DatabaseException databaseException) { //RunntimeException müsste nicht behandelt werden
            System.out.println("Datenbankfehler: " + databaseException.getMessage());
        } catch (Exception exception) {
            System.out.println("Unbekannter Fehler bei Anzeige aller Buchungen: " + exception.getMessage());
        }
    }

    private void inputError() {
        System.out.println("Bitte nur die Zeichen des Menüs verwenden!");
    }
}
