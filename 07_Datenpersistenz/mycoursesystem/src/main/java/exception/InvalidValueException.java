package exception;

public class InvalidValueException extends RuntimeException { //Unhandled Exception
    public InvalidValueException(String message) {
        super(message);
    }
}
