import dataaccess.booking.MariaDbBookingRepositoryDAO;
import dataaccess.course.MariaDbCourseRepositoryDAO;
import dataaccess.student.MariaDbStudentRepositoryDAO;
import ui.Cli;

import java.sql.SQLException;

public class Main {

    public static void main(String[] args) {

        try {
            Cli cliMenue = new Cli(new MariaDbCourseRepositoryDAO(), new MariaDbStudentRepositoryDAO(), new MariaDbBookingRepositoryDAO()); //hier wird die Datenbankverbindung aufgebaut
            //Cli cliMenue = new Cli(new MySQLCourseRepositoryDAO(), new MySQLStudentRepositoryDAO(), new MariaDbBookingRepositoryDAO()); //hier wird die Datenbankverbindung aufgebaut
            cliMenue.start();
        } catch (SQLException e) {
            System.out.println(e.getMessage() + "SQL State: " + e.getSQLState());
        } catch (ClassNotFoundException e) {
            System.out.println("Datenbankfehler: " + e.getMessage());
        }
    }

}
