package dataaccess;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MySQLDatabaseConnection {

    private static Connection connection = null;

    private MySQLDatabaseConnection() { //hier kann keine Instanz per new ... gebildet werden
    }

    public static Connection getConnection(String url, String user, String pwd) throws ClassNotFoundException, SQLException {
        if (connection != null) {
            return connection;
        } else {
            Class.forName("com.mysql.cj.jdbc.Driver"); //Prüft ob die Klasse vorhanden ist
            connection = DriverManager.getConnection(url, user, pwd);
            return connection;
        }
    }
}

