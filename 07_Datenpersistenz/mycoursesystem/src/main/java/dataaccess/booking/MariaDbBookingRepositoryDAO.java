package dataaccess.booking;

import dataaccess.MariaDbDatabaseConnection;
import domain.Booking;
import domain.Course;
import domain.Student;
import exception.DatabaseException;
import util.Assert;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class MariaDbBookingRepositoryDAO implements BookingRepositoryDAO {
    public static final String CONNECTION_URL = "jdbc:mariadb://10.211.55.4:3306/courssystem"; //Treiber/Datenbankverbindung/DB
    public static final String USERNAME = "root"; //Username Datenbank
    public static final String PASSWORD = "123"; //Password Datenbank

    private final Connection CONNECTION; //Datenbankverbindung

    public MariaDbBookingRepositoryDAO() throws SQLException, ClassNotFoundException {
        this.CONNECTION = MariaDbDatabaseConnection.getConnection(CONNECTION_URL, USERNAME, PASSWORD);
    }

    @Override
    public Optional<Booking> insert(Booking booking) {
        //hier über statische Methode prüfen ob Fremdschlüssel in der Datenbank vorhanden, oder besser in Service Layer? (Abhängigkeit)
        Assert.notNull(booking);
        try {
            String sqlStatement = "INSERT INTO `bookings` (`course_id`, `student_id`) VALUES (?,?)";
            PreparedStatement preparedStatement = CONNECTION.prepareStatement(sqlStatement, Statement.RETURN_GENERATED_KEYS);  //um später den Generierten Key zu holen
            preparedStatement.setLong(1, booking.getCourseId());
            preparedStatement.setLong(2, booking.getStudentId());

            int affectedRows = preparedStatement.executeUpdate(); //wie viele Datensätze betroffen sind

            if (affectedRows == 0) {
                return Optional.empty(); //speichern hat nicht funktioniert
            }

            ResultSet generatedKeys = preparedStatement.getGeneratedKeys(); //generierten Schlüssel holen

            if (generatedKeys.next()) {
                return this.getById(generatedKeys.getLong(1)); //neu generierter Datensatz aus Datenbank holen
            } else {
                return Optional.empty();
            }
        } catch (SQLException sqlException) {
            throw new DatabaseException("Datenbankfehler: " + sqlException.getMessage());
        }

    }

    @Override
    public Optional<Booking> getById(Long id) {
        Assert.notNull(id); //null-check
        if (countBookingsInDbWithId(id) == 0) {
            return Optional.empty();
        } else {
            try {
                String sqlStatement = "SELECT * FROM `bookings` WHERE `bookings`.`id` = ?";
                PreparedStatement preparedStatement = CONNECTION.prepareStatement(sqlStatement);
                preparedStatement.setLong(1, id);
                ResultSet resultSet = preparedStatement.executeQuery();
                resultSet.next();
                //ORM
                Booking booking = new Booking(
                        resultSet.getLong("id"),
                        resultSet.getDate("create_date"),
                        resultSet.getLong("course_id"),
                        resultSet.getLong("student_id")
                );
                return Optional.of(booking);
            } catch (SQLException sqlException) {
                throw new DatabaseException("Datenbankfehler: " + sqlException.getMessage());
            }
        }
    }

    private int countBookingsInDbWithId(Long id) {
        try {
            String sqlStatement = "SELECT COUNT(*) FROM `bookings` WHERE `bookings`.`id` = ?";
            PreparedStatement preparedStatement = CONNECTION.prepareStatement(sqlStatement);
            preparedStatement.setLong(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            return resultSet.getInt(1); //Anzahl COUNT
        } catch (SQLException sqlException) {
            throw new DatabaseException("Datenbankfehler: " + sqlException.getMessage());
        }
    }

    @Override
    public List<Booking> getAll() {
        String sqlStatement = "SELECT * FROM `bookings`";
        try {
            PreparedStatement preparedStatement = CONNECTION.prepareStatement(sqlStatement);
            ResultSet resultSet = preparedStatement.executeQuery();
            List<Booking> bookingList = fillBookingListHelper(resultSet);
            return bookingList;
        } catch (SQLException sqlException) {
            throw new DatabaseException("Datenbankfehler: " + sqlException.getMessage());
        }
    }

    @Override
    public Optional<Booking> update(Booking entity) {
        //hier über statische Methode prüfen ob Fremdschlüssel in der Datenbank vorhanden, oder besser in Service Layer? (Abhängigkeit)
        return Optional.empty();
    }

    @Override
    public boolean deleteById(Long id) {
        return false;
    }

    private List<Booking> fillBookingListHelper(ResultSet resultSet) {
        try {
            List<Booking> bookingList = new ArrayList<>();
            while (resultSet.next()) {
                //ORM
                bookingList.add(new Booking(
                                resultSet.getLong("id"),
                                resultSet.getDate("create_date"),
                                resultSet.getLong("course_id"),
                                resultSet.getLong("student_id")
                        )
                );
            }
            return bookingList;
        } catch (SQLException sqlException) {
            throw new DatabaseException("Datenbankfehler: " + sqlException.getMessage());
        }
    }

    @Override
    public List<Course> findAllBookingsByCourse(Course course) {
        return null;
    }

    @Override
    public List<Course> findAllBookingsByStudent(Student student) {
        return null;
    }
}
