package dataaccess.booking;

import dataaccess.BaseRepositoryDAO;
import domain.Booking;
import domain.Course;
import domain.Student;

import java.util.List;

/**
 * Konkretes Interface das vob BasisRepositoryDAO Interface erbt und spezifizierte Methoden bereitstellt.
 * Hier muss beim Erben der Konkrete Typ angegeben werden.
 */
public interface BookingRepositoryDAO extends BaseRepositoryDAO<Booking, Long> {
    //hier kann vom Interface geerbt werden und die Typen spezialisiert werden
    //hier erben wir alle Methoden mit den spezialisierten Typen (Parametern)

    //Spezifische Methoden
    List<Course> findAllBookingsByCourse(Course course);
    List<Course> findAllBookingsByStudent(Student student);
}
