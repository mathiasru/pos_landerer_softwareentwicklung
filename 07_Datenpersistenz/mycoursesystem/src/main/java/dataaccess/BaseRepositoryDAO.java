package dataaccess;

import java.util.List;
import java.util.Optional;

/**
 * Standardisiertes Interface welches alle Standard CRUD-Methoden bereitstellt.
 * Von Diesem Interface können alle spezifischen DAO-Interfaces erben.
 * Beim Erben müssen konrtete Typen für die Platzhalter (Gererics T,I) angegeben werden
 * @param <T> Platzhalte Typ 1
 * @param <I> Platzhalte Typ 2
 */
public interface BaseRepositoryDAO<T, I> { //Generische Typinformation (Platzhalter T, I)

    //hier können durch die Verwendung von Generics alle beliebige Typen übergeben werden
    // Optional ist ein Wrappertyp der ohne oder mit Inhalt sein kann
    Optional<T> insert(T entity);
    Optional<T> getById(I id);
    List<T> getAll();
    Optional<T> update(T entity);
    boolean deleteById(I id);
}
