package dataaccess.student;

import dataaccess.BaseRepositoryDAO;
import domain.Student;

import java.sql.Date;
import java.util.List;

/**
 * Konkretes Interface das vob BasisRepositoryDAO Interface erbt und spezifizierte Methoden bereitstellt.
 * Hier muss beim Erben der Konkrete Typ angegeben werden.
 */
public interface StudentRepositoryDAO extends BaseRepositoryDAO<Student, Long> {
    //hier kann vom Interface geerbt werden und die Typen spezialisiert werden
    //hier erben wir alle Methoden mit den spezialisierten Typen (Parametern)

    //Spezifische Methoden
    List<Student> findAllStudentsByFirstnameOrLastname(String name);
    List<Student> findAllStudentsByBirthdateBetweenDate(Date startdate, Date enddate);
}
