package dataaccess.student;

import dataaccess.MariaDbDatabaseConnection;
import domain.Student;
import exception.DatabaseException;
import util.Assert;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class MariaDbStudentRepositoryDAO implements StudentRepositoryDAO {
    public static final String CONNECTION_URL = "jdbc:mariadb://10.211.55.4:3306/courssystem"; //Treiber/Datenbankverbindung/DB
    public static final String USERNAME = "root"; //Username Datenbank
    public static final String PASSWORD = "123"; //Password Datenbank

    private final Connection CONNECTION; //Datenbankverbindung

    public MariaDbStudentRepositoryDAO() throws SQLException, ClassNotFoundException {
        this.CONNECTION = MariaDbDatabaseConnection.getConnection(CONNECTION_URL, USERNAME, PASSWORD);
    }

    @Override
    public Optional<Student> insert(Student student) {
        Assert.notNull(student);
        try {
            String sqlStatement = "INSERT INTO `students` (`firstname`, `lastname`, `birthdate`) VALUES (?,?,?)";
            PreparedStatement preparedStatement = CONNECTION.prepareStatement(sqlStatement, Statement.RETURN_GENERATED_KEYS);  //um später den Generierten Key zu holen
            preparedStatement.setString(1, student.getFirstname());
            preparedStatement.setString(2, student.getLastname());
            preparedStatement.setDate(3, student.getBirthdate());

            int affectedRows = preparedStatement.executeUpdate(); //wie viele Datensätze betroffen sind

            if (affectedRows == 0) {
                return Optional.empty(); //speichern hat nicht funktioniert
            }

            ResultSet generatedKeys = preparedStatement.getGeneratedKeys(); //generierten Schlüssel holen

            if (generatedKeys.next()) {
                return this.getById(generatedKeys.getLong(1)); //neu generierter Datensatz aus Datenbank holen
            } else {
                return Optional.empty();
            }
        } catch (SQLException sqlException) {
            throw new DatabaseException("Datenbankfehler: " + sqlException.getMessage());
        }

    }

    @Override
    public Optional<Student> getById(Long id) {
        Assert.notNull(id); //null-check
        if (countStudentsInDbWithId(id) == 0) {
            return Optional.empty();
        } else {
            try {
                String sqlStatement = "SELECT * FROM `students` WHERE `students`.`id` = ?";
                PreparedStatement preparedStatement = CONNECTION.prepareStatement(sqlStatement);
                preparedStatement.setLong(1, id);
                ResultSet resultSet = preparedStatement.executeQuery();
                resultSet.next();
                //ORM
                Student student = new Student(
                        resultSet.getLong("id"),
                        resultSet.getString("firstname"),
                        resultSet.getString("lastname"),
                        resultSet.getDate("birthdate"));
                return Optional.of(student);
            } catch (SQLException sqlException) {
                throw new DatabaseException("Datenbankfehler: " + sqlException.getMessage());
            }
        }
    }

    private int countStudentsInDbWithId(Long id) {
        try {
            String sqlStatement = "SELECT COUNT(*) FROM `students` WHERE `students`.`id` = ?";
            PreparedStatement preparedStatement = CONNECTION.prepareStatement(sqlStatement);
            preparedStatement.setLong(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            int studentCount = resultSet.getInt(1); //Anzahl COUNT
            return studentCount;
        } catch (SQLException sqlException) {
            throw new DatabaseException("Datenbankfehler: " + sqlException.getMessage());
        }
    }

    @Override
    public List<Student> getAll() {
        String sqlStatement = "SELECT * FROM `students`";
        try {
            PreparedStatement preparedStatement = CONNECTION.prepareStatement(sqlStatement);
            ResultSet resultSet = preparedStatement.executeQuery();
            List<Student> studentList = fillStudentListHelper(resultSet);
            return studentList;
        } catch (SQLException sqlException) {
            throw new DatabaseException("Datenbankfehler: " + sqlException.getMessage());
        }
    }

    @Override
    public Optional<Student> update(Student student) {
        Assert.notNull(student);

        if (countStudentsInDbWithId(student.getId()) == 0) {
            return Optional.empty();
        }

        try {
            String sqlStatement = "UPDATE `students` SET `firstname` = ?, `lastname` = ?, `birthdate` = ? WHERE `students`.`id` = ?";
            PreparedStatement preparedStatement = CONNECTION.prepareStatement(sqlStatement);
            preparedStatement.setString(1, student.getFirstname());
            preparedStatement.setString(2, student.getLastname());
            preparedStatement.setDate(3, student.getBirthdate());

            int affectedRows = preparedStatement.executeUpdate();
            if (affectedRows == 0) {
                return Optional.empty();
            } else {
                return this.getById(student.getId());
            }

        } catch (SQLException sqlException) {
            throw new DatabaseException("Datenbankfehler: " + sqlException.getMessage());
        }

    }

    @Override
    public boolean deleteById(Long id) {
        Assert.notNull(id);
        boolean deleted = false;
        try {
            if (countStudentsInDbWithId(id) == 1) {
                String sqlStatement = "DELETE FROM `students` WHERE `students`.`id` = ?";
                PreparedStatement preparedStatement = CONNECTION.prepareStatement(sqlStatement);
                preparedStatement.setLong(1, id);
                preparedStatement.executeUpdate();
                deleted = true;
            }
            return deleted;
        } catch (SQLException sqlException) {
            throw new DatabaseException("Datenbankfehler: " + sqlException.getMessage());
        }
    }

    @Override
    public List<Student> findAllStudentsByFirstnameOrLastname(String name) {
        Assert.notNull(name);

        try {
            String sqlStatement = "SELECT * FROM `students` WHERE LOWER(`firstname`) LIKE LOWER(?) OR LOWER(`lastname`) LIKE LOWER(?) ";
            PreparedStatement preparedStatement = CONNECTION.prepareStatement(sqlStatement);
            preparedStatement.setString(1, "%" + name + "%");
            preparedStatement.setString(2, "%" + name + "%");
            ResultSet resultSet = preparedStatement.executeQuery();
            List<Student> studentList = fillStudentListHelper(resultSet);
            return studentList;
        } catch (SQLException sqlException) {
            throw new DatabaseException("Datenbankfehler: " + sqlException.getMessage());
        }
    }

    @Override
    public List<Student> findAllStudentsByBirthdateBetweenDate(Date startdate, Date enddate) {
        Assert.notNull(startdate);
        Assert.notNull(enddate);

        try {
            //SELECT * FROM students WHERE birthdate BETWEEN '1999-01-01' AND '2022-01-01';
            String sqlStatement = "SELECT * FROM `students` WHERE `birthdate` BETWEEN ? AND ?";
            PreparedStatement preparedStatement = CONNECTION.prepareStatement(sqlStatement);
            preparedStatement.setDate(1, startdate);
            preparedStatement.setDate(2, enddate);
            ResultSet resultSet = preparedStatement.executeQuery();
            List<Student> studentList = fillStudentListHelper(resultSet);
            return studentList;
        } catch (SQLException sqlException) {
            throw new DatabaseException("Datenbankfehler: " + sqlException.getMessage());
        }
    }

    private List<Student> fillStudentListHelper(ResultSet resultSet){
        try {
            List<Student> studentList = new ArrayList<>();
            while (resultSet.next()) {
                //ORM
                studentList.add(new Student(
                        resultSet.getLong("id"),
                        resultSet.getString("firstname"),
                        resultSet.getString("lastname"),
                        resultSet.getDate("birthdate"))
                );
            }
            return studentList;
        } catch (SQLException sqlException) {
            throw new DatabaseException("Datenbankfehler: " + sqlException.getMessage());
        }
    }
}
