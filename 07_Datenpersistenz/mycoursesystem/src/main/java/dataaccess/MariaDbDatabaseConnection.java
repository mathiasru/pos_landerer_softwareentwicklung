package dataaccess;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MariaDbDatabaseConnection {

    private static Connection connection = null;

    private MariaDbDatabaseConnection() { //hier kann keine Instanz per new ... gebildet werden
    }

    public static Connection getConnection(String url, String user, String pwd) throws ClassNotFoundException, SQLException {
        if (connection != null) {
            return connection;
        } else {
            Class.forName("org.mariadb.jdbc.Driver"); //Prüft ob die Klasse vorhanden ist
            connection = DriverManager.getConnection(url, user, pwd);
            return connection;
        }
    }
}

