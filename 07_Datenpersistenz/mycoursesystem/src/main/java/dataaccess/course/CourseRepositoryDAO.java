package dataaccess.course;

import dataaccess.BaseRepositoryDAO;
import domain.Course;

import java.util.List;

/**
 * Konkretes Interface das vob BasisRepositoryDAO Interface erbt und spezifizierte Methoden bereitstellt.
 * Hier muss beim Erben der Konkrete Typ angegeben werden.
 */
public interface CourseRepositoryDAO extends BaseRepositoryDAO<Course, Long> {
    //hier kann vom Interface geerbt werden und die Typen spezialisiert werden
    //hier erben wir alle Methoden mit den spezialisierten Typen (Parametern)

    //Spezifische Methoden
    List<Course> findAllCoursesByNameOrDescription(String searchtext);
    List<Course> findAllRunningCourses();

}
