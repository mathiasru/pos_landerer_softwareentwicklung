package dataaccess.course;

import dataaccess.MySQLDatabaseConnection;
import domain.Course;
import domain.CourseType;
import exception.DatabaseException;
import util.Assert;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class MySQLCourseRepositoryDAO implements CourseRepositoryDAO {
    public static final String CONNECTION_URL = "jdbc:mysql://10.211.55.4:3306/courssystem"; //Treiber/Datenbankverbindung/DB
    public static final String USERNAME = "root"; //Username Datenbank
    public static final String PASSWORD = "123"; //Password Datenbank

    private final Connection CONNECTION; //Datenbankverbindung

    public MySQLCourseRepositoryDAO() throws SQLException, ClassNotFoundException {
        this.CONNECTION = MySQLDatabaseConnection.getConnection(CONNECTION_URL, USERNAME, PASSWORD);
    }

    @Override
    public Optional<Course> insert(Course course) {
        Assert.notNull(course);
        try {
            String sqlStatement = "INSERT INTO `courses` (`name`, `description`, `hours`, `beginDate`, `endDate`, `courseType`) VALUES (?,?,?,?,?,?)";
            PreparedStatement preparedStatement = CONNECTION.prepareStatement(sqlStatement, Statement.RETURN_GENERATED_KEYS);  //um später den Generierten Key zu holen
            preparedStatement.setString(1, course.getName());
            preparedStatement.setString(2, course.getDescription());
            preparedStatement.setInt(3, course.getHours());
            preparedStatement.setDate(4, course.getBeginDate());
            preparedStatement.setDate(5, course.getEndDate());
            preparedStatement.setString(6, course.getCourseType().toString());

            int affectedRows = preparedStatement.executeUpdate(); //wie viele Datensätze betroffen sind

            if (affectedRows == 0) {
                return Optional.empty(); //speichern hat nicht funktioniert
            }

            ResultSet generatedKeys = preparedStatement.getGeneratedKeys(); //generierten Schlüssel holen

            if (generatedKeys.next()) {
                return this.getById(generatedKeys.getLong(1)); //neu generierter Datensatz aus Datenbank holen
            } else {
                return Optional.empty();
            }
        } catch (SQLException sqlException) {
            throw new DatabaseException("Datenbankfehler: " + sqlException.getMessage());
        }

    }

    @Override
    public Optional<Course> getById(Long id) {
        Assert.notNull(id); //null-check
        if (countCoursesInDbWithId(id) == 0) {
            return Optional.empty();
        } else {
            try {
                String sqlStatement = "SELECT * FROM `courses` WHERE `courses`.`id` = ?";
                PreparedStatement preparedStatement = CONNECTION.prepareStatement(sqlStatement);
                preparedStatement.setLong(1, id);
                ResultSet resultSet = preparedStatement.executeQuery();
                resultSet.next();
                //ORM
                Course course = new Course(
                        resultSet.getLong("id"),
                        resultSet.getString("name"),
                        resultSet.getString("description"),
                        resultSet.getInt("hours"),
                        resultSet.getDate("beginDate"),
                        resultSet.getDate("endDate"),
                        CourseType.valueOf(resultSet.getString("courseType"))
                );
                return Optional.of(course);
            } catch (SQLException sqlException) {
                throw new DatabaseException("Datenbankfehler: " + sqlException.getMessage());
            }
        }
    }

    private int countCoursesInDbWithId(Long id) {
        try {
            String sqlStatement = "SELECT COUNT(*) FROM `courses` WHERE `courses`.`id` = ?";
            PreparedStatement preparedStatement = CONNECTION.prepareStatement(sqlStatement);
            preparedStatement.setLong(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            return resultSet.getInt(1);
        } catch (SQLException sqlException) {
            throw new DatabaseException("Datenbankfehler: " + sqlException.getMessage());
        }
    }

    @Override
    public List<Course> getAll() {
        String sqlStatement = "SELECT * FROM `courses`";
        try {
            PreparedStatement preparedStatement = CONNECTION.prepareStatement(sqlStatement);
            ResultSet resultSet = preparedStatement.executeQuery();
            return fillCourseListHelper(resultSet);
        } catch (SQLException sqlException) {
            throw new DatabaseException("Datenbankfehler: " + sqlException.getMessage());
        }
    }

    @Override
    public Optional<Course> update(Course course) {
        Assert.notNull(course);

        if (countCoursesInDbWithId(course.getId()) == 0) {
            return Optional.empty();
        }

        try {
            String sqlStatement = "UPDATE `courses` SET `name` = ?, `description` = ?, `hours` = ?, `beginDate` = ?, `endDate` = ?, `courseType` = ? WHERE `courses`.`id` = ?";
            PreparedStatement preparedStatement = CONNECTION.prepareStatement(sqlStatement);
            preparedStatement.setString(1, course.getName());
            preparedStatement.setString(2, course.getDescription());
            preparedStatement.setInt(3, course.getHours());
            preparedStatement.setDate(4, course.getBeginDate());
            preparedStatement.setDate(5, course.getEndDate());
            preparedStatement.setString(6, course.getCourseType().toString());
            preparedStatement.setLong(7, course.getId());

            int affectedRows = preparedStatement.executeUpdate();
            if (affectedRows == 0) {
                return Optional.empty();
            } else {
                return this.getById(course.getId());
            }

        } catch (SQLException sqlException) {
            throw new DatabaseException("Datenbankfehler: " + sqlException.getMessage());
        }

    }

    @Override
    public boolean deleteById(Long id) {
        Assert.notNull(id);
        boolean deleted = false;
        try {
            if (countCoursesInDbWithId(id) == 1) {
                String sqlStatement = "DELETE FROM `courses` WHERE `courses`.`id` = ?";
                PreparedStatement preparedStatement = CONNECTION.prepareStatement(sqlStatement);
                preparedStatement.setLong(1, id);
                preparedStatement.executeUpdate();
                deleted = true;
            }
            return deleted;
        } catch (SQLException sqlException) {
            throw new DatabaseException("Datenbankfehler: " + sqlException.getMessage());
        }
    }

    @Override
    public List<Course> findAllCoursesByNameOrDescription(String searchtext) {
        Assert.notNull(searchtext);
        try {
            String sqlStatement = "SELECT * FROM `courses` WHERE LOWER(`name`) LIKE LOWER(?) OR LOWER(`description`) LIKE LOWER(?) ";
            PreparedStatement preparedStatement = CONNECTION.prepareStatement(sqlStatement);
            preparedStatement.setString(1, "%" + searchtext + "%");
            preparedStatement.setString(2, "%" + searchtext + "%");
            ResultSet resultSet = preparedStatement.executeQuery();
            return fillCourseListHelper(resultSet);
        } catch (SQLException sqlException) {
            throw new DatabaseException("Datenbankfehler: " + sqlException.getMessage());
        }
    }

    @Override
    public List<Course> findAllRunningCourses() {
        try {
            String sqlStatement = "SELECT * FROM `courses` WHERE NOW()<`enddate`";
            PreparedStatement preparedStatement = CONNECTION.prepareStatement(sqlStatement);
            ResultSet resultSet = preparedStatement.executeQuery();
            return fillCourseListHelper(resultSet);
        } catch (SQLException sqlException) {
            throw new DatabaseException("Datenbankfehler: " + sqlException.getMessage());
        }
    }

    private List<Course> fillCourseListHelper(ResultSet resultSet) {
        try {
            List<Course> courseList = new ArrayList<>();
            while (resultSet.next()) {
                //ORM
                courseList.add(new Course(
                                resultSet.getLong("id"),
                                resultSet.getString("name"),
                                resultSet.getString("description"),
                                resultSet.getInt("hours"),
                                resultSet.getDate("beginDate"),
                                resultSet.getDate("endDate"),
                                CourseType.valueOf(resultSet.getString("courseType"))
                        )
                );
            }
            return courseList;
        } catch (SQLException sqlException) {
            throw new DatabaseException("Datenbankfehler: " + sqlException.getMessage());
        }
    }

}
