import java.sql.*;

public class JdbcAddresses {
    public static final String CONNECTION_URL = "jdbc:mariadb://10.211.55.4:3306/jdbcdemo"; //Datenbankverbindung/Treiber/DB
    public static final String USERNAME = "root"; //Username Datenbank
    public static final String PASSWORD = "123"; //Password Datenbank

    public static void main(String[] args) {
        connectionTest();
        //selectAll();
       //insert("Hauptstraße", 12, "6511");
       //insert("Hauptstraße", 13, "6511");
       //insert("Hauptstraße", 14, "6511");
       update(2, "Siemensstraße", 22, "6020");
       delete(6);
       selectAll();
    }

    public static void connectionTest() {
        //Verbindungsaufbau testen
        try (Connection connection = DriverManager.getConnection(CONNECTION_URL, USERNAME, PASSWORD)) {
            System.out.println("Datenbankverbindung wurde hergestellt");
        } catch (SQLException sqlException) {
            System.out.println("Fehler beim Verbindungsaufbau der Datenbank: " + sqlException.getMessage());
        }
    }

    public static void selectAll() {
        try (Connection connection = DriverManager.getConnection(CONNECTION_URL, USERNAME, PASSWORD)) { //Verbindungsaufbau öffnen und schließen
            String sqlStatement = "SELECT * FROM `addresses`";
            PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement); //Statememt wird kompiliert und vorausgeführt
            ResultSet resultSet = preparedStatement.executeQuery(); //ausführen und Resultset zuweisen (Ergebnismenge)

            while (resultSet.next()) { //solange es einen nächsten Datensatz gibt zeigt er auf den Nächsten
                int id = resultSet.getInt("id");
                String streetName = resultSet.getString("street_name");
                int houseNumber = resultSet.getInt("house_number");
                String postalCode = resultSet.getString("postal_code");
                System.out.println("Student aus der DB: id=" + id + " street_name=" + streetName + " house_number=" + houseNumber + " postal_code=" + postalCode);
            }
        } catch (SQLException sqlException) {
            System.out.println("Fehler beim Verbindungsaufbau der Datenbank: " + sqlException.getMessage());
        }
    }

    public static void insert(String streetName, int houseNumber, String postalCode) {
        try (Connection connection = DriverManager.getConnection(CONNECTION_URL, USERNAME, PASSWORD)) { //Verbindungsaufbau öffnen und schließen
            String sqlStatement = "INSERT INTO `addresses` (`id`, `street_name`, `house_number`, `postal_code`) VALUES (NULL, ?,?,?)";
            PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement); //Statememt wird kompiliert und vorausgeführt

            try { //Zusätzliches Try/Catch um einen Fehler des SQL-Statements zu ermitteln
                //schützt vor SQL-Injection
                preparedStatement.setString(1, streetName);
                preparedStatement.setInt(2, houseNumber);
                preparedStatement.setString(3, postalCode);
                int rowAffected = preparedStatement.executeUpdate(); //liefert die Anzahl der eingefügten Datensätze und führt aus
                System.out.println(rowAffected + " Datensätze hinzugefügt");
            } catch (SQLException sqlException) {
                System.out.println("Fehler im SQL-INSERT Statement: " + sqlException.getMessage());
            }

        } catch (SQLException sqlException) {
            System.out.println("Fehler beim Verbindungsaufbau der Datenbank: " + sqlException.getMessage());
        }
    }

    public static void update(int id, String streetName, int houseNumber, String postalCode) {
        try (Connection connection = DriverManager.getConnection(CONNECTION_URL, USERNAME, PASSWORD)) { //Verbindungsaufbau öffnen und schließen
            String sqlStatement = "UPDATE `addresses` SET `street_name`= ?, `house_number`= ?, `postal_code`= ? WHERE `addresses`.`id` = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement); //Statememt wird kompiliert und vorausgeführt

            try { //Zusätzliches Try/Catch um einen Fehler des SQL-Statements zu ermitteln
                //schützt vor SQL-Injection
                preparedStatement.setString(1, streetName);
                preparedStatement.setInt(2, houseNumber);
                preparedStatement.setString(3, postalCode);
                preparedStatement.setInt(4, id);
                int rowAffected = preparedStatement.executeUpdate();
                System.out.println(rowAffected + " Datensätze geändert");
            } catch (SQLException sqlException) {
                System.out.println("Fehler im SQL-UPDATE Statement: " + sqlException.getMessage());
            }

        } catch (SQLException sqlException) {
            System.out.println("Fehler beim Verbindungsaufbau der Datenbank: " + sqlException.getMessage());
        }
    }

    public static void delete(int id) {
        try (Connection connection = DriverManager.getConnection(CONNECTION_URL, USERNAME, PASSWORD)) { //Verbindungsaufbau öffnen und schließen
            String sqlStatement = "DELETE FROM `addresses` WHERE `addresses`.`id` = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement); //Statememt wird kompiliert und vorausgeführt

            try { //Zusätzliches Try/Catch um einen Fehler des SQL-Statements zu ermitteln
                //schützt vor SQL-Injection
                preparedStatement.setInt(1, id);
                int rowAffected = preparedStatement.executeUpdate();
                System.out.println(rowAffected + " Datensätze gelöscht");
            } catch (SQLException sqlException) {
                System.out.println("Fehler im SQL-DELETE Statement: " + sqlException.getMessage());
            }

        } catch (SQLException sqlException) {
            System.out.println("Fehler beim Verbindungsaufbau der Datenbank: " + sqlException.getMessage());
        }
    }
}
