import java.sql.*;

public class JdbcStudents {
    public static final String CONNECTION_URL = "jdbc:mariadb://10.211.55.4:3306/jdbcdemo"; //Datenbankverbindung/Treiber/DB
    public static final String USERNAME = "root"; //Username Datenbank
    public static final String PASSWORD = "123"; //Password Datenbank

    public static void main(String[] args) {
        connectionTest();
        //selectAll();
        //insertStudent("Dario Skocibusic", "d.skocibusic@icloud.com");
        //updateStudent(4, "Test", "test@icloud.com");
        //deleteStudent(3);
        //selectAll();
        findAllStudentsByNameLike("Mat");
    }

    public static void connectionTest() {
        //Verbindungsaufbau testen
        try (Connection connection = DriverManager.getConnection(CONNECTION_URL, USERNAME, PASSWORD)) {
            System.out.println("Datenbankverbindung wurde hergestellt");
        } catch (SQLException sqlException) {
            System.out.println("Fehler beim Verbindungsaufbau der Datenbank: " + sqlException.getMessage());
        }
    }

    public static void selectAll() {
        try (Connection connection = DriverManager.getConnection(CONNECTION_URL, USERNAME, PASSWORD)) { //Verbindungsaufbau öffnen und schließen
            String sqlStatement = "SELECT * FROM `students`";
            PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement); //Statememt wird kompiliert und vorausgeführt
            ResultSet resultSet = preparedStatement.executeQuery(); //ausführen und Resultset zuweisen (Ergebnismenge)

            while (resultSet.next()) { //solange es einen nächsten Datensatz gibt zeigt er auf den Nächsten
                int id = resultSet.getInt("id");
                String name = resultSet.getString("name");
                String email = resultSet.getString("email");
                System.out.println("Student aus der DB: id=" + id + " name=" + name + " email=" + email);
            }
        } catch (SQLException sqlException) {
            System.out.println("Fehler beim Verbindungsaufbau der Datenbank: " + sqlException.getMessage());
        }
    }

    public static void insertStudent(String name, String email) {
        try (Connection connection = DriverManager.getConnection(CONNECTION_URL, USERNAME, PASSWORD)) { //Verbindungsaufbau öffnen und schließen
            String sqlStatement = "INSERT INTO `students` (`id`, `name`, `email`) VALUES (NULL, ?,?)";
            PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement); //Statememt wird kompiliert und vorausgeführt

            try { //Zusätzliches Try/Catch um einen Fehler des SQL-Statements zu ermitteln
                //schützt vor SQL-Injection
                preparedStatement.setString(1, name);
                preparedStatement.setString(2, email);
                int rowAffected = preparedStatement.executeUpdate(); //liefert die Anzahl der eingefügten Datensätze und führt aus
                System.out.println(rowAffected + " Datensätze hinzugefügt");
            } catch (SQLException sqlException) {
                System.out.println("Fehler im SQL-INSERT Statement: " + sqlException.getMessage());
            }

        } catch (SQLException sqlException) {
            System.out.println("Fehler beim Verbindungsaufbau der Datenbank: " + sqlException.getMessage());
        }
    }

    public static void updateStudent(int id, String name, String email) {
        try (Connection connection = DriverManager.getConnection(CONNECTION_URL, USERNAME, PASSWORD)) { //Verbindungsaufbau öffnen und schließen
            String sqlStatement = "UPDATE `students` SET `name`= ?, `email`= ? WHERE `students`.`id` = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement); //Statememt wird kompiliert und vorausgeführt

            try { //Zusätzliches Try/Catch um einen Fehler des SQL-Statements zu ermitteln
                //schützt vor SQL-Injection
                preparedStatement.setString(1, name);
                preparedStatement.setString(2, email);
                preparedStatement.setInt(3, id);
                int rowAffected = preparedStatement.executeUpdate();
                System.out.println(rowAffected + " Datensätze geändert");
            } catch (SQLException sqlException) {
                System.out.println("Fehler im SQL-UPDATE Statement: " + sqlException.getMessage());
            }

        } catch (SQLException sqlException) {
            System.out.println("Fehler beim Verbindungsaufbau der Datenbank: " + sqlException.getMessage());
        }
    }

    public static void deleteStudent(int id) {
        try (Connection connection = DriverManager.getConnection(CONNECTION_URL, USERNAME, PASSWORD)) { //Verbindungsaufbau öffnen und schließen
            String sqlStatement = "DELETE FROM `students` WHERE `students`.`id` = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement); //Statememt wird kompiliert und vorausgeführt

            try { //Zusätzliches Try/Catch um einen Fehler des SQL-Statements zu ermitteln
                //schützt vor SQL-Injection
                preparedStatement.setInt(1, id);
                int rowAffected = preparedStatement.executeUpdate();
                System.out.println(rowAffected + " Datensätze gelöscht");
            } catch (SQLException sqlException) {
                System.out.println("Fehler im SQL-DELETE Statement: " + sqlException.getMessage());
            }

        } catch (SQLException sqlException) {
            System.out.println("Fehler beim Verbindungsaufbau der Datenbank: " + sqlException.getMessage());
        }
    }

    public static void findAllStudentsByNameLike(String pattern) {
        try (Connection connection = DriverManager.getConnection(CONNECTION_URL, USERNAME, PASSWORD)) { //Verbindungsaufbau öffnen und schließen
            String sqlStatement = "SELECT * FROM `students` WHERE `students`.`name` LIKE ?";
            PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement); //Statememt wird kompiliert und vorausgeführt
            preparedStatement.setString(1, "%"+pattern+"%");
            ResultSet resultSet = preparedStatement.executeQuery(); //ausführen und Resultset zuweisen (Ergebnismenge)

            while (resultSet.next()) { //solange es einen nächsten Datensatz gibt zeigt er auf den Nächsten
                int id = resultSet.getInt("id");
                String name = resultSet.getString("name");
                String email = resultSet.getString("email");
                System.out.println("Student aus der DB: id=" + id + " name=" + name + " email=" + email);
            }
        } catch (SQLException sqlException) {
            System.out.println("Fehler beim Verbindungsaufbau der Datenbank: " + sqlException.getMessage());
        }
    }
}
