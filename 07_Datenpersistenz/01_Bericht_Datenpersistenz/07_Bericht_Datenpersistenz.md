---
title: "Datenpersistenz"
author: [Mathias Rudig]
date: "2022-02-24"
subject: "Markdown"
keywords: [Markdown, Example]
subtitle: "Datenpersistenz mit JDBC und DAO"
lang: "de"
titlepage: true,
titlepage-rule-color: "360049"
titlepage-background: "backgrounds/background1.pdf"
page-background: "backgrounds/background1.pdf"
toc-own-page: true,
...

- [JDBC Intro](#jdbc-intro)
  - [Datenbankserver einreichten mittels Docker](#datenbankserver-einreichten-mittels-docker)
  - [Java-Maven-Projekt erstellen](#java-maven-projekt-erstellen)
  - [Dependency POM](#dependency-pom)
  - [Verbindung zur Datenbank aufbauen](#verbindung-zur-datenbank-aufbauen)
  - [Prepared-Statement SELECT](#prepared-statement-select)
  - [Prepared-Statement INSERT](#prepared-statement-insert)
  - [Prepared-Statement UPDATE](#prepared-statement-update)
  - [Prepared-Statement DELETE](#prepared-statement-delete)
  - [Zweite Tabelle](#zweite-tabelle)
- [JDBC UND DAO](#jdbc-und-dao)
  - [Datenbankverbindung mittels Singelton-Pattern](#datenbankverbindung-mittels-singelton-pattern)
  - [Domain-Klasse](#domain-klasse)
  - [DAO-Pattern](#dao-pattern)
- [JDBC UND DAO – BUCHUNGEN](#jdbc-und-dao--buchungen)
  - [Domainklasse:](#domainklasse)
  - [Businesslogik:](#businesslogik)
  - [CLI:](#cli)
- [DATABASE](#database)

# JDBC Intro

- Java Database Connectivity

## Datenbankserver einreichten mittels Docker

- Docker Script Maar Philipp Link: [Docker-Skriptum.pdf](https://gitlab.com/it-kolleg-unterlagen/skriptum/docker/-/blob/master/Docker-Skriptum.pdf)
- Virtuelle Maschine mit Linux Ubuntu Server
- Docker Container lt. docker-compose.yml
    - MariaDB, MySQL
    - Adminer
    - Apache (mysql Adapter)

```yaml
#docker-compose
version: '3.1'

services:

  apache:
    build: 
      dockerfile: ./Dockerfile
      context: ./
    container_name: apache
    restart: unless-stopped
    ports:
      - 80:80
    volumes:
      - ./www:/var/www/html/

  ##Alternative
	mysql:
    image: mysql/mysql-server:8.0
    container_name: mysql
    restart: unless-stopped
    environment:
      MYSQL_ROOT_PASSWORD: "123"
      MYSQL_ROOT_HOST: "%"
    ports:
      - 3306:3306

	mariadb:
    image: mariadb
    container_name: mariadb
    restart: unless-stopped
    environment:
      MYSQL_ROOT_PASSWORD: "123"
      MYSQL_ROOT_HOST: "%"
    ports:
      - 3306:3306

  adminer:
    image: adminer:4.8.0
    container_name: adminer
    restart: unless-stopped
    ports:
      - 8080:8080
    environment:
        ADMINER_DESIGN: "hever"
        ADMINER_PLUGINS: "edit-calendar edit-foreign edit-textarea" 
        ADMINER_DEFAULT_SERVER: "mysql"
        ADMINER_SERVER: "mysql"
        ADMINER_USERNAME: "root"
        ADMINER_PASSWORD: "123"
```

```docker
#Dockerfile
FROM php:7.2-apache
RUN apt -yqq update
RUN apt -yqq install libxml2-dev
RUN docker-php-ext-install pdo_mysql
RUN docker-php-ext-install xml
RUN a2enmod rewrite
```

## Java-Maven-Projekt erstellen

![CreateMaven-Project](07_Bericht_Datenpersistenz_Bilder/Create-Maven-Project.png)

## Dependency POM

für die Verbindung einer MySQL-Datenbank in der pom.xml hinzufügen

```xml
<dependencies>
	<dependency>
	    <groupId>org.mariadb.jdbc</groupId>
        <artifactId>mariadb-java-client</artifactId>
        <version>3.0.3</version>
	 </dependency>
	<dependency>
        <groupId>mysql</groupId>
        <artifactId>mysql-connector-java</artifactId>
        <version>8.0.28</version>
    </dependency>
</dependencies>
```

[https://mvnrepository.com](https://mvnrepository.com/)

## Verbindung zur Datenbank aufbauen

- Sofern die Datenbankverbindung in einem Try/Catch ausgeführt wird, wird die Verbindung a, Ende wieder geschlossen! (es darf nur eine Verbindung geben/ Singelton Prinzip anwenden)

```java
public static final String CONNECTION_URL = "jdbc:mariadb://10.211.55.4:3306/jdbcdemo"; //Datenbankverbindung/Treiber/DB
public static final String USERNAME = "root"; //Username Datenbank
public static final String PASSWORD = "123"; //Password Datenbank

public static void connectionTest(){
        //Verbindungsaufbau testen
        try(Connection connection = DriverManager.getConnection(CONNECTION_URL, USERNAME, PASSWORD)) { 
            System.out.println("Datenbankverbindung wurde hergestellt");
        } catch (SQLException sqlException){
            System.out.println("Fehler beim Verbindungsaufbau der Datenbank: " + sqlException.getMessage());
        }
}
```

## Prepared-Statement SELECT

- anhand des ResultSets können Datensätze anhand ihrer Spalten weiter verarbeitet werden

```java
public static void selectAll(){
        try(Connection connection = DriverManager.getConnection(CONNECTION_URL, USERNAME, PASSWORD)) { //Verbindungsaufbau öffnen und schließen
            String sqlStatement = "SELECT * FROM `students`";
            PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement);//Statememt wird kompiliert und vorausgeführt
            ResultSet resultSet = preparedStatement.executeQuery(); //ausführen und Resultset zuweisen (Ergebnismenge)

            while (resultSet.next()){ //solange es einen nächsten Datensatz gibt zeigt er auf den Nächsten
                int id = resultSet.getInt("id");
                String name = resultSet.getString("name");
                String email = resultSet.getString("email");
                System.out.println("Student aus der DB: id=" + id + " name=" + name + " email=" + email);
            }
        } catch (SQLException sqlException){
            System.out.println("Fehler beim Verbindungsaufbau der Datenbank: " + sqlException.getMessage());
        }
    }
```

```java
public static void findAllStudentsByNameLike(String pattern) {
        try (Connection connection = DriverManager.getConnection(CONNECTION_URL, USERNAME, PASSWORD)) { //Verbindungsaufbau öffnen und schließen
            String sqlStatement = "SELECT * FROM `students` WHERE `students`.`name` LIKE ?";
            PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement); //Statememt wird kompiliert und vorausgeführt
            preparedStatement.setString(1, "%"+pattern+"%");
            ResultSet resultSet = preparedStatement.executeQuery(); //ausführen und Resultset zuweisen (Ergebnismenge)

            while (resultSet.next()) { //solange es einen nächsten Datensatz gibt zeigt er auf den Nächsten
                int id = resultSet.getInt("id");
                String name = resultSet.getString("name");
                String email = resultSet.getString("email");
                System.out.println("Student aus der DB: id=" + id + " name=" + name + " email=" + email);
            }
        } catch (SQLException sqlException) {
            System.out.println("Fehler beim Verbindungsaufbau der Datenbank: " + sqlException.getMessage());
        }
    }
```

## Prepared-Statement INSERT

```java
public static void insertStudent(String name, String email) {
        try (Connection connection = DriverManager.getConnection(CONNECTION_URL, USERNAME, PASSWORD)) { //Verbindungsaufbau öffnen und schließen
            String sqlStatement = "INSERT INTO `students` (`id`, `name`, `email`) VALUES (NULL, ?,?)";
            PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement); //Statememt wird kompiliert und vorausgeführt

            try { //Zusätzliches Try/Catch um einen Fehler des SQL-Statements zu ermitteln
                //schützt vor SQL-Injection
                preparedStatement.setString(1, name);
                preparedStatement.setString(2, email);
                int rowAffected = preparedStatement.executeUpdate(); //liefert die Anzahl der eingefügten Datensätze und führt aus
                System.out.println(rowAffected + " Datensätze hinzugefügt");
            } catch (SQLException sqlException) {
                System.out.println("Fehler im SQL-INSERT Statement: " + sqlException.getMessage());
            }

        } catch (SQLException sqlException) {
            System.out.println("Fehler beim Verbindungsaufbau der Datenbank: " + sqlException.getMessage());
        }
    }
```

## Prepared-Statement UPDATE

```java
public static void updateStudent(int id, String name, String email) {
        try (Connection connection = DriverManager.getConnection(CONNECTION_URL, USERNAME, PASSWORD)) { //Verbindungsaufbau öffnen und schließen
            String sqlStatement = "UPDATE `students` SET `name`= ?, `email`= ? WHERE `students`.`id` = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement); //Statememt wird kompiliert und vorausgeführt

            try { //Zusätzliches Try/Catch um einen Fehler des SQL-Statements zu ermitteln
                //schützt vor SQL-Injection
                preparedStatement.setString(1, name);
                preparedStatement.setString(2, email);
                preparedStatement.setInt(3, id);
                int rowAffected = preparedStatement.executeUpdate();
                System.out.println(rowAffected + " Datensätze geändert");
            } catch (SQLException sqlException) {
                System.out.println("Fehler im SQL-UPDATE Statement: " + sqlException.getMessage());
            }

        } catch (SQLException sqlException) {
            System.out.println("Fehler beim Verbindungsaufbau der Datenbank: " + sqlException.getMessage());
        }
    }
```

## Prepared-Statement DELETE

```java
public static void deleteStudent(int studentId) {
        try (Connection connection = DriverManager.getConnection(CONNECTION_URL, USERNAME, PASSWORD)) { //Verbindungsaufbau öffnen und schließen
            String sqlStatement = "DELETE FROM `students` WHERE `students`.`id` = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement); //Statememt wird kompiliert und vorausgeführt

            try { //Zusätzliches Try/Catch um einen Fehler des SQL-Statements zu ermitteln
                //schützt vor SQL-Injection
                preparedStatement.setInt(1, studentId);
                int rowAffected = preparedStatement.executeUpdate();
                System.out.println(rowAffected + " Datensätze gelöscht");
            } catch (SQLException sqlException) {
                System.out.println("Fehler im SQL-DELETE Statement: " + sqlException.getMessage());
            }

        } catch (SQLException sqlException) {
            System.out.println("Fehler beim Verbindungsaufbau der Datenbank: " + sqlException.getMessage());
        }
    }
```

## Zweite Tabelle

```sql
SET NAMES utf8mb4;

DROP TABLE IF EXISTS `addresses`;
CREATE TABLE `addresses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `street_name` varchar(200) NOT NULL,
  `house_number` int(11) NOT NULL,
  `postal_code` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `addresses` (`id`, `street_name`, `house_number`, `postal_code`) VALUES
(1,	'Hauptstraße',	12,	'6511'),
(2,	'Siemensstraße',	22,	'6020'),
(3,	'Hauptstraße',	14,	'6511');
```

# JDBC UND DAO

- Data Access Object (DAO) ist ein Entwurfsmuster, welches die Datenbank vom Restsystem mithilfe von Interfaces abstrahiert und somit einfach austauschbar macht.  (Open Closed Prinzip)
- Mittels Objektrelationalem Mapping werden aus den Relationalen Objekten, Data Transferobjekte (DTO) gebildet.

## Datenbankverbindung mittels Singelton-Pattern

- Stellt sicher, dass es zur Laufzeit nur eine Instanz gibt und die bestehende verwendet wird.
- nur eine Datenbankverbindung

```java
public class MariaDbDatabaseConnection {

    private static Connection connection = null;

    private MariaDbDatabaseConnection() { //hier kann keine Instanz per new ... gebildet werden
    }

    public static Connection getConnection(String url, String user, String pwd) throws ClassNotFoundException, SQLException {
        if (connection != null) {
            return connection;
        } else {
            Class.forName("org.mariadb.jdbc.Driver"); //Prüft ob die Klasse vorhanden ist
            connection = DriverManager.getConnection(url, user, pwd);
            return connection;
        }
    }
}
```

```java
//Instanzierung
Connection connection = MariaDbDatabaseConnection.getConnection(CONNECTION_URL, USERNAME, PASSWORD);
```

![Dependency-Driver MariaDB](07_Bericht_Datenpersistenz_Bilder/Dependency-Driver-MariaDB.png)

## Domain-Klasse

- Da jede Domäne eine id braucht, bietet es sich an eine Abstrakte Klasse nur mit dem Datenfeld id zu implementieren.
- Von diesem können alle weiteren erben und sollten zwei Konstruktoren bereitstellen (übergebene id, id mit null), da die id erst in der Datenbank per AI erzeugt wird.

![UML-Diagramm Base Entity](07_Bericht_Datenpersistenz_Bilder/UML-Diagramm-Base-Entity.png)

## DAO-Pattern

Das DAO-Pattern stellt und ein oder mehrere Interfaces bereit, welche unsere CRUD respektive konkreten Methoden vorgibt und den Datenbankzugriff vom Restsystem abstrahiert. 

Im besten Falle gibt es ein BaseRepository, welches Generics verwendet um von diesem zu einem späteren Zeitpunkt mit konkreten Typen zu erben. (Interfaces sind vererbbar nicht nur implementierbar)

![UML-Diagramm DAO](07_Bericht_Datenpersistenz_Bilder/UML-Diagramm-DAO.png)

- ORM - wird in der konkreten Klasse angewendet (MariaDbCourseRepositoryDAO) und somit wird nur mehr mit Objekten gearbeitet
- Die Methode getAll() gibt dann beispielsweise eine Liste vom jeweiligen Objekt-Typ (Course) zurück.
- Exceptions können als RunntimeException definiert werden, damit sie Unhandelt werden

# JDBC UND DAO – BUCHUNGEN

Gib einen textuellen Vorschlag hab, wie man die bisher programmierte Applikation für die Buchung von Kursen
durch Studenten erweitern könnte. Beschreibe, wie eine neue Buchungs-Domänenklasse ausschauen sollte, wie man ein DAO für Buchungen dazu entwickeln sollte, wie man die CLI anpassen müsste und welche Anwendungsfälle der Benutzer brauchen könnte (wie etwa „Buchung erstellen“). Verwende zur Illustration insb. auch UML-Diagramme.

## Domainklasse:

Long id; //von BaseEntity erben

Date createDate; //beim Erstellen in Datenbank NOW()

Long courseId; //Fremdschlüssel Course

Long studentId; //Fremdschlüssel Student

## Businesslogik:

- eventuell eigenen Service Layer da Logik mit Abhängigkeit entsteht.
- Prüfen, ob ein Datensatz mit dem Fremdschlüssel existiert, nur dann zuweisen.
- Student kann nur laufenden Kursen zugeordnet werden.
- jeder Kurs/Student kann einer oder mehreren Buchungen zugeordnet sein.
- jede Buchung muss genau einem Studenten und einem Kurs zugeordnet sein.
- Buchung enspricht einem Intersection-Entity

Die Buchungen könnten als Intersection Entity gesehen werden, da einer Buchung nur ein existierender Student und Kurs zugeordnet werden kann. Umgekehrt kann ein Kurs/Student mehreren Buchungen zugeordnet werden. Per Businesslogik müsste beim Erstellen und Updaten sichergestellt werden, dass ein Student den selben Kurs nur einmal buchen kann (per SQL-Statement prüfen). Beim Löschen von Kursen/ Studenten sollte zuerst geprüft werden, ob eine Buchung eine Referenz auf eine der Beiden Entities vorweist, damit gegebenenfalls diese auch gelöscht werden (onDelete(CASCADE)).

## CLI:

Beim Anlegen/ Update von Buchungen, könnte jeweils eine Liste von Kursen und Studenten ausgegeben werden, um per ID den jeweiligen Fremdschlüsseleintrag  zu wählen. Weiter Standard CRUD-Methoden wie getAll(), getPerId(id), delete(id) können einfach ausprogrammiert werden. Als zusätzliche Methoden könnte man alle Buchungen (Liste) einer bestimmten Person anzeigen oder alle Buchungen bestimmter Kurse anzeigen.

![UML-Diagramm](07_Bericht_Datenpersistenz_Bilder/UML-Diagramm.png)

# DATABASE
```php
CREATE DATABASE jdbcdemo;
USE jdbcdemo;

DROP TABLE IF EXISTS `addresses`;
CREATE TABLE `addresses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `street_name` varchar(200) NOT NULL,
  `house_number` int(11) NOT NULL,
  `postal_code` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `addresses` (`id`, `street_name`, `house_number`, `postal_code`) VALUES
(1,	'Hauptstraße',	12,	'6511'),
(2,	'Siemensstraße',	22,	'6020'),
(3,	'Hauptstraße',	14,	'6511');

DROP TABLE IF EXISTS `students`;
CREATE TABLE `students` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `students` (`id`, `name`, `email`) VALUES
(1,	'Mathias Rudig',	'mathias.rudig@icloud.com'),
(2,	'Tobias Tilg',	'tobias.tilg@icloud.com'),
(4,	'Test',	'test@icloud.com'),
(5,	'Dario Skocibusic',	'd.skocibusic@icloud.com');

```

```php
CREATE DATABASE courssystem;
USE courssystem;

DROP TABLE IF EXISTS `courses`;
CREATE TABLE `courses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8mb3_bin NOT NULL,
  `description` text COLLATE utf8mb3_bin NOT NULL,
  `hours` int(11) NOT NULL,
  `begindate` date NOT NULL,
  `enddate` date NOT NULL,
  `coursetype` varchar(2) COLLATE utf8mb3_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin;

INSERT INTO `courses` (`id`, `name`, `description`, `hours`, `begindate`, `enddate`, `coursetype`) VALUES
(1,	'POS',	'Dieser Kurs beschäftigt sich mit den Grundlagen der Objektorientierten-Programmierung.',	2,	'2022-02-01',	'2022-02-16',	'ZA'),
(2,	'FSE',	'Dieser Kurs beschäftigt sich mit den Grundlagen der Mikroarchitektur.',	3,	'2022-02-01',	'2022-02-24',	'ZA'),
(10,	'FSE',	'Dieser Kurs beschäftigt sich mit den Grundlagen der Mikroarchitektur.',	5,	'2022-01-01',	'2022-02-01',	'ZA');

DROP TABLE IF EXISTS `students`;
CREATE TABLE `students` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `birthdate` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `students` (`id`, `firstname`, `lastname`, `birthdate`) VALUES
(1,	'Mathias',	'Rudig',	'1993-01-29'),
(2,	'Tobias',	'Tilg',	'2001-01-29');

DROP TABLE IF EXISTS `bookings`;
CREATE TABLE `bookings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_date` date NOT NULL DEFAULT current_timestamp(),
  `course_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `course_id` (`course_id`),
  KEY `student_id` (`student_id`),
  CONSTRAINT `bookings_ibfk_1` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`) ON DELETE CASCADE,
  CONSTRAINT `bookings_ibfk_2` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `bookings` (`id`, `create_date`, `course_id`, `student_id`) VALUES
(1,	'2022-01-01',	2,	1),
(2,	'2022-01-01',	10,	1);
```