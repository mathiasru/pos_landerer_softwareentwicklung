# BlueJ-Buch Klassenentwurf Kapitel 8

- [x]  Übung 8.1
    - Was tut diese Anwendung?
    Es ist ein textbasierendes Adventure Game, in dem sich ein Spieler per Eingaben bewegen kann.
    - Welche Befehle akzeptiert des Spiel?
    "go", "quit", "help"
    - Was bewirken diese einzelnen Befehle?
    Sie werden geprüft und dienen zur Steuerung.
    go - Steuern, quiet - Beenden, help - Befehle anzeigen
    - Wie viele Räume gibt es in der virtuellen Umgebung?
    Es gibt fünf Räume (draussen, hoersaal, cafeteria, labor, buero)
    - Zeichnen Sie eine Karte der gegebenen Räume.
    
    ![BlueJ_Kapitel8_Bilder/Bildschirmfoto_2021-03-16_um_2.22.45_PM.png](BlueJ_Kapitel8_Bilder/Bildschirmfoto_2021-03-16_um_2.22.45_PM.png)
    
- [x]  Übung 8.2
    - Spiel: erzeugt alle Instanzen und ist für den Spielablauf zuständig
    - Parser: ließt die Befehle ein und übersetzt die Eingabe
    - Befehl: überprüft die eingegebenen Befehle auf ihre Gültigkeit
    - Raum: beinhaltet die Räume und die Ausgänge, zudem werden die Ausgänge der Räume gesetzt
    - Befehlswort: enthält Befehlswörter und überprüft die Eingabe
- [x]  Übung 8.3
    
    Einkaufszentrum den Ausgang im Foyer suchen
    
    Zeitbeschränkung, Gegenstand=Schlüssel
    
- [x]  Übung 8.4
    
    ![BlueJ_Kapitel8_Bilder/Bildschirmfoto_2021-03-16_um_3.13.53_PM.png](BlueJ_Kapitel8_Bilder/Bildschirmfoto_2021-03-16_um_3.13.53_PM.png)
    
    ```java
    private void raeumeAnlegen()
        {
            Raum foyer, info, halle, imbiss, shopSport, shopTech;
          
            // die R�ume erzeugen
            foyer = new Raum("Eingang des Einkaufszentrums");
            info = new Raum("Infopoint");
            halle = new Raum("Halle");
            imbiss = new Raum("Imbiss");
            shopSport = new Raum("Sport Shop");
            shopTech = new Raum("Technik Shop");
            
            // die Ausg�nge initialisieren
            foyer.setzeAusgaenge(info, null, null, null);
            info.setzeAusgaenge(halle, null, foyer, shopSport);
            halle.setzeAusgaenge(null, shopSport, info, imbiss);
            imbiss.setzeAusgaenge(null, halle, null, null);
            shopSport.setzeAusgaenge(null, null, shopTech, halle);
            shopTech.setzeAusgaenge(shopSport, null, null, info);
    
            aktuellerRaum = imbiss;  // das Spiel startet draussen
        }
    ```
    
- [x]  Übung 8.5
    
    ```java
    /**
    * Gibt die Info des aktuellen Raumes und die möglichen Ausgänge aus.
    */
        private void rauminfoAusgeben()
        {
            System.out.println("Sie sind " + aktuellerRaum.gibBeschreibung());
            System.out.print("Ausgaenge: ");
            if(aktuellerRaum.nordausgang != null) {
                System.out.print("north ");
            }
            if(aktuellerRaum.ostausgang != null) {
                System.out.print("east ");
            }
            if(aktuellerRaum.suedausgang != null) {
                System.out.print("south ");
            }
            if(aktuellerRaum.westausgang != null) {
                System.out.print("west ");
            }
            System.out.println();
        }
    ```
    
- [x]  Übung 8.6
    
    ```java
    public Raum gibAusgang(String richtung){
            if (richtung.equals("north")){
                return nordausgang;
            }
            if (richtung.equals("east")){
                return ostausgang;
            }
            if (richtung.equals("south")){
                return suedausgang;
            }
            if (richtung.equals("west")){
                return westausgang;
            }
            return null;
        }
    
    private void wechsleRaum(Befehl befehl) 
        {
            if(!befehl.hatZweitesWort()) {
                // Gibt es kein zweites Wort, wissen wir nicht, wohin...
                System.out.println("Wohin m�chten Sie gehen?");
                return;
            }
            String richtung = befehl.gibZweitesWort();
            Raum naechsterRaum = aktuellerRaum.gibAusgang(richtung);
            if (naechsterRaum == null) {
                System.out.println("Dort ist keine Tuer!");
            }
            else {
                aktuellerRaum = naechsterRaum;
                rauminfoAusgeben();
            }
        }
    ```
    
- [x]  Übung 8.7
    
    ```java
    public String gibAusgaengeAlsString() {
            String ausgabe = "Ausgaenge: ";
            if(nordausgang != null) {
                ausgabe = ausgabe.concat("north ");
            }
            if(ostausgang != null) {
                ausgabe = ausgabe.concat("east ");
            }
            if(suedausgang != null) {
                ausgabe = ausgabe.concat("south ");
            }
            if(westausgang != null) {
                ausgabe = ausgabe.concat("west ");
            }
            return ausgabe;
        }
    ```
    
- [x]  Übung 8.8
    
    Alles auf HashMap implementiert
    
- [x]  Übung 8.9
    
    Gibt eine Set-Ansicht der in dieser Map enthaltenen Schlüssel zurück.
    
- [x]  Übung 8.10
    
    erzeugen einen String mit "Ausgänge:" 
    
    erzeugen ein String Set welches alle Schlüssel der ausgaenge mittes der Methode keySet() zugewiesen bekommt.
    
    anschließend wird mittels einer forEach Schleife jeder ausgang an unseren String concatiniert, welcher dann returned wird.
    
- [x]  Übung 8.11
    
    ```java
    public String gibAusgaengeAlsString() {
            String ausgabe = "Ausgaenge: ";
            Set<String> keys = ausgaenge.keySet();
            for (String ausgang :keys) {
                ausgabe += " " + ausgang;
            }
            return ausgabe;
        }
    public String gibLangeBeschreibung()
        {
            return "Sie sind " + this.beschreibung + ".\n" + gibAusgaengeAlsString();
        }
    public void rauminfoAusgeben() {
            System.out.println(aktuellerRaum.gibLangeBeschreibung());
            System.out.println();
        }
    ```
    
- [x]  Übung 8.12
    
    Es werden beim Erstellen eines neuen Spieles alle Räume, und ein Parser inkl. Befehlswörter erzeugt.
    
- [x]  Übung 8.13
    
     Es wird ein neues Befehls Objekt nach Ergänzung der Eingabe mit z.B. "go east" erzeugt.
    
    Ansonsten kommt nur eine Gefehlseingabe.
    
- [x]  Übung 8.14
    
    ```java
    private void umsehen() {
            System.out.println(aktuellerRaum.gibLangeBeschreibung());
        }
    ```
    
- [x]  Übung 8.15
    
    ```java
    private void eat() {
            System.out.println("Sie haben nun gegessen und sind nicht mehr hungrig.");
        }
    ```
    
- [x]  Übung 8.16
    
    ```java
    public void zeigeBefehle(){
            befehle.alleAusgeben();
        }
    public void alleAusgeben(){
            for (String befehl : gueltigeBefehle) {
                System.out.print(befehl + " ");
            }
            System.out.println();
        }
    ```
    
- [x]  Übung 8.17
    
    Da die Methode verarbeiteBefehl angepasst werden muss, damit die Eingabe auf Gleichheit der hinterlegten Befehle überprüft wird.
    
- [x]  Übung 8.18
    
    ```java
    public String zeigeBefehle(){
            return befehle.gibBefehlliste();
        }
    public String gibBefehlliste(){
            String befehlliste ="Befehle: ";
            for (String befehl : gueltigeBefehle) {
                befehlliste += befehl + " ";
            }
            return befehlliste;
        }
    //help
    System.out.println(parser.zeigeBefehle());
    ```
    
- [x]  Übung 8.19
    
    Model View Controller (MVC, englisch für Modell-Präsentation-Steuerung) ist ein Muster zur Unterteilung einer Software in die drei Komponenten Datenmodell (englisch model), Präsentation (englisch view) und Programmsteuerung (englisch controller). Das Muster kann sowohl als Architekturmuster als auch als Entwurfsmuster eingesetzt werden. (Quelle: Wikipedia)
    
    Steuerung und Präsentation sind in unserem Fall in der Klasse Spiel dargestellt, während ich die restlichen Klassen alle in das Datenmodell einordnen würde.
    
- [x]  Übung 8.20
    
    Eigene Klasse erstellt, Datenfeld Gegenstände in der Klasse Raum angelegt und einen zusätzlichen Konstruktor dder einen Gegenstand erwartet. Ausgabe wurde verändert. Zusätzlich habe ich noch folgende Methode geschrieben, damit keine Nullpointer fällt, sobald kein Gegenstand vorhanden ist. 
    
    ```java
    public String gibLangeBeschreibung()
        {
            return "Sie sind " + this.beschreibung + ".\n" + gibAusgaengeAlsString() + ".\n" + gibbBeschreibungGegenstand() ;
        }
        public String gibbBeschreibungGegenstand(){
            if (this.gegenstand != null){
                return "Gegenstand: " + this.gegenstand.getBeschreibung() +", "+ this.gegenstand.getGewicht() + "g";
            } else {
                return "Gegensstand: Es gibt keinen Gegenstand!";
            }
        }
    ```
    
- [x]  Übung 8.21
    
    Gegenstände werden im Raum erzeugt und beim Erzeugen des Raumes über den Konstruktor mitgegeben und zugewiesen. somit ist der Gegenstand im Raum und kann über diese Klasse ausgegeben werden. Gleich wie die Ausgänge. 
    
- [x]  Übung 8.22
    
    ```java
    public Raum(String beschreibung, Gegenstaende gegenstand) {
            this.beschreibung = beschreibung;
            this.ausgaenge = new HashMap<>();
            this.gegenstaendeImRaum = new ArrayList<>();
            gegenstandAblegen(gegenstand);
        }
    public void gegenstandAblegen(Gegenstaende gegenstand){
            this.gegenstaendeImRaum.add(gegenstand);
        }
    private String gibbBeschreibungGegenstand() {
            String gegenstaendeAusgabe = "Gegenstand: ";
            if (gegenstaendeImRaum.isEmpty()){
                gegenstaendeAusgabe += "Es gibt keinen Gegenstand!";
            }else {
                for (Gegenstaende gegenstandImRaum : gegenstaendeImRaum) {
                    if (gegenstandImRaum != null) {
                        gegenstaendeAusgabe += gegenstandImRaum.getBeschreibung() + " ";
                    }
                }
            }
            return gegenstaendeAusgabe;
        }
    ```
    
- [x]  Übung 8.23
    
    ```java
    private Raum vorherigerRaum;
    //wechsle Raum den aktuellen raum auf den vorherigen referenzieren
    private void wechsleInVorherigenRaum(){
            aktuellerRaum = vorherigerRaum;
            rauminfoAusgeben();
        }
    ```
    
- [x]  Übung 8.24
    
    Das zweite Wort wird ignorriert, da bei der Überprüfung nur das Befehlswort auf equals geprüft wird. Gleich wie bei look und eat.
    
- [x]  Übung 8.25
    
    Da beim Wechseln der Räume immer der vorherige Raum gespeichert wird, kan nur einmal zurück gesprungen werden. Wäre sinnvoller alle Schritte in einem Stack zu speichern.
    
- [x]  Übung 8.26
    
    ```java
    //Datenfeld
    private Stack<Raum> vorherigeRaeume;
    //räume anlegen
    vorherigeRaeume = new Stack<>();
    private void wechsleInVorherigenRaum() {
            //aktuellerRaum = vorherigerRaum;
            if (!vorherigeRaeume.empty()) {
                aktuellerRaum = vorherigeRaeume.pop();
            } else {
                System.out.println("Du stehst am Anfang.");
            }
            rauminfoAusgeben();
        }
    ```
    
- [ ]  Übung 8.27
    
    ???
    
- [x]  Übung 8.28
    
    Spielerklasse mit folgenden Datenfeldern angelegt
    
    ```java
    private String name;
    private ArrayList<Gegenstand> gegenstand;
    private int tragkraft;
    private Raum aktuellerRaum;
    ```
    
- [x]  Übung 8.29
    
    Befehle in der Befehlswörterklasse erweitern
    
    ```java
    //verarveiteBefehl
    case "drop":
                    if (spieler.gegenstandAblegen(befehl.gibZweitesWort())){
                        System.out.println("Gegenstand konnte abgelegt werden.");
                    }else{
                        System.out.println("Gegenstand konnte nicht abgelegt werden.");
                    };
                    break;
                case "take":
                    if (spieler.gegenstandAufnehmen(befehl.gibZweitesWort())){
                        System.out.println("Gegenstand konnte aufgenommen werden.");
                    }else{
                        System.out.println("Gegenstand konnte nicht aufgenommen werden.");
                    };
                    break;
    ```
    
- [x]  Übung 8.30
    
    Wurde mittels einer ArrayList gelöst.
    
- [x]  Übung 8.31
    
    ```java
    
        public int getanzahlGewicht(){
            int anzahlGewicht = 0;
            for (Gegenstand gegenstandSpieler : gegenstand) {
                anzahlGewicht += gegenstandSpieler.getGewicht();
            }
            return anzahlGewicht;
        }
    ```
    
- [x]  Übung 8.32
    
    ```java
    public String stausGegenstaende(){
            String status = "Gegenstaende: ";
            for (Gegenstand gegensteande :gegenstand) {
                status += gegensteande.getName() + " ";
            }
            status += "Gewicht: " + getanzahlGewicht();
            return status;
        }
    
    ```
    
- [x]  Übung 8.33
    
    Der Befehl eat muffin müsste beim Parser adaptiert werden, da dieser Befehle immer zerlegt.
    
    ```java
    case "eat":
                    if (befehl.hatZweitesWort() && befehl.gibZweitesWort().equals("Muffin")){
                        eatMuffin(befehl);
                    }else{
                        eat();
                    }
                    break;
    
    private void eatMuffin(Befehl befehl) {
            if(spieler.hasMuffin()){
                System.out.println("Sie haben den Muffin gegessen und ein Tragkraft von " + spieler.getTragkraft() +"kg.");
            }else{
                System.out.println("Sie haben leider keinen Muffin");
            }
        }
    ```
    
- [x]  Übung 8.34
- [x]  Übung 8.35
    
    LOOK in der Klasse Befehlswort hizufügen und in der Klasse Befehlswörter ins HaschMap hinzufügen
    
- [x]  Übung 8.36
    
    Befehlswörter im HashMap ändern
    
- [x]  Übung 8.37
    
    es wird help ausgegeben, da es fix im String steht.
    
- [x]  Übung 8.38
    
    in Projekt ZUUL-mit-Enums-V1
    
- [x]  Übung 8.39
    
    Damit der Befehl funktioniert muss natürlich auch die Methode verarbeiteBefehl in der Spiel Klasse geändert werden.
    
- [x]  Übung 8.40
    
    es wird genau der Test ausgegeben, welcher bei den enums definiert wurde.
    
- [x]  Übung 8.41
    
    ```java
    //Datenfeld anlegen
    //spielen
    while (!beendet && this.anzahlRaumGewechselt < 3) {
                Befehl befehl = parser.liefereBefehl();
                beendet = verarbeiteBefehl(befehl);
            }
    //bei jedem Raumwechsel hochzählen
    ```
    
- [x]  Übung 8.42
    
    ```java
    //spielen
    while (!beendet && this.anzahlRaumGewechselt < 3 && spieler.getAktuellerRaum().hasAnyExit() ) {
                Befehl befehl = parser.liefereBefehl();
                beendet = verarbeiteBefehl(befehl);
            }
    //Raum
    public boolean hasAnyExit(){
            if (this.ausgaenge.isEmpty()){
                return false;
            }
            return true;
        }
    ```
    
- [x]  Übung 8.43
    
    ```java
    //Gegenstand
    public boolean getBeamerGeladen() {
            return beamerGeladen;
        }
    
        public void setBeamerGeladen(boolean beamerGeladen) {
            this.beamerGeladen = beamerGeladen;
        }
    //Spiel
    public void beamerLaden() {
            if (spieler.hasBeamer()) {
                System.out.println("Der Beamer wurde geladen!");
                spieler.setBeamerGeladen();
                spieler.setBeamerRaum(spieler.getAktuellerRaum());
            } else {
                System.out.println("Du hast keinen Beamer der geladen werden kann!");
            }
        }
    
        public void beamerEinsetzen() {
            if (spieler.hasBeamer() && spieler.getBeamerGeladen()) {
                System.out.println("Der Beamer wurde eingesetzt und an dem Ort an dem er eingesetzt wurde, abgelegt.");
                spieler.gegenstandAblegen("Beamer");
                spieler.setAktuellerRaum(spieler.getBeamerRaum());
                rauminfoAusgeben();
            } else {
                System.out.println("Es hat nicht funktioniert!");
            }
        }
    //Spieler
    public boolean hasBeamer(){
            for (Gegenstand gegenstaende : gegenstand) {
                if (gegenstaende.getName().equals("Beamer")){
                    return true;
                }
            }
            return false;
        }
    
        public boolean setBeamerGeladen(){
            for (Gegenstand gegenstaende : gegenstand) {
                if (gegenstaende.getName().equals("Beamer")){
                    gegenstaende.setBeamerGeladen(true);
                    return true;
                }
            }
            return false;
        }
    
        public boolean getBeamerGeladen(){
            for (Gegenstand gegenstaende : gegenstand) {
                if (gegenstaende.getName().equals("Beamer") && gegenstaende.getBeamerGeladen()){
                    return true;
                }
            }
            return false;
        }
    ```
    
- [x]  Übung 8.44
    
    Räume und Gegenstände als Datenfelder (Instanzvaruiablen) angelegt;
    
    ```java
    //Raum
    public void loescheAusgaenge(String richtung, Raum nachbar) {
            ausgaenge.remove(richtung, nachbar);
        }
    //Spieler
    public boolean hasKey(){
            for (Gegenstand gegenstaende : gegenstand) {
                if (gegenstaende.getName().equals("Schluessel")){
                    return true;
                }
            }
            return false;
        }
    //Spiel
    private void wechsleRaum(Befehl befehl) {
            if (!befehl.hatZweitesWort()) {
                // Gibt es kein zweites Wort, wissen wir nicht, wohin...
                System.out.println("Wohin m�chten Sie gehen?");
                return;
            }
    
            String richtung = befehl.gibZweitesWort();
            Raum naechsterRaum = spieler.getAktuellerRaum().gibAusgang(richtung);
    
            if (naechsterRaum == null) {
                System.out.println("Dort ist keine Tuer!");
            } else if (naechsterRaum.gibBeschreibung().equals("verschlossen") && spieler.hasKey()){
                System.out.println("Die Tür wurde geöffnet.");
                vorherigeRaeume.push(spieler.getAktuellerRaum());
                halle.loescheAusgaenge("east", verschlossenerRaum);
                halle.setzeAusgaenge("east", shopSport);
                spieler.setAktuellerRaum(shopSport);
                this.anzahlRaumGewechselt++;
                rauminfoAusgeben();
            } else if (naechsterRaum.gibBeschreibung().equals("verschlossen")){
                System.out.println("Die Tür ist verschlossen, Sie benötigen ein Schlussel.");
            } else {
                //vorherigerRaum = aktuellerRaum;
                vorherigeRaeume.push(spieler.getAktuellerRaum());
                spieler.setAktuellerRaum(naechsterRaum);
                this.anzahlRaumGewechselt++;
                rauminfoAusgeben();
            }
        }
    ```
    
- [ ]  Übung 8.45
    
    Könnte über ein ArrayList und einer Random Zahl gelöst werden, die in der While Schleife ausgelöst wird ,sobald man in dem bestimmten Raum ist.