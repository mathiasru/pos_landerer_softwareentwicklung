package adapter;

import exception.KeineZustaendigkeitGefundenException;
import main.ISammelumrechnung;
import main.IUmrechnen;

public class WaehrungsrechnerAdapter implements ISammelumrechnung {

    private IUmrechnen iUmrechnen; //delegierter Klassenadapter

    public WaehrungsrechnerAdapter(IUmrechnen iUmrechnen) {
        this.iUmrechnen = iUmrechnen;
    }

    /**
     * Methode Adaptiert eine ISammelrechnung zu einer IUmrechnung und summiert alle berechneten Beträge auf
     * @param variante übergebende Variante vom Typ String.
     * @param betraege übergebende Beträge vom Typ String-Array.
     * @return gibt eine berechnete Summe der berechneten Beträge vom Typ double zurück
     * @throws KeineZustaendigkeitGefundenException
     */
    @Override
    public double sammelumrechnen(String variante, double[] betraege) throws KeineZustaendigkeitGefundenException {
        double summe = 0;
        for (int i = 0; i < betraege.length; i++) {
            summe += iUmrechnen.umrechnen(variante, betraege[i]);
        }
        return Math.round((summe) * 100.0) / 100.0;
    }
}
