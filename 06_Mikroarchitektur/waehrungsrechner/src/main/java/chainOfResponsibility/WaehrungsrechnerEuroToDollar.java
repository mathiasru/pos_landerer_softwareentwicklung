package chainOfResponsibility;


import exception.KeineZustaendigkeitGefundenException;
import exception.NichtAlleBenoetigtenFelderWurdenGesetzt;
import main.IUmrechnen;
import main.Waehrungsrechner;

public class WaehrungsrechnerEuroToDollar extends Waehrungsrechner {

    //evtl. über builder lösen (builder übergeben und objekt erzeugen)
    public WaehrungsrechnerEuroToDollar(IUmrechnen naechsterIUmrechnen) {
        super(naechsterIUmrechnen); //builder.neachsterAufruf
    }

    /**
     * Methode berechnet den Betrag sofern er den Kriterien entspricht, ansonsten wird an das nächste Kettenmitglied delegiert.
     * @param variante übergebende Variante vom Typ String.
     * @param betrag übergebender Betrag vom Typ double.
     * @return gibt den berechneten Betrag vom Typ double zurück.
     * @throws KeineZustaendigkeitGefundenException
     */
    public double umrechnen(String variante, double betrag) throws KeineZustaendigkeitGefundenException {
        if (variante.equals("EuroToDollar")) {
            return super.berechnen(betrag, getFaktor());
        } else {
            return super.umrechnen(variante, betrag);
        }
    }

    public double getFaktor() {
        //hier evtl. Rest-API anzapfen
        return 1.14;
    }

    //Builder Pattern zum Erzeugen von Objekten
    public static class Builder {

        private IUmrechnen naechsterIUmrechnen;

        public Builder() {
        }

        public Builder setNaechsterIUmrechnen(IUmrechnen naechsterIUmrechnen){
            this.naechsterIUmrechnen = naechsterIUmrechnen;
            return this;
        }

        public WaehrungsrechnerEuroToDollar build() throws NichtAlleBenoetigtenFelderWurdenGesetzt {
            //hier evtl. prüfen auf gesetzte Felder (Exception NichtAlleBenoetigtenFelderWurdenGesetzt werfen) in diesem Projekt nicht verwendbar
            return new WaehrungsrechnerEuroToDollar(this.naechsterIUmrechnen);
        }
    }


}
