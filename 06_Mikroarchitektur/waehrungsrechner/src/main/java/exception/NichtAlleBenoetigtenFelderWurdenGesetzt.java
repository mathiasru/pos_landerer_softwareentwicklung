package exception;

public class NichtAlleBenoetigtenFelderWurdenGesetzt extends Exception {
    public NichtAlleBenoetigtenFelderWurdenGesetzt(String message) {
        super(message);
    }
}
