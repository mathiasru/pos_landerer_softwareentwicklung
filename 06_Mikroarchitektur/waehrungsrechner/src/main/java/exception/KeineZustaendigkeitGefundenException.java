package exception;

public class KeineZustaendigkeitGefundenException extends Exception {

    public KeineZustaendigkeitGefundenException(String message) {
        super(message);
    }
}
