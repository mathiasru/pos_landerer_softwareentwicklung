package main;

import exception.KeineZustaendigkeitGefundenException;

public interface ISammelumrechnung {

    double sammelumrechnen(String variante, double[] betraege) throws KeineZustaendigkeitGefundenException;
}
