package main;

import exception.KeineZustaendigkeitGefundenException;
import observer.IWaehrungsrechnerObserver;

import java.util.ArrayList;
import java.util.List;


public abstract class Waehrungsrechner implements IUmrechnen {

    public IUmrechnen naechsterIUmrechnen; //nächstes Kettenmiglied
    public List<IWaehrungsrechnerObserver> observers = new ArrayList<>(); //Observerliste

    public Waehrungsrechner(IUmrechnen naechsterIUmrechnen) {
        this.naechsterIUmrechnen = naechsterIUmrechnen; //setzen des nächsten Kettenmidliedes
    }

    /**
     * Methode gibt bei einem gesetzten nächstenIUmrechen weiter an das nächste Kettenglied, ansonsten wird eine Exceptiopn geworfen.
     *
     * @param variante übergebende Variante vom Typ String.
     * @param betrag   übergebender Betrag vom Typ double.
     * @return gibt den umgerechneten Betrag vom Typ double zurück.
     * @throws KeineZustaendigkeitGefundenException sofern keine Zuständigkeit in der Kette gefunden wurde
     */
    public double umrechnen(String variante, double betrag) throws KeineZustaendigkeitGefundenException {
        if (this.naechsterIUmrechnen != null) {
            double berechneterBetrag = naechsterIUmrechnen.umrechnen(variante, betrag); //weitergabe an nächstes Kettenmitglied
            this.benachrichtigeObserver(variante, betrag, berechneterBetrag); //Observer mit Werten benachrichtigen
            return berechneterBetrag;
        } else {
            throw new KeineZustaendigkeitGefundenException("Variante: " + variante + " nicht vorhanden!");
        }
    }

    //Template Hook Pattern //sinnhaftigkeit?
    /**
     * Methode berechnet den Betrag in dem der Ausgangsbetrag mit dem übergebenen Faktor multipliziert wird.
     *
     * @param betrag übergebener Ausgangsbetrag vom Typ double.
     * @param faktor übergebener Faktor vom Typ double.
     * @return gibt den berechneten Betrag zurück.
     */
    public double berechnen(double betrag, double faktor) {
        return runden(betrag * faktor);
    }

    /**
     * Methode rundet den Betrag auf zwei Kommastellen und im Falle ab. (floor)
     *
     * @param berechneterBetrag übergebener Betrag vom Typ double.
     * @return gibt den gerundeten Betrag vom Tyo double zurück.
     */
    public double runden(double berechneterBetrag) {
        return Math.floor(berechneterBetrag * 100.0) / 100.0; //floor rundet im Falle ab
    }

    //Observer-Pattern
    /**
     * Methode benachrichtet alle angemeldeten Observer und übergibt Informationen.
     *
     * @param variante          übergebende Variante vom Typ String.
     * @param betrag            übergebender Betrag vom Typ double.
     * @param berechneterBetrag übergebener berechneter Betrag vom Typ double.
     */
    private void benachrichtigeObserver(String variante, double betrag, double berechneterBetrag) {
        observers.forEach(observers -> observers.benachrichtigeObserver(variante, betrag, berechneterBetrag)); //Lambda-Ausdruck
    }

    /**
     * Methode meldet übergebene Observer an.
     *
     * @param observer übergebener Observer.
     */
    public void hinzufuegenObserver(IWaehrungsrechnerObserver observer) {
        observers.add(observer);
    }

    /**
     * Methode meldet übergebene Observer ab.
     *
     * @param observer übergebener Observer.
     */
    public void entfernenObserver(IWaehrungsrechnerObserver observer) {
        observers.remove(observer);
    }


}
