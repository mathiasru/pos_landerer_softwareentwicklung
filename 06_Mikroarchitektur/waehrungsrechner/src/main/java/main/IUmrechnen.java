package main;

import exception.KeineZustaendigkeitGefundenException;


public interface IUmrechnen {

    double umrechnen(String variante, double betrag) throws KeineZustaendigkeitGefundenException;

}
