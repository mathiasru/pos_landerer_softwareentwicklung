import adapter.WaehrungsrechnerAdapter;
import chainOfResponsibility.WaehrungsrechnerEuroToDollar;
import chainOfResponsibility.WaehrungsrechnerEuroToYen;
import chainOfResponsibility.WaehrungsrechnerPfundToEuro;
import decorater.WaehrungsrechnerGebuehrenWaehrungsrechnerDekorierer;
import exception.KeineZustaendigkeitGefundenException;
import exception.NichtAlleBenoetigtenFelderWurdenGesetzt;
import main.ISammelumrechnung;
import main.IUmrechnen;
import main.Waehrungsrechner;
import observer.WaehrungsrechnerAtomFeedObserver;
import observer.WaehrungsrechnerLogObserver;

public class Main {

    public static void main(String[] args) {

        //erzeugen eines konkreten Währungsrechners mittels Builder-Pattern
        IUmrechnen waehrungsrechner = null;
        try {
            waehrungsrechner = new WaehrungsrechnerEuroToDollar.Builder().
                    setNaechsterIUmrechnen(null).
                    build();
        } catch (NichtAlleBenoetigtenFelderWurdenGesetzt e) {
            System.out.println(e.getMessage());
        }

        //Chain of responsibility mit verschiedenen Konkreten Währungsrechnern erzeugen
        Waehrungsrechner waehrungsrechner1 = new WaehrungsrechnerEuroToYen(new WaehrungsrechnerPfundToEuro(waehrungsrechner));

        //Dekorierer erzeugen und die oben erstellte Kette einfügen
        waehrungsrechner1 = new WaehrungsrechnerGebuehrenWaehrungsrechnerDekorierer(waehrungsrechner1);

        //Observer anmelden
        waehrungsrechner1.hinzufuegenObserver(new WaehrungsrechnerLogObserver());
        waehrungsrechner1.hinzufuegenObserver(new WaehrungsrechnerAtomFeedObserver());


        double[] werteSammelumrechnung = {13.0, 10.0, 5.0};

        //dem Adapter die Kette mitgeben um beim Aufrufen der sammelumrechnung die jeweiligen konreten Währungsrechner ansprechen zu können
        ISammelumrechnung iSammelumrechnung1 = new WaehrungsrechnerAdapter(waehrungsrechner1);

        try {
            System.out.println("EuroToYen inkl. Gebühr: " + waehrungsrechner1.umrechnen("EuroToYen", 1.0));
            System.out.println("");
            System.out.println("Sammelumrechnung inkl. Gebühr: " + iSammelumrechnung1.sammelumrechnen("EuroToDollar", werteSammelumrechnung));
            System.out.println("");
            System.out.println("PfundToEuro inkl. Gebühr: " + waehrungsrechner1.umrechnen("PfundToEuro", 1.0));
            System.out.println("");
            System.out.println(waehrungsrechner1.umrechnen("EuroToPfund", 1.0));
        } catch (KeineZustaendigkeitGefundenException exception) {
            System.out.println(exception.getMessage());
        }
    }

}
