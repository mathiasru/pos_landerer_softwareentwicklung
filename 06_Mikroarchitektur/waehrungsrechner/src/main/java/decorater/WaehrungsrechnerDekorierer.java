package decorater;


import main.IUmrechnen;
import main.Waehrungsrechner;

public abstract class WaehrungsrechnerDekorierer extends Waehrungsrechner {

    public WaehrungsrechnerDekorierer(IUmrechnen naechsterIUmrechnen) {
        super(naechsterIUmrechnen);
    }

}
