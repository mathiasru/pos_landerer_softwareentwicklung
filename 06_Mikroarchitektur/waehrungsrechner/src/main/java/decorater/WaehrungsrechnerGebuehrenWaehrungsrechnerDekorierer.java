package decorater;

import main.IUmrechnen;
import exception.KeineZustaendigkeitGefundenException;

public class WaehrungsrechnerGebuehrenWaehrungsrechnerDekorierer extends WaehrungsrechnerDekorierer {

    private static final double GEBUEHR_FIX = 5.0;
    private static final double GEBUEHR_FLEXIBEL = 0.005;

    public WaehrungsrechnerGebuehrenWaehrungsrechnerDekorierer(IUmrechnen naechsterIUmrechnen) {
        super(naechsterIUmrechnen);
    }

    /**
     * Methode dekoriert die Umrechnungsmethode mit verschiedenen Gebühren.
     * @param variante übergebende Variante vom Typ String.
     * @param betrag übergebender Betrag vom Typ double.
     * @return gibt den berechneten Betrag vom Typ double zurück.
     * @throws KeineZustaendigkeitGefundenException
     */
    @Override
    public double umrechnen(String variante, double betrag) throws KeineZustaendigkeitGefundenException {
        if (variante.startsWith("Eur")) {
            //im Falle, dass ein Betrag von 0€ umgerechnet werden soll, werden keine Gebühren verrechnet.
            return betrag != 0.0 ? (super.umrechnen(variante, betrag + GEBUEHR_FIX)) : 0.0;
        } else {
            return super.runden(super.umrechnen(variante, betrag * (1 + GEBUEHR_FLEXIBEL)));
        }
    }

}
