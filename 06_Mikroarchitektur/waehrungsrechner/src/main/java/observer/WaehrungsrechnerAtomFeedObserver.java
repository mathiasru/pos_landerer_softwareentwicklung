package observer;

import com.sun.syndication.feed.synd.*;
import com.sun.syndication.io.SyndFeedOutput;

import java.util.ArrayList;
import java.util.List;

public class WaehrungsrechnerAtomFeedObserver extends WaehrungsrechnerObserver {

    //Dateiname zum Speichern
    private final String DATEINAME = "AtomFeedObserver.xml";
    private static List entries = new ArrayList();

    /**
     * Methode benachrichtigt Observer, welche das Intaface implementieren mit bestimmten Inhalten.
     * @param variante Währungsart vom Typ String
     * @param betrag Anfangsbetrag der jeweiligen Währung vom Tyo double
     * @param berechneterBetrag Zielbetrag der jeweiligen Währung vom Tyo double
     */
    @Override
    public void benachrichtigeObserver(String variante, double betrag, double berechneterBetrag) {
        String logdateiOutput = super.logdateiInhaltErstellen(variante, betrag, berechneterBetrag);
        super.logdateiSchreiben(feedErzeugen(logdateiOutput), DATEINAME, false);
    }

    /**
     * Methode erzeugt einen Syndfeed und gibt diesen in Form eines Strings zurück. //nicht wirklich sinnvoll
     * @param logdateiOutput übergebener String
     * @return gibt den Syndfeed in Form eines Strings zurück
     */
    public String feedErzeugen(String logdateiOutput){
        String erzeugterFeed = "";
        try {
            SyndFeed feed = new SyndFeedImpl();
            feed.setFeedType("atom_1.0");
            feed.setTitle("Atom-Feed-Observer");
            feed.setAuthor("Mathias Rudig");
            feed.setLink("https://example.com");

            SyndEntry entry = new SyndEntryImpl();
            entry.setTitle("Eintrag");
            SyndContent description = new SyndContentImpl();
            description.setType("text/plain");
            description.setValue(logdateiOutput);
            entry.setDescription(description);
            entries.add(entry);

            feed.setEntries(entries);

            erzeugterFeed = new SyndFeedOutput().outputString(feed); //feed als String auslesen
        } catch (Exception exception) {
            System.out.println(exception.getMessage());
        }
        return erzeugterFeed;
    }
}
