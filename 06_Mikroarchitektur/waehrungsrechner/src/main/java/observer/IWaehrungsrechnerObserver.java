package observer;

public interface IWaehrungsrechnerObserver {

    /**
     * Methode benachrichtigt Observer, welche das Intaface implementieren mit bestimmten Inhalten.
     * @param variante Währungsart vom Typ String
     * @param betrag Anfangsbetrag der jeweiligen Währung vom Tyo double
     * @param berechneterBetrag Zielbetrag der jeweiligen Währung vom Tyo double
     */
    void benachrichtigeObserver(String variante, double betrag, double berechneterBetrag);
}
