package observer;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Klasse wird als Helperklasse für nachstehende Observer verwendet.
 */
public abstract class WaehrungsrechnerObserver implements IWaehrungsrechnerObserver {
    //Formatieren der Datumsausgabe
    public final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");

    /**
     * Methode teilt den übergebenen String an der Stelle "To" und erzeugt daraus ein String-Array.
     * @param variante übergebener String der gesplittet wird.
     * @return gibt das Ergebnis in Form eines String Arrays zurück.
     */
    public String[] parseWaehrung(String variante) {
        String[] waehrungen = variante.split("To"); //Hier könnte es bei speziellen Währungen kritisch werden, besser wäre ein Delimiter -,;/
        return waehrungen;
    }

    /**
     * Methode erstellt einen formatierten String aus den übergebenen Parametern für eine Logdatei und gibt diesen zurück.
     * @param variante übergebener String die Ausgangs und Zielwährung beschreibt.
     * @param betrag übergebener Anfgangsbetrag vom Typ double.
     * @param berechneterBetrag übergebener Zielbetrag vom Typ double.
     * @return gibt den formatierten String zurück
     */
    public String logdateiInhaltErstellen(String variante, double betrag, double berechneterBetrag) {
        String[] waehrung = parseWaehrung(variante);
        String logdateiOutput = "";
        logdateiOutput += "Uhrzeit: " + DATE_TIME_FORMATTER.format(LocalDateTime.now()) + "\n";
        logdateiOutput += "Ausgangswährung: " + waehrung[0] + "\n";
        logdateiOutput += "Ausgangsbetrag: " + betrag + "\n";
        logdateiOutput += "Zielwährung: " + waehrung[1] + "\n";
        logdateiOutput += "Zielbetrag: " + berechneterBetrag + "\n\n";
        return logdateiOutput;
    }

    /**
     * Methode schreibt den übergebenen String in eine Ausgabedatei mit dem Dateinamen, der übergeben wurde.
     * Sofern die Datei existiert wird angehängt, ansonsten eine neue Datei erstellt.
     * @param logdateiOutput übergebener String, welcher in die Ausgabedatei angefügt wird.
     * @param filename übergebender Dateiname der Ausgabedatei.
     */
    public void logdateiSchreiben(String logdateiOutput, String filename, boolean anfuegen) {
        BufferedWriter ausgabe;
        try {
            ausgabe = new BufferedWriter(
                    new OutputStreamWriter(
                            new FileOutputStream("./logdateien/" + filename, anfuegen))); //parameter true lasst ubns in vorhandene Files Inhalte anfügen
            ausgabe.write(logdateiOutput); //schreiben inDdatei
            ausgabe.close(); //nicht vergessen
        } catch (IOException excrption) {
            excrption.printStackTrace();
        }
    }

}
