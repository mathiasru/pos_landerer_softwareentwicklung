package observer;

public class WaehrungsrechnerLogObserver extends WaehrungsrechnerObserver {

    //Dateiname zum Speichern
    private final String DATEINAME = "LogObserver.txt";

    /**
     * Methode benachrichtigt Observer, welche das Intaface implementieren mit bestimmten Inhalten.
     * @param variante Währungsart vom Typ String
     * @param betrag Anfangsbetrag der jeweiligen Währung vom Tyo double
     * @param berechneterBetrag Zielbetrag der jeweiligen Währung vom Tyo double
     */
    @Override
    public void benachrichtigeObserver(String variante, double betrag, double berechneterBetrag) {
        String logdateiOutput = super.logdateiInhaltErstellen(variante, betrag, berechneterBetrag);
        super.logdateiSchreiben(logdateiOutput, DATEINAME, true); //hier evtl. Datei nacph außen schreiben
    }

}
