package com.kolleg;

public class Temperatur {
    public String name;
    public Double istWert;
    public String einheit;

    public Temperatur(String name, Double istWert) {
        this.name = name;
        this.istWert = istWert;
        this.einheit = "°C";
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getIstWert() {
        return istWert;
    }

    public void setIstWert(Double istWert) {
        this.istWert = istWert;
    }

    public String getEinheit() {
        return einheit;
    }

    public void setEinheit(String einheit) {
        this.einheit = einheit;
    }

    @Override
    public String toString() {
        return "Temperatur{" +
                "name='" + name + '\'' +
                ", istWert=" + istWert +
                ", einheit='" + einheit + '\'' +
                '}';
    }
}
