package com.kolleg.decorator;

import com.kolleg.Temperatur;

import java.util.List;

public class KonkreteKomponente implements Komponente{

    @Override
    public void operation(List<Temperatur> temperaturListe) {
        System.out.println("Konkrete Aufgabe eingesetzt!");
        //Hier kann die Methode implementiert werden
    }
}
