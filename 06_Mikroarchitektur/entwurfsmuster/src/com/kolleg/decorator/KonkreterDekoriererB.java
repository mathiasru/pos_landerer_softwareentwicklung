package com.kolleg.decorator;

import com.kolleg.Temperatur;

import java.util.List;

public class KonkreterDekoriererB extends Dekorierer{

    public KonkreterDekoriererB(Komponente komponente) {
        super(komponente);
    }

    @Override
    public void operation(List<Temperatur> temperaturListe) {
        komponente.operation(temperaturListe);
        System.out.println("KonkreterDekoriererB");
        //Hier kann die Methode implementiert werden
    }
}
