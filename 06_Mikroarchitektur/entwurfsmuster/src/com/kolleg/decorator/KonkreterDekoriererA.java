package com.kolleg.decorator;

import com.kolleg.Temperatur;

import java.util.List;

public class KonkreterDekoriererA extends Dekorierer{

    public KonkreterDekoriererA(Komponente komponente) {
        super(komponente);
    }

    @Override
    public void operation(List<Temperatur> temperaturListe) {
        System.out.println("KonkreterDekoriererA");
        //Hier kann die Methode implementiert werden
        komponente.operation(temperaturListe);
    }
}
