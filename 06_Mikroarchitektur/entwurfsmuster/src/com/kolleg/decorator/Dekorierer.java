package com.kolleg.decorator;

public abstract class Dekorierer implements Komponente {

    public Komponente komponente;

    public Dekorierer(Komponente komponente) {
        this.komponente = komponente;
    }

}
