package com.kolleg.decorator;

import com.kolleg.Temperatur;

import java.util.List;

public interface Komponente {
    public void operation (List<Temperatur> temperaturListe);
}
