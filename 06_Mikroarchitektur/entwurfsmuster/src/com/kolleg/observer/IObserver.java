package com.kolleg.observer;

public interface IObserver {
    void update(String event);
}
