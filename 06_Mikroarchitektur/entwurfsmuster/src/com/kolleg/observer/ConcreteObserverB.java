package com.kolleg.observer;

public class ConcreteObserverB implements IObserver{
    @Override
    public void update(String event) {
        System.out.println("Update Observer B mittels Event: " + event);
    }
}
