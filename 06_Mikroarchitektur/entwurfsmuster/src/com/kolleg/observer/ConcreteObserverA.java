package com.kolleg.observer;

public class ConcreteObserverA implements IObserver{
    @Override
    public void update(String event) {
        System.out.println("Update Observer A mittels Event: " + event);
    }
}
