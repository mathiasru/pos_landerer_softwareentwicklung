package com.kolleg.observer;

import java.util.ArrayList;
import java.util.List;

public class Subject {

    private List<IObserver> observers = new ArrayList<>();

    public void notifyObserver(String event){
        observers.forEach(observers -> observers.update(event));
    }

    public void attachObserver(IObserver observer){
        observers.add(observer);
    }

    public void detachObserver(IObserver observer){
        observers.remove(observer);
    }
}
