package com.kolleg.strategy;

import com.kolleg.Temperatur;

import java.util.List;

public class Kontext {
    public Strategie strategie;

    public Kontext(Strategie strategie) {
        this.strategie = strategie;
    }

    public void strategieDurchfuehren(List<Temperatur> temperatur){
        strategie.strategieDurchfuehren(temperatur);
    }
}
