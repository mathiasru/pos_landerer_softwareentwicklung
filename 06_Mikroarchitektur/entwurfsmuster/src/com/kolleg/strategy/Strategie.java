package com.kolleg.strategy;

import com.kolleg.Temperatur;
import java.util.List;

public interface Strategie {
    void strategieDurchfuehren(List<Temperatur> temperatur);
}
