package com.kolleg.strategy;

import com.kolleg.Temperatur;

import java.util.List;

public class StrategieZwei implements Strategie{
    @Override
    public void strategieDurchfuehren(List<Temperatur> temperatur) {
        System.out.println("Strategie Zwei durchgeführt.");
        //Hier kann die Methode implementiert werden
    }
}
