package com.kolleg.strategy;

import com.kolleg.Temperatur;

import java.util.List;

public class StrategieEins implements Strategie{
    @Override
    public void strategieDurchfuehren(List<Temperatur> temperatur) {
        System.out.println("Strategie Eins durchgeführt.");
        //Hier kann die Methode implementiert werden
    }
}
