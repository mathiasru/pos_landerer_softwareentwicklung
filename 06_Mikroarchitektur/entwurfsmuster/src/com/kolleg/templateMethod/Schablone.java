package com.kolleg.templateMethod;

import com.kolleg.Temperatur;

public abstract class Schablone {

    public Temperatur schablonenmethode(Temperatur temperatur) {
        primitiveMethode1(temperatur);
        primitiveMethode2(temperatur);
        return temperatur;
    }

    public abstract void primitiveMethode1(Temperatur temperatur);

    public abstract void primitiveMethode2(Temperatur temperatur);

}
