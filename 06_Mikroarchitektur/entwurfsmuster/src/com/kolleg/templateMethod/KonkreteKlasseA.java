package com.kolleg.templateMethod;

import com.kolleg.Temperatur;

public class KonkreteKlasseA extends Schablone {

    @Override
    public void primitiveMethode1(Temperatur temperatur) {
        temperatur.setIstWert(temperatur.istWert * (9.0/5.0) + 32);
    }

    @Override
    public void primitiveMethode2(Temperatur temperatur) {
        temperatur.setEinheit("°F");
    }
}
