package com.kolleg.templateMethod;

import com.kolleg.Temperatur;

public class KonkreteKlasseB extends Schablone{

    @Override
    public void primitiveMethode1(Temperatur temperatur) {
        temperatur.setIstWert(temperatur.istWert + 273.15);
    }

    @Override
    public void primitiveMethode2(Temperatur temperatur) {
        temperatur.setEinheit("°K");
    }
}
