package com.kolleg.chainOfResponsibility;


import com.kolleg.Temperatur;

import java.util.List;

public abstract class Bearbeiter{ //Kann keine Instanz gebildet werden
    public Bearbeiter naechsterBearbeiter; //Rekursion

    public Bearbeiter(Bearbeiter naechsterBearbeiter){
        this.naechsterBearbeiter = naechsterBearbeiter;
    }

    public void anfrageBearbeiten(String aufgabenart, List<Temperatur> temperaturListe){
        if (this.naechsterBearbeiter != null){ //
            naechsterBearbeiter.anfrageBearbeiten(aufgabenart, temperaturListe);
            //weitergabe an den nächsterBearbeiter, welche dem Konstruktor übergeben wurde
        }else{
            System.out.println("Keine Aufgabe gefunden!");
        }
    }
}