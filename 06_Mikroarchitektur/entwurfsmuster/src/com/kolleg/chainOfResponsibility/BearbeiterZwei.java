package com.kolleg.chainOfResponsibility;

import com.kolleg.Temperatur;

import java.util.List;

public class BearbeiterZwei extends Bearbeiter {
    public BearbeiterZwei(Bearbeiter naechsterBearbeiter) {
        super(naechsterBearbeiter);
    }

    @Override
    public void anfrageBearbeiten(String aufgabenart, List<Temperatur> temperaturListe) {
        if (aufgabenart.equals("AufgabeZwei")) { //
            System.out.println("Aufgabe zwei erledigt!");
            //Hier kann die Methode implementiert werden
        } else {
            super.anfrageBearbeiten(aufgabenart, temperaturListe); //Aufruf der Methode in der Mutterklasse
        }
    }
}