package com.kolleg.chainOfResponsibility;

import com.kolleg.Temperatur;

import java.util.List;

public class BearbeiterEins extends Bearbeiter {

    public BearbeiterEins(Bearbeiter naechsterBearbeiter) {
        super(naechsterBearbeiter);
    }

    @Override
    public void anfrageBearbeiten(String aufgabenart, List<Temperatur> temperaturListe) {
        if (aufgabenart.equals("AufgabeEins")) { //
            System.out.println("Aufgabe eins erledigt!");
            //Hier kann die Methode implementiert werden
        } else {
            super.anfrageBearbeiten(aufgabenart, temperaturListe); //Aufruf der Methode in der Mutterklasse
        }
    }
}