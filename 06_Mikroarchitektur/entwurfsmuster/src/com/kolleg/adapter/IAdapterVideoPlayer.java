package com.kolleg.adapter;

public interface IAdapterVideoPlayer {
    void play(String videoName);
}
