package com.kolleg.adapter;

public class AdapterVideoPlayer implements IAdapterVideoPlayer {

    IAdaptierterVideoPlayer iAdaptierterVideoPlayer;

    public AdapterVideoPlayer(IAdaptierterVideoPlayer iAdaptierterVideoPlayer) {
        this.iAdaptierterVideoPlayer = iAdaptierterVideoPlayer;
    }

    @Override
    public void play(String videoName) {
        iAdaptierterVideoPlayer.abspielen(videoName);
    }
}
