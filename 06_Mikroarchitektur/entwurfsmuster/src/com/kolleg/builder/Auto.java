package com.kolleg.builder;

public class Auto {

    private String marke;
    private String modell;
    private int anzahlTueren;

    private Auto(AutoBuilder builder) {
        this.marke = builder.marke;
        this.modell = builder.modell;
        this.anzahlTueren = builder.anzahlTueren;
    }

    @Override
    public String toString() {
        return "Auto{" +
                "marke='" + marke + '\'' +
                ", modell='" + modell + '\'' +
                ", anzahlTueren=" + anzahlTueren +
                '}';
    }

    public static class AutoBuilder {
        private String marke;
        private String modell;
        private int anzahlTueren;

        public AutoBuilder(String marke, String modell) {
            this.marke = marke;
            this.modell = modell;
        }

        public AutoBuilder anzahlTueren(int anzahlTueren) {
            this.anzahlTueren = anzahlTueren;
            return this;
        }

        public Auto build() {
            //ggf. prüfen ob alle Datenfelder gesetzt wurden
            return new Auto(this);
        }
    }
}