package com.kolleg;

import com.kolleg.adapter.*;
import com.kolleg.adapter.AdapterVideoPlayer;
import com.kolleg.adapter.IAdapterVideoPlayer;
import com.kolleg.builder.Auto;
import com.kolleg.chainOfResponsibility.Bearbeiter;
import com.kolleg.chainOfResponsibility.BearbeiterEins;
import com.kolleg.chainOfResponsibility.BearbeiterZwei;
import com.kolleg.decorator.Dekorierer;
import com.kolleg.decorator.KonkreteKomponente;
import com.kolleg.decorator.KonkreterDekoriererA;
import com.kolleg.decorator.KonkreterDekoriererB;
import com.kolleg.observer.ConcreteObserverA;
import com.kolleg.observer.ConcreteObserverB;
import com.kolleg.observer.IObserver;
import com.kolleg.observer.Subject;
import com.kolleg.strategy.Kontext;
import com.kolleg.strategy.StrategieEins;
import com.kolleg.strategy.StrategieZwei;
import com.kolleg.templateMethod.KonkreteKlasseA;
import com.kolleg.templateMethod.Schablone;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        List temperaturListe = new ArrayList<Temperatur>();
        temperaturListe.add(new Temperatur("Wohnzimmer", 21.0));  //Klient
        temperaturListe.add(new Temperatur("Küche", 22.0));

        System.out.println("Strategy Pattern");
        Kontext kontextEins = new Kontext(new StrategieEins());
        kontextEins.strategieDurchfuehren(temperaturListe);
        Kontext kontextZwei = new Kontext(new StrategieZwei());
        kontextZwei.strategieDurchfuehren(temperaturListe);
        System.out.println("");

        System.out.println("Cain of Responisbility Pattern");
        Bearbeiter bearbeiter = new BearbeiterEins(new BearbeiterZwei(null));
        bearbeiter.anfrageBearbeiten("AufgabeEins", temperaturListe);
        bearbeiter.anfrageBearbeiten("AufgabeZwei", temperaturListe);
        bearbeiter.anfrageBearbeiten("AufgabeNichtsVorhanden", temperaturListe);
        System.out.println("");

        System.out.println("Decorator Pattern");
        Dekorierer dekorierer = new KonkreterDekoriererA(new KonkreterDekoriererB(new KonkreteKomponente()));
        dekorierer.operation(temperaturListe);
        System.out.println("");

        System.out.println("Template Method Pattern");
        Schablone schablone = new KonkreteKlasseA();
        System.out.println(schablone.schablonenmethode(new Temperatur("WC", 19.0)));
        System.out.println("");

        System.out.println("Builder Pattern");
        Auto auto = new Auto.AutoBuilder("VW", "Polo").anzahlTueren(3).build();
        System.out.println(auto);
        System.out.println("");

        System.out.println("AdapterVideoPlayer Pattern");
        IAdaptierterVideoPlayer mp4Player = new AdaptierterMp4Player();
        IAdaptierterVideoPlayer vlcPlayer = new AdaptierterVLCPlayer();
        IAdapterVideoPlayer adapter = new AdapterVideoPlayer(mp4Player);
        adapter.play("Video_01.wav");
        adapter = new AdapterVideoPlayer(vlcPlayer);
        adapter.play("Video_02.mp4");
        System.out.println("");

        System.out.println("Observer Pattern");
        Subject subject = new Subject();
        subject.attachObserver(new ConcreteObserverA());
        subject.attachObserver(new ConcreteObserverB());
        subject.notifyObserver("Event_01");

    }
}
