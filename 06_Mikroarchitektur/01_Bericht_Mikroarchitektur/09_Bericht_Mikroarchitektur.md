
- [Prinzipien des objektorientierten Entwurfs](#prinzipien-des-objektorientierten-entwurfs)
  - [SOLID Prinzip](#solid-prinzip)
  - [DRY](#dry)
  - [KISS](#kiss)
  - [YAGNI](#yagni)
- [Entwurfsmuster](#entwurfsmuster)
  - [Verhaltensmuster](#verhaltensmuster)
    - [Strategy](#strategy)
    - [Chain of Responsibility](#chain-of-responsibility)
    - [Template Method](#template-method)
    - [Observer](#observer)
  - [Erzeugungsmuster](#erzeugungsmuster)
    - [Builder](#builder)
    - [Singelton](#singelton)
  - [Strukturmuster](#strukturmuster)
    - [Decorator](#decorator)
    - [Adapter](#adapter)
  
# Prinzipien des objektorientierten Entwurfs

## SOLID Prinzip

- **Single Responsibility** (Es sollte nie mehr als einen Grund geben, eine Klasse zu ändern; Eine Klasse sollte nur eine Aufgabe haben)
- **Open Closed** (Module sollten sowohl offen für Erweiterungen, als auch verschlossen für Modifikationen sein; Abstraktion)
- **Liskov Substitution** (Verhalten muss über die Vererbungshierarchie gleich bleiben)
- **Interface Aggregation** (Klassen sollten alle Methoden eines Interfaces implementieren können; Schnittstellen aufteilen)
- **Dependency Inversion** (Klassen und Details sollten von Abstraktionen(Interface) abhängen) Umkehren von Abhängigkeiten, Autowired (z.B.: Datenbank abhängig von Servicelayer)

## DRY

- Don’t repeat yourself
- vermeide Redundanzen

## KISS

- Keep it simple and stupid
- es sollte die einfachste Lösung gewählt werden

## YAGNI

- You Aren’t Gonna Need It
- Funktionalität erst dann implementieren, wenn man sie auch wirklich braucht
- Standardimplementierungen für Erweiterungen sollten hier nicht berücksichtigt werden

# Entwurfsmuster

**Entwurfsmuster** oder Design Patterns sind Lösungsschablonen um bestimmte Problemstellungen einfacher lösen zu können. Sie stellen eine wiederverwendbare Vorlage zur Problemlösung dar, die in einem bestimmten Zusammenhang einsetzbar ist. 

[http://wwwswt.informatik.uni-rostock.de/patterns/](http://wwwswt.informatik.uni-rostock.de/patterns/)

## Verhaltensmuster

### Strategy

Ist ein Verhaltensmuster, das beispielsweise für unterschiedliche Varianten von Verhaltensweisen verwendet wird. 

![Strategie01.png](09_Bericht_Mikroarchitektur_Bilder/Strategie01.png)

Das Ganze kann mittels Strategie(Interface), welches uns die Methodensignatur vorgibt und den jeweilig verschiedenen Strategieklassen, die das Interface und die Methode implementieren. Durch verwenden einer Kontextklasse, welche ein Datenfeld des Strategie(Interface) hält und einen Konstruktor der eine Strategie erwartet, wird die Strategien gewählt und ausgeführt. Dadurch entstehen keine Abhängigkeiten.

```java
public interface Strategie {
    void strategieDurchfuehren(List<Temperatur> temperatur);
}
```

```java
public class StrategieEins implements Strategie{
    @Override
    public void strategieDurchfuehren(List<Temperatur> temperatur) {
        System.out.println("Strategie Eins durchgeführt.");
        //Hier kann die Methode implementiert werden
    }
}
```

```java
public class Kontext {
    public Strategie strategie;

    public Kontext(Strategie strategie) {
        this.strategie = strategie;
    }

    public void strategieDurchfuehren(List<Temperatur> temperatur){
        strategie.strategieDurchfuehren(temperatur);
    }
}
```

```java
Kontext kontextEins = new Kontext(new StrategieEins());
kontextEins.strategieDurchfuehren(temperaturListe);
Kontext kontextZwei = new Kontext(new StrategieZwei());
kontextZwei.strategieDurchfuehren(temperaturListe); 
```

### Chain of Responsibility

Ist ein Verhaltensmuster, das Zuständigkeiten innerhalb einer Kette weitergibt und ggf. ausführt.

![ChainOfResponsibility01.png](09_Bericht_Mikroarchitektur_Bilder/ChainOfResponsibility01.png)

[https://de.wikipedia.org/wiki/Zuständigkeitskette#/media/Datei:Zustaendigkeitskette.svg](https://de.wikipedia.org/wiki/Zust%C3%A4ndigkeitskette#/media/Datei:Zustaendigkeitskette.svg)

Die Zuständigkeitskette reicht den Aufruf solange durch, bis eine Zusändige Klasse gefunden oder das Ende erreicht wird und führt diese dann aus. Das Ganze kann ganz einfach mittels abstrakter Klasse die ein Datenfeld seiner selbst beinhaltet(Rekusiv) und welches über den Konstruktor gesetzt werden kann. Zusätzlich wird dann noch eine Methode mit den zu überprüfenden Parametern, welche die Zuständikeit definiert, implementiert. Abschließend muss nur noch eine Kette erzeugt und an der Kette die jeweilige Methode aufgerufen werden.

Beispielsweise:

```java
public abstract class Bearbeiter{ //Kann keine Instanz gebildet werden
    public Bearbeiter naechsterBearbeiter; //Rekursion

    public Bearbeiter(Bearbeiter naechsterBearbeiter){
        this.naechsterBearbeiter = naechsterBearbeiter;
    }
    public void anfrageBearbeiten(String aufgabenart, List<Temperatur> temperaturListe){
        if (this.naechsterBearbeiter != null){ //
            naechsterBearbeiter.anfrageBearbeiten(aufgabenart, temperaturListe);
            //weitergabe an den nächsterBearbeiter, welche dem Konstruktor übergeben wurde
        }else{
            System.out.println("Keine Aufgabe gefunden!");
        }
    }
}
```

```java
public class BearbeiterEins extends Bearbeiter {

    public BearbeiterEins(Bearbeiter naechsterBearbeiter) {
        super(naechsterBearbeiter);
    }

    @Override
    public void anfrageBearbeiten(String aufgabenart, List<Temperatur> temperaturListe) {
        if (aufgabenart.equals("AufgabeEins")) { //
            System.out.println("Aufgabe eins erledigt!");
            //Hier kann die Methode implementiert werden
        } else {
            super.anfrageBearbeiten(aufgabenart, temperaturListe); //Aufruf der Methode in der Mutterklasse
        }
    }
}
```

```java
Bearbeiter bearbeiter = new BearbeiterEins(new BearbeiterZwei(null));
bearbeiter.anfrageBearbeiten("AufgabeEins", temperaturListe);
bearbeiter.anfrageBearbeiten("AufgabeZwei", temperaturListe);
bearbeiter.anfrageBearbeiten("AufgabeNichtsVorhanden", temperaturListe);
```

### Template Method

Das Template Method Pattern oder Schablonenmethode ist ein Verhaltensmuster, um Teilbereiche von Code variabel gestalten zu können.

![TemplateHook01.png](09_Bericht_Mikroarchitektur_Bilder/TemplateHook01.png)

Dieses Pattern stellt mittels abstrakter Klasse und abstrakten Methoden ein Schablone fest, welche in den Konkreten Klassen implementiert werden. Innerhalb der Schablonenmethode wird beispielsweise der Aublauf eines Algorithmus definiert, welcher in den Konkreten Klassen implementiert wird.

```java
public abstract class Schablone {

    public Temperatur schablonenmethode(Temperatur temperatur) {
        primitiveMethode1(temperatur);
        primitiveMethode2(temperatur);
        return temperatur;
    }

    public abstract void primitiveMethode1(Temperatur temperatur);

    public abstract void primitiveMethode2(Temperatur temperatur);

}
```

```java
public class KonkreteKlasseA extends Schablone {

    @Override
    public void primitiveMethode1(Temperatur temperatur) {
        temperatur.setIstWert(temperatur.istWert * (9.0/5.0) + 32);
    }

    @Override
    public void primitiveMethode2(Temperatur temperatur) {
        temperatur.setEinheit("°F");
    }
}
```

```java
public class KonkreteKlasseB extends Schablone{

    @Override
    public void primitiveMethode1(Temperatur temperatur) {
        temperatur.setIstWert(temperatur.istWert + 273.15);
    }

    @Override
    public void primitiveMethode2(Temperatur temperatur) {
        temperatur.setEinheit("°K");
    }
}
```

```java
Schablone schablone = new KonkreteKlasseA();
System.out.println(schablone.schablonenmethode(new Temperatur("WC", 19.0)));
```

### Observer

Wird auch Publish Subscribe Pattern genannt und zählt zu einem der wichtigsten Verhaltensmustern. Hierbei gibt es sogenannte Observer die sich bei einem Client anmelden (attach) und dan über den Client(Subject) eventgetriggert benachrichtigt werden (notify). Zudem können sie sich auch wieder abmelden (detach) und erhalten  durch dieses Muster eine loose Kopplung. 

![Observer01.png](09_Bericht_Mikroarchitektur_Bilder/Observer01.png)

```java
public class Subject {

    private List<IObserver> observers = new ArrayList<>();

    public void notifyObserver(String event){
        observers.forEach(observers -> observers.update(event));
    }

    public void attachObserver(IObserver observer){
        observers.add(observer);
    }

    public void detachObserver(IObserver observer){
        observers.remove(observer);
    }
}
```

```java
public interface IObserver {
    void update(String event);
}
```

```java
public class ConcreteObserverA implements IObserver{
    @Override
    public void update(String event) {
        System.out.println("Update Observer A mittels Event: " + event);
    }
}
```

```java
public class ConcreteObserverB implements IObserver{
    @Override
    public void update(String event) {
        System.out.println("Update Observer B mittels Event: " + event);
    }
}
```

```java
System.out.println("Observer Pattern");
        Subject subject = new Subject();
        subject.attachObserver(new ConcreteObserverA());
        subject.attachObserver(new ConcreteObserverB());
        subject.notifyObserver("Event_01");
```

## Erzeugungsmuster

### Builder

Das Buildern Pattern (Erzeugungsmuster) wird zum Erzeugen von Instanzen unter Verwenung einer statischen Klasse innerhalb der Instanz(Builder Klasse).

![Builder01.png](09_Bericht_Mikroarchitektur_Bilder/Builder01.png)

Die innere Klasse hält die selben Datenfelder wie die äußere und der Konstruktor übernimmt in den meisten Fällen diverse verpflichtende Felder. Zudem werden setter für die anderen Felder definiert die, das aktuelle Objekt zurückgeben. Über eine build()-Methode wird am Ende eine Instanz der äußeren Klasse gebildet und zurückgegeben. 

```java
public class Auto {

    private String marke;
    private String modell;
    private int anzahlTueren;

    private Auto(AutoBuilder builder) {
        this.marke = builder.marke;
        this.modell = builder.modell;
        this.anzahlTueren = builder.anzahlTueren;
    }

    @Override
    public String toString() {
        return "Auto{" +
                "marke='" + marke + '\'' +
                ", modell='" + modell + '\'' +
                ", anzahlTueren=" + anzahlTueren +
                '}';
    }

    public static class AutoBuilder {
        private String marke;
        private String modell;
        private int anzahlTueren;

        public AutoBuilder(String marke, String modell) {
            this.marke = marke;
            this.modell = modell;
        }

        public AutoBuilder anzahlTueren(int anzahlTueren) {
            this.anzahlTueren = anzahlTueren;
            return this;
        }

        public Auto build() {
            //ggf. prüfen ob alle Datenfelder gesetzt wurden
            return new Auto(this);
        }
    }
}
```

```java
Auto auto = new Auto.AutoBuilder("VW", "Polo").anzahlTueren(3).build();
System.out.println(auto);
```
### Singelton

Mithilfe des Singelton-Patterns kann man sicherstellen, dass nur eine Instanz des Objekt gebildet werden kann. Dies geschieht über einen privaten Konstruktor der nur über eine statische Methode aufgerufen und die Instanz an ein statisches Datenfeld zugewiesen wird. Ein angerer Weg wäre, dass die Instanz schon gebildet wurde und nur mehr zurückgegeben werden kann. 

```java
public class Singelton{
 private static Singelton singelton = new Singelton();

 public static Singelton getSingelton(){
  return singelton;
 }
 
 private Singelton(){
 }
}
```

## Strukturmuster

### Decorator

Der Decorator ist ein Strukturmuster und wird verwendet um bestehenden Code erweitern zu können, ohne diesen verändern zu müssen (open close).

![Decorater01.png](09_Bericht_Mikroarchitektur_Bilder/Decorater01.png)

Hierbei kann ein Interface definiert werden, das die jeweilige Aufgabe implementiert. Zusätzlich wird eine Abstrakte Klasse welche ebenfalls das Interface implementiert und ein Datenfeld vom Interface trägt benötigt. Diese zusätzliche Klasse wird als Decorator bezeichnet, von der dan bestimmte Erweiterungsklassen erben können. Hierbei sind alle Komponenten vom Typ des Interfaces und es kann ähnlich wie bei der Chain of Responsibility eine Befehlskette erzeugt werden. (Um mehrere Zusätze anzusprechen)

```java
public interface Komponente {
    public void operation (List<Temperatur> temperaturListe);
}
```

```java
public abstract class Dekorierer implements Komponente{

    public Komponente komponente;

    public Dekorierer(Komponente komponente) {
        this.komponente = komponente;
    }

}
```

```java
public class KonkreteKomponente implements Komponente{

    @Override
    public void operation(List<Temperatur> temperaturListe) {
        System.out.println("Konkrete Aufgabe eingesetzt!");
        //Hier kann die Methode implementiert werden
    }
}
```

```java
public class KonkreterDekoriererA extends Dekorierer{

    public KonkreterDekoriererA(Komponente komponente) {
        super(komponente);
    }

    @Override
    public void operation(List<Temperatur> temperaturListe) {
        System.out.println("KonkreterDekoriererA");
        //Hier kann die Methode implementiert werden
        komponente.operation(temperaturListe);
    }
}
```

```java
public class KonkreterDekoriererB extends Dekorierer{

    public KonkreterDekoriererB(Komponente komponente) {
        super(komponente);
    }

    @Override
    public void operation(List<Temperatur> temperaturListe) {
        komponente.operation(temperaturListe);
        System.out.println("KonkreterDekoriererB");
        //Hier kann die Methode implementiert werden
    }
}
```

```java
Dekorierer dekorierer = new KonkreterDekoriererA(new KonkreterDekoriererB(new KonkreteKomponente()));
dekorierer.operation(temperaturListe);
```

### Adapter

Wird auch Wrapper genannt und zählt zu den Strukturmustern. Es wird verwendet um zwei inkompatible Interfaces über einen Adapter kompatibel zu machen  (wie bei einem Reisestecker). Es gibt Objektadapter und Klassenadapter. 

Objektadapter (Quelle: [https://en.wikipedia.org/wiki/Adapter_pattern](https://en.wikipedia.org/wiki/Adapter_pattern)) | Klassenadapter (Quelle: [https://en.wikipedia.org/wiki/Adapter_pattern](https://en.wikipedia.org/wiki/Adapter_pattern))
 --- | ---
<img src="09_Bericht_Mikroarchitektur_Bilder/Adapter03.png" alt="Adapter03" width="400"/> | <img src="09_Bericht_Mikroarchitektur_Bilder/Adapter02.png" alt="Adapter02" width="400"/>

Das Adaptermuster wandelt die Schnittstelle einer Klasse in eine andere Schnittstelle um, die Clients erwarten. Adapter lässt Klassen zusammenarbeiten, die sonst aufgrund inkompatibler Schnittstellen nicht möglich wären.

![Adapter01.png](09_Bericht_Mikroarchitektur_Bilder/Adapter01.png)

In diesem Objektadapter-Beispiel kann ein Videoplayer in einen der speziell implementierten Player konvertiert werden. Der Adapter liefert ein Interface, das der Client aufrufen kann. 

```java
public interface IAdapterVideoPlayer {
    void play(String videoName);
}
```

```java
public class AdapterVideoPlayer implements IAdapterVideoPlayer {

    IAdaptierterVideoPlayer iAdaptierterVideoPlayer;

    public AdapterVideoPlayer(IAdaptierterVideoPlayer iAdaptierterVideoPlayer) {
        this.iAdaptierterVideoPlayer = iAdaptierterVideoPlayer;
    }

    @Override
    public void play(String videoName) {
        iAdaptierterVideoPlayer.abspielen(videoName);
    }
}
```

```java
public interface IAdaptierterVideoPlayer {
    void abspielen(String videoName);
}
```

```java
public class AdaptierterMp4Player implements IAdaptierterVideoPlayer {
    
    @Override
    public void abspielen(String videoName) {
        System.out.println("Mp4 Player spielt " + videoName);
    }
}
```

```java
public class AdaptierterVLCPlayer implements IAdaptierterVideoPlayer {
    
    @Override
    public void abspielen(String videoName) {
        System.out.println("VLC Player spielt " + videoName);
    }
}
```

```java
System.out.println("AdapterVideoPlayer Pattern");
        IAdaptierterVideoPlayer mp4Player = new AdaptierterMp4Player();
        IAdaptierterVideoPlayer vlcPlayer = new AdaptierterVLCPlayer();
        IAdapterVideoPlayer adapter = new AdapterVideoPlayer(mp4Player);
        adapter.play("Video_01.wav");
        adapter = new AdapterVideoPlayer(vlcPlayer);
        adapter.play("Video_02.mp4");
        System.out.println("");
```