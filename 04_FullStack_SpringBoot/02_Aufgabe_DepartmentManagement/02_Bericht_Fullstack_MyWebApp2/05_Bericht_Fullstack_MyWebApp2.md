# SPRING BOOT FULLSTACK DEPARTMENT-MANAGEMENT

Entwickeln einer REST-API mittels Spring-Boot Framework. Globales Exception-Handling und automatisierte Tests implementieren. Vorhandenes Wissen aus vorherigen Einheiten vertiefen.

- [SPRING BOOT FULLSTACK DEPARTMENT-MANAGEMENT](#spring-boot-fullstack-department-management)
  - [Dependency Injection](#dependency-injection)
  - [Einrichten des Projektes](#einrichten-des-projektes)
  - [Implementieren einer einfachen API](#implementieren-einer-einfachen-api)
  - [Spring Boot DevTools](#spring-boot-devtools)
  - [Architektur und Aufbau der Applikation (Ports und Adapters)](#architektur-und-aufbau-der-applikation-ports-und-adapters)
    - [Hinzufügen von H2 und JPA Dependencies](#hinzufügen-von-h2-und-jpa-dependencies)
- [Implementieren der Komponenten](#implementieren-der-komponenten)
    - [Entity Klasse Department](#entity-klasse-department)
    - [Controller Klasse DepartmentController](#controller-klasse-departmentcontroller)
    - [Service Klasse und Interface DepartmentService](#service-klasse-und-interface-departmentservice)
    - [Repository Interface Department Repository](#repository-interface-department-repository)
    - [Testen der REST-API mit insomnia](#testen-der-rest-api-mit-insomnia)
    - [Abfragen der Daten mitel GET-Request](#abfragen-der-daten-mitel-get-request)
  - [Suchfunktion nach einer bestimmten Department ID](#suchfunktion-nach-einer-bestimmten-department-id)
  - [Löschfunktion nach einer bestimmten Department ID](#löschfunktion-nach-einer-bestimmten-department-id)
  - [Updatefunktion nach einer bestimmten Department ID](#updatefunktion-nach-einer-bestimmten-department-id)
  - [Suchfunktion nach einem bestimmten Namen](#suchfunktion-nach-einem-bestimmten-namen)
- [Hibernate Valididation](#hibernate-valididation)
- [Logging-Protokoll](#logging-protokoll)
- [Project Lombok](#project-lombok)
- [Exception Handling](#exception-handling)
- [Changing H2 db to MySQL](#changing-h2-db-to-mysql)
- [Unit Tests](#unit-tests)
  - [Testing Service Layer](#testing-service-layer)
  - [Testing Repository Layer](#testing-repository-layer)
  - [Testing Controller Layer](#testing-controller-layer)
- [Adding Config in properties file](#adding-config-in-properties-file)
  - [application.yml und Profiles](#applicationyml-und-profiles)
- [Deploying](#deploying)
- [Actuator](#actuator)
- [Different JPA Annotations](#different-jpa-annotations)
- [Literaturverzeichnis](#literaturverzeichnis)

## Dependency Injection

Gehört zu den fundamentalen Eigenschaften von Spring Boot und löst das Problem fester Abhängigkeit bei Objekten(lose Kopplung) zur Laufzeit. Dies kann durch Verwenden von Schnittstellen und der `@Autowired` Annotation geschaffen werden um die Aufgabe der Instanzierung von Objekten dem Container zu übergeben. Mit `@Autowired` wird unter Springboot nach einer Bestimmten Implementierungen gesucht und wie oben beschrieben ein Objekt zur Laufzeit erzeugt.

## Einrichten des Projektes

- Konfiguration online generieren lassen: [https://start.spring.io](https://start.spring.io/)

<img src="05_Bericht_Fullstack_MyWebApp2_Bilder/Einrichten_01.png" alt="Einrichten_01.png" width="500"/>

- Generiertes Projekt in IntelliJ öffnen (es werden alle Dependencies runtergeladen)
<br><br>

## Implementieren einer einfachen API

- `@SpringbootApplication` → definiert die Hauptklasse einer Spring Boot-Applikation und beinhaltet die Auto-Konfiguration.
- Die Anwendung kann dadurch direkt über diese Klasse gestartet werden und startet somit den Webserver auf Port 8080.
- `@RestController`  → Diese Notation fügt die Klasse zum Spring Boot Container zu Laufzeit hinzu un definiert die Klasse als RestController. (neuere Version von Controller inkl. `@ResponseBody`)
- Um Default-Einstelleungen(Auto-Konfig-Einstellungen) ändern zu können, kann die application.propperties-Datei angepasst werden. Zum Beispiel den Port des Webservers zu Ändern : `server.port=8082` .
- Definieren eines einfacher GET-Requests innerhalb des angelegten Controllers:

```java
/**
     * Definiert den Einstiegspunktes des Webservers mit einer GET-Methode
     * @return gibt einen String zurück
     */
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String helloWorld() {
        return "Welcome to my WebApp!";
    }
```
<br><br>
## Spring Boot DevTools

- führt einen automatischen Restart der Applikation bei Änderungen durch.
- wird über die pom.xml-Datei hinzugefügt und Maven muss anschließend aktualisiert werden.

```xml
<dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-devtools</artifactId>
      <scope>runtime</scope>
      <optional>true</optional>
</dependency>
```

- Vor Version 2021.2.X konnte unter Registry → compiler.automake.allow.app.running → aktiviert werden.
- Seit Version 2021.2.X kann unter Preferences → Advanced Settings → folgender Punkt aktiviert werden:

| Automake while running                                                                        | Build project automatically                                                                   |
| --------------------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------------- |
| <img src="05_Bericht_Fullstack_MyWebApp2_Bilder/DevTools_01.png" alt="DevTools_01" width=""/> | <img src="05_Bericht_Fullstack_MyWebApp2_Bilder/DevTools_02.png" alt="DevTools_02" width=""/> |
| <img src="05_Bericht_Fullstack_MyWebApp2_Bilder/DevTools_03.png" alt="DevTools_03" width=""/> | <img src="05_Bericht_Fullstack_MyWebApp2_Bilder/DevTools_04.png" alt="DevTools_04" width=""/> |

- Nun sollte das Programm noch einmal neu gestartet werden und die Live-Änderungen anzeigen.
<br><br>

## Architektur und Aufbau der Applikation (Ports und Adapters)

- REST-API(Controller GET, POST, PUT, DELETE) →
- Service Layer(Bussinesslogik, Use-Cases) →
- Data Access/ Repository Layer →
- Database(MySQL)

→ Repo kommuniziert mit Daten (Domain) in der Datenbank (Entity)<br>
→ Service hält Repo und gibt Aktionen vor (Bussinieslogik)<br>
→ Controller hält Service und stellt Schnittstelle bereit (API/REST-API - MVC)

Beispiel:
<img src="05_Bericht_Fullstack_MyWebApp2_Bilder/Architektur01.png" alt="Architektur01" width=""/>

### Hinzufügen von H2 und JPA Dependencies
H2 ist eine in-memory Datenbank (Datenbanksystem das den Arbeitsspeicher des Computers nutzt) die oft zu Testzwecken verwendet wird.
- H2 → OpenSource Datenbank (schon integriert)
- JPA → wird als Zusatz für die Datenbank benötigt (Persistenzschicht)

```xml
<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-data-jpa</artifactId>
</dependency>
```

- Anpassen der application.propperties

```xml
spring.h2.console.enabled=true
spring.datasource.url=jdbc:h2:mem:dcbapp
spring.datasource.driver-class-name=org.h2.Driver
spring.datasource.username=sa
spring.datasource.password=password
spring.jpa.database-platform=org.hibernate.dialect.H2Dialect
```

- nun Kann die Applikation gestartet werden und mittels [`localhost:8082/h2-console`](http://localhost:8082/h2-console)  auf die H2-Konsole zugegriffen und die Datenbankverbindung getestet werden. (PW: password)
<br><br>

# Implementieren der Komponenten

- Aufteilung der Architektur in die jeweiligen packages → entity, service, controller, repository

### Entity Klasse Department

- im Package entity anlegen
- Damit eine Interaktion mit der Datenbank möglich ist, muss die Department-Klasse mit `@Entity` initialisiert werden.
- Zusätzlich benötigt jede Tabelle einen PK welcher in unserem Fall automatisch generiert wird.

```java
@Entity //Damit eine Interaktion mit der Datenbank möglich ist
public class Department {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
		private Long departmentId;
    private String departmentName;
    private String departmentAddress;
    private String departmentCode;

		//Konstruktor, Setter, Getter, ToString
```
### Controller Klasse DepartmentController

- im Package controller anlegen
- `@RestController` → damit die Klasse als REST-API definiert wird
- Jeder Controller ist außerdem ein @Component und daher innerhalb des SpringBoot Containers (Klasse steht immer zur Verfügung).
### Service Klasse und Interface DepartmentService

- Um eine Ports und Adapters Architektur zu erzeugen, wird ein Interface für den Service definiert.
- Dieses Interface wird con der Klasse DepartmentServiceImpl implementiert und die Klasse muss als `@Service` deklariert werden
### Repository Interface Department Repository

- Das interface wird definiert um auf bereits vorhandene Datenbank-Methoden zugreifen zu können.
- Dazu müssen wir das Interface von `JpaRepository<Department, Long>` erben lassen und das Interface als `@Repository` definieren. (Department ist das Objekt und Long der PK)
- Dieses JpaRepository implementiert das CrudRepository und stellt sämmtliche Methoden bereit.
- Zusätzlich können eigene custom Methoden-Signaturen in dem Interface implementiert werden, welche bei Einhaltung spezieller Namensdefinition von Spring Boot automatisch implementiert werden. (countById() → würde die Anzahl an Einträgen zurückgeben )
### Testen der REST-API mit insomnia

- Download-Link: [https://insomnia.rest/download](https://insomnia.rest/download)

<img src="05_Bericht_Fullstack_MyWebApp2_Bilder/RestRequest_01.png" alt="RestRequest_01" width=""/>
<br><br>

### Abfragen der Daten mitel GET-Request

- Methode im Controller als GET-Request, welche eine Methode vom DepartmentService aufruft und uns eine Liste vom Typ Department zurückliefert.
- Methodensignatur im DepartmentService Interface implementieren.
- Methode in der DepartmentServiceImpl Klasse implementieren und aus dem DepartmentRepository die findAll() Methode verwenden.
- Testen der Anfrage mittels Insomnia

<img src="05_Bericht_Fullstack_MyWebApp2_Bilder/RestRequest_02.png" alt="RestRequest_02" width=""/>
<br><br>

## Suchfunktion nach einer bestimmten Department ID

- GET-Rest Methode mit einem Variablen Parameter

```java
@GetMapping("/departments/{id}") //{id} hier kann eine Variable eingesetzt werden.
    public Department fetchDaepartmentById(@PathVariable("id") Long departmentId){
        return departmentService.fetchDepartmentById(departmentId).get();
    }
```

## Löschfunktion nach einer bestimmten Department ID

- Unter Verwendung einer DELETE-Mapping Methode

```java
@DeleteMapping("/departments/{id}")
    public String deleteDepartmentById(@PathVariable("id") Long departmentId){
        departmentService.deleteDepartmentById(departmentId);
        return "Department deleted Successfully!!";
    }
```
## Updatefunktion nach einer bestimmten Department ID

- Unter Verwendung einer PUT-Mapping Methode

```java
@PutMapping("/departments/{id}")
    public Department updateDepartemnt(@PathVariable("id") Long departmentId, @RequestBody Department department){
        return departmentService.updateDepartment(departmentId, department);
    }
```

- Zusätzlich müssen in die neueun Werte auf Gültigkeit geprüft werden.
## Suchfunktion nach einem bestimmten Namen

```java
@GetMapping("/departments/name/{name}")
    public Department fetchDepartmentByName(@PathVariable("name") String departmentName){
        return departmentService.fetchDepartmentByName(departmentName);
    }
```

- Die Metho gibt es per Default im JPA-Repo nicht und kann ganz einfach unter Einhaltung der Syntax atomatisch von Spring implementiert werden. `public Department findByDepartmentName(String departmentName);`
- Die Keywords findet man in der Doku: [https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#jpa.query-methods](https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#jpa.query-methods)
- Zusätzlich gibt es noch die Möglichkeit SQL-Querys zu verwenden `@Query`

```java
@Query("select u from User u where u.emailAddress = ?1")
  User findByEmailAddress(String emailAddress);
```
# Hibernate Valididation

- hinzufügen der Dependency

```xml
<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-validation</artifactId>
</dependency>
```

- Um eine Validierung zu erzeugen, muss in der Jeweiligen Methode ein `@Valid` platziert werden, um auf die Bedingungen(in der Entity-Klasse definiert) prüfen zu können.

```java
//Controller
@PostMapping("/departments")
    public Department saveDepartment(@Valid @RequestBody Department department) {
        return departmentService.saveDepartment(department);
    }

//Entity
@NotBlank(message = "Please Add Department Name!") //message wird im Fehlerfall geschrieben
/*  @Length(min = 1, max = 5)
    @Size(min = 0, max = 10)
    @Email
    @Positive
    @Negative
    @Future
    @PositiveOrZero
*/
private String departmentName;
```

# Logging-Protokoll

Ein Logger kan innerhalb einer Klasse als Final Variable initialisiert werden.

```java
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
private final Logger LOGGER = (Logger) LoggerFactory.getLogger(DepartmentController.class); //erstellt einen Logger für die Klasse
```

Um Messages/ Infos auf der Konsole ausgeben zu können,  kann folgender Code angewendet werden:

```java
@PostMapping("/departments")
public Department saveDepartment(@Valid @RequestBody Department department) { //konvertiert ein JSON Objekt in ein Department Objekt
    LOGGER.info("Inside saveDepartment of DepartmentController"); //Logger-Msg
    return departmentService.saveDepartment(department);
}
```

Ausgabe:

<img src="05_Bericht_Fullstack_MyWebApp2_Bilder/Logging01.png" alt="Logging01.png" width=""/>

# Project Lombok

**Project Lombok** ist eine beliebte Java-Bibliothek, um die Menge an Boilerplate-Code(Getter/Setter/Constructor) zu reduzieren, die ein Entwickler schreiben muss.

- Dependency und Plugin in der pom.xml anpassen

```xml
<dependency>
	<groupId>org.projectlombok</groupId>
  <artifactId>lombok</artifactId>
  <optional>true</optional>
</dependency>

<plugin>
	<groupId>org.springframework.boot</groupId>
	<artifactId>spring-boot-maven-plugin</artifactId>
	<configuration>
		<excludes>
			<exclude>
				<groupId>org.projectlombok</groupId>
				<artifactId>lombok</artifactId>
			</exclude>
		</excludes>
	</configuration>
</plugin>

```

- nun können die Getter/Setter/Constructors gelöscht werden und die @Data Annotation der Klasse angefügt werden. `@Data //beinhaltet Getter/Setter/Constructor`

# Exception Handling

- Exceptionhandling wie gewohnt anwenden
- Um den Inhalt von Fehlermeldungen lesbarer zu machen, müssen folgende Schritte gemacht werden
- Neu Klasse innerhalb des exception Package anlegen, welche unsere Responses von Exceptions handelt:

```java
@ControllerAdvice //beim werfen einer Exception wird dieser Controller angesprochen
@ResponseStatus
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    /**
     * Die Methode behandelt die DepartmentNotFoundException und gibt eine definierte Message in Form eines ResponseEntity zurück.
     * @param exception übergebene Exception
     * @param request Übergebener Webrequest
     * @return gibt ein Objekt vom Typ ResponseEntity<ErrorMessage> zurück
     */
    @ExceptionHandler(DepartmentNotFoundException.class)
    public ResponseEntity<ErrorMessage> departmentNotFoundException(DepartmentNotFoundException exception, WebRequest request){
        ErrorMessage message = new ErrorMessage(HttpStatus.NOT_FOUND, exception.getMessage());
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(message);
    }
}
```

Mit der `@ControllerAdvice` Annotation, wird beim Auftreten einer Exception innerhalb eines Controllers, die Exception von dieser Klasse behandelt. Die Controller-Klasse `RestResponseEntityExceptionHandler` erbt von `ResponseEntityExceptionHandler.` Der sogenannte **ExceptionHandler** hält dann für jede Exception eine Methode, in der diese Exception behandelt wird und ein Objekt vom Typ **ResponseEntity** zurückliefert . 

Zusätzlich wird eine neue Klasse als Domäne implementiert, welche unsere `ErrorMessage` definiert.

# Changing H2 db to MySQL

Durch unsere strukturierte Architektur, können wir sehr einfach die Datenbnk abändern.

- Anpassen der application.proberties

```xml
#MySQL Config
spring.jpa.hibernate.ddl-auto=update
spring.datasource.url=jdbc:mysql://10.37.129.3:3306/departmentapp
spring.datasource.username=root
spring.datasource.password=123
spring.datasource.driver-class-name=com.mysql.jdbc.Driver
spring.jpa.show-sql=true
```

- MySQL Driver als Dependency innerhalb der pom.xml hinzufügen

```xml
<dependency>
   <groupId>mysql</groupId>
   <artifactId>mysql-connector-java</artifactId>
   <scope>runtime</scope>
</dependency>
```

- Datenbank departmentapp anlegen und Applikation starten

# Unit Tests

Mithilfe unterschiedlicher Libaries, wie JUnit oder Mockito können Tests in jedem Layer oder mehreren Layern durchgeführt werden. 
Dazu Müssen wir in der pom.xml folgende dependency hinzufügen:

```xml
<dependency>
  <groupId>org.springframework.boot</groupId>
  <artifactId>spring-boot-starter-test</artifactId>
	<scope>test</scope>
</dependency>
```

Wird ein Controller getestet, so ist seine Funktionalität vom Servicelayer und dem Repositorylayer abhängig. **Mocking** bewirkt, dass unabhängig von anderen Layern, direkt das gewünschte Ergebnis an den Controller geliefert wird. Unter IntelliJ können Testklassen automatisch mit `Command + N` generiert werden, welche anschließend implementiert werden können.

## Testing Service Layer

- deklarieren der Testklassen mit `@SpringBootTest`
- Testmethoden mit `@Test` Annotieren
- Testnamen sollten Eindeutig sein und können gerne etwas länger ausfallen

```java
@Test //ausführbare Testmethode für Spring
    @DisplayName("Get Data based on Valid Department Name")
    @Disabled //wenn die ganze Klasse ausgeführt wird, wird diese Methode ausgelassen
    public void whenValidDepartmentName_thenDepartmentShouldFound() {
        String departmentName = "DepartmentTest";
        Department found = departmentService.fetchDepartmentByName(departmentName);
        assertEquals(departmentName, found.getDepartmentName());
   }
```

- Das Builder Pattern wird zum Mocking verwendet. `Builder Pattern Kann Objekte aus bestimmten Datenfeldern erzeugen.`
  
## Testing Repository Layer

- deklarieren mit `@DataJpaTest //Da es sich hier um ein Repository handelt`
- restliche Doku wird im Programm als Kommentar festgehalten.

## Testing Controller Layer

- deklarieren mit `@WebMvcTest(DepartmentController.class) //um Endpoints zu testen`
- restliche Doku wird im Programm als Kommentar festgehalten.

# Adding Config in properties file

Es können auch eigene Properties angelegt werden, wie zum Beispile in unserem Fall Texte: `welcome.message = Welcome to my WebApp!`

Diese können so abgerufen werden: 

```java
@Value("${welcome.message}")
private String welcomeMessage;
```

## application.yml und Profiles

Die `application.properties` Datei kann auch als `application.yaml` gespeichert werden, um die Datei übersichtlicher zu gestalten.

- [http://mageddo.com/tools/yaml-converter](http://mageddo.com/tools/yaml-converter) (online Konvertierer)

Zudem können verschiedene Spring Boot Profile anlegelegt und  je nach Bedarf angepasst werden können. Somit kann mittels Profilwechsels die Configuration gewechselt werden. (zb. für verschiedene Datenbanken)

```yaml
server:
  port: '8082'

spring:
  profiles:
    active: dev
---

hier folgen mehrere Profile
```
# Deploying

- Erzeugen eines JAR-Files mittels `mvn clean install`
- im target Verzeichnis liegt nun unsere ausführbare Datei
- Die Datei kann mit den unterschidliechen Profilen ausgeführt werden

`java -jar Spring.boot-tutorial-1.0.0.jar --spring.profiles.active=dev`

# Actuator

Der Actuator stellt über den URL Pfad `/ancunator`Endpoints bereit. Eigene Endpoints lassen sich mit der `@Endpoint` Annotation am besten innerhalb eines config Packages innerhalb einer neuen Klasse erstellen ( /actuator/features). Zudem können Enpoints auch in der application.yml excludet werden werden.

- Hinzufügen der Depnedency

```yaml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-actuator</artifactId>
</dependency>
```

# Literaturverzeichnis
* https://www.youtube.com/watch?v=c3gKseNAs9w