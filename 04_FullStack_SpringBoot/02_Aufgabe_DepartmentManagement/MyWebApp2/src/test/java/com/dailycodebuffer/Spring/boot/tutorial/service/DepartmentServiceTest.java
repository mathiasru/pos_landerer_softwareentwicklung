package com.dailycodebuffer.Spring.boot.tutorial.service;

import com.dailycodebuffer.Spring.boot.tutorial.entity.Department;
import com.dailycodebuffer.Spring.boot.tutorial.repository.DepartmentRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest //Spring mitteilen, dass es sich um eine Testklasse handelt
class DepartmentServiceTest {
    @Autowired
    private DepartmentService departmentService;

    @MockBean
    private DepartmentRepository departmentRepository;

    @BeforeEach
        //Diese Methode wird immer als erstes aufgerufen
    void setUp() {
        Department department = Department.builder()
                .departmentName("DepartmentTest")
                .departmentAddress("TestCity")
                .departmentCode("00T")
                .departmentId(1L)
                .build();

        //hier wird das oben erstellte Objekt zurückgegeben und nicht auf die Datenbank zugegriffen.
        Mockito.when(departmentRepository.findByDepartmentNameIgnoreCase("DepartmentTest")).thenReturn(department);
    }

    @Test //ausführbare Testmethode für Spring
    @DisplayName("Get Data based on Valid Department Name")
    @Disabled //wenn die ganze Klasse ausgeführt wird, wird diese Methode ausgelassen
    public void whenValidDepartmentName_thenDepartmentShouldFound() {
        String departmentName = "DepartmentTest";
        Department found = departmentService.fetchDepartmentByName(departmentName);
        assertEquals(departmentName, found.getDepartmentName());
    }
}