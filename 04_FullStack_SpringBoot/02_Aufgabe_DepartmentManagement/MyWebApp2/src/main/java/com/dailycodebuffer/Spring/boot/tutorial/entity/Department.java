package com.dailycodebuffer.Spring.boot.tutorial.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;

/**
 * Die Klasse repräsentiert unsere Datenbanktabelle Department.
 */
@Entity //Damit eine Interaktion mit der Datenbank möglich ist
@Data //beinhaltet Getter/Setter/Constructor
@NoArgsConstructor //Default Konstruktor
@AllArgsConstructor //Konstruktor mit allen Datenfeldern
@Builder //Builder Pattern Kann objekte aus bestimmten Datenfeldern erzeugen.
public class Department {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long departmentId;

    @NotBlank(message = "Please Add Department Name!") //message wird im Fehlerfall geschrieben
    /*@Length(min = 1, max = 5)
    @Size(min = 0, max = 10)
    @Email
    @Positive
    @Negative
    @Future
    @PositiveOrZero
    */
    private String departmentName;
    private String departmentAddress;
    private String departmentCode;
}
