package com.dailycodebuffer.Spring.boot.tutorial.service;

import com.dailycodebuffer.Spring.boot.tutorial.entity.Department;
import com.dailycodebuffer.Spring.boot.tutorial.exception.DepartmentNotFoundException;

import java.util.List;

/**
 * Hier werden die Methodensignaturen für die DepartmentServiceImpl Klasse implementiert, um eine loose
 * Kopplung zu erzeugen.
 */
public interface DepartmentService {
    /**
     * Methode Speichert das übergebene Daepartment in der Datenbank und gibt es zurück.
     *
     * @param department übergebenes Department, welches gespeichert wird.
     * @return git das gespeicherte Objekt vom Typ Department zurück.
     */
    public Department saveDepartment(Department department);

    /**
     * Die Methode gibt alle Department Einträge der Datenbank in Form einer Liste zurück.
     *
     * @return gibt eine Liste vom Typ Department zurück.
     */
    public List<Department> fetchDepartmentList();

    /**
     * Die Methode sucht anhand der übergebenen ID ein Department in der Datenbank und gibt dieses dann zurück.
     *
     * @param departmentId übergebene Id(pk) vom Typ Long.
     * @return gibt ein Objekt vom Typ Department zurück.
     */
    public Department fetchDepartmentById(Long departmentId) throws DepartmentNotFoundException;

    /**
     * Die Methode löscht einen Datenbankeintrag anhand der übergebenen Id.
     *
     * @param departmentId übergebene Id(pk) vom Typ Long.
     */
    public void deleteDepartmentById(Long departmentId);

    /**
     * Die Methode updated einen Eintrag anhand der übergebenen ID und dem RequestBody und gibt das Objekt zurück.
     *
     * @param departmentId übergebene Id(pk) vom Typ Long.
     * @param department   übergebenes Department für den Update Befehl.
     * @return gibt das neue Objekt vom Typ Department zurück.
     */
    public Department updateDepartment(Long departmentId, Department department);

    /**
     * Die Methode sucht anhand des übergebenen Namens ein Department in der Datenbank und gibt dieses dann zurück.
     *
     * @param departmentName übergebener Name vom Typ String.
     * @return gibt ein Objekt vom Typ Department zurück.
     */
    public Department fetchDepartmentByName(String departmentName);
}
