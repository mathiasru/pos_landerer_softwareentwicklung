package com.itkolleg.mywebapp.user;

import org.springframework.data.repository.CrudRepository;

/**
 * Wird benötigt, damit der Servicelayer mit dem UserRepository und der Datenbank interagieren kann.
 */
public interface UserRepository extends CrudRepository <User, Integer> { //Integer ist der (PK)
    public Long countById(Integer id);
}
