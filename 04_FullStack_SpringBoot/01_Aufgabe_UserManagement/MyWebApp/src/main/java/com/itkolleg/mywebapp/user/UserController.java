package com.itkolleg.mywebapp.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

/**
 * Controller managed den Austausch zwischen Model(Daten) und dem View(Frontend) und implementiert die API-Schnittstelle.
 */
@Controller
public class UserController {
    @Autowired private UserService service; //Referenziert auf den UserService für die Dateninteraktion

    @GetMapping("/users")
    public String showUserList(Model model) {
        List<User> listUsers = service.listAll(); //ermittelt alle User der Datenbank und speichert diese in eine List
        model.addAttribute("listUsers", listUsers); //übergibt dem Model die Liste aller Users
        return "users";
    }

    @GetMapping("/users/new")
    public String showNewForm(Model model) {
        model.addAttribute("user", new User()); //übergibt dem Model ein leeres User-Objekt
        model.addAttribute("pageTitle", "Add New User"); //übergibt dem Model einen String für den Titel der Seite
        return "user_form";
    }

    @PostMapping("/users/save")
    public String saveUser(User user, RedirectAttributes redirectAttributes){
        service.save(user); //wird über CrudRepository implementiert
        redirectAttributes.addFlashAttribute("message", "The user has been saved successfully."); //mitgeben einer Message für den Alert
        return "redirect:/users"; //zurück auf die Startseite
    }
    @GetMapping("/users/edit/{id}")
    public String showEditForm(@PathVariable("id") Integer id, Model model, RedirectAttributes redirectAttributes){
        try {
            User user = service.get(id);
            model.addAttribute("user", user); //übergibt dem Model eine User-Objekt
            model.addAttribute("pageTitle", "Edit User (ID: " + id + ")"); //übergibt dem Model einen String für den Titel der Seite
            return "user_form";
        } catch (UserNotFoundException e) {
            redirectAttributes.addFlashAttribute("message", "The user has been saved successfully.");
            return "redirect:/users"; //zurück auf die Startseite
        }
    }

    @GetMapping("/users/delete/{id}")
    public String showDeleteForm(@PathVariable("id") Integer id, Model model, RedirectAttributes redirectAttributes){
        try {
            service.delete(id);
            redirectAttributes.addFlashAttribute("message", "The user ID: " + id + " has been deleted successfully.");
        } catch (UserNotFoundException e) {
            redirectAttributes.addFlashAttribute("message", e.getMessage());
        }
        return "redirect:/users"; //zurück auf die Startseite
    }

}
