package com.itkolleg.mywebapp.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;
import java.util.Optional;

/**
 * Wird für die Interaktion mit dem Controller und UserRepository benötigt.
 */
@Service
public class UserService {
    @Autowired private UserRepository repo; //Referenz auf das Userrepository

    /**
     * Die Methode gibt alle Einträge von Usern in vorm einer List vom Typ User zurück.
     * @return eine List vom Typ User
     */
    public List<User> listAll(){
        return (List<User>)repo.findAll(); //gibt eine Iterable aller Einträge zurück, welches zu einer List gecastet wird.
    }

    /**
     * Ruft die save Methode des UserRepository Interfaces auf um den User in der Datenbank über das CrudRepository
     * speichern zu können.
     * @param user übergebener User vom Typ User.
     */
    public void save(User user){
        repo.save(user); //speichert den User in der Datenbank
    }

    /**
     * Die Methode liefert anhand des übergebenen Schlüssels vom Typ Integer einen User zurück.
     * @param id übergebene id (PK) vom Typ Integer
     * @return gitb einen User zurück
     */
    public User get(Integer id) throws UserNotFoundException {
        Optional<User> result = repo.findById(id);
        if (result.isPresent()){
            return result.get();
        }
        throw new UserNotFoundException("Could not find any users with ID: " + id);
    }

    /**
     * Methode löscht anhand des übergebenen Schlüssels vom Typ Integer einen Eintrag, falls vorhanden, ansonsten
     * wird eine Exception geworfen.
     * @param id übergebene id (PK) vom Typ Integer
     */
    public void delete(Integer id) throws UserNotFoundException {
        Long count = repo.countById(id);
        if (count == null || count == 0 ){
            throw new UserNotFoundException("Could not find any users with ID: " + id);
        }
        repo.deleteById(id);
    }

}
