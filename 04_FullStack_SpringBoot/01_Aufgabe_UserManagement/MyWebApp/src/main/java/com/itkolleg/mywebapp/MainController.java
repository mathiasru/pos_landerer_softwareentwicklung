package com.itkolleg.mywebapp;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MainController {

    @GetMapping("") //to handle the requests
    public String showHomePage() {
        System.out.println("MainController");
        return "index";
        //bei einer leeren GET-Anfrage localhost:8080
        //wird hier die index.html zurückgegeben
    }
}
