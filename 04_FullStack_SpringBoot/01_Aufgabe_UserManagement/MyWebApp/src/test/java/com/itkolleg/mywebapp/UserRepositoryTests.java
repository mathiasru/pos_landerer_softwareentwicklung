package com.itkolleg.mywebapp;

import com.itkolleg.mywebapp.user.User;
import com.itkolleg.mywebapp.user.UserRepository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;

import java.util.Optional;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE) //Einträge werden nicht überschrieben
//Wenn dieser Eintrag schon existiert, schlägt der Test fehl
@Rollback(value = false)
public class UserRepositoryTests {
    @Autowired private UserRepository repo;


    /***
     * Die Methode testet das Anlegen eines Users in der users Tabelle und prüft anschließend
     * ob der angelegte user nicht null und einen PK größer 0 hat.
     */
    @Test
    public void testAddNew(){
        User user = new User();
        user.setEmail("tobias.tilg@icloud.com");
        user.setPassword("toias_12345!");
        user.setFirstName("Tobias");
        user.setLastName("Tilg");

        User savedUser = repo.save(user); //Methode wird vom CrudRepository Interface bereitgestellt
        Assertions.assertThat(savedUser).isNotNull();
        Assertions.assertThat(savedUser.getId()).isGreaterThan(0);
    }

    /**
     * Die Methode gitbt alle Einträge der users Tabelle, falls vorhanden (liste größer 0) auf der Kommandozeile aus.
     */
    @Test
    public void testListAll(){
        Iterable<User> users = repo.findAll();
        Assertions.assertThat(users).hasSizeGreaterThan(0);

        for (User user :users) {
            System.out.println(user);
        }
    }

    /**
     * Die Methode Bearbeitet einen bestimmten Datensatz, gewählt über die Id(pk) und prüfft
     * die Änderung anschließend.
     */
    @Test
    public void testUpdate(){
        Integer userId = 1;
        Optional<User> optionalUser = repo.findById(userId);
        User user = optionalUser.get();
        user.setPassword("123");
        repo.save(user);

        User updatedUser = repo.findById(userId).get();
        Assertions.assertThat(updatedUser.getPassword()).isEqualTo("123");
    }

    /**
     * Die Methode gibt einen bestimmten Datensatz, falls vorhanden auf der Konsole aus.
     * Ermittelt wird dieser über die Id(pk). Und prüft anschließen auf isPresent().
     */
    @Test
    public void TestGet(){
        Integer userId = 2;
        Optional<User> optionalUser = repo.findById(userId);
        User user = optionalUser.get();

        Assertions.assertThat(optionalUser).isPresent();
        System.out.println(optionalUser.get());
    }

    /**
     * Die Methode löscht einen bestimmten Eintrag, gewählt über die Id(pk) und prüft anschließend
     * auf isNotPresent().
     */
    @Test
    public void testDelete() {
        Integer userId = 2;
        repo.deleteById(userId);

        Optional<User> optionalUser = repo.findById(userId);
        Assertions.assertThat(optionalUser).isNotPresent();
    }

}
