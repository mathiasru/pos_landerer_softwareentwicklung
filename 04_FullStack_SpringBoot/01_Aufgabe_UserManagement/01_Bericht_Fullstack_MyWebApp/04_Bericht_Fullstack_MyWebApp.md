# Fullstack MyWebApp Spring Boot

- [Fullstack MyWebApp Spring Boot](#fullstack-mywebapp-spring-boot)
- [Spring Boot CRUD](#spring-boot-crud)
  - [Verwendete Technologien](#verwendete-technologien)
- [Implementierung](#implementierung)
  - [Schritte der Implementierung](#schritte-der-implementierung)
  - [Anlegen und Konfigurieren eines Spring Boot Projektes](#anlegen-und-konfigurieren-eines-spring-boot-projektes)
    - [1. Anlegen des Projektes](#1-anlegen-des-projektes)
    - [2. Anlegen eines Datenbank Schemas](#2-anlegen-eines-datenbank-schemas)
    - [3. Konfiguration von Datasource Eigenschaften](#3-konfiguration-von-datasource-eigenschaften)
  - [Implementieren der Homepage](#implementieren-der-homepage)
    - [1. Implementieren der MainController Klasse](#1-implementieren-der-maincontroller-klasse)
    - [2. Implementieren der index.html](#2-implementieren-der-indexhtml)
    - [3. Testen der Applikation](#3-testen-der-applikation)
    - [4. Konfigurieren der IDE für die Verwendung von DEVTool](#4-konfigurieren-der-ide-für-die-verwendung-von-devtool)
    - [5. Verwenden von Bootstrap](#5-verwenden-von-bootstrap)
  - [Implementieren der User Klasse und dem User Interface](#implementieren-der-user-klasse-und-dem-user-interface)
  - [Implementieren der Unit Tests](#implementieren-der-unit-tests)
    - [Test Klasse und Methoden](#test-klasse-und-methoden)
  - [Ports und Adapters Architecture](#ports-und-adapters-architecture)
    - [MVC - Model View Controller](#mvc---model-view-controller)
  - [Code Users Listing Page](#code-users-listing-page)
  - [Implementieren der Add User Function](#implementieren-der-add-user-function)
    - [Validierung der Formualardaten](#validierung-der-formualardaten)
    - [Speichern eines Users](#speichern-eines-users)
    - [Alert nach Speichern eines Users](#alert-nach-speichern-eines-users)
  - [Implementieren der Edit und Update User Function](#implementieren-der-edit-und-update-user-function)
  - [Implementieren der Delete User Function](#implementieren-der-delete-user-function)
- [Literaturverzeichnis](#literaturverzeichnis)
</br>
</br>  

# Spring Boot CRUD

Estellen einer Java Web Application mit CRUD(Create, Retrieve, Update & Delete) operations, basierend auf dem Spring Framework.

## Verwendete Technologien

- IntelliJ IDEA - Entwicklungsumgebung
- Spring Boot Web - Weblayer
- Spring Data JPA & Hibernate - Data Access Layer
- MySQL - Database
- Thymeleaf - Template Engine
- HTML & Bootstrap - responsive Userinterface
- JUnit 5 & AssertJ - Unit Testing
</br>
</br>

# Implementierung

## Schritte der Implementierung

1. Anlegen und Konfigurieren eines Spring Boot Projektes
2. Implementieren der Homepage
3. Implementieren der User Klasse und dem User Interface
4. Implementieren der Unit Tests
5. Implementieren der User Listing Page
6. Implementieren der Add User Funktion
7. Implementieren der Update User Funktion
8. Implementieren der Delete User Funktion

## Anlegen und Konfigurieren eines Spring Boot Projektes

1. Deklarieren von Abhängigkeiten
    - Spring Web
    - Spring Data JPA
    - Thymeleaf
    - MySQL JDBC Driver
    - Spring Boot DevTools (für automatisches Laden von Änderungen)
    - Webjars für Bootstrap
2. Anlegen eines Datenbank Schemas
3. Konfiguration von Datasource in den application.properties

### 1. Anlegen des Projektes
Java-Version beachten | Dependencies wählen 
 --- | ---
<img src="04_Bericht_Fullstack_MyWebApp_Bilder/Anlegen_01.png" alt="drawing" width="400"/> | <img src="04_Bericht_Fullstack_MyWebApp_Bilder/Anlegen_02.png" alt="drawing" width="400"/>

Spring Boot verwendet Maven um die Dependencies zu laden (pom.mxl).

### 2. Anlegen eines Datenbank Schemas

Verbindung zu UbuntuServer (Docker mit MySQL und Adminer) mittels SSH einrichten und Datenbank(root, 123) verbinden.

Datenbank verbinden | SSH-Verbindung zu Server(Docker MySQL, Adminer) 
 --- | ---
<img src="04_Bericht_Fullstack_MyWebApp_Bilder/ConnectDatabase_01.png" alt="drawing" width="400"/> | <img src="04_Bericht_Fullstack_MyWebApp_Bilder/ConnectDatabase_02.png" alt="drawing" width="400"/>

Neues Datenbankschema anlegen → usersdb | utf8mb4_general_ci → und dieses als default festlegen

### 3. Konfiguration von Datasource Eigenschaften

Anpassen der application.properties

```yaml
spring.datasource.url=jdbc:mysql://10.37.129.3:3306/usersdb
spring.datasource.username=root
spring.datasource.password=123
spring.jpa.hibernate.ddl-auto=update 
spring.jpa.properties.hibernate.show_sql=true
```
</br>
</br>

## Implementieren der Homepage

1. Implementieren der MainController Klasse
2. Implementieren der index.html
3. Testen der Applikation
4. Konfigurieren der IDE für die Verwendung von DEVTool (Automatisches Laden bei Änderung)
5. Verwenden von Bootstrap

### 1. Implementieren der MainController Klasse

- MainController Klasse anlegen und als Controller mit `@Controller` definieren

```java
@GetMapping("") //to handle the requests
    public String showHomePage() {
        return "index"; 
				//bei einer leeren GET-Anfrage localhost:8080 
				//wird hier die index.html zurückgegeben
    }
```
### 2. Implementieren der index.html

index.html unter resources → templates anlegen und einen titel und h1 Überschrift anlegen

### 3. Testen der Applikation

- Programm über die MainMethode starten
- ERROR: Programm konnte nicht gestartet werden

![Bildschirmfoto 2021-10-09 um 8.43.18 PM.png](04_Bericht_Fullstack_MyWebApp_Bilder/ErrorMessage.png)

Fehler durch hinzufügen einer dependency in der pom.xml behoben

```yaml
<dependency>
  <groupId>mysql</groupId>
	<artifactId>mysql-connector-java</artifactId>
</dependency>
```
### 4. Konfigurieren der IDE für die Verwendung von DEVTool

(Automatisches Laden bei Änderung)

- Im Normalfall müsste man bei Änderung der index.html Datei das Programm immer neu starten, doch durch Verwendung von Spring Boot DevTools können Änderungen live sichtlich gemacht werden.
- Vor Version 2021.2.X konnte unter Registry → compiler.automake.allow.app.running → aktiviert werden.
- Seit Version 2021.2.X kann unter Preferences → Advanced Settings → folgender Punkt aktiviert werden:

Automake while running | Build project automatically
 --- | ---
<img src="04_Bericht_Fullstack_MyWebApp_Bilder/DevTools_01.png" alt="drawing" width=""/> | <img src="04_Bericht_Fullstack_MyWebApp_Bilder/DevTools_02.png" alt="drawing" width=""/>
<img src="04_Bericht_Fullstack_MyWebApp_Bilder/DevTools_03.png" alt="drawing" width=""/> | <img src="04_Bericht_Fullstack_MyWebApp_Bilder/DevTools_04.png" alt="drawing" width=""/>

- Nun sollte das Programm noch einmal neu gestartet werden und die Live-Änderungen anzeigen.
### 5. Verwenden von Bootstrap

- Hinzufügen fehlernder Dependencies
    
    ```xml
    <dependency>
    			<groupId>org.webjars</groupId>
    			<artifactId>bootstrap</artifactId>
    			<version>4.3.1</version>
    </dependency>
    <dependency>
    			<groupId>org.webjars</groupId>
    			<artifactId>webjars-locator-core</artifactId>
    </dependency>
    ```
    
- Hinzufügen fehlernder Metatags innerhalb der index.html (Pfad zur css)
    
    ```html
    <html lang="en" xmlns:th="http://www.thymeleaf.org">
    <link rel="stylesheet" type="text/css" th:href="@{/webjars/bootstrap/css/bootstrap.min.css}"/> 
    ```
    
- Reload Projekt, nun kann auf das Bootstrap-css zugegriffen werden
</br>
</br>

## Implementieren der User Klasse und dem User Interface

- neues Subpackage user mit einer Klasse User anlegen
- Die Klasse als Entity/ Table Klasse definieren um mithilfe dieser später eine Tabelle users mit bestimmten Attributen generieren zu lassen

```java
@Entity
@Table(name = "users")
public class User {
    @Id //Primary-Key
    @GeneratedValue(strategy = GenerationType.IDENTITY) //Autogenerierter Wert
    private Integer id;
    @Column(nullable = false, unique = true, length = 45) //verpflichtend, einzigartig, Länge einschränken
    private String email;
    @Column(length = 15, nullable = false)
    private String password;
    @Column(length = 45, nullable = false, name = "first_name") //default name
    private String firstName;
    @Column(length = 45, nullable = false, name = "last_name") //default name
    private String lastName;

		//Setter und Getter
		//...
}
```

- Setter und Getter generieren lassen
- Interface UserRepository anlegen welches von CrudRepository erbt und gibt den Typ und Schüsseltyp mit <User, Integer>.
- UserService → Wird für die Interaktion mit dem Controller und UserRepository benötigt.
- Nun kann das Programm neu gestartet werden und es wird die vorher definierte Tabelle innerhalb der MySQL-Datenbank angelegt.

![Bildschirmfoto 2021-10-10 um 7.56.10 PM.png](04_Bericht_Fullstack_MyWebApp_Bilder/DatabaseUsers_01.png)
</br>
</br>

## Implementieren der Unit Tests

Testen der **CRUD - Operationen** mittels **Spring Data JPA Test** anhand unserer Datenbank.

Konzept → TDD (Test Driven Development)

### Test Klasse und Methoden

- Vorhandene Test-Klasse löschen und eine neue UserRepositoryTests Klasse innerhalb des Verzeichnisses test anlegen und diverse Bibliotheken importieren (@DataJpaTest, @Rollback, @AutoConfigureTestDatabase).
- Datenfeld für die Interaktion mit der Datenbank anlegen `@Autowired private UserRepository repo;`
- Mittels Assertions-Anweisung werden gewisse Bedingungen zur Laufzeit sichergestellt(Wahrheitswert).
- Anlegen eines Users testen `testAddNew()`
- Testen des Test direkt über das Ausführen der Methode
- Ausgeben der Einträge innerhalb des users Tables `testListAll()`
- Update eines Eintrages testen `testUpdate()`
- Einen bestimmten Eintrag prüfen, ob dieser vorhanden ist `testGet()`
- Löschen eines Users testen `testDelete()`

## Ports und Adapters Architecture

Infrastruktur → Service → Domain (Interagieren immer über Interfaces (Ports und Adapters))
Zugriff von Außen über Ports → Controller → Servicelayer(Usecases, Businesslogik) → DomainKlasse(z.B. Student)
Zugriff von Innen über Adapter → Domain → Servicelayer → Repository(CrudRepository)

### MVC - Model View Controller

Dieser ermöglicht eine Trennung von Daten und deren graphischer Repräsentation.
- Model - hält die Arbeitsdaten (Benutzereingaben, Datenbank)
- View - Darstellung von Daten
- Controller - Vermittelt die Daten zwischen Model und View
## Code Users Listing Page

(Hauptseite aller Datenbankeinträge)

- Implementieren der UserService Klasse, welche uns mittels `@Autowired private UserRepository repo;` einen Zugriff auf die Datenbank ermöglicht. (wird mit @Service deklariert)
- UserService → Wird für die Interaktion mit dem Controller und UserRepository benötigt.
- Implementieren einer Methode, welch uns eine Liste aller User-Datenbankeinträge zurückgibt. `return (List<User>)repo.findAll();`
- Implementieren der UserController Klasse, welche unseren MVC darstellt und mittels `@Autowired private UserService service;` auf unsere vorher definierte Klasse und deren Methoden Zugriff gibt.
- Implementieren einer API-GETMapping Methode, welche uns einen Get-Request auf unsere Datenbank ermöglicht.
```java
@GetMapping("/users")
    public String showUserList(Model model) {
        List<User> listUsers = service.listAll(); //ermittelt alle User der Datenbank und speichert diese in eine List
        model.addAttribute("listUsers", listUsers); //übergibt dem Model die Liste aller Users
        return "users";
    }
```
- Anlegen und implementieren einer user.html, welche all unsere Einträge in Form einer Tabelle schön darstellt. Dies wird mittels folgendem Code umgesetzt:
- Wichtig!!! auf den HTML-Seiten immer darauf achten, dass Thymleaf und Bootstrap integriert sind.

```java
<th:block th:each="user : ${listUsers}">
                <tr>
                    <td>[[${user.id}]]</td>
                    <td>[[${user.email}]]</td>
                    <td>[[${user.firstName}]]</td>
                    <td>[[${user.lastName}]]</td>
                    <td>
                        <a class="h4" th:href="@{'/users/edit/' + ${user.id}}">Edit</a>
                        <a class="h4 mr-3" th:href="@{'/users/delete/' + ${user.id}}">Delete</a>
                    </td>
                </tr>
            </th:block>
```

## Implementieren der Add User Function

(Formular um einen neuen User Anzulegen)

- Erweitern der UserController Klasse `@GetMapping("/users/new")`  mit einer Methode in der dem Model ein neues, leeres User-Objekt mitgegeben wird und eine neue HTML Seite `user_form.html` aufgerufen/ zurückgegeben wird.
- Alegen einer user_form.html Seite und implementieren `<form th:action="@{/users/save}" method="post" style="max-width: 500px; margin: 0 auto;" th:object="${user}">`
- Label und input-Felder definieren und die input Felder mittels `<input type="email" th:field="*{email}" class="form-control"/>` definieren. (email ist ein Datenfeld aus der Klasse User)
- Save und Cancel buttons anlegen
- Vergessenes Datenfeld vom Typ `boolean eanabled`  und deren Setter/ Getter implementieren  und prüfen ob die Tabelle der Datenbank aktualisiert wurde.

### Validierung der Formualardaten

- mittels HTML-Attributes `required minlength="8" maxlength="45"`
- Implementieren von JavaScript innerhalb des bodys um mittels Cancel Button wieder auf die Hauptseite zu springen. `onlick="Methodenaufruf"`

```java
<script type="text/javascript" >
    function cancelForm(){
        window.location="[[@{/users}]]";
    }
</script>
```
### Speichern eines Users

- In der UserController Klasse eine PostMapping Methode implementieren, welche einen User übernimmt und diesen über den UserService speichert.
- Hierfür muss in der UserService Klasse noch eine save()-Methode programmiert werden, welche über das UserRepository-Interface ausgeführt wird und den User anschließend in der Datenbank speichert.

```java
@PostMapping("/users/save")
    public String saveUser(User user){
        service.save(user);
    }
```
### Alert nach Speichern eines Users

- in der user.html unterhalb der Überschrift folgenden Code hinzufügen

```java
<div th:if="${message}" class="alert alert-success text-center">
	[[${message}]]
</div>
```

- in der UserController Klasse die Methode , wecke für das Speichern verantwortlich anpassen

```java
@PostMapping("/users/save")
    public String saveUser(User user, RedirectAttributes redirectAttributes){
        service.save(user); //wird über CrudRepository implementiert
        redirectAttributes.addFlashAttribute("message", "The user has been saved successfully."); //mitgeben einer Message für den Alert
        return "redirect:/users"; //zurück auf die Startseite
    }
```

## Implementieren der Edit und Update User Function

Das Editieren erfolgt über die Mitgabe der ID

- Implementieren einer Methode in der UserService Klasse, welche uns anhand übergebener ID einen User zurückliefert.
- Diese wirft bei Fehlerhaften Suche eine Exception, welche eine Nachricht mitbekommt und implementiert werden muss. (UserNotFoundException)
- Implementieren einer GetMapping Methde, welche uns einen vorhandenen User Editieren lässt oder im Fehlerfall eine Exception wirft und auf die Startseite springt.

```java
@GetMapping("/users/edit/{id}")
    public String showEditForm(@PathVariable("id") Integer id, Model model, RedirectAttributes redirectAttributes){
        try {
            User user = service.get(id);
            model.addAttribute("user", user); //übergibt dem Model eine User-Objekt
            model.addAttribute("pageTitle", "Edit User (ID: " + id +")"); //übergibt dem Model einen String für den Titel der Seite
            return "user_form";
        } catch (UserNotFoundException e) {
            redirectAttributes.addFlashAttribute("message", "The user has been saved successfully.");
            return "redirect:/users"; //zurück auf die Startseite
        }
    }
```

- Da wir die user_form.html nun für zwei Szenarien verwenden geben wir ihr den pageTitle mit und müssen diesen innerhalb der HTML-Seite einbinden. `<title>[[${pageTitle}]]</title>`
- hinzufügen eines versteckten Feldes, welche die ID unter den Entwicklertools anzeigt (F12) <input type="hidden" th:field="*{id}"> 

## Implementieren der Delete User Function

- Implementieren einer Custom Methodensignatur im Interface UserRepository `public Long countById(Integer id);` Durch den Namen(count by id) der Methode weiß Spring welchen Zweck die Methode hat.
- Implementieren der delete()-Methode anhand der ID innerhalb der UserService Klasse
- Implementieren des GET-Requests im UserController

```java
@GetMapping("/users/delete/{id}")
    public String showDeleteForm(@PathVariable("id") Integer id, Model model, RedirectAttributes redirectAttributes){
        try {
            service.delete(id);
            redirectAttributes.addFlashAttribute("message", "The user ID: " + id + " has been deleted successfully.");
        } catch (UserNotFoundException e) {
            redirectAttributes.addFlashAttribute("message", e.getMessage());
        }
        return "redirect:/users"; //zurück auf die Startseite
    }
```


</br>
</br>
</br>
</br>

# Literaturverzeichnis

- [https://www.youtube.com/watch?v=u8a25mQcMOI](https://www.youtube.com/watch?v=u8a25mQcMOI)
