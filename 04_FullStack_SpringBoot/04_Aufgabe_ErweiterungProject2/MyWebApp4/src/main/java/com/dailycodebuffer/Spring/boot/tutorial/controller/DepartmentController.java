package com.dailycodebuffer.Spring.boot.tutorial.controller;

import com.dailycodebuffer.Spring.boot.tutorial.entity.Department;
import com.dailycodebuffer.Spring.boot.tutorial.exception.DepartmentNotFoundException;
import com.dailycodebuffer.Spring.boot.tutorial.service.department.DepartmentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


/**
 * Die Klasse stellt uns unsere REST-API Methoden zur Verfügung.
 */
@RestController
public class DepartmentController {
    @Autowired
    private DepartmentService departmentService; //Dependency Injektion wird hier angewandt um keine manuelles Objekt zu erzeugen
    private final Logger LOGGER = LoggerFactory.getLogger(DepartmentController.class); //erstellt einen Logger für die Klasse

    /**
     * Methode Speichert das übergebene Daepartment in der Datenbank und gibt es zurück.
     *
     * @param department übergebenes Department, welches gespeichert wird.
     * @return git das gespeicherte Objekt vom Typ Department zurück.
     */
    @PostMapping("/departments")
    public Department saveDepartment(@Valid @RequestBody Department department) { //konvertiert ein JSON Objekt in ein Department Objekt
        LOGGER.info("Inside saveDepartment of DepartmentController"); //Logger-Msg
        return departmentService.saveDepartment(department);
    }


    /**
     * Die Methode gibt alle Department Einträge der Datenbank in Form einer Liste zurück.
     *
     * @return gibt eine Liste vom Typ Department zurück.
     */
    @GetMapping("/departments")
    public List<Department> fetchDepartmentList() {
        LOGGER.info("Inside fetchDepartmentList of DepartmentController"); //Logger-Msg
        return departmentService.fetchDepartmentList();
    }

    /**
     * Die Methode sucht anhand der übergebenen ID ein Department in der Datenbank und gibt dieses dann zurück.
     *
     * @param departmentId übergebene Id(pk) vom Typ Long.
     * @return gibt ein Objekt vom Typ Department zurück.
     */
    @GetMapping("/departments/{id}") //{id} hier kann eine Variable eingesetzt werden.
    public Department fetchDaepartmentById(@PathVariable("id") Long departmentId) throws DepartmentNotFoundException {
        LOGGER.info("Inside fetchDaepartmentById of DepartmentController"); //Logger-Msg
        return departmentService.fetchDepartmentById(departmentId);
    }

    /**
     * Die Methode löscht einen Datenbankeintrag anhand der übergebenen Id.
     *
     * @param departmentId übergebene Id(pk) vom Typ Long.
     * @return gibt bei erfolgreichem Ausführen eine Message in Form eines Strings zurück.
     */
    @DeleteMapping("/departments/{id}") //verknüpft mit PathVariable, siehe nächste Zeile
    public String deleteDepartmentById(@PathVariable("id") Long departmentId) throws DepartmentNotFoundException {
        departmentService.deleteDepartmentById(departmentId);
        LOGGER.info("Inside deleteDepartmentById of DepartmentController"); //Logger-Msg
        return "Department deleted Successfully!!";
    }

    /**
     * Die Methode updated einen Eintrag anhand der übergebenen ID und dem RequestBody und gibt das Objekt zurück.
     *
     * @param departmentId übergebene Id(pk) vom Typ Long.
     * @param department   übergebenes Department für den Update Befehl.
     * @return gibt das neue Objekt vom Typ Department zurück.
     */
    @PutMapping("/departments/{id}")
    public Department updateDepartemnt(@PathVariable("id") Long departmentId, @RequestBody Department department) throws DepartmentNotFoundException {
        LOGGER.info("Inside updateDepartemnt of DepartmentController"); //Logger-Msg
        return departmentService.updateDepartment(departmentId, department);
    }

    /**
     * Die Methode sucht anhand des übergebenen Namens ein Department in der Datenbank und gibt dieses dann zurück.
     *
     * @param departmentName übergebener Name vom Typ String.
     * @return gibt ein Objekt vom Typ Department zurück.
     */
    @GetMapping("/departments/name/{name}")
    public Department fetchDepartmentByName(@PathVariable("name") String departmentName) {
        LOGGER.info("Inside fetchDepartmentByName of DepartmentController"); //Logger-Msg
        return departmentService.fetchDepartmentByName(departmentName);
    }
}
