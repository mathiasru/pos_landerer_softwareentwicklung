package com.dailycodebuffer.Spring.boot.tutorial.service.department;

import com.dailycodebuffer.Spring.boot.tutorial.entity.Department;
import com.dailycodebuffer.Spring.boot.tutorial.exception.DepartmentNotFoundException;

import java.util.List;

/**
 * Hier werden die Methodensignaturen für die DepartmentServiceImpl Klasse implementiert, um eine loose
 * Kopplung zu erzeugen.
 */
public interface DepartmentService {
    /**
     * Methode Speichert das übergebene Daepartment in der Datenbank und gibt es zurück.
     *
     * @param department übergebenes Department, welches gespeichert wird.
     * @return git das gespeicherte Objekt vom Typ Department zurück.
     */
    Department saveDepartment(Department department);

    /**
     * Methode Löscht das Daepartment anhand der Id(pk) aus der Datenbank und gibt es zurück.
     *
     * @param departmentId übergebene Id(pk) vom Typ Long.
     */
    void deleteDepartmentById(Long departmentId) throws DepartmentNotFoundException;

    /**
     * Die Methode gibt alle Department Einträge der Datenbank in Form einer Liste zurück.
     *
     * @return gibt eine Liste vom Typ Department zurück.
     */
    List<Department> fetchDepartmentList();

    /**
     * Die Methode sucht anhand der übergebenen ID ein Department in der Datenbank und gibt dieses dann zurück.
     *
     * @param departmentId übergebene Id(pk) vom Typ Long.
     * @return gibt ein Objekt vom Typ Department zurück.
     */
    Department fetchDepartmentById(Long departmentId) throws DepartmentNotFoundException;

    /**
     * Die Methode updated einen Eintrag anhand der übergebenen ID und dem RequestBody und gibt das Objekt zurück.
     *
     * @param departmentId übergebene Id(pk) vom Typ Long.
     * @param department   übergebenes Department für den Update Befehl.
     * @return gibt das neue Objekt vom Typ Department zurück.
     */
    Department updateDepartment(Long departmentId, Department department) throws DepartmentNotFoundException;

    /**
     * Die Methode sucht anhand des übergebenen Namens ein Department in der Datenbank und gibt dieses dann zurück.
     *
     * @param departmentName übergebener Name vom Typ String.
     * @return gibt ein Objekt vom Typ Department zurück.
     */
    Department fetchDepartmentByName(String departmentName);
}
