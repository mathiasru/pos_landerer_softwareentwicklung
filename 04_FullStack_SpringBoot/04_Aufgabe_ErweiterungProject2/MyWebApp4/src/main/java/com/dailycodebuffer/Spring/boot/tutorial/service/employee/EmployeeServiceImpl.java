package com.dailycodebuffer.Spring.boot.tutorial.service.employee;

import com.dailycodebuffer.Spring.boot.tutorial.entity.Department;
import com.dailycodebuffer.Spring.boot.tutorial.entity.Employee;
import com.dailycodebuffer.Spring.boot.tutorial.exception.DepartmentNotFoundException;
import com.dailycodebuffer.Spring.boot.tutorial.exception.EmployeeNotFoundException;
import com.dailycodebuffer.Spring.boot.tutorial.repository.DepartmentRepository;
import com.dailycodebuffer.Spring.boot.tutorial.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    private EmployeeRepository employeeRepository;
    @Autowired
    private DepartmentRepository departmentRepository;

    /**
     * Methode Speichert den übergebenen Employee in der Datenbank und gibt diesen als Objekt zurück.
     *
     * @param employeeDTO übergebeneer Employee, welcher gespeichert wird.
     * @return git das gespeicherte Objekt vom Typ Employee zurück.
     */
    @Override
    public Employee saveEmployee(EmployeeDTO employeeDTO) throws DepartmentNotFoundException {

        Optional<Department> department = departmentRepository.findById(employeeDTO.getDepartmentId());
        if (department.isEmpty()) {
            throw new DepartmentNotFoundException("Department NOT Available!");
        }

        Employee employee = Employee.builder()
                .firstName(employeeDTO.getFirstName())
                .lastName(employeeDTO.getLastName())
                .emailAddress(employeeDTO.getEmailAddress())
                .mobileNumber(employeeDTO.getMobileNumber())
                .department(department.get())
                .build();

        return this.employeeRepository.save(employee);
    }

    /**
     * Die Methode löscht einen Datenbankeintrag anhand der übergebenen Id.
     *
     * @param employeeId übergebene Id(pk) vom Typ Long.
     */
    @Override
    public void deleteEmployeeById(Long employeeId) throws EmployeeNotFoundException {
        Optional<Employee> employeeDb = employeeRepository.findById(employeeId);
        if (employeeDb.isEmpty()) {
            throw new EmployeeNotFoundException("Employee NOT Available!");
        }
        employeeRepository.deleteById(employeeId); //löscht den Datensatz aus der Datenbank
    }

    /**
     * Die Methode gibt alle Employee Einträge der Datenbank in Form einer Liste zurück.
     *
     * @return gibt eine Liste vom Typ EmployeeViewTDO zurück.
     */
    @Override
    public List<EmployeeViewTDO> fetchEmployeeList() {

        List<Employee> employeeList = employeeRepository.findAll();
        List<EmployeeViewTDO> employeeViewTDOList = new ArrayList<>();

        employeeList.forEach(employees -> {
            EmployeeViewTDO employeeViewTDO = EmployeeViewTDO.builder()
                    .employeeId(employees.getEmployeeId())
                    .firstName(employees.getFirstName())
                    .lastName(employees.getLastName())
                    .emailAddress(employees.getEmailAddress())
                    .mobileNumber(employees.getMobileNumber())
                    .departmentName(employees.getDepartment().getDepartmentName())
                    .build();
            employeeViewTDOList.add(employeeViewTDO);
        });

        return employeeViewTDOList;
    }

    /**
     * Die Methode sucht anhand der übergebenen ID einen Employee in der Datenbank und gibt diesen dann zurück.
     *
     * @param employeeId übergebene Id(pk) vom Typ Long.
     * @return gibt ein Objekt vom Typ Employee zurück.
     */
    @Override
    public EmployeeDTO fetchEmployeeById(Long employeeId) throws EmployeeNotFoundException {
        Optional<Employee> employeeDb = employeeRepository.findById(employeeId);
        if (employeeDb.isEmpty()) {
            throw new EmployeeNotFoundException("Employee NOT Available!");
        }

        EmployeeDTO employee = EmployeeDTO.builder()
                .employeeId(employeeDb.get().getEmployeeId())
                .firstName(employeeDb.get().getFirstName())
                .lastName(employeeDb.get().getLastName())
                .emailAddress(employeeDb.get().getEmailAddress())
                .mobileNumber(employeeDb.get().getMobileNumber())
                .departmentId(employeeDb.get().getDepartment().getDepartmentId())
                .build();

        return employee;
    }

    /**
     * Die Methode gibt alle Employee Einträge, welche einem bestimmten Department angehören der Datenbank in Form einer Liste zurück.
     *
     * @param departmentId übergebene Id(pk) vom Typ Long.
     * @return gibt eine Liste vom Typ Employee zurück.
     */
    @Override
    public List<Employee> fetchEmployeeListByDepartmentId(Long departmentId) {
        return employeeRepository.getAllByDepartment_DepartmentId(departmentId);
    }

    /**
     * Die Methode updated einen Eintrag anhand der übergebenen ID und dem RequestBody und gibt das Objekt zurück.
     *
     * @param employeeId  übergebene Id(pk) vom Typ Long.
     * @param employeeDTO übergebe eines EmployeeDTO für den Update Befehl.
     * @return gibt das neue Objekt vom Typ Employee zurück.
     */
    @Override
    public Employee updateEmployee(Long employeeId, EmployeeDTO employeeDTO) throws EmployeeNotFoundException, DepartmentNotFoundException {
        Optional<Employee> employee = employeeRepository.findById(employeeId);
        if (employee.isEmpty()) {
            throw new EmployeeNotFoundException("Employee NOT Available!");
        }

        Optional<Department> department = departmentRepository.findById(employeeDTO.getDepartmentId());
        if (department.isEmpty()) {
            throw new DepartmentNotFoundException("Department NOT Available!");
        }

        Employee employeeDb = employee.get();
        if (Objects.nonNull(employeeDTO.getFirstName()) && !"".equalsIgnoreCase(employeeDTO.getFirstName())) {
            employeeDb.setFirstName(employeeDTO.getFirstName());
        }
        if (Objects.nonNull(employeeDTO.getLastName()) && !"".equalsIgnoreCase(employeeDTO.getLastName())) {
            employeeDb.setLastName(employeeDTO.getLastName());
        }
        if (Objects.nonNull(employeeDTO.getEmailAddress()) && !"".equalsIgnoreCase(employeeDTO.getEmailAddress())) {
            employeeDb.setEmailAddress(employeeDTO.getEmailAddress());
        }
        if (Objects.nonNull(employeeDTO.getMobileNumber()) && !"".equalsIgnoreCase(employeeDTO.getMobileNumber())) {
            employeeDb.setMobileNumber(employeeDTO.getMobileNumber());
        }
        employeeDb.setDepartment(department.get());
        return employeeRepository.save(employeeDb);
    }

    /**
     * Die Methode sucht anhand der übergebenen Email-Adresse einen Employee in der Datenbank und gibt diesen dann zurück.
     *
     * @param employeeEmail übergebene Email-Adresse vom Typ String.
     * @return gibt ein Objekt vom Typ Employee zurück.
     */
    @Override
    public Employee fetchDepartmentByEmail(String employeeEmail) {
        return employeeRepository.findByEmailAddress(employeeEmail);
    }

}
