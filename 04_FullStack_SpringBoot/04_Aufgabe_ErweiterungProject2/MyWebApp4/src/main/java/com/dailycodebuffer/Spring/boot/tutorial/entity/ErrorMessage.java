package com.dailycodebuffer.Spring.boot.tutorial.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

/**
 * Die Klasse definiert unseren Inhalt der ErrorMassages.
 */
@Data //Lombok
@NoArgsConstructor
@AllArgsConstructor
public class ErrorMessage {

    private HttpStatus status; //404, 200, ...
    private String message;

}
