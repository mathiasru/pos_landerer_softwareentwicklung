package com.dailycodebuffer.Spring.boot.tutorial.controller;

import com.dailycodebuffer.Spring.boot.tutorial.entity.Employee;
import com.dailycodebuffer.Spring.boot.tutorial.exception.DepartmentNotFoundException;
import com.dailycodebuffer.Spring.boot.tutorial.exception.EmployeeNotFoundException;
import com.dailycodebuffer.Spring.boot.tutorial.service.employee.EmployeeDTO;
import com.dailycodebuffer.Spring.boot.tutorial.service.employee.EmployeeService;
import com.dailycodebuffer.Spring.boot.tutorial.service.employee.EmployeeViewTDO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController

public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;
    private final Logger LOGGER = LoggerFactory.getLogger(EmployeeController.class); //erstellt einen Logger für die Klasse

    /**
     * Methode Speichert den übergebenen Employee in der Datenbank und gibt diesen als Objekt zurück.
     *
     * @param employeeDTO übergebeneer Employee, welcher gespeichert wird.
     * @return git das gespeicherte Objekt vom Typ Employee zurück.
     */
    @PostMapping("/employee")
    public Employee saveEmployee(@RequestBody EmployeeDTO employeeDTO) throws DepartmentNotFoundException {
        LOGGER.info("Inside saveEmployee of EmployeeController"); //Logger-Msg
        return employeeService.saveEmployee(employeeDTO);
    }

    /**
     * Die Methode löscht einen Datenbankeintrag anhand der übergebenen Id.
     *
     * @param employeeId übergebene Id(pk) vom Typ Long.
     */
    @DeleteMapping("/employee/{id}")
    public String deleteEmployee(@PathVariable("id") Long employeeId) throws EmployeeNotFoundException {
        employeeService.deleteEmployeeById(employeeId); //wird über CrudRepository implementiert
        LOGGER.info("Inside deleteEmployeeById of EmployeeController"); //Logger-Msg
        return "Employee deleted Successfully!!";
    }

    /**
     * Die Methode gibt alle Employee Einträge der Datenbank in Form einer Liste zurück.
     *
     * @return gibt eine Liste vom Typ EmployeeViewTDO zurück.
     */
    @GetMapping("/employee")
    public List<EmployeeViewTDO> fetchEmployeeList() {
        LOGGER.info("Inside fetchEmployeeList of EmployeeController"); //Logger-Msg
        return employeeService.fetchEmployeeList();
    }

    /**
     * Die Methode sucht anhand der übergebenen ID eines Employee in der Datenbank und gibt diesen dann zurück.
     *
     * @param employeeId Id(pk) vom Typ Long.
     * @return gibt ein Objekt vom Typ Employee zurück.
     */
    @GetMapping("/employee/{id}")
    public EmployeeDTO fetchEmployeeById(@PathVariable("id") Long employeeId) throws EmployeeNotFoundException {
        LOGGER.info("Inside fetchEmployeeById of EmployeeController"); //Logger-Msg
        return employeeService.fetchEmployeeById(employeeId);
    }

    /**
     * Die Methode gibt alle Employee Einträge, welche einem bestimmten Department angehören der Datenbank in Form einer Liste zurück.
     *
     * @param departmentId übergebene Id(pk) vom Typ Long.
     * @return gibt eine Liste vom Typ Employee zurück.
     */
    @GetMapping("/employee/department/{id}")
    public List<Employee> fetchEmployeeListByDepartmentId(@PathVariable("id") Long departmentId) {
        LOGGER.info("Inside fetchEmployeeListByDepartmentId of EmployeeController"); //Logger-Msg
        return employeeService.fetchEmployeeListByDepartmentId(departmentId);
    }

    /**
     * Die Methode updated einen Eintrag anhand der übergebenen ID und dem RequestBody und gibt das Objekt zurück.
     *
     * @param employeeId  übergebene Id(pk) vom Typ Long.
     * @param employeeDTO übergebe eines Employee für den Update Befehl.
     * @return gibt das neue Objekt vom Typ Employee zurück.
     */
    @PutMapping("/employee/{id}")
    public Employee updateEmployee(@PathVariable("id") Long employeeId, @RequestBody EmployeeDTO employeeDTO) throws EmployeeNotFoundException, DepartmentNotFoundException {
        LOGGER.info("Inside updateEmployee of EmployeeController"); //Logger-Msg
        return employeeService.updateEmployee(employeeId, employeeDTO);
    }

    /**
     * Die Methode sucht anhand der übergebenen Email-Adresse einen Employee in der Datenbank und gibt diesen dann zurück.
     *
     * @param employeeEmail übergebene Email-Adresse vom Typ String.
     * @return gibt ein Objekt vom Typ Employee zurück.
     */
    @GetMapping("/employee/email/{email}")
    public Employee fetchDepartmentByEmail(@PathVariable("email") String employeeEmail) {
        LOGGER.info("Inside fetchDepartmentByName of DepartmentController"); //Logger-Msg
        return employeeService.fetchDepartmentByEmail(employeeEmail);
    }

}
