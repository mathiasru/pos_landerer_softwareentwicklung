package com.dailycodebuffer.Spring.boot.tutorial;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication //Definiert den Einstiegspunkt der Applikation
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

}
