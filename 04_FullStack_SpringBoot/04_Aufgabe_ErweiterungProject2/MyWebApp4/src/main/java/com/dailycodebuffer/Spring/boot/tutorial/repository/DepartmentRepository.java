package com.dailycodebuffer.Spring.boot.tutorial.repository;

import com.dailycodebuffer.Spring.boot.tutorial.entity.Department;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Das Interface lässt uns direkt auf unsere Datenbank über das JPARepository zugreifen.
 */
@Repository
public interface DepartmentRepository extends JpaRepository<Department, Long> {
    //Dieses JpaRepository implementiert das CrudRepository und stellt sämmtliche Methoden bereit.
    Department findByDepartmentName(String departmentName);
    Department findByDepartmentNameIgnoreCase(String departmentName);
}