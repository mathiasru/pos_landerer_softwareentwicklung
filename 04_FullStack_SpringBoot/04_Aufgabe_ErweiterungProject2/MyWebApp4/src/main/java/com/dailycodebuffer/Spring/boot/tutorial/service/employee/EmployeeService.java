package com.dailycodebuffer.Spring.boot.tutorial.service.employee;

import com.dailycodebuffer.Spring.boot.tutorial.entity.Employee;
import com.dailycodebuffer.Spring.boot.tutorial.exception.DepartmentNotFoundException;
import com.dailycodebuffer.Spring.boot.tutorial.exception.EmployeeNotFoundException;

import java.util.List;

public interface EmployeeService {

    /**
     * Methode Speichert den übergebenen Employee in der Datenbank und gibt diesen als Objekt zurück.
     *
     * @param employeeDTO übergebeneer Employee, welcher gespeichert wird.
     * @return git das gespeicherte Objekt vom Typ Employee zurück.
     */
    Employee saveEmployee(EmployeeDTO employeeDTO) throws DepartmentNotFoundException;

    /**
     * Die Methode löscht einen Datenbankeintrag anhand der übergebenen Id.
     *
     * @param employeeId übergebene Id(pk) vom Typ Long.
     */
    void deleteEmployeeById(Long employeeId) throws EmployeeNotFoundException;

    /**
     * Die Methode gibt alle Employee Einträge der Datenbank in Form einer Liste zurück.
     *
     * @return gibt eine Liste vom Typ EmployeeViewTDO zurück.
     */
    List<EmployeeViewTDO> fetchEmployeeList();

    /**
     * Die Methode sucht anhand der übergebenen ID einen Employee in der Datenbank und gibt diesen dann zurück.
     *
     * @param employeeId übergebene Id(pk) vom Typ Long.
     * @return gibt ein Objekt vom Typ Employee zurück.
     */
    EmployeeDTO fetchEmployeeById(Long employeeId) throws EmployeeNotFoundException;

    /**
     * Die Methode gibt alle Employee Einträge, welche einem bestimmten Department angehören der Datenbank in Form einer Liste zurück.
     * @param departmentId übergebene Id(pk) vom Typ Long.
     * @return gibt eine Liste vom Typ Employee zurück.
     */
    List<Employee> fetchEmployeeListByDepartmentId(Long departmentId);

    /**
     * Die Methode updated einen Eintrag anhand der übergebenen ID und dem RequestBody und gibt das Objekt zurück.
     * @param employeeId übergebene Id(pk) vom Typ Long.
     * @param employeeDTO übergebe eines EmployeeDTO für den Update Befehl.
     * @return gibt das neue Objekt vom Typ Employee zurück.
     */
    Employee updateEmployee(Long employeeId, EmployeeDTO employeeDTO) throws EmployeeNotFoundException, DepartmentNotFoundException;

    /**
     * Die Methode sucht anhand der übergebenen Email-Adresse einen Employee in der Datenbank und gibt diesen dann zurück.
     *
     * @param employeeEmail übergebene Email-Adresse vom Typ String.
     * @return gibt ein Objekt vom Typ Employee zurück.
     */
    Employee fetchDepartmentByEmail(String employeeEmail);
}
