package com.dailycodebuffer.Spring.boot.tutorial.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Über diese RestController Klasse wird unsere Rest-Api definiert.
 */
@Controller
//Diese Notation fügt die Klasse zum Spring Boot Container zu Laufzeit hinzu un definiert die Klasse als Controller.
public class HelloController {

    @Value("${welcome.message}")
    private String welcomeMessage;

    /**
     * Definiert den Einstiegspunktes des Webservers mit einer GET-Methode
     *
     * @return gibt einen String zurück
     */
    @GetMapping("/")
    public String showHomePage() {
        return "index.html";
    }
}
