package com.dailycodebuffer.Spring.boot.tutorial.exception;

import com.dailycodebuffer.Spring.boot.tutorial.entity.ErrorMessage;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * Diese Klasse hädelt unsere Exceptions und gibt definierte Response-Objects zurück.
 */
@ControllerAdvice //beim werfen einer Exception wird dieser Controller angesprochen
@ResponseStatus
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    /**
     * Die Methode behandelt die DepartmentNotFoundException und gibt eine definierte Message in Form eines ResponseEntity zurück.
     *
     * @param exception übergebene Exception
     * @return gibt ein Objekt vom Typ ResponseEntity<ErrorMessage> zurück
     */
    @ExceptionHandler(DepartmentNotFoundException.class)
    public ResponseEntity<ErrorMessage> departmentNotFoundException(DepartmentNotFoundException exception) {
        ErrorMessage message = new ErrorMessage(HttpStatus.NOT_FOUND, exception.getMessage());
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(message);
    }

    /**
     * Die Methode behandelt die EmployeeNotFoundException und gibt eine definierte Message in Form eines ResponseEntity zurück.
     *
     * @param exception übergebene Exception
     * @return gibt ein Objekt vom Typ ResponseEntity<ErrorMessage> zurück
     */
    @ExceptionHandler(EmployeeNotFoundException.class)
    public ResponseEntity<ErrorMessage> departmentNotFoundException(EmployeeNotFoundException exception) {
        ErrorMessage message = new ErrorMessage(HttpStatus.NOT_FOUND, exception.getMessage());
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(message);
    }
}
