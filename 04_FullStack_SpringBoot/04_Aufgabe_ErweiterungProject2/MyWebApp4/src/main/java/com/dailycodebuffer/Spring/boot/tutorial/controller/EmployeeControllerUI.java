package com.dailycodebuffer.Spring.boot.tutorial.controller;

import com.dailycodebuffer.Spring.boot.tutorial.exception.DepartmentNotFoundException;
import com.dailycodebuffer.Spring.boot.tutorial.exception.EmployeeNotFoundException;
import com.dailycodebuffer.Spring.boot.tutorial.service.employee.EmployeeDTO;
import com.dailycodebuffer.Spring.boot.tutorial.service.employee.EmployeeService;
import com.dailycodebuffer.Spring.boot.tutorial.service.employee.EmployeeViewTDO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

@Controller
public class EmployeeControllerUI {
    @Autowired
    private EmployeeService employeeService;

    @GetMapping("/employee/list")
    public String showEmployeeList(Model model) {
        List<EmployeeViewTDO> employeeList = employeeService.fetchEmployeeList();
        model.addAttribute("employeeList", employeeList); //übergibt dem Model die Liste aller Users
        return "employeelist.html";
    }

    @GetMapping("/employee/new")
    public String showNewForm(Model model) {
        model.addAttribute("employee", new EmployeeDTO()); //übergibt dem Model ein leeres User-Objekt
        model.addAttribute("pageTitle", "Add New Employee"); //übergibt dem Model einen String für den Titel der Seite
        return "employee_form_save.html";
    }

    @PostMapping("/employee/save")
    public String saveEmployee(EmployeeDTO employeeDTO, RedirectAttributes redirectAttributes) throws DepartmentNotFoundException {
        employeeService.saveEmployee(employeeDTO); //wird über CrudRepository implementiert
        redirectAttributes.addFlashAttribute("message", "The employee has been saved successfully."); //mitgeben einer Message für den Alert
        return "redirect:/employee/list"; //zurück auf die Startseite
    }

    @GetMapping("/employee/delete/{id}")
    public String deleteEmployee(@PathVariable("id") Long employeeId, RedirectAttributes redirectAttributes) throws EmployeeNotFoundException {
        employeeService.deleteEmployeeById(employeeId); //wird über CrudRepository implementiert
        redirectAttributes.addFlashAttribute("message", "The employee has been deleted successfully."); //mitgeben einer Message für den Alert
        return "redirect:/employee/list"; //zurück auf die Startseite
    }

    @PostMapping("/employee/update")
    public String updateEmployee(EmployeeDTO employeeDTO, RedirectAttributes redirectAttributes) throws EmployeeNotFoundException, DepartmentNotFoundException {
        employeeService.updateEmployee(employeeDTO.getEmployeeId(), employeeDTO); //wird über CrudRepository implementiert
        redirectAttributes.addFlashAttribute("message", "The department has been saved successfully."); //mitgeben einer Message für den Alert
        return "redirect:/employee/list"; //zurück auf die Startseite
    }

    @GetMapping("/employee/edit/{id}")
    public String showEditForm(@PathVariable("id") Long employeeId, Model model, RedirectAttributes redirectAttributes) {
        try {
            EmployeeDTO employee = employeeService.fetchEmployeeById(employeeId);
            model.addAttribute("employee", employee); //übergibt dem Model eine User-Objekt
            model.addAttribute("pageTitle", "Edit Employee (ID: " + employeeId + ")"); //übergibt dem Model einen String für den Titel der Seite
            return "employee_form_update.html";
        } catch (EmployeeNotFoundException employeeNotFoundException) {
            redirectAttributes.addFlashAttribute("message", "The employee has been saved successfully.");
            return "redirect:/employee/list"; //zurück auf die Startseite
        }
    }
}
