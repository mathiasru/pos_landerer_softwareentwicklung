package com.dailycodebuffer.Spring.boot.tutorial.service.department;

import com.dailycodebuffer.Spring.boot.tutorial.entity.Department;
import com.dailycodebuffer.Spring.boot.tutorial.exception.DepartmentNotFoundException;
import com.dailycodebuffer.Spring.boot.tutorial.repository.DepartmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service //auch eine Component
public class DepartmentServiceImpl implements DepartmentService {
    @Autowired //Dependency Injektion wird hier angewandt um keine manuelles Objekt zu erzeugen
    private DepartmentRepository departmentRepository;

    /**
     * Methode Speichert das übergebene Daepartment in der Datenbank und gibt es zurück.
     *
     * @param department übergebenes Department, welches gespeichert wird.
     * @return git das gespeicherte Objekt vom Typ Department zurück.
     */
    @Override
    public Department saveDepartment(Department department) {
        return departmentRepository.save(department); //speichert den Datensatz in der Datenbank
    }

    /**
     * Die Methode gibt alle Department Einträge der Datenbank in Form einer Liste zurück.
     *
     * @return gibt eine Liste vom Typ Department zurück.
     */
    @Override
    public List<Department> fetchDepartmentList() {
        return departmentRepository.findAll();
    }

    /**
     * Die Methode sucht anhand der übergebenen ID ein Department in der Datenbank und gibt dieses dann zurück.
     *
     * @param departmentId übergebene Id(pk) vom Typ Long.
     * @return gibt ein Objekt vom Typ Department zurück.
     */
    @Override
    public Department fetchDepartmentById(Long departmentId) throws DepartmentNotFoundException {
        Optional<Department> department = departmentRepository.findById(departmentId);
        if (department.isEmpty()) {
            throw new DepartmentNotFoundException("Department NOT Available!");
        }
        return department.get();
    }

    /**
     * Die Methode löscht einen Datenbankeintrag anhand der übergebenen Id.
     *
     * @param departmentId übergebene Id(pk) vom Typ Long.
     */
    @Override
    public void deleteDepartmentById(Long departmentId) throws DepartmentNotFoundException {
        Optional<Department> departmentDb = departmentRepository.findById(departmentId);
        if (departmentDb.isEmpty()) {
            throw new DepartmentNotFoundException("Department NOT Available!");
        }
        departmentRepository.deleteById(departmentId); //löscht den Datensatz aus der Datenbank
    }

    /**
     * Die Methode updated einen Eintrag anhand der übergebenen ID und dem RequestBody und gibt das Objekt zurück.
     *
     * @param departmentId übergebene Id(pk) vom Typ Long.
     * @param department   übergebenes Department für den Update Befehl.
     * @return gibt das neue Objekt vom Typ Department zurück.
     */
    @Override
    public Department updateDepartment(Long departmentId, Department department) throws DepartmentNotFoundException {
        Optional<Department> depDB = departmentRepository.findById(departmentId);
        if (depDB.isEmpty()) {
            throw new DepartmentNotFoundException("Department NOT Available!");
        }
        //prüfen ob das übergebene Department gültige Werte beinhaltet
        if (Objects.nonNull(department.getDepartmentName()) && !"".equalsIgnoreCase(department.getDepartmentName())) {
            depDB.get().setDepartmentName(department.getDepartmentName());
        }
        if (Objects.nonNull(department.getDepartmentCode()) && !"".equalsIgnoreCase(department.getDepartmentCode())) {
            depDB.get().setDepartmentCode(department.getDepartmentCode());
        }
        if (Objects.nonNull(department.getDepartmentAddress()) && !"".equalsIgnoreCase(department.getDepartmentAddress())) {
            depDB.get().setDepartmentAddress(department.getDepartmentAddress());
        }
        return departmentRepository.save(depDB.get());
    }

    /**
     * Die Methode sucht anhand des übergebenen Namens ein Department in der Datenbank und gibt dieses dann zurück.
     *
     * @param departmentName übergebener Name vom Typ String.
     * @return gibt ein Objekt vom Typ Department zurück.
     */
    @Override
    public Department fetchDepartmentByName(String departmentName) {
        return departmentRepository.findByDepartmentNameIgnoreCase(departmentName);
    }


}
