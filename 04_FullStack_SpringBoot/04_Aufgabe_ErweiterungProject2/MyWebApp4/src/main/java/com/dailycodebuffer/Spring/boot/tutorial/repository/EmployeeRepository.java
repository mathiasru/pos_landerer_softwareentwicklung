package com.dailycodebuffer.Spring.boot.tutorial.repository;

import com.dailycodebuffer.Spring.boot.tutorial.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {

    List<Employee> getAllByDepartment_DepartmentId(Long departmentId);
    Employee findByEmailAddress(String employeeEmail);
}
