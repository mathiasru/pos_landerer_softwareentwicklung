package com.dailycodebuffer.Spring.boot.tutorial.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

/**
 * Die Klasse repräsentiert unsere Datenbanktabelle Employee.
 */
@Entity //Damit eine Interaktion mit der Datenbank möglich ist
@Data //beinhaltet Getter/Setter/Constructor
@NoArgsConstructor //Default Konstruktor
@AllArgsConstructor //Konstruktor mit allen Datenfeldern
@Builder //Builder Pattern, kann Objekte aus bestimmten Datenfeldern erzeugen
@Table(name = "tbl_employee")
public class Employee {

    @Id
    @SequenceGenerator(name = "employee_sequence", sequenceName = "employee_sequence", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "employee_sequence")
    @Column(name = "employee_id")
    private Long employeeId;

    @Column(name = "firstname", nullable = false, length = 20)
    @NotBlank(message = "Please Add Employee Firstname!")
    private String firstName;

    @Column(name = "lastname", nullable = false, length = 20)
    @NotBlank(message = "Please Add Employee Lastname!")
    private String lastName;

    @Column(name = "email_address", nullable = false, length = 40, unique = true)
    @Email
    @NotBlank(message = "Please Add Employee Email-Address!")
    private String emailAddress;

    @Column(name = "mobile_number", length = 20)
    private String mobileNumber;

    @ManyToOne(
            optional = false
    )
    @JoinColumn(
            name = "department_id",
            referencedColumnName = "department_id"
    )
    private Department department;


}
