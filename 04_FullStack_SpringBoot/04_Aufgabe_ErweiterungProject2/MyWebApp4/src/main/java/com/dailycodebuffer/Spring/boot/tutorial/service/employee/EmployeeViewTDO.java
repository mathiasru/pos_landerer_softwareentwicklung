package com.dailycodebuffer.Spring.boot.tutorial.service.employee;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Die Klasse wird für die Interaktion zwischen Daten und Representation benötigt (MVC).
 */
@Data //beinhaltet Getter/Setter/Constructor
@NoArgsConstructor //Default Konstruktor
@AllArgsConstructor //Konstruktor mit allen Datenfeldern
@Builder //Builder Pattern Kann objekte aus bestimmten Datenfeldern erzeugen.
public class EmployeeViewTDO {
    private Long employeeId;
    private String firstName;
    private String lastName;
    private String emailAddress;
    private String mobileNumber;
    private String departmentName;

}
