package com.dailycodebuffer.Spring.boot.tutorial.controller;

import com.dailycodebuffer.Spring.boot.tutorial.entity.Department;
import com.dailycodebuffer.Spring.boot.tutorial.exception.DepartmentNotFoundException;
import com.dailycodebuffer.Spring.boot.tutorial.service.department.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

@Controller
public class DepartmentControllerUI {
    @Autowired
    private DepartmentService departmentService;

    @GetMapping("/department/list")
    public String showDepartmentList(Model model) {
        List<Department> departmentList = departmentService.fetchDepartmentList();
        model.addAttribute("departmentList", departmentList); //übergibt dem Model die Liste aller Users
        return "departmentlist.html";
    }

    @GetMapping("/department/new")
    public String showNewForm(Model model) {
        model.addAttribute("department", new Department()); //übergibt dem Model ein leeres User-Objekt
        model.addAttribute("pageTitle", "Add New Department"); //übergibt dem Model einen String für den Titel der Seite
        //model.addAttribute("formMethod", "save"); //übergibt dem Model eine User-Objekt
        return "department_form_save.html";
    }

    @PostMapping("/department/save")
    public String saveDepartment(Department department, RedirectAttributes redirectAttributes){
        departmentService.saveDepartment(department); //wird über CrudRepository implementiert
        redirectAttributes.addFlashAttribute("message", "The department has been saved successfully."); //mitgeben einer Message für den Alert
        return "redirect:/department/list"; //zurück auf die Startseite
    }

    @GetMapping("/department/delete/{id}")
    public String deleteDepartment(@PathVariable("id") Long departmentId, RedirectAttributes redirectAttributes) throws DepartmentNotFoundException {
        departmentService.deleteDepartmentById(departmentId); //wird über CrudRepository implementiert
        redirectAttributes.addFlashAttribute("message", "The department has been deleted successfully."); //mitgeben einer Message für den Alert
        return "redirect:/department/list"; //zurück auf die Startseite
    }

    @PostMapping("/department/update")
    public String updateDepartment(Department department, RedirectAttributes redirectAttributes) throws DepartmentNotFoundException {
        departmentService.updateDepartment(department.getDepartmentId(), department); //wird über CrudRepository implementiert
        redirectAttributes.addFlashAttribute("message", "The department has been saved successfully."); //mitgeben einer Message für den Alert

        return "redirect:/department/list"; //zurück auf die Startseite
    }

    @GetMapping("/department/edit/{id}")
    public String showEditForm(@PathVariable("id") Long departmentId, Model model, RedirectAttributes redirectAttributes){
        try {
            Department department = departmentService.fetchDepartmentById(departmentId);
            model.addAttribute("department", department); //übergibt dem Model eine User-Objekt
            model.addAttribute("pageTitle", "Edit Department (ID: " + departmentId + ")"); //übergibt dem Model einen String für den Titel der Seite
            //model.addAttribute("formMethod", "update"); //übergibt dem Model eine User-Objekt
            return "department_form_update.html";
        } catch (DepartmentNotFoundException departmentNotFoundException) {
            redirectAttributes.addFlashAttribute("message", "The department has been saved successfully.");
            return "redirect:/department/list"; //zurück auf die Startseite
        }
    }

}
