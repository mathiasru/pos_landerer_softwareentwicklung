package com.dailycodebuffer.Spring.boot.tutorial.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

/**
 * Die Klasse repräsentiert unsere Datenbanktabelle Department.
 */
@Entity //Damit eine Interaktion mit der Datenbank möglich ist
@Data //beinhaltet Getter/Setter/Constructor
@NoArgsConstructor //Default Konstruktor
@AllArgsConstructor //Konstruktor mit allen Datenfeldern
@Builder //Builder Pattern, kann Objekte aus bestimmten Datenfeldern erzeugen
@Table(name = "tbl_department")
public class Department {

    @Id
    @SequenceGenerator(name = "department_sequence", sequenceName = "department_sequence", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "department_sequence")
    @Column(name = "department_id")
    private Long departmentId;

    @Column(name = "department_name", nullable = false, length = 30)
    @NotBlank(message = "Please Add Department Name!")
    private String departmentName;

    @Column(name = "department_address", nullable = false, length = 30)
    @NotBlank(message = "Please Add Department Address!")
    private String departmentAddress;

    @Column(name = "department_code", nullable = false, length = 30)
    @NotBlank(message = "Please Add Department Address!")
    private String departmentCode;
}
