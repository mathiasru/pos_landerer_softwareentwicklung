package com.dailycodebuffer.Spring.boot.tutorial.controller;

import com.dailycodebuffer.Spring.boot.tutorial.entity.Department;
import com.dailycodebuffer.Spring.boot.tutorial.service.department.DepartmentService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(DepartmentController.class) //um Endpoints zu testen
class DepartmentControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private DepartmentService departmentService;

    private Department department; //damit wir von mehreren Methoden aus zugreifen können

    @BeforeEach
    void setUp() {
        this.department = Department.builder()
                .departmentName("Informatiik Engineering")
                .departmentAddress("Zams")
                .departmentCode("IE-010")
                .departmentId(1L)
                .build();
    }

    @Test
    void saveDepartment() throws Exception {
        Department inputDepartment = Department.builder()
                .departmentName("Informatiik Engineering")
                .departmentAddress("Zams")
                .departmentCode("IE-010")
                .build();
        //Mocking
        Mockito.when(departmentService.saveDepartment(inputDepartment)).thenReturn(department);

        //vergleicht den gespeicherten Wert mit folgendem POST-Request
        mockMvc.perform(post("/departments")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\n" +
                                "\t\"departmentName\": \"Informatiik Engineering\",\n" +
                                "\t\"departmentAddress\": \"Zams\",\n" +
                                "\t\"departmentCode\": \"IE-010\"\n" +
                                "}"))
                .andExpect(status().isOk());
    }

    @Test
    void fetchDaepartmentById() throws Exception {
        Mockito.when(departmentService.fetchDepartmentById(1L)).thenReturn(department);

        //vergleicht das gemockte Department mit dem GET-Request und vergleicht zudem noch den Namen
        mockMvc.perform(get("/departments/1")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.departmentName")
                        .value(department.getDepartmentName()));
    }
}