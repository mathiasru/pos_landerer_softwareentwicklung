package com.dailycodebuffer.Spring.boot.tutorial.controller;

import com.dailycodebuffer.Spring.boot.tutorial.entity.Department;
import com.dailycodebuffer.Spring.boot.tutorial.entity.Employee;
import com.dailycodebuffer.Spring.boot.tutorial.service.employee.EmployeeService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(EmployeeController.class) //um Endpoints zu testen
class EmployeeControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private EmployeeService employeeService;

    private Department department;
    private Employee employee;

    @BeforeEach
    void setUp() {
        this.department = Department.builder()
                .departmentCode("Test")
                .departmentName("Test")
                .departmentAddress("Test")
                .build();

        this.employee = Employee.builder()
                .firstName("MathiasTest1")
                .lastName("RudigTest1")
                .emailAddress("mathias1@icloud.test")
                .mobileNumber("12345678")
                .department(this.department)
                .build();
    }

    @Test
    void fetchEmployeeByEmail() throws Exception {
        Mockito.when(employeeService.fetchDepartmentByEmail("mathias1@icloud.test")).thenReturn(employee);

        //vergleicht das gemockte Department mit dem GET-Request und vergleicht zudem noch den Namen
        mockMvc.perform(get("/employee/1")
                        .contentType(MediaType.APPLICATION_JSON))
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("$.firstName")
                        .value(this.employee.getFirstName()));
    }

}