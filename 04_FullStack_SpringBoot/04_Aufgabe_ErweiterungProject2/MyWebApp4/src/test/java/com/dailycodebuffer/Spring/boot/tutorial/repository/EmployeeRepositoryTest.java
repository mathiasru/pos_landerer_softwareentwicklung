package com.dailycodebuffer.Spring.boot.tutorial.repository;

import com.dailycodebuffer.Spring.boot.tutorial.entity.Department;
import com.dailycodebuffer.Spring.boot.tutorial.entity.Employee;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DataJpaTest
class EmployeeRepositoryTest {
    @Autowired
    private EmployeeRepository employeeRepository;
    @Autowired
    private TestEntityManager entityManager;

    @BeforeEach
    void setUp() {
        Department department = Department.builder()
                .departmentCode("Test")
                .departmentName("Test")
                .departmentAddress("Test")
                .build();

        Employee employee = Employee.builder()
                .firstName("MathiasTest1")
                .lastName("RudigTest1")
                .emailAddress("mathias1@icloud.test")
                .mobileNumber("12345678")
                .department(department)
                .build();
        entityManager.persist(employee);
    }

    @Test
    public void whenFindById_thenReturnDepartment() {
        Employee employee = employeeRepository.findById(1L).get();
        assertEquals(employee.getFirstName(), "MathiasTest1");
    }

}