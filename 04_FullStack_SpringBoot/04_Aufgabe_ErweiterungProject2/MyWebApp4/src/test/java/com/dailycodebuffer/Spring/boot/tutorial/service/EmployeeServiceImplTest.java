package com.dailycodebuffer.Spring.boot.tutorial.service;

import com.dailycodebuffer.Spring.boot.tutorial.entity.Department;
import com.dailycodebuffer.Spring.boot.tutorial.entity.Employee;
import com.dailycodebuffer.Spring.boot.tutorial.repository.EmployeeRepository;
import com.dailycodebuffer.Spring.boot.tutorial.service.employee.EmployeeService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest //Spring mitteilen, dass es sich um eine Testklasse handelt
class EmployeeServiceImplTest {

    @Autowired
    private EmployeeService employeeService;

    @MockBean
    private EmployeeRepository employeeRepository;

    @BeforeEach
        //Diese Methode wird immer als erstes aufgerufen
    void setUp() {
        Department department = Department.builder()
                .departmentCode("Test")
                .departmentName("Test")
                .departmentAddress("Test")
                .build();

        Employee employee = Employee.builder()
                .firstName("MathiasTest1")
                .lastName("RudigTest1")
                .emailAddress("mathias1@icloud.test")
                .mobileNumber("12345678")
                .department(department)
                .build();

        //hier wird das oben erstellte Objekt zurückgegeben und nicht auf die Datenbank zugegriffen.
        Mockito.when(employeeRepository.findByEmailAddress("mathias1@icloud.test")).thenReturn(employee);
    }

    @Test //ausführbare Testmethode für Spring
    @DisplayName("Get Data based on Valid Employee Name")
    @Disabled //wenn die ganze Klasse ausgeführt wird, wird diese Methode ausgelassen
    public void whenValidEmployeeEmail_thenEmployeeShouldFound() {
        String employeeEmail = "mathias1@icloud.test";
        Employee found = employeeService.fetchDepartmentByEmail(employeeEmail);
        System.out.println(found.getFirstName());
        assertEquals(employeeEmail, found.getEmailAddress());
    }

}