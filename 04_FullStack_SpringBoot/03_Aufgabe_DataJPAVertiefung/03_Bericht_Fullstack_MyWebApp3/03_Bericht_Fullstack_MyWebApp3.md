# SPRING BOOT FULLSTACK Spring Data JPA

- [SPRING BOOT FULLSTACK Spring Data JPA](#spring-boot-fullstack-spring-data-jpa)
  - [Warum Spring Data JPA?](#warum-spring-data-jpa)
  - [ORM - *object relational mapping*](#orm---object-relational-mapping)
  - [Relationales Datenbankmodell](#relationales-datenbankmodell)
- [Erstellen des Projektes](#erstellen-des-projektes)
- [Different JPA Annotations](#different-jpa-annotations)
  - [@Embeddable and @Embedded](#embeddable-and-embedded)
- [Repositories and Methods](#repositories-and-methods)
  - [JPA @Query Annotation](#jpa-query-annotation)
  - [Native Query Examples](#native-query-examples)
  - [Query Named Params](#query-named-params)
  - [@Transactional & @Modifying Annotation](#transactional--modifying-annotation)
- [One to One Realtionship](#one-to-one-realtionship)
  - [Cascading (Cascading Types)](#cascading-cascading-types)
  - [Fetch Types](#fetch-types)
  - [Uni & Bi directional Relationship](#uni--bi-directional-relationship)
- [One-To-Many Relationship](#one-to-many-relationship)
- [Many-To-One Relationship](#many-to-one-relationship)
- [Paging and Sorting](#paging-and-sorting)
- [Many-To-Many Relationship](#many-to-many-relationship)
- [Literaturverzeichnis](#literaturverzeichnis)

Entwickeln Applikation mit mehreren Entitysmittels Spring-Boot Framework. Umsetzen der verschiedenen Relationships in einer Datenbank. Detailierten Einblick in Spring Data JPA.

## Warum Spring Data JPA?

Hibernate ist eine JPA-Implementierung, während Spring Data JPA eine JPA-Datenzugriffsabstraktion ist. Es wird verwendet um die Technologie im Hintergrund (Hibernate, ..) einfacher austauschbar zu machen. Hibernate wird per default in der `spring-boot-starter-data-jpa` verwendet und kann abgeändert werden.

## ORM - *object relational mapping*

**Objektrelationale Abbildung** ist eine Technik der Softwareentwicklung, mit der ein in einer objektorientierten Programmiersprache geschriebenes Anwendungsprogramm seine Objekte in einer relationalen Datenbank ablegen kann. Dem Programm erscheint die Datenbank dann als objektorientierte Datenbank, was die Programmierung erleichtert.

## Relationales Datenbankmodell

![RelationalesDatenbankmodell.png](03_Bericht_Fullstack_MyWebApp3_Bilder/RelationalesDatenbankmodell.png)

# Erstellen des Projektes

- [https://start.spring.io](https://start.spring.io/) (Generieren und in der IDE importieren(Ordner öffnen und vertrauen))

![startProject.png](03_Bericht_Fullstack_MyWebApp3_Bilder/startProject.png)

- Datenbank anlegen und Verbindung mittels application.proberties/.yml herstellen

```yaml
spring:
  datasource:
    driver-class-name: com.mysql.jdbc.Driver
    username: root
    url: jdbc:mysql://10.37.129.3:3306/schoolapp
    password: '123'
  jpa:
    hibernate:
      ddl-auto: update
    show-sql: 'true'
```

# Different JPA Annotations

Die Annotationen werden verwendet, um die Tabellen und Spalten einer Datenbank zu konfigurieren.

```java
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(
        name = "tbl_student", //definiert den Namen der Tabelle in der Datenbank
        uniqueConstraints = @UniqueConstraint( //definiert einen UniqueConstraint
                name = "emailid_unique",
                columnNames = "email_address" //dieser Tabelle
        )
)
public class Student {

    @Id
    @SequenceGenerator( //definiert einen Id Generator, der von der Annotation @GeneratedValue referenziert werden kann;
            // Der Generator verwendet eine Sequenz.
            name = "student_sequence", //Name des Generators, der in der Annotation @GeneratedValue verwendet werden kann
            sequenceName = "student_sequence", //optional, Name der Sequenz in der Datenbank
            allocationSize = 1) //Es wird nicht für jeden neuen Datensatz eine Datenbanksequenz aufgerufen.
    // Das verbessert die Performance. Der Nachteil ist, dass die vergebenen Ids bei einem Neustart der Anwendung Lücken haben.
    @GeneratedValue( // dient zum Konfigurieren der Inkrementierungsmethode für die angegebene Spalte
            strategy = GenerationType.SEQUENCE, //definiert den Typ Sequenz
            generator = "student_sequence") //definiert die oben angelegte Sequenz
    private Long studentId;
    private String firstName;
    private String lastName;

    @Column(
            name = "email_address", //definiert den Namen der Spalte in der Datenbank
            nullable = false //Feld darf nicht null sein
    )
    private String emailId;
    private String guardianName;
    private String guardianEmail;
    private String guardianMobile;

}
```
## @Embeddable and @Embedded

Die Annotationen lassen uns eingebettete Eigenschaften einer Entity, einer einzelnen Datenbanktabelle zuordnen. Zusätzlich können die Eigenschaften der Attribute überschrieben werden.

```java
@Embeddable //macht die Klasse "einbettbar"
@Data //Lombok
@NoArgsConstructor //Lombok
@AllArgsConstructor //Lombok
@Builder //Builder Pattern
@AttributeOverrides({
        @AttributeOverride( //Überschreiben der Attributeigenschaft
                name = "name",
                column = @Column(name = "guardian_name")
        )
})
public class Guardian {
    private String name;
    private String email;
    private String mobile;
}
```

In der Klasse in der die Embaddable-Klasse eingebettet werden soll, kann folgendes Datenfeld stehen:

```java
@Embedded
private Guardian guardian;
```

# Repositories and Methods

Ein definiertes Repository `@Repository` Interface dient als Interaktion der Datenbank und stellt uns verschiedenste default-Methoden bereit. Es gitb viele Verschiedene Repos von denen das Interface erben kann. Zudem git es auch die möglichkeit unter einhaltung der Syntax selbst Methodensignaturen zu definieren, die Spring im Hintergrund implementiert. Doku: [https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#jpa.query-methods](https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#jpa.query-methods) 

```java
@Repository //definiert ein Repository
public interface StudentRepository extends JpaRepository<Student, Long> {
    //custom Methode
    List<Student> findByFirstName(String firstName);
    List<Student> findByFirstNameContaining(String firstName);
    List<Student> findByLastNameNotNull();
    List<Student> findByGuardianName(String name);
    
}
```

## JPA @Query Annotation

Mit der `@Query` Annotation können wir beispielsweise einfache SELECT-Statements zum Filtern unserern Datensätze verwenden. Werden auf Klassennamen und Datenfelder angewendet.

```java
//JPQL-Query Annotation
    @Query("SELECT student FROM Student student where student.emailId = ?1") //?1 definiert den ersten Parameter //Studenten_Klasse
    Student getStudentByEmailAddress(String emailId);

    @Query("SELECT student.firstName FROM Student student where student.emailId = ?1")
    String getStudentFirstNameByEmailAddress(String emailId);
```

## Native Query Examples

Es können auch Native Querys verwendet werden, um richtige SQL-Statements auf die Datenbank bezogen abzusetzen.

```java
@Query (
        value = "SELECT * FROM tbl_student student WHERE student.email_address = ?1",
        nativeQuery = true
)
Student getStudentByEmailAddressNative(String emailId);
```

## Query Named Params

Sollte bei Verwendung mehrerer Parameter verwendet werden, um es lesbarer zu machen.

```java
//Native Named Parameter
@Query(
       value = "SELECT * FROM tbl_student student WHERE student.email_address = :emailid",
       nativeQuery = true
)
Student getStudentByEmailAddressNativeNamedParam(@Param("emailId") String emailId);
```

## @Transactional & @Modifying Annotation

Die `@Modifying` Annotation muss bei Änderung der Datenbankeinträge verwendet werden. (UPDATE, DELETE, INSERT)

Wir können `@Transactional` verwenden, um eine Methode in eine Datenbanktransaktion e*inzubeziehen.* Es ermöglicht uns propagation, isolation, timeout, read-only, and rollback Bedingungen für unsere Transaktion festzulegen. 

```java
@Modifying
@Transactional
@Query(
       value = "UPDATE tbl_student SET first_name = ?1 WHERE email_address = ?2",
       nativeQuery = true
)
int updateStudentNameByEmailId(String firstName, String emailId);
```
# One to One Realtionship

Implementieren einer One to One Relationship:

```java
//Klasse Course
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Course {
    @Id
    @SequenceGenerator(
            name = "course_sequence",
            sequenceName = "course_sequence";
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "course_sequence"
    )
    private Long courseId;
    private String courseTitle;
    private Integer credit;
}
```

```java
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CourseMaterial {
    @Id
    @SequenceGenerator(
            name = "course_material_sequence",
            sequenceName = "course_material_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "course_material_sequence"
    )
    private Long courseMaterialId;
    private String url;

    @OneToOne( //definiert eine 1:1 Relationship
            cascade = CascadeType.ALL //muss definiert werden, um Einträge aus Zusammengesetzen Tabellen zu ermöglichen
    )
    @JoinColumn( //PK der Tabelle mit der eine Relationship bestehen soll
            name = "course_id",
            referencedColumnName = "courseId"
    )
    private Course course;
}
```

- Nach Erstellen der Klassen müssen die jeweiligen Repositories angelegt und ggf. die Testmethoden implementiert werden.

## Cascading (Cascading Types)

Entitätsbeziehungen hängen oft von der Existenz einer anderen Entität ab, beispielsweise der Beziehung `Course – CourseMaterials` . Ohne den `Course` hat die Entität `CouseMaterials` keine eigene Bedeutung. Wenn wir die Entität `Course` löschen , sollte auch unsere Entität `CouseMaterials` gelöscht werden. Kaskadieren ist der Weg, dies zu erreichen. Wenn wir eine Aktion für die Zielentität ausführen, wird dieselbe Aktion auf die zugehörige Entität angewendet. (Quelle: [https://www.baeldung.com/jpa-cascade-types](https://www.baeldung.com/jpa-cascade-types))

Cascading Types:

- *ALL*
- *PERSIST*
- *MERGE*
- *REMOVE*
- *REFRESH*
- *DETACH*

## Fetch Types

Es gibt zwei verschiedene Fetch Types.

`FetchType.LAZY` - Die `FetchType.LAZY` holt nur die Elemente aus der Datenbank und ignoriert die in Beziehung stehenden.

`FetchType.EAGER` - Der `FetchType.EAGER` weist Hibernate an, alle Elemente einer Beziehung *abzurufen*, wenn die Root-Entität ausgewählt wird. (Default)

## Uni & Bi directional Relationship

```java
@OneToOne(
          mappedBy = "course" //definiert einer Bidirectionale Beziehung
)
private CourseMaterial courseMaterial;
```

# One-To-Many Relationship

```java
//in der Teacher Klasse
@OneToMany( //definiert eine 1:M Beziehung
           cascade = CascadeType.ALL //muss definiert werden, um Einträge aus Zusammengesetzen Tabellen zu ermöglichen
)
@JoinColumn( //PK der Tabelle mit der eine Relationship bestehen soll
            name = "teacher_id",
            referencedColumnName = "teacherId"
)
private List<Course> courses;
```

# Many-To-One Relationship

Das Ergebnis ist das gleiche wie bei einer 1:M Beziehung, nur ist diese Variante etwas leserlicher

```java
//in der Course Klasse
@ManyToOne( //definiert eine M:1 Beziehung
            cascade = CascadeType.ALL
)
@JoinColumn(
            name = "teacher_id",
            referencedColumnName = "teacherId"
)
private Teacher teacher;
```
# Paging and Sorting

Die Paginierung ist oft hilfreich, wenn wir ein großes Dataset haben und es dem Benutzer in kleineren Blöcken präsentieren möchten. 

(Doku: [https://www.baeldung.com/spring-data-jpa-pagination-sorting](https://www.baeldung.com/spring-data-jpa-pagination-sorting))

```java
@Test
public void findALlPagination() {
        Pageable firstPageWithThreeRecords = PageRequest.of(0, 3);
        Pageable secondPageWithTwoRecords = PageRequest.of(1, 2);

        List<Course> courseList = courseRepository.findAll(firstPageWithThreeRecords).getContent();
        long totalElements = courseRepository.findAll(firstPageWithThreeRecords).getTotalElements();
        long totalPages = courseRepository.findAll(firstPageWithThreeRecords).getTotalPages();

        System.out.println("total elements = " + totalElements);
        System.out.println("total pages = " + totalPages);
        System.out.println("course = " + courseList);
}

@Test
public void findAllSorting() {
        Pageable sortByTitle = PageRequest.of(0, 2, Sort.by("title"));
        Pageable sortByCredit = PageRequest.of(1, 2, Sort.by("credit").descending());
        Pageable sortByTitleAndCreditDesc = PageRequest.of(0, 2, Sort.by("title").descending().and(Sort.by("credit")));

        List<Course> courses = courseRepository.findAll(sortByTitle).getContent();

        System.out.println("courses = " + courses);
}
```

# Many-To-Many Relationship

Many-To-Many Beziehungen werden immer durch Intersection Entities aufgelöst.

```java
@ManyToMany //definiert eine M:M Beziehung
    @JoinTable( //Intersection Entity
            name = "student_course_mapping",
            joinColumns = @JoinColumn(
                    name = "course_id",
                    referencedColumnName = "courseId"
            ),
            inverseJoinColumns = @JoinColumn(
                    name = "student_id",
                    referencedColumnName = "studentId"
            )
    )
    private List<Student> studentList;

    public void addStudent(Student student) {
        if (studentList == null) studentList = new ArrayList<>();
        studentList.add(student);
    }
```

# Literaturverzeichnis

- [https://www.youtube.com/watch?v=XszpXoII9Sg&list=WL&index=1&t=32s](https://www.youtube.com/watch?v=XszpXoII9Sg&list=WL&index=1&t=32s)
- [https://de.wikipedia.org/wiki/Objektrelationale_Abbildung](https://de.wikipedia.org/wiki/Objektrelationale_Abbildung)