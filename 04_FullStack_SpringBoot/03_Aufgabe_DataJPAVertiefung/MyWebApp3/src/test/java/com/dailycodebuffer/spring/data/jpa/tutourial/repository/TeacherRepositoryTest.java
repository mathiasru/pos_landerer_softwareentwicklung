package com.dailycodebuffer.spring.data.jpa.tutourial.repository;

import com.dailycodebuffer.spring.data.jpa.tutourial.entity.Course;
import com.dailycodebuffer.spring.data.jpa.tutourial.entity.Teacher;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
@SpringBootTest
class TeacherRepositoryTest {
    @Autowired
    private TeacherRepository teacherRepository;

    @Test
    public void saveTeacher(){
        Course coursePOS = Course.builder()
                .courseTitle("POS")
                .credit(5)
                .build();

        Course courseDBI = Course.builder()
                .courseTitle("DBI")
                .credit(5)
                .build();


        Teacher teacher = Teacher.builder()
                .firstName("Claudio")
                .lastName("Landerer")
                //.courses(List.of(coursePOS, courseDBI))
                .build();

        teacherRepository.save(teacher);
    }

}