package com.dailycodebuffer.spring.data.jpa.tutourial.repository;

import com.dailycodebuffer.spring.data.jpa.tutourial.entity.Guardian;
import com.dailycodebuffer.spring.data.jpa.tutourial.entity.Student;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class StudentRepositoryTest {

    @Autowired
    private StudentRepository studentRepository;

    @Test
    public void saveStudent() {
        Student student = Student.builder() //Verwendung des Builder Pattern
                .emailId("mathias@icloud.com")
                .firstName("Mathias")
                .lastName("Rudig")
                /*.guardianName("Tobias")
                .guardianEmail("tobias@icloud.com")
                .guardianMobile("123456")*/
                .build();
        studentRepository.save(student); //Speichern des Studenten
    }

    @Test
    public void saveStudentWithGuardian() {

        Guardian guardian = Guardian.builder()
                .name("Tobias")
                .email("tobias@icloud.com")
                .mobile("123456")
                .build();

        Student student = Student.builder()
                .emailId("dario@icloud.com")
                .firstName("Dario")
                .lastName("Sko")
                .guardian(guardian)
                .build();
        studentRepository.save(student);
    }

    @Test
    public void printAllStudent() {
        List<Student> studentList = studentRepository.findAll();
        System.out.println("studentlist = " + studentList);
    }

    @Test
    public void printStudentByFirstName() {
        List<Student> studentList = studentRepository.findByFirstName("Mathias");
        System.out.println("studentlist = " + studentList);
    }

    @Test
    public void printStudentByFirstNameContaining() {
        List<Student> studentList = studentRepository.findByFirstNameContaining("Mat");
        System.out.println("studentlist = " + studentList);
    }

    @Test
    public void printStudentByNotNull() {
        List<Student> studentList = studentRepository.findByLastNameNotNull();
        System.out.println("studentlist = " + studentList);
    }

    @Test
    public void printStudentByGuardianName() {
        List<Student> studentList = studentRepository.findByGuardianName("Tobias");
        System.out.println("studentlist = " + studentList);
    }

    @Test
    public void printStudentByEmailId() {
        Student student = studentRepository.getStudentByEmailAddress("dario@icloud.com");
        System.out.println("Student = " + student);
    }

    @Test
    public void printStudenFirstNametByEmailId() {
        String studentFirstName = studentRepository.getStudentFirstNameByEmailAddress("dario@icloud.com");
        System.out.println("Student = " + studentFirstName);
    }

    @Test
    public void printStudentByEmailIdNative() {
        Student student = studentRepository.getStudentByEmailAddressNative("dario@icloud.com");
        System.out.println("Student = " + student);
    }

    @Test
    public void printStudentByEmailIdNativeNamedParam() {
        Student student = studentRepository.getStudentByEmailAddressNativeNamedParam("dario@icloud.com");
        System.out.println("Student = " + student);
    }

    @Test
    public void  updateStudentNameByEmailIdTest(){
        studentRepository.updateStudentNameByEmailId("DarioSko", "dario@icloud.com");
    }

}