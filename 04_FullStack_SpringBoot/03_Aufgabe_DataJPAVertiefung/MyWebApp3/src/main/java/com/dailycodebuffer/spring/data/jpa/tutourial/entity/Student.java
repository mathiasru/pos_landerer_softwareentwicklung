package com.dailycodebuffer.spring.data.jpa.tutourial.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity //definieren als Entity
@Data //Lombok
@NoArgsConstructor //Lombok
@AllArgsConstructor //Lombok
@Builder //defienieren des Builder Pattern
@Table(
        name = "tbl_student", //definiert den Namen der Tabelle in der Datenbank
        uniqueConstraints = @UniqueConstraint( //definiert einen UniqueConstraint
                name = "emailid_unique",
                columnNames = "email_address" //dieser Tabelle
        )
)
public class Student {

    @Id
    @SequenceGenerator( //definiert einen Id Generator, der von der Annotation @GeneratedValue referenziert werden kann;
            // Der Generator verwendet eine Sequenz.
            name = "student_sequence", //Name des Generators, der in der Annotation @GeneratedValue verwendet werden kann
            sequenceName = "student_sequence", //optional, Name der Sequenz in der Datenbank
            allocationSize = 1
    ) //Es wird nicht für jeden neuen Datensatz eine Datenbanksequenz aufgerufen.
    // Das verbessert die Performance. Der Nachteil ist, dass die vergebenen Ids bei einem Neustart der Anwendung Lücken haben.
    @GeneratedValue( // dient zum Konfigurieren der Inkrementierungsmethode für die angegebene Spalte
            strategy = GenerationType.SEQUENCE, //definiert den Typ Sequenz
            generator = "student_sequence" //definiert die oben angelegte Sequenz
    )
    private Long studentId;
    private String firstName;
    private String lastName;

    @Column(
            name = "email_address", //definiert den Namen der Spalte in der Datenbank
            nullable = false //Feld darf nicht null sein
    )
    private String emailId;

    @Embedded
    private Guardian guardian;
}
