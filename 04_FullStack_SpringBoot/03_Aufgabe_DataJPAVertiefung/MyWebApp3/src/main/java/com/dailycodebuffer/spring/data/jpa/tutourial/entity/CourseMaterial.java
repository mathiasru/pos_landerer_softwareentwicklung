package com.dailycodebuffer.spring.data.jpa.tutourial.entity;

import lombok.*;

import javax.persistence.*;

@Entity //definieren als Entity
@Data //Lombok
@NoArgsConstructor //Lombok
@AllArgsConstructor //Lombok
@Builder //defienieren des Builder Pattern
@ToString(exclude = "course") //excludet das Datenfeld
public class CourseMaterial {

    @Id
    @SequenceGenerator( //definiert einen Id Generator, der von der Annotation @GeneratedValue referenziert werden kann;
            // Der Generator verwendet eine Sequenz.
            name = "course_material_sequence", //Name des Generators, der in der Annotation @GeneratedValue verwendet werden kann
            sequenceName = "course_material_sequence", //optional, Name der Sequenz in der Datenbank
            allocationSize = 1 //Es wird nicht für jeden neuen Datensatz eine Datenbanksequenz aufgerufen.
    )
    // Das verbessert die Performance. Der Nachteil ist, dass die vergebenen Ids bei einem Neustart der Anwendung Lücken haben.
    @GeneratedValue( // dient zum Konfigurieren der Inkrementierungsmethode für die angegebene Spalte
            strategy = GenerationType.SEQUENCE, //definiert den Typ Sequenz
            generator = "course_material_sequence" //definiert die oben angelegte Sequenz
    )
    private Long courseMaterialId;
    private String url;

    @OneToOne( //definiert eine 1:1 Relationship
            cascade = CascadeType.ALL, //muss definiert werden, um Einträge aus Zusammengesetzen Tabellen zu ermöglichen
            fetch = FetchType.LAZY, //holt nur die Elemente aus der Datenbank und ignoriert die in Beziehung stehenden.
            optional = false //definiert eine Verpflichtende Beziehung
    )
    @JoinColumn( //PK der Tabelle mit der eine Relationship bestehen soll
            name = "course_id",
            referencedColumnName = "courseId"
    )
    private Course course;
}
