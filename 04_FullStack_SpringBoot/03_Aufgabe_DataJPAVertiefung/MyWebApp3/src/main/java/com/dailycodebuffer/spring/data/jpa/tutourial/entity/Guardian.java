package com.dailycodebuffer.spring.data.jpa.tutourial.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable //macht die Klasse "einbettbar"
@Data //Lombok
@NoArgsConstructor //Lombok
@AllArgsConstructor //Lombok
@Builder //Builder Pattern
@AttributeOverrides({
        @AttributeOverride( //Überschreiben der Attributeigenschaft
                name = "name",
                column = @Column(name = "guardian_name")
        ),
        @AttributeOverride( //Überschreiben der Attributeigenschaft
                name = "email",
                column = @Column(name = "guardian_email")
        ),
        @AttributeOverride( //Überschreiben der Attributeigenschaft
                name = "mobile",
                column = @Column(name = "guardian_mobile")
        )
})
public class Guardian {
    private String name;
    private String email;
    private String mobile;
}
