package com.dailycodebuffer.spring.data.jpa.tutourial.repository;

import com.dailycodebuffer.spring.data.jpa.tutourial.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository //definiert ein Repository
public interface StudentRepository extends JpaRepository<Student, Long> {
    //custom Methode https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#jpa.query-methods
    List<Student> findByFirstName(String firstName);

    List<Student> findByFirstNameContaining(String firstName);

    List<Student> findByLastNameNotNull();

    List<Student> findByGuardianName(String name);

    //JPQL-Query Annotation
    @Query("SELECT student FROM Student student where student.emailId = ?1")
    //?1 definiert den ersten Parameter //Studenten_Klasse
    Student getStudentByEmailAddress(String emailId);

    @Query("SELECT student.firstName FROM Student student where student.emailId = ?1")
    String getStudentFirstNameByEmailAddress(String emailId);

    //Native Query (Namen der Tabellen und Spalten nicht der Klasse)
    @Query(
            value = "SELECT * FROM tbl_student student WHERE student.email_address = ?1",
            nativeQuery = true
    )
    Student getStudentByEmailAddressNative(String emailId);

    //Native Named Parameter
    @Query(
            value = "SELECT * FROM tbl_student student WHERE student.email_address = :emailId",
            nativeQuery = true
    )
    Student getStudentByEmailAddressNativeNamedParam(@Param("emailId") String emailId);

    @Modifying
    @Transactional
    @Query(
            value = "UPDATE tbl_student SET first_name = ?1 WHERE email_address = ?2",
            nativeQuery = true
    )
    int updateStudentNameByEmailId(String firstName, String emailId);


}
