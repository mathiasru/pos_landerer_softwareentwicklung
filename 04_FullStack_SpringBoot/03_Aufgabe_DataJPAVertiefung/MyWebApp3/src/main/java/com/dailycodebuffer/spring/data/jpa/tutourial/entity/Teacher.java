package com.dailycodebuffer.spring.data.jpa.tutourial.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity //definieren als Entity
@Data //Lombok
@NoArgsConstructor //Lombok
@AllArgsConstructor //Lombok
@Builder //defienieren des Builder Pattern
public class Teacher {

    @Id
    @SequenceGenerator( //definiert einen Id Generator, der von der Annotation @GeneratedValue referenziert werden kann;
            // Der Generator verwendet eine Sequenz.
            name = "teacher_sequence", //Name des Generators, der in der Annotation @GeneratedValue verwendet werden kann
            sequenceName = "teacher_sequence", //optional, Name der Sequenz in der Datenbank
            allocationSize = 1
    ) //Es wird nicht für jeden neuen Datensatz eine Datenbanksequenz aufgerufen.
    // Das verbessert die Performance. Der Nachteil ist, dass die vergebenen Ids bei einem Neustart der Anwendung Lücken haben.
    @GeneratedValue( // dient zum Konfigurieren der Inkrementierungsmethode für die angegebene Spalte
            strategy = GenerationType.SEQUENCE, //definiert den Typ Sequenz
            generator = "teacher_sequence"
    ) //definiert die oben angelegte Sequenz
    private Long teacherId;
    private String firstName;
    private String lastName;

   /* @OneToMany( //definiert eine 1:M Beziehung
            cascade = CascadeType.ALL //muss definiert werden, um Einträge aus Zusammengesetzen Tabellen zu ermöglichen
    )
    @JoinColumn( //PK der Tabelle mit der eine Relationship bestehen soll
            name = "teacher_id",
            referencedColumnName = "teacherId"
    )
    private List<Course> courses;*/



}
