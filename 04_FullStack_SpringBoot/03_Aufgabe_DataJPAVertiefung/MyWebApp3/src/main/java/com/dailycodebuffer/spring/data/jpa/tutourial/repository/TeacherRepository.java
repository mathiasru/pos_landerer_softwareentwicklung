package com.dailycodebuffer.spring.data.jpa.tutourial.repository;

import com.dailycodebuffer.spring.data.jpa.tutourial.entity.Teacher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TeacherRepository extends JpaRepository<Teacher, Long> {
}
