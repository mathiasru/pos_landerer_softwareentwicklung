package com.dailycodebuffer.spring.data.jpa.tutourial.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity //definieren als Entity
@Data //Lombok
@NoArgsConstructor //Lombok
@AllArgsConstructor //Lombok
@Builder //defienieren des Builder Pattern
public class Course {
    @Id
    @SequenceGenerator( //definiert einen Id Generator, der von der Annotation @GeneratedValue referenziert werden kann;
            // Der Generator verwendet eine Sequenz.
            name = "course_sequence", //Name des Generators, der in der Annotation @GeneratedValue verwendet werden kann
            sequenceName = "course_sequence", //optional, Name der Sequenz in der Datenbank
            allocationSize = 1 //Es wird nicht für jeden neuen Datensatz eine Datenbanksequenz aufgerufen.
    )
    // Das verbessert die Performance. Der Nachteil ist, dass die vergebenen Ids bei einem Neustart der Anwendung Lücken haben.
    @GeneratedValue( // dient zum Konfigurieren der Inkrementierungsmethode für die angegebene Spalte
            strategy = GenerationType.SEQUENCE, //definiert den Typ Sequenz
            generator = "course_sequence" //definiert die oben angelegte Sequenz
    )
    private Long courseId;
    private String courseTitle;
    private Integer credit;

    @OneToOne(
            mappedBy = "course" //definiert einer Bidirectionale Beziehung
    )
    private CourseMaterial courseMaterial;

    @ManyToOne( //definiert eine M:1 Beziehung
            cascade = CascadeType.ALL
    )
    @JoinColumn(
            name = "teacher_id",
            referencedColumnName = "teacherId"
    )
    private Teacher teacher;

    @ManyToMany( //definiert eine M:M Beziehung
            cascade = CascadeType.ALL
    )
    @JoinTable( //Intersection Entity
            name = "student_course_mapping",
            joinColumns = @JoinColumn(
                    name = "course_id",
                    referencedColumnName = "courseId"
            ),
            inverseJoinColumns = @JoinColumn(
                    name = "student_id",
                    referencedColumnName = "studentId"
            )
    )
    private List<Student> studentList;

    public void addStudent(Student student) {
        if (studentList == null) studentList = new ArrayList<>();
        studentList.add(student);
    }

}
