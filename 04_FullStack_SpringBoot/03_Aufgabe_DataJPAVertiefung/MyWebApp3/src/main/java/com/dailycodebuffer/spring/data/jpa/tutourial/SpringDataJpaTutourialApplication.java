package com.dailycodebuffer.spring.data.jpa.tutourial;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringDataJpaTutourialApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringDataJpaTutourialApplication.class, args);
	}

}
