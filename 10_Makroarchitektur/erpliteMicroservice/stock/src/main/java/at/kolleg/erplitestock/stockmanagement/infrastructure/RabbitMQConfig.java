package at.kolleg.erplitestock.stockmanagement.infrastructure;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

@Configuration
public class RabbitMQConfig {

    //https://www.rabbitmq.com/tutorials/tutorial-one-spring-amqp.html
    //https://springhow.com/spring-boot-rabbitmq/

    @Bean
    public Queue orderPaymentCheck() {
        return new Queue("order_paymentcheck");
    }

    @Bean
    public Queue stockOrderPacked() {
        return new Queue("stock_orderpacked");
    }

    @Bean
    public StockIncomingRabbitMessageRelay receiver() {
        return new StockIncomingRabbitMessageRelay();
    }

    @Bean
    MessageConverter messageConverter(){
        return new Jackson2JsonMessageConverter();
    }

    @Bean
    TopicExchange exchange() {
        return new TopicExchange("erpliteexchange");
    }

    @Bean
    Binding binding1(TopicExchange exchange) {
        return BindingBuilder.bind(orderPaymentCheck()).to(exchange).with(orderPaymentCheck().getName());
    }

    @Bean
    Binding binding2(TopicExchange exchange) {
        return BindingBuilder.bind(stockOrderPacked()).to(exchange).with(stockOrderPacked().getName());
    }
}
