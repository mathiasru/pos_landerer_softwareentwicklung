package at.kolleg.erplite.ordermanagement.ports.in;


import at.kolleg.erplite.sharedkernel.events.OrderPackedEvent;

public interface OrderIncomingMessagesPort {
    void handle(OrderPackedEvent orderPackedEvent);
}
