package at.kolleg.erplite.ordermanagement.messaging.rabbitmq;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMQConfig {

    //https://www.rabbitmq.com/tutorials/tutorial-one-spring-amqp.html
    //https://springhow.com/spring-boot-rabbitmq/

    @Bean
    TopicExchange exchange() {
        return new TopicExchange("erpliteexchange");
    }

    @Bean
    public Queue orderPaymentCheck() {
        return new Queue("order_paymentcheck");
    }

    @Bean
    public Queue orderPlaced() {
        return new Queue("order_placed");
    }

    @Bean
    public Queue stockOrderPacked() {
        return new Queue("stock_orderpacked");
    }

    @Bean
    Binding orderPaymendCheckBinding(TopicExchange exchange) {
        return BindingBuilder.bind(orderPlaced()).to(exchange).with(orderPlaced().getName());
    }

    @Bean
    Binding orderPaymentCheckBinding(TopicExchange exchange) {
        return BindingBuilder.bind(orderPaymentCheck()).to(exchange).with(orderPaymentCheck().getName());
    }

    @Bean
    Binding stockOrderPacked(TopicExchange exchange) {
        return BindingBuilder.bind(stockOrderPacked()).to(exchange).with(stockOrderPacked().getName());
    }

    @Bean
    public OrderIncomingRabbitMessageRelay receiver() {
        return new OrderIncomingRabbitMessageRelay();
    }

    @Bean
    MessageConverter messageConverter() {
        return new Jackson2JsonMessageConverter();
    }
}
