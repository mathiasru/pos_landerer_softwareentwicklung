package at.kolleg.erplite.ordermanagement.messaging.rabbitmq;

import at.kolleg.erplite.ordermanagement.ports.in.OrderIncomingMessagesPort;
import at.kolleg.erplite.sharedkernel.events.OrderPackedEvent;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.logging.Level;
import java.util.logging.Logger;


public class OrderIncomingRabbitMessageRelay {

    @Autowired
    private OrderIncomingMessagesPort orderIncomingMessagesPort;

    /*@RabbitListener(queues = "stock")
    public void receive(OrderPackedEvent orderPackedEvent) {
        System.out.println(" [x] Received ORDER PACKED '" + orderPackedEvent + "'");
    }*/


    @RabbitListener(queues = "stock_orderpacked")
    public void receive(OrderPackedEvent orderPackedEvent) {
        Logger.getLogger(this.getClass().getName()).log(Level.INFO, "Order packed event received for order# " + orderPackedEvent.orderId() + "!");
        this.orderIncomingMessagesPort.handle(orderPackedEvent);
        Logger.getLogger(this.getClass().getName()).log(Level.INFO, "Order packed event handled for order# " + orderPackedEvent.orderId() + "!");

    }
}
