- [Makroarchitektur](#makroarchitektur)
  - [Ziele](#ziele)
  - [SOLID Prinzip](#solid-prinzip)
  - [Clean Architecture (Ports&Adapters, Onion Architecture)](#clean-architecture-portsadapters-onion-architecture)
- [Architekturanalyse ohne Makroarchitektur](#architekturanalyse-ohne-makroarchitektur)
  - [Gesamtsystem als Monolith (eine zusammenhängende Einheit)](#gesamtsystem-als-monolith-eine-zusammenhängende-einheit)
  - [Ordermanagament als Clean Architecture](#ordermanagament-als-clean-architecture)
    - [UML-Diagramm Ordermanagement](#uml-diagramm-ordermanagement)
    - [Aufbau Ordermanagement](#aufbau-ordermanagement)
    - [Benötigte REST-Endpunkte um Funktionalität testen zu können](#benötigte-rest-endpunkte-um-funktionalität-testen-zu-können)
  - [Stock-Service ohne Clean Architecture](#stock-service-ohne-clean-architecture)
    - [Aufbau Stockmanagement](#aufbau-stockmanagement)
    - [Benötigte REST-Endpunkte um Funktionalität testen zu können](#benötigte-rest-endpunkte-um-funktionalität-testen-zu-können-1)
- [Svelte Frontend](#svelte-frontend)
  - [Neues Projekt erstellen](#neues-projekt-erstellen)
  - [Frontend Kundenübersicht](#frontend-kundenübersicht)
  - [Frontend Abrechnungsübersicht](#frontend-abrechnungsübersicht)
  - [Lagerübersicht](#lagerübersicht)
- [**Architekturanalyse mit Makroarchitektur**](#architekturanalyse-mit-makroarchitektur)
  - [Gesamtsystem als Modulith](#gesamtsystem-als-modulith)
    - [API Gateway und Service Discovery](#api-gateway-und-service-discovery)
    - [RabbitMQ](#rabbitmq)
    - [Datenbank](#datenbank)
  - [Inbetriebnahme Applikation](#inbetriebnahme-applikation)
    - [Docker für Infrastruktur](#docker-für-infrastruktur)

# Makroarchitektur

## Ziele
- Conways Law - Organisationsstrukturen werden in der Software abgebildet
- Nicht Funktionale Anforderungen erfüllen (änderbar, erweiterbar, wartbar, Stabilität, lesbar )
- Modularität schaffen (Modolithen/ Microservices), Hierarchien definieren, Kommunikation durch Schnittstellen bereitstellen
- Je granularer die Verantwortung im System verteilt ist (hohe Kohäsion),  je geringer der Kopplungsgrad zwischen Klassen im System 
- Services mittels Strangler Pattern (würgen) bis sie klein genug sind und ein eigenes Microservice/ Modolithen darstellen.
- Dadurch können Services in Teams aufgeteilt werden (Conways Law)
- Mapping zwischen Domäne und Adapter (Adapter muss bei Änderung angepasst werden (single-point-of-change)

## SOLID Prinzip

- **Single Responsibility** (Es sollte nie mehr als einen Grund geben, eine Klasse zu ändern; **Eine Klasse sollte nur eine Aufgabe haben**)
- **Open Closed** (Module sollten sowohl offen für Erweiterungen, als auch verschlossen für Modifikationen sein **Abstraktion**)
- **Liskov Substitution** (Verhalten muss über die Vererbungshierarchie gleich bleiben)
- **Interface Aggregation** (Klassen sollten alle Methoden eines Interfaces implementieren können; Schnittstellen aufteilen)
- **Dependency Inversion** (Klassen und Details sollten von Abstraktionen(Interface) abhängen) **Umkehren von Abhängigkeiten** Inversion of Control


## Clean Architecture (Ports&Adapters, Onion Architecture)
* Port und Adapter - Businesslogik weiß von außen nichts (nur über Schnittstellen in beide Richtung)
* Umkehren von Abhängigkeiten - Dependency Inversion/Injection → mittels `@AllArgsConstructor` , `@Autowired` oder direkter Construktor-Injektion umgesetzt
* Trennen der Infrastruktur von der Domäne durch Adapter/ Schnittstellen.
* Man unterscheidet Schnittstellen in:
  * Adapter - implementieren den Ports.
  * Ports - sind nur eine Definition dessen, was sie nach außerhalb bereitstellen. (Abstrakt durch Interface)
    * INCOMING PORT (Adapter in Domäne implementiert)
    * OUTGOING PORT (Adapter in Technologie/ Infrastruktur implementiert)

Aufbau von innen nach außen (kommunikation lediglich über Schnittstellen)
- Domain
    - Entities, Events
- Service
    - Use-Cases
- Infrastructure
    - Controller
    - Repository

# Architekturanalyse ohne Makroarchitektur 

## Gesamtsystem als Monolith (eine zusammenhängende Einheit)
- Applikation basiert auf dem MVC-Pattern, welches die Controller per REST-API Enpunkte (JSON) erreichbar macht.
- Somit kann jegliche Frontendtechnologie eingesetzt werden, welche per HTTP-Anfrage JSON-Objekte verarbeiten kann (in unserem Fall SVELTE).
  - Wichtig dabei Cross-Side-Scripting aktivieren, falls von einem anderen Port oder IP-Adresse kommuniziert werden soll `@CrossOrigin(origins = "http://localhost:3000")` am REST-Controller annotieren (Socket ggf. anpassen).
- Zudem kann per swagger.ui (Dokumentation einer REST-API, inkl. möglichkeit Anfragen abzusetzen) und der konfiguriereten internen H2-Datenbank direkt nach Start des Spring-Boot-Containers interagiert werden. Aus der Doku können die geforderten JSON-Objekte der REST-Endpunkte entnommen werden.
- mittels Application-Runner werden Datenbankeinträge (Dummydaten) beim Hochfahren durchgeführt.

![Sequenzdiagramm Zugriff - Quelle Tobias Antensteiner ](./10_Bericht_Makroarchitektur_Bilder/Sequenzdiagramm.png)

## Ordermanagament als Clean Architecture
### UML-Diagramm Ordermanagement

```mermaid
classDiagram
class JpaRepository~OrderDbEntity, String~{
    <<interface>>
}
class OrderJpaRepository{
	<<interface>>
}
OrderJpaRepository --|> JpaRepository
OrderRepositoryImpl ..> OrderJpaRepository : Dependency Injection
OrderRepositoryImpl --> DbOrderMapperService : ORM
class OrderRepository~Order, OrderId~{
	<<interface OUT>>
}
class BaseRepository~T, I~{
	<<interface OUT>>
}
OrderRepositoryImpl ..|> OrderRepository
OrderRepository --|> BaseRepository

class OrderCommandService{
	<<interface IN>>
}
class OrderCommandServiceImpl{
	<<Service Layer>>
}

class OrderQueryService{
	<<interface IN>>
}
class OrderQueryServiceImpl{
	<<Service Layer>>
}

class OrderIncomingMessagePort{
	<<Service Layer>>
}
class OrderIncomingMessageAdapter{
	<<Service Layer>>
}
class IncomingOrderPackedSpringEventHandler{
	<<Incoming Events>>
}

OrderCommandServiceImpl ..> OrderRepository : Dependency Injection
OrderQueryServiceImpl ..> OrderRepository : Dependency Injection
OrderIncomingMessageAdapter ..> OrderRepository : Dependency Injection
OrderCommandServiceImpl ..|> OrderCommandService
OrderQueryServiceImpl ..|> OrderQueryService
OrderIncomingMessageAdapter ..|> OrderIncomingMessagePort
OrderIncomingMessagePort ..> IncomingOrderPackedSpringEventHandler : Dependency Injection

class OrderOutgoingMessageRelay{
	<<interface OUT>>
}
class OrderOutgoingSpringMessageRelayImpl{
	<<Outgoing Events>>
}
OrderOutgoingSpringMessageRelayImpl ..|> OrderOutgoingMessageRelay
OrderCommandServiceImpl ..> OrderOutgoingMessageRelay : Dependency Injection

class OrderRestController{
	<<Controller>>
}
OrderRestController ..> OrderCommandService : Dependency Injection
OrderRestController ..> OrderQueryService : Dependency Injection
```

### Aufbau Ordermanagement
- Layer sind über Ports (Clean Architecture) lose gekoppelt.
- Durch Entkoppeln mittels Abstraktion (Interfaces), können innerhalb des Order-Services Technologien einfach ausgetauscht werden.
- Klassen und Interfaces sind package-private, lediglich die Schnittstellen nach außen sind sichtbar.
- Exceptions werden über eigenen Zentralen Controller behandelt(Einheitlich), **ExceptionRestController** der alle Exception beinhaltet, welche behandelt werden sollten.

**Database**
- Mapping beim Schreiben und Lesen in die Datenbank (Order → OrderDbEntity).
- Daten werden immer zuerst aus DB geladen, dann abgeändert, damit mittels Businesslogik Datenkonsistenz gewährleistet wird (über den gesamten Prozess Order-Instanz).
- Order Repository stellt Adapter-Pattern bereit.

**Events**
- Commands (Befehle) und Querys (Anfragen) sind über eigene Klassen und Interfaces implementiert.
- Events werden über handler mittels überschriebener Methoden gehandelt. (eine Art Strategy Pattern)
- Events werden über IN-Ports entgegen genommen (internen Spring-Boot ApplicationEventPublisher) und asynchron über  OUT-Ports (internen Spring-Boot ApplicationListerner) ausgeführt.
- Services im Monolithen (customermanagement, ordermanagement, stockmanagement) sind lediglich über Events und deren Payload gekoppelt.
- Shared Kernel enthält Daten für die Kommunikation (Queries, Commands, Responses, Events).

**Domain**
- Business-Constraints werden zudem in den Aggrigaten und den ValueTypes implementiert.
- ValueTypes, Immutable Objects (Records) im DDD gibt keine primitiven Datentypen mehr.
- Aggrigat kapselt und enthält mehrere ValueTypes (Unit)
- Aggrigatsgleicheit nur wenn die selbe Identität gegeben ist (ID)
- Service Layer enthält Use-Cases/ Businesslogik 
  
### Benötigte REST-Endpunkte um Funktionalität testen zu können
* **Bestellung aufgeben**
  * POST "http://localhost:8081/api/v1/orders/"
  * `OrderPlacedEvent`
* **Bestellung auf bBezahlt setzen**
  * Alles bestellungen holen - GET  "http://localhost:8081/api/v1/orders/"
  * Bestellung auf Bezahlt setzten - POST "http://localhost:8081/api/v1/orders/checkpayment/ + orderID" Status nun PAYMENT_VERIFIED
  * `OrderPaymentValidatedSpringEvent`

## Stock-Service ohne Clean Architecture
![UML-Diagramm Stockmanagement](./10_Bericht_Makroarchitektur_Bilder/UML_Stockmanagement.png)

### Aufbau Stockmanagement
* Keine Clean Architecture Struktur
* Weder Service-Layer noch irgendwelche Abstraktionen durch Interfaces
* Controller und Events interagieren direkt über Repository mit der Datenbank
* Datenbanktransaktionen direkt über Repository und direkt über Entity-Objete (kein zusätzliches Mapping)
* Kommunikation der Events, direkt über die Klassen für Eingehende- resprektiver Ausgehende Events
* Durch die oben genannten Punkte direkt Technologie- und Klassenabhäbngig (Änderungen haben große Auswirkungen auf den gesamten Service)
* Businesslogik eigentlich im gesamten infrastructure-package
* lediglich grobe Aufteilungen durch Packages

### Benötigte REST-Endpunkte um Funktionalität testen zu können
* **Packliste generieren**
  * `StockIncomingMessageHandler` legt durch das generierte Event (entsteht durch PaymentValidated) eine Packliste an 
* **Items in der Packetliste abhaken**
  * Alle Packings (Liste) holen - GET "http://localhost:8081/stock/packings"
  * Item als verpackt markieren - POST "http://localhost:8081/stock/setPackedForPacking/ + itemID"
  * Wenn alle Items einer bestellung verpackt sind, wird per `OrderPackedSpringEvent` auf das Ordermanagement der Status der Bestellung auf IN_DELIVERY upgedatet

# Svelte Frontend
* sehr junges Java reaktives JavaScript-Framework 
* ohne virtuellen DOM 
* sehr performant, kompakte Schreibweise
* npm und nodejs vorausgesetzt
* wird direkt kompiliert beim Erstellen der App und nicht im Browser
* Komponenten orientiert (je granularer desto besser im Sinne der Wiederverwendbarkeit)
* Aufbau und Handling (Binding, Components, Probs, Lifecycle-Methods) ähnlich wie bekannte Frameworks wie VueJS, React oder Angular

## Neues Projekt erstellen
* npm init vite@latest   (Vite-Builder starten)
* Schritte durchführen
* npm install (Bibliotheken installieren)
* npm run dev (Server inkl. Applikation hochfahren)

## Frontend Kundenübersicht
Die Kundenübersicht bildet einen Webshop ab, bei dem Produkte hinzugefügt werden können und mittels Checkout die Bestellung aufgegeben werden kann. Dies ist nur möglich sofern Produkte im Warenkorb liegen. Zudem ist die Kundenansicht der Einstiegspunkt der Applikation. Über ein Dropdown-Menü kann zwischen den Ansichten gewechselt werden.
![Kundenübersicht Warenkorb](./10_Bericht_Makroarchitektur_Bilder/Frontend_4.png)

## Frontend Abrechnungsübersicht
Die Abrechnungsansicht listet alle Bestellungen des Systemes auf und bietet die Möglichkeit, Bestellungen auf bezahlt zu setzen. Dadurch wird eine Packliste durch ein Event im Backend erzeugt.
![Abrechungsübersicht neue Bestelllung](./10_Bericht_Makroarchitektur_Bilder/Frontend_5.png)
![Abrechungsübersicht Bestelllung bezahlt](./10_Bericht_Makroarchitektur_Bilder/Frontend_6.png)

## Lagerübersicht
Die Lagerübersicht listet alle Orders und deren Artikel auf. Zudem können Mitarbeiter den Status der Artikel auf "gepackt" setzen. Nachdem alle Items eines Bestellung gepackt wurden, wird wieder ein Event an das Ordermanagemnt um die Bestellung auf "PREPARING_FOR_DELIVERY" zu setzen.

![Lagerübersicht nicht geppackte Items](./10_Bericht_Makroarchitektur_Bilder/Frontend_8.png)
![Lagerübersicht gepackte Items](./10_Bericht_Makroarchitektur_Bilder/Frontend_9.png)

# **Architekturanalyse mit Makroarchitektur**

Applikation wurde in Services aufgeteilt. Die Funktionalität hat sich dabei aber nicht verändert.

![Makroarchitektur Services](./10_Bericht_Makroarchitektur_Bilder/Makroarchitektur_Services.png)

## Gesamtsystem als Modulith

Das System wurde komplett aufgebrochen und in Mikroservices aufgeteilt. Somit läuft nun jedes Service “komplett unabhängig” vom anderen in einem eigenen SpringBoot-Container. Dadurch dass die Basis in einem sauberen Monolithen lag, hält sich der Aufwand beim Aufteilen der einzelnen Services in grenzen. 

- Das Aufteilen einzelner Teile und Erzeugen neuer Mikroservices wurde mit dem Strangler-Pattern durchgeführt. Dabei Werden Programmteile solange “gewürgt” (Programmteile abgekapselt) bis einzelne Services entstehen, welche selbständig ohne Abhängigkeit lauffähig und wiederverwendbar sind.
- Je granularer die Verantwortung im System verteilt ist (hohe Kohäsion), je geringer der Kopplungsgrad zwischen Klassen im System.

### API Gateway und Service Discovery

Durch Auslagern in mehrerer Mikroservices entstanden zwei weitere Services, welche für die Kommunikation der einzelnen Services und dem Zugriff von Außen zuständig sind. Die beiden folgenden Services sind essenziell für den Betieb und können repliziert werden (Redundanz), da sie stateless sind.

**API-Gateway** beschreibt ein eigenes Pattern. Das GW bietet uns die Möglichkeit, dass wir nur einen Einstiegspunkt in unsere Applikation haben. Sprich wir können beide Services über das GW über nur einen Socket (IP + Port) ansprechen. Dabei fungiert das GW wie ein Router oder Handler, der uns je nach Anfrage auf das jeweilige Service weiterleitet. Dabei muss das GW über die application.yml oder application.propperties-Datei konfiguriert werden. Hier werden die Routen und deren Anfragen der jeweiligen Services definiert. Zudem können hier die Einstellungen bezüglich des Cross-Origin Resource Sharing und der einzelnen Methoden (GET, PUT, POST, DELETE)  für den Zugriff der API getätigt werden. Dabei muss bei erlaubten POST-Anfragen der Eintrag Content-Type getätigt werden. 

* Achtung - Socket an die Zugriffsgegebenheiten anpassen (localhost:3000)!
  
Zudem bekommen wir durch das GW weitere Möglichkeiten wie

* Resilienz Pattern,  bei nicht vorhandenen oder ausgefallenen Services
* Authentifizierung via TOKEN-Relay (Mapping Session - Token)
    
Als wichtiger Punkt muss hier noch erwähnt werden, dass die Services vom GW nicht über die IP-Adresse angesprochen werden, sondern über einen definierten Namen, eine Art “Hostname”. Damit dieser verwendbar ist, wird das zweite zusätzliche Service “Service-Discovery” benötigt. Alle zusät5zlich  benötigten Konfigurationen, werden dem GW über das Service-Discovery per Konfigurationdatei bereitgestellt (Configuration Externalize - Konfiguration per Yaml um keine Downtime zu haben).
    
    ```yaml
    routes:
        - id: stock
          uri: lb://erplitestock #Hostname des Services
    	    predicates:
    	      - Path=/stock/** #Anfragen mit dieser URL werden auf uri umgeleitet
    ```
    
**Service Discovery** handelt Kommunikation (Art DNS) als eiges Service und muss als erstes Service hochgefahren werden. Das Ziel dabei ist das Weckabstrahieren der IP-Adresse, um bei Änderung über den weiter Namen ansprechbar zu sein.

### RabbitMQ

Zudem kommunzieren die Services (Orders und Stock) nicht mehr über den von SpringBoot bereitgestellten ApplicationEventPublisher respektive ApplicationListerner, sondern über einen mittels Docker bereitgestellten RabbitMQ - Message Broker Server umgesetzt. Microservices kommunizieren interrn über RabbitMQ - Message Broker per Events (keine Abhängigkeiten) und extern über die API (API-Gateway).

- Hier musste der größte Eingriff in die Applikation gemacht werden. Jedoch nur innerhalb der Implementierung der konkreten Technologie innerhlab des Packages “messaging.rabbitmq”. Die Kommunikation wird weiter über die Interfaces IN (OrderIncomingMessagesPort) und OUT (OrderOutgoingMessageRelay) umgesetzt. Innerhalb der Klasse “RabbitMQConfig” wird die Konfiguration der Queues (Sender und Receiver) in Form von @Beans definiert.
- Die queue()Methode erstellt eine Warteschlange. Die exchange()Methode schafft einen Themenaustausch. Die binding()Methode bindet diese beiden zusammen und definiert das Verhalten, das auftritt, wenn RabbitTemplate published.
- Der RabbitMQ kann per Webinterface mit den Zugangsdaten (guest/guest) abgerufen werden. Dabei können die Ques und deren Eingehenden- repektive Ausgehenden-Events beobachtet werden.
- Achtung - Falls beim Hochfahren Probleme entstehen, können vorhandene Ques gelöscht werden!
- Achtung - IP-Adressen in den einzelnen Services an die Gegebenheiten des RabbitMQ-Servers anpassen!

https://spring.io/guides/gs/messaging-rabbitmq/

### Datenbank

Abschließend wurde noch die Datenbank von der internen H2-Datenbank, gegen auf Docker laufenden mariaDB-Datenbanken getauscht. 

- Wichtiger Grundsatz - jedes Mikroservice braucht seine eigene Datenbank, um komplett umabhängig zu sein!
- Dadurch entstanden für das Ordermanagement- und Stockmanagement-Service jeweils eine unabhängige Datenbank.
- Dabei musste lediglich folgender Code innerhalb der application.propperties-Datei angepasst werden

```yaml
#MariaDB
spring.datasource.url=jdbc:mariadb://10.211.55.4:3306/ordermgmt #IP von Ubuntu-Server auf dem Docker läuft
spring.datasource.username=root
spring.datasource.password=notSecureChangeMe
spring.jpa.properties.hibernate.dialect=org.hibernate.dialect.MySQL5InnoDBDialect
spring.datasource.driver-class-name=org.mariadb.jdbc.Driver
```
## Inbetriebnahme Applikation

1. Projekt importieren und alle Dependencies vie Maven laden
2. Services hinzufügen, damit alles in einem Projekt lauffähig ist
3. application.propperties der einzelnen Services an Gegebenheiten anpassen (IP-Adressen)
4. Service Discovery und API-Gateway starten
5. Docker-Container für Datenbanken und Message-Broker hochfahren
6. Domänenservices starten
7. Frontend hochfahren
8. Events können nun über das Webinterface (Port 15672) des RabbitMQ-Servers beobachtet und die Anwendung getestet werden

### Docker für Infrastruktur

```docker
version: "3.2"

services:
  rabbitmq:
    image: rabbitmq:3-management-alpine
    restart: always
    container_name: 'rabbitmq'
    ports:
        - 5672:5672
        - 15672:15672

  db:
    image: mariadb:10.3
    restart: always
    container_name: 'mariadb'
    hostname: mariadb
    ports:
      - 3306:3306
    environment:
      MYSQL_ROOT_PASSWORD: notSecureChangeMe

  phpmyadmin:
    image: phpmyadmin
    container_name: 'phpmyadmin'
    restart: always
    ports:
      - 8090:80
    environment:
      - PMA_ARBITRARY=1
```
