# Version Control System
- [Version Control System](#version-control-system)
- [Projektmanagement (VCS - Version Control System)](#projektmanagement-vcs---version-control-system)
- [Plattformen](#plattformen)
- [CheatSheet's und Tutourials](#cheatsheets-und-tutourials)
- [Begriffe](#begriffe)
- [Einrichtung SSH-Zugriff](#einrichtung-ssh-zugriff)
  - [Generate SSH-Keygen on Mac](#generate-ssh-keygen-on-mac)
  - [Save these settings in the ~/.ssh/config file.](#save-these-settings-in-the-sshconfig-file)
- [Entwicklungsumgebungen und Tools](#entwicklungsumgebungen-und-tools)
  - [IntelliJ](#intellij)
  - [Visual Studio Code](#visual-studio-code)
  - [Git-Kraken](#git-kraken)
- [Git-Bash](#git-bash)
  - [Git Global Setup anpassen](#git-global-setup-anpassen)
    - [Variante mittels Befehle](#variante-mittels-befehle)
    - [Variante mittels config](#variante-mittels-config)
  - [Push einen vorhandenen Ordner](#push-einen-vorhandenen-ordner)
    - [Push ein vorhandenes Repo](#push-ein-vorhandenes-repo)
  - [Neunen branch(Zweig) erstellen und wieder hochladen](#neunen-branchzweig-erstellen-und-wieder-hochladen)
  - [Git Befehle](#git-befehle)
- [Merge/Pull-Request](#mergepull-request)
  - [Merge-Konflikt](#merge-konflikt)
- [.gitignore](#gitignore)
- [Quellenverzeichnis](#quellenverzeichnis)

# Projektmanagement (VCS - Version Control System)

![Abbildung1](02_Bericht_VCS_Bilder/Untitled.png) Abbildung 1

# Plattformen

- Gitlab
- Github
- ...

# CheatSheet's und Tutourials

- [https://training.github.com/downloads/de/github-git-cheat-sheet/](https://training.github.com/downloads/de/github-git-cheat-sheet/) [Git-Bash Befehle]
- [https://rogerdudler.github.io/git-guide/index.de.html](https://rogerdudler.github.io/git-guide/index.de.html) [Tutourial Befehle]
- [https://learngitbranching.js.org/?locale=de_DE](https://learngitbranching.js.org/?locale=de_DE) [Interaktives Tutourial]
- [https://www.youtube.com/watch?v=6uvzIMdqV2U&t=3028s](https://www.youtube.com/watch?v=6uvzIMdqV2U&t=3028s) [Video Tutourial]

# Begriffe

- Repository → Ein Repository oder Git-Projekt umfasst die gesamte Sammlung von Dateien und Ordnern, die einem Projekt zugeordnet sind, zusammen mit dem Revisionsverlauf jeder Datei.
- init → lokales Verzeichnis initialisieren
- add → check der Änderungen, Zwischenbereich/Stagebereich
- commit → bestätigen einer Änderung inkl. Kommentar
- Branch → Zweige (Feature Branch um parallel arbeiten zu können)
- Merge → Zweige zusammenfügen (Achtung Konflikte müssen manuell gelöst werden!)
- Pull/Merge request → Anfrage auf einen Merge stellen

# Einrichtung SSH-Zugriff

## Generate SSH-Keygen on Mac

```php
ssh-keygen -t rsa -b 2048 -C "deine email"
eval "$(ssh-agent -s)"
ssh-add ~/.ssh/id_rsa

## Erstellten Key in die Zwischenablage ablegen
tr -d '\n' < ~/.ssh/id_rsa.pub | pbcopy

## Import auf Gitlab Account
Settings -> SSH keys -> add key
```

## Save these settings in the ~/.ssh/config file.

```php
# GitLab.com
Host gitlab.com
  PreferredAuthentications publickey
  IdentityFile ~/.ssh/id_rsa
```

# Entwicklungsumgebungen und Tools

## IntelliJ

- IntelliJ kann mit GitHub/GitLab direkt verbunden werden. Somit lassen sich alle wichtigen Git-Funktionen grafisch steuern.

    ![Abbildung2](02_Bericht_VCS_Bilder/Bildschirmfoto_2021-09-24_um_8.19.46_PM.png)

- müssen unter VCS → Enable Version Control Integration aktiviert und ein jeweiliger Account verknüpft werden.

    ![Abbildung3](02_Bericht_VCS_Bilder/Bildschirmfoto_2021-09-15_um_5.56.31_PM.png)

    enable VCS IntelliJ

## Visual Studio Code

- VS -Code hat eine Git-Versionierung integriert und mit + können Änderungen gestaged und mit command + enter können sie committet werden.
- Mittels GitLens PlugIn kann die History (mittels Rechtsklick) und deren Versionierungsstände angezeigt werden.

## Git-Kraken

- Ist ein grafisches VCS-Tool welches einen Account voraussetzt und alle Features zum Thema VCS bietet.

    ![Abbildung4](02_Bericht_VCS_Bilder/Bildschirmfoto_2021-09-24_um_8.00.36_PM.png)

# Git-Bash

- Durch die bereits installierte der GitBash unter MacOS, kann direkt über das integrierte Terminal mit einem Account (Github/ Gitlab) eine Verbindung aufgebaut werden und somit eine Codeversionierung umgesetzt werden.

## Git Global Setup anpassen

### Variante mittels Befehle

```yaml
git config --global user.name "Mathias Rudig"
git config --global user.email "matrudig@icloud.com"
git config --global core.editor nano
```

### Variante mittels config

```yaml
git config --global --edit

##Inhalt
[user]
        name = Mathias Rudig
        email = mathias.rudig@icloud.com
[core]
        autocrlf = input
        editor = nano
```

## Push einen vorhandenen Ordner

```yaml

cd 'Verzeichnis'
git add .
git commit -m "first commit"
git branch -M main
git remote add origin git@gitlab.com:'mathiasru/learninggitlabvcs.git' oder git remote add origin <gitlab url>
git push -u origin main 

##oder beim Erstellen eines neuen Repos
git clone git@gitlab.com:'mathiasru/learngit.git'
cd 'Verzeichnis'
##und die oberen Schritte
```

### Push ein vorhandenes Repo

```yaml
cd 'Verzeichnis'
git remote rename origin old-origin
git remote add origin git@gitlab.com:'mathiasru/learngit.git'
git push -u origin --all
git push -u origin --tags
```

## Neunen branch(Zweig) erstellen und wieder hochladen

```yaml
cd 'Verzeichnis'
git branch 'feature-a'
git checkout feature-a
## edit files
git add 'Stage Änderung'
git commit -m 'Commit Änderung'
git push | beim erstmaligen pushen eines branches git push --set-upstream origin 'feature-a'
git checkout master
git merge 'feature-a'
git push
```

![Abbildung 5](02_Bericht_VCS_Bilder/Untitled%201.png)
Abbildung 5
</br>
</br>
## Git Befehle

| Befehlt | Beschreibung | Zusatz |
| --- | --- | --- |
| git —version | gibt die aktuell installierte Version aus |
| git log | zeigt den Commit-Log an	 | git log —pretty=online —graph zeigt einen Grapgen an |
| git config —global —edit | anpassen der Config (Userdaten) |
| git init [project-name] | Legt ein neues lokales Repository mit dem angegebenen Namen an |
| git clone [url] | Klont ein Projekt und lädt seine gesamte Versionshistorigit remote add origin benutzername@host:/pfad/zum/repository | Remote Repository verbinden und hinzufügen |
| git status | Listet alle zum Commit bereiten neuen oder geänderten Dateien auf |
| git add [file] | Indiziert den derzeitigen Stand der Datei für die Versionierung (Datei wird gestaged) |
| git commit -a -m "message" | ührt sowohl ein git add . als auch dem Commit aus | -a steht führ add |
| git commit -m "message" | Nimmt alle derzeit indizierten Dateien permanent in die Versionshistorie auf | -m steht für message |
| git rm --cached [file] | Entfernt die Datei aus der Stage-Area, behält sie jedoch lokal |
| git push [branch] | Pusht alle Commits auf dem lokalen Branch zu GitHub | Upstream mit -u setzen |
| git pull | hier wird das lokale repository geupdated | merge und fetch |
| git branch | Listet alle lokalen Branches im aktuellen Repository auf | -a  gibt die Übersicht aller Branches, auch Remote |
| git branch [branch-name] | Erzeugt einen neuen Branch | 
| git branch -d [branch-name] | um den erstellten Branch wieder zu löschen |
| git checkout [branch-name] | Wechselt auf den angegebenen Branch und aktualisiert das Arbeitsverzeichnis |  			
| git merge [branch] | Stand zusammenzuführen | achtung hier können Knoflikte entstehen |
| git fetch | in Arbeitskopie die Änderungen herunterzuladen |
| git checkout $hashcode | zurückspringen zu einem gewissen Commit	 |
| git reset HEAD | holt alles aus dem Stage |
</br>
</br>		
# Merge/Pull-Request

Ist eine Möglichkeit dem Entwickler eine Anfrage für einen Code-Vorschlag zu geben und diesen ggf. zu mergen.

- Fork auf Website erstellen
- Änderung durchführen ggf. über eigenen branch (welcher per merge automatisch gelöscht werden kann)

```yaml
git clone git@gitlab.com:'mathias36/gitlab-pull-request-fork.git'
cd 'gitlab-pull-request-fork'
##hier könnte ein neuer branch erstellt werden
nano datei.txt
git add datei.txt
git commit -m "commit datei"
git push
```

- Merge-Request auf Website erstellen
</br>
</br>
## Merge-Konflikt

Du bist verantwortlich, diese Konflikte durch manuelles Editieren der betroffenen Dateien zu lösen.

Bevor du Änderungen zusammenführst, kannst du dir die Differenzen auch anschauen:

`git diff <quell_branch> <ziel_branch>`

# .gitignore

- kann verwendet werden um Daten für den Upload zu ignorieren.
- unter IntelliJ kann hierfür ein Plugin installiert werden.
- nützliche Templates: [https://github.com/github/gitignore](https://github.com/github/gitignore)

# Quellenverzeichnis
* Abbildung 1 - https://www.google.com/url?sa=i&url=https%3A%2F%2Fwww.devguide.at%2Fgit%2Fpull-push-das-remote-repository%2F&psig=AOvVaw2K-XokK1XcB3k64Jv4jmVR&ust=1632570410596000&source=images&cd=vfe&ved=0CAsQjRxqFwoTCPjB5e7El_MCFQAAAAAdAAAAABAD
* Abbildung 5 - https://www.google.com/url?sa=i&url=https%3A%2F%2Fdigitalvarys.com%2Fgit-branch-and-its-operations%2F&psig=AOvVaw1d5QQ1N-lVRVZFI-aiSVKo&ust=1632570306907000&source=images&cd=vfe&ved=0CAsQjRxqFwoTCMiW6LzEl_MCFQAAAAAdAAAAABAD