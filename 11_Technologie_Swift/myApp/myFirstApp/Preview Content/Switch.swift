//
//  Switch.swift
//  myFirstApp
//
//  Created by Mathias Rudig on 15.06.22.
//

import Foundation

class Switch  {
    var name: String
    var description:String
    
    init( name: String, description: String) {
    
        self.name = name
        self.description = description
    }

    func toString() -> String{
        return self.description + " " + self.name
    }
}
