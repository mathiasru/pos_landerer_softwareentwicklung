//
//  ContentView.swift
//  myFirstApp
//
//  Created by Mathias Rudig on 09.06.22.
//

import SwiftUI


struct ContentView: View {
    @State private var percent = 50.0
    @State private var isEditing = false
    @State private var increase = 0
    
    var body: some View {
        VStack {
            NavigationView{
                List{
                    VStack{
                        HStack{
                            Text("Esstisch indirekt").padding(25)
                            Button(action: {sendHttpGet(uuid: "indirekt/\(percent)")}) {
                                Text("ON")
                                    .padding(15)
                                    .foregroundColor(.blue)
                                    .border(Color.blue, width: 2)
                            }.buttonStyle(PlainButtonStyle())
                            Button(action: {sendHttpGet(uuid: "indirekt/0")}) {
                                Text("OF")
                                    .padding(15)
                                    .foregroundColor(.blue)
                                    .border(Color.blue, width: 2)
                            }.buttonStyle(PlainButtonStyle())
                        }
                        HStack {
                            Slider(
                                value: $percent,
                                in: 0...100,
                                onEditingChanged: { editing in
                                    isEditing = editing
                                }
                            )
                            Text("\(Int(percent))%")
                        }
                        HStack{
                            Text("Spots Couch").padding(25)
                        
                            Button(action: {sendHttpGet(uuid: "Couch/ein")}) {
                                Text("ON")
                                    .padding(15)
                                    .foregroundColor(.blue)
                                    .border(Color.blue, width: 2)
                            }.buttonStyle(PlainButtonStyle())
                            Button(action: {sendHttpGet(uuid: "Couch/aus")}) {
                                Text("OF")
                                    .padding(15)
                                    .foregroundColor(.blue)
                                    .border(Color.blue, width: 2)
                            }.buttonStyle(PlainButtonStyle())
                            
                        }
                        HStack{
                            Text("Wohnen").padding(25)
                            Button(action: {increaseToggle()}) {
                                Text("Szene \(increase)")
                                    .padding(15)
                                    .foregroundColor(.blue)
                                    .border(Color.blue, width: 2)
                            }.buttonStyle(PlainButtonStyle())
                            Button(action: {sendHttpGet(uuid: "Wohnen/aus")}) {
                                Text("OF")
                                    .padding(15)
                                    .foregroundColor(.blue)
                                    .border(Color.blue, width: 2)
                            }.buttonStyle(PlainButtonStyle())
                        }
                    }
                }.navigationTitle("Wohnzimmer")
            }
        }
    }
    
    func increaseToggle(){
        if(increase < 3){
            increase += 1
        }else{
            increase = 1
        }
        sendHttpGet(uuid: "Wohnen/\(increase)")
    }
    
    
    func sendHttpGet(uuid: String) {
        let url = URL(string: "http://emloxone:emloxone@192.168.2.4/jdev/sps/io/"+uuid)!
        let request = URLRequest(url: url)
        
        let dataTask = URLSession.shared.dataTask(with: request) { (data, response, error) in
            if let error = error {
                print("Request error: ", error)
                return
            }
            
            guard let response = response as? HTTPURLResponse else { return }
            
            if response.statusCode == 200 {
                print("everything ok")
            } else {
                print("something went wrong")
            }
        }
        
        dataTask.resume()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
            .preferredColorScheme(.dark)
            .previewInterfaceOrientation(.portrait)
    }
}


