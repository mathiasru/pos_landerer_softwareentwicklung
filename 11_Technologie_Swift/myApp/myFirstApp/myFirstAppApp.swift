//
//  myFirstAppApp.swift
//  myFirstApp
//
//  Created by Mathias Rudig on 09.06.22.
//

import SwiftUI

@main
struct myFirstAppApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
