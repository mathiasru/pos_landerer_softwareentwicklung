//
//  Device.swift
//  myFirstApp
//
//  Created by Mathias Rudig on 15.06.22.
//

import Foundation

class Device {
    var title: String
    init(title: String) {
        self.title = title
    }

}
