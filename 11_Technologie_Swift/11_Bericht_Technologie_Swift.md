- [Was ist Swift?](#was-ist-swift)
  - [Merkmale von Swift](#merkmale-von-swift)
    - [Lernplattformen](#lernplattformen)
- [Swift vs Kotlin](#swift-vs-kotlin)
  - [Gemeinsamkeiten](#gemeinsamkeiten)
    - [Unterschiede](#unterschiede)
  - [Ranking der Programmiersprachen](#ranking-der-programmiersprachen)
- [SwiftUI](#swiftui)
  - [Beispiel](#beispiel)
- [Quellenverzeichnis](#quellenverzeichnis)
  
# Was ist Swift?

Swift ist eine Programmiersprache die 2014 von Apple und dem Softwarearchitekten Chris Lattern released wurde. Dadurch, dass es eine relativ junge Sprache ist, wurde die Syntax innerhalb der ersten Versionen häufig verändert. Die Aktuelle Version 5.6.1 bringt mittlerweile nur noch funktionale Updates mit sich. Die Sprache Swift ist stark beeinflusst von Objectiv-C, Rust, Haskell, Ruby, Python und C#.  Dabei ist Swift im Vergleich zur ursprünglichen Programmiersprache Objectiv-C performanter, kompakter und stellt eine einfachere Handhabung für den Entwickler bereit. Mittlerweile können Apps rein mithilfe des GUI-Frameworks von Apple SwiftUI (basierend auf MVVM-Pattern) auf leichtgewichtige Weise, reaktiv umgesetzt werden. Dadurch möchte sich Apple langsam von ihrem alten Framework UIKit lösen.

![Swift Logo.png](11_Bericht_Technologie_Swift_Bilder/Logo.png)

Swift bietet die Möglichkeit native Apps für alle Apple Betriebssysteme wie iOS, macOS, tvOS und watchOS zu entwickeln. Als Entwicklungsumgebung wird Xcode von Apple vorausgesetzt. Es handelt sich dabei um eine schwergewichtige IDE mit ca. 33 GB, bei der durch Modularität Programme in mehreren Sprachen(C, C++) entwickelt werden können. Um Apps für aktuelle Apple-Betriebssysteme entwickeln zu können, bedarf es immer der aktuellsten Xcode Version (aktuelle Version 13.4.1). Auch unter Linux und Windows können Anwendungen entwickelt werden, jedoch mit gewissen Einschränkungen. Zudem können unter Verwendung des Swift Android Compilers auch Android Apps entwickerlt werden.

## Merkmale von Swift

- sehr modern
- objektorientiert (Klassen, Vererbung)
- generische Typen
- funktional
- stark, statische Typisierung
- sehr Performant
- sichere Programmiersprache

### Lernplattformen

Apple bietet zudem eine Menge Lernplattformen, um in die Welt der Swift-Programmierung einzusteigen.

- [https://www.apple.com/de/swift/playgrounds/](https://www.apple.com/de/swift/playgrounds/)
- [https://developer.apple.com/tutorials/swiftui](https://developer.apple.com/tutorials/swiftui)

# Swift vs Kotlin

Nachstehend wird zwischen der Sprache Swift und Kotlin im Bezug auf Syntax und Eigenschaften verglichen.

Kotlin ist eine von JetBrains entwickelte Programmiersprache, die in Bytecode für die Java Virtual Machine übersetzt wird und eine Menge Funktionen zur App-Enwicklung liefert. Als Enwicklungsumgebung wird dabei Android-Studio empfohlen.

## Gemeinsamkeiten

- beide sind Open-Source-Sprachen
- beide Sprachen bieten guten Support seitens der Hersteller und haben eine große, stetig wachsende Community
- sie basieren beide auf den LLVM Compiler (**Low Level Virtual Machine**)
- beide haben eine automatische Speicherverwaltung, die sich jedoch voneinander unterscheiden
- ähnliche Deklaration von Properties-Eigenschaften (Variablen) (mutable-veränderbar und immutable-unveränderbar)
- Ähnlichkeiten in den Datenstrukturen
- Behaviours-Verhalten (Funktionen) werden vergleichbar behandelt
- verwenden anonyme-Funktionen (Ausdrücke)
    - Kotlin - Lamda Funktionen
    - Swift - Closures
- per Default können Objekte nicht Null sein (muss klar deklariert werden), dadurch sicherer gegen Fehler
- ähnliche Syntax von Programmablaufsteuerungen wie If-Else und Schleifen
- ähnliche Deklaration von Klassen und Erweiterungen
- es kann sowohl Swift für Android als auch Kotlin für iOS-Anwendungen verwendet werden. Es macht aber zukünftig durchaus Sinn, native Apps zu entwickeln.

### Unterschiede

- Unterschiedliche Speicherverwaltung
    - Kotlin - Garbage Collector
    - Swift - Automatic Reference Counting
- Kotlin verfügt über Datenklassen (bekannt aus Java - JavaBeans)
- Kotlin verfügt über delegierte Klassen (alternative zur Vererbung, kann auch als Beziehung betrachtet werden)
    - Delegation bedeutet, dass ein Objekt einer anderen Klasse als Instanzvariable verwendet werden kann um dessen Verhalten zu nutzen.
- Swift bietet eine erweiterte Funktion von ENUM-Klassen (mehrere Typen)
- Swift unterstützt Tupel (ein Typ, der mehrere Werte in einem zusammengesetzte Werte enthalten kann)
- Swift unterstützt Typen-Alias
- Swift unterstütz keine Annotationen
- Es gibt diverse Syntaxunterschiede
    - Hier ein Link, der die Syntax beider Sprachen aufzeigt: [http://nilhcem.com/swift-is-like-kotlin/](http://nilhcem.com/swift-is-like-kotlin/)

Zudem bieten beide Sprachen vergleichbare Frontend-Frameworks an, Apple mit ihrem aktuellen SwiftUI und Android mit Jetpack Compose.

Welche Sprache grundsätzlich die bessere Wahl ist, hängt vom Use-Case und den Anforderungen der Applikation ab.

## Ranking der Programmiersprachen

![Ranking Programmiersprachen](11_Bericht_Technologie_Swift_Bilder/Ranking.png)

# SwiftUI

Wie eingangs erwähnt, sollte das Framework SwiftUI das ältere UIKit zukünftig ablösen. Die beiden Frameworks unterscheiden sich dabei in großen Teilen, wie Klassen, Protokolle (Interface), Eigenschaften und im generellen Entwicklungsprozess. UIKit bietet dabei einen grafischen Ansatz, im Gegensatz zu SwiftUI, dass rein auf programmatisch orientierte Programmierer abzielt. Beim Erstellen einer App muss sich der Entwickler zwischen den beiden Varianten entscheiden. SwiftUI bietet mitlerweile auch die Möglichkeit, vordefinierte Templates zu laden, welche eine bestimmte Codestruktur vorgeben. Diese müssen dann nur noch mit Funktion befüllt werden.


## Beispiel

Der nachstehenden Codeabschnitte beschreiben eine App, welche den Inhalt aus dem ContentView (Hello World) auf dem Device ausgibt.

```swift
import SwiftUI

@main
struct helloWorldApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
```

```swift
import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, world!")
            .padding()
    }
}

//Vorschau
struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
```

![Applikation Hello-World](11_Bericht_Technologie_Swift_Bilder/Applikation.png)

# Literaturverzeichnis

- [https://www.swift.org](https://www.swift.org/)
- [https://de.wikipedia.org/wiki/Kotlin_(Programmiersprache)](https://de.wikipedia.org/wiki/Kotlin_(Programmiersprache))
- [https://de.wikipedia.org/wiki/Swift_(Programmiersprache)](https://de.wikipedia.org/wiki/Swift_(Programmiersprache))
- [https://www.devteam.space/blog/kotlin-vs-swift-programming-language/](https://www.devteam.space/blog/kotlin-vs-swift-programming-language/)
- [https://www.bluelabellabs.com/blog/swiftui-vs-uikit/](https://www.bluelabellabs.com/blog/swiftui-vs-uikit/)
- [https://proandroiddev.com/swiftui-vs-jetpack-compose-by-an-android-engineer-6b48415f36b3](https://proandroiddev.com/swiftui-vs-jetpack-compose-by-an-android-engineer-6b48415f36b3)
