# IDEs und JavaDoc

- [IDEs und JavaDoc](#ides-und-javadoc)
- [IDE's](#ides)
  - [IntelliJ](#intellij)
    - [Schnelles Editieren über Shortcuts](#schnelles-editieren-über-shortcuts)
    - [Projektmanagement (VCS - Version Control System)](#projektmanagement-vcs---version-control-system)
    - [Wichtige Plugins](#wichtige-plugins)
    - [Code-Funktionen](#code-funktionen)
    - [Navigation / Struktur](#navigation--struktur)
    - [Code Inspektionen](#code-inspektionen)
    - [UML-Generierung](#uml-generierung)
  - [Visual Studio Code](#visual-studio-code)
    - [Schnelles Editieren über Shortcuts](#schnelles-editieren-über-shortcuts-1)
    - [Projektmanagement (VCS - Version Control System)](#projektmanagement-vcs---version-control-system-1)
    - [Wichtige Plugins](#wichtige-plugins-1)
    - [Code-Funktionen](#code-funktionen-1)
    - [Navigation / Struktur](#navigation--struktur-1)
    - [Code Inspektionen](#code-inspektionen-1)
  - [Spring und Spring Boot](#spring-und-spring-boot)
- [JavaDoc Dokumentation](#javadoc-dokumentation)
    - [Erzeugen einer JavaDoc unter IntelliJ](#erzeugen-einer-javadoc-unter-intellij)
    - [Anleitung JavaDoc mittels CLI](#anleitung-javadoc-mittels-cli)
    - [Verwendung von JavaDoc-Funktionen (Beispiel aus dem Semesterprojekt)](#verwendung-von-javadoc-funktionen-beispiel-aus-dem-semesterprojekt)

# IDE's

## IntelliJ

Verwendet nur Projektdateien, welche eine bestimmte Struktur aufweisen. Funktioniert out of the Box und ist für Java-Projekte konzipiert.

### Schnelles Editieren über Shortcuts

[Shortcuts IntelliJ](https://www.notion.so/7c45ef943e6949e5bf671f007fe01682)

Shortcuts können unter Preferences → Keymap → Main Menu → Code angepasst respektive erstellt werden.

![Abbildung 1](01_Bericht_IDE_JavaDoc_Bilder/Bildschirmfoto_2021-09-15_um_10.45.41_AM.png)

Code Vervollständigen mittels Templates:

Code Templates können unter Preferences → Editor → General → Postfix Completion angepasst respektive erstellt werden. 

![Abbildung 2](01_Bericht_IDE_JavaDoc_Bilder/Bildschirmfoto_2021-09-15_um_10.45.20_AM.png)

---

### Projektmanagement (VCS - Version Control System)

- Gitlab
- Github

müssen nur unter VCS → Enable Version Control Integration aktiviert und ein jeweiliger Account verknüpft werden.

![Abbildung 3](01_Bericht_IDE_JavaDoc_Bilder/Bildschirmfoto_2021-09-15_um_5.56.31_PM.png)

---

### Wichtige Plugins

Es wird ein Großteil der Plugins vorinstalliert wie z.B.: Angular und Angular JS, Spring, Spring Boot, Maven ...

- .ignore (um Ausnahmen beim Pushen auf Github/ Gitlab zu erstellen)
- Kotlin (Android App Entwicklung)
- Code With Me (Partnerprogrammierung per Remoteverbindung (Voraussetzung Ultimate Lizenz))
- VisualVM Launcher (Tracken des Heap/ Stackspeichers während der Laufzeit)

---

### Code-Funktionen

- Codeanalyse (Vorschläge/ Warnungen/ Fehleranzeige / Hinweise zur Optimierung )
- ausführliche und intelligente Codevervollständigung
- direkte Einsicht in die Java-Bibliothek
- Ausführen vom Code
- Debugging durch Setzen von Breakpoints

---

### Navigation / Struktur

Beim Anlegen eines Projektes wird automatisch eine Projektstruktur angelegt. Innerhalb des src Verzeichnis kann eine eigene Architektur mit verschiedensten Packages etc. gewählt werden, um dem Programmier/ -in dem Projekt eine individuell anpassbare Struktur zu geben. Um im Sinne der Skalierbarkeit und Wartbarkeit eine sinnvolle Aufteilung gewährleisten zu können.

![Abbildung 4](01_Bericht_IDE_JavaDoc_Bilder/Bildschirmfoto_2021-09-15_um_7.59.05_PM.png)

---

### Code Inspektionen

Durch Verwenden des Debuggers kann an bestimmte Breakpoints gesprungen und mittels Buttons (jump in/ jump forward) durch das Programm navigiert werden. Dieses "Tool" ist sehr geeignet um Fehler ausfindig machen zu können.

---

### UML-Generierung

Unter der Ultimate-Version kann mit einem Rechtsklich am Ordnerbaum Punkt Diagrams ausgewählt werden.

![Abbildung 5](01_Bericht_IDE_JavaDoc_Bilder/Bildschirmfoto_2021-09-15_um_8.27.08_PM.png)

Dadurch wird ein Klassendiagramm erzeugt und kann über eine Schaltfläche angepasst werden (Ansicht/Properties/Behaviours/Dependencies).

![Abbildung 6](01_Bericht_IDE_JavaDoc_Bilder/Bildschirmfoto_2021-09-15_um_8.31.06_PM.png)

UML-Diagramm aus meinem Semesterprojekt

## Visual Studio Code

Ist ein Texteditor und verwendet keine Projektdateien, lediglich einzelne Dateien, wobei bei erstellung eines Workspaces der Aufbau einem Projekt ähnelt. Ist wie ein Baukasten, der mittels Plugins zu einer extrem Funktionalen Entwicklungsumgebung konfiguriert werden kann.

### Schnelles Editieren über Shortcuts

[Shortcuts Visual Studio Code](https://www.notion.so/8f11cb9963094f38819d98c833b05042)

---

### Projektmanagement (VCS - Version Control System)

Durch die bereits installierte der GitBash unter MacOS, kann direkt über das integrierte Terminal mit einem Account (Github/ Gitlab) eine Verbindung aufgebaut werden und somit eine Codeversionierung umgesetzt werden. 

```yaml
## Git-Befehle
git init ##Vorberietung für git
git remote add origin <gitlab url>
git add .
git status
git commit -m "commit text"
git push -u origin master
```

![Abbildung 7](01_Bericht_IDE_JavaDoc_Bilder/Untitled.png)

---

### Wichtige Plugins

Um eine Java Entwicklungsumgebung verwenden zu können, kann das Extension Pack for Java installiert werden.
Um dieses verwenden zu können muss auf dem PC mindesten eine Java Development Kit in der Version 11 oder höher installiert sein.

- Language Support for Java
- Debugger for Java
- Test Runner for Java
- Maven for Java
- Project Manager for Java
- Visual Studio IntelliCode
- IntelliJ IDEA Keybindings (um die selben Shortcuts wie bei IntelliJ verwenden zu können)
- Spring Boot Extension Pack

---

### Code-Funktionen

- kann mittels Plugins erweitert werden
- Codeanalyse (Fehleranzeige durch rote Wellenlinien)
- Ausführen vom Code
- Debugging durch Setzen von Breakpoints

---

### Navigation / Struktur

Beim Anlegen eines Workspaces (eine Art Projekt) wird automatisch eine Struktur angelegt. Innerhalb des src Verzeichnis kann eine eigene Architektur mit verschiedensten Packages etc. gewählt werden, um dem Programmier/ -in dem Projekt eine individuell anpassbare Struktur zu geben. Um im Sinne der Skalierbarkeit und Wartbarkeit eine sinnvolle Aufteilung gewährleisten zu können.

![Abbildung 8](01_Bericht_IDE_JavaDoc_Bilder/Bildschirmfoto_2021-09-15_um_8.22.35_PM.png)

---

### Code Inspektionen

Durch Verwenden des Debuggers (mittels Plugin installieren) kann an bestimmte Breakpoints gesprungen und mittels Buttons (jump in/ jump forward) durch das Programm navigiert werden. Dieses "Tool" ist sehr geeignet um Fehler ausfindig machen zu können.

---

## Spring und Spring Boot

Das Open-Source-Framework Spring reduziert die Komplexität der Java-Programmierung. Das Tool Spring Boot zielt  darauf ab, die Entwicklung von Enterprise-Apllikationen in Java weiter zu vereinfachen. ([https://www.dev-insider.de/was-ist-spring-boot-a-1009135/](https://www.dev-insider.de/was-ist-spring-boot-a-1009135/))

- Während das Framework bei IntelliJ schon vorinstalliert ist, muss ein Extension Pack unter VS-Code nachinstalliert werden.

---

# JavaDoc Dokumentation

Die Java Bibliothek ([https://docs.oracle.com/javase/8/docs/api/](https://docs.oracle.com/javase/8/docs/api/)) basiert auf JavaDoc.

### Erzeugen einer JavaDoc unter IntelliJ

Unter Tools → Generate JavaDoc kann folgendes Konfigurationsfenster für die Generierung eines JavaDocs angepasst werden.

![Abbildung 9](01_Bericht_IDE_JavaDoc_Bilder/Bildschirmfoto_2021-09-16_um_8.06.22_PM.png)

- **Private** : um alle Klassen und Mitglieder in die Referenz einzubeziehen.
- **Package** : um alle Klassen und Mitglieder außer den privaten einzuschließen.
- **Protected** : Um öffentliche und geschützte Klassen und Member einzuschließen.
- Public : um nur öffentliche Klassen und Mitglieder einzuschließen.

### Anleitung JavaDoc mittels CLI

Mit dem Terminal lässt sich mittels installiertem JDK ein JavaDoc erstellen. Somit kann unter VS-Code das intigrierte Terminal unter MacOS genutz werden. 

1. in das entsprechende Projektverzeichnis wechseln
2. einen Ordner für die JavaDoc erstellen
`mkdir Ordnername`
3. anschließend kann das JavaDoc erstellt werden
`javadoc -d Ordnername src/Dateiname.java`
4. abschließend kann die JavaDoc geöffnet werden
`open index.html` 

---

### Verwendung von JavaDoc-Funktionen (Beispiel aus dem Semesterprojekt)

```java
/**
 * Die Klasse ist für das Speichern und Auslesen in/aus Binärdateien von bestehenden Objekten zuständig und
 * implementiert die Methoden vom Interface DataStore.Wichtig dabei ist, dass die betroffenen Klassen das
 * Marker-Interface Serializable implmentieren um das Speichern/Auslesen realisieren zu können.
 *
 * @author mathiasrudig
 * @version 1.0
 * @since 16.09.2021
 */
public class FileDataStore implements DataStore {
    //Dateinamen
    private static final String DATEINAME_RAUMLISTE = "./exportFiles/raumListe.bin";
    private static final String DATEINAME_STOCKWERKLISTE = "./exportFiles/stockwerkListe.bin";
    private static final String DATEINAME_SAMMLUNG_STOERUNGEN = "./exportFiles/sammlungStoerungen.bin";

    /**
     * Die Methode implementiert das Speichern von übergebenen Objekten vom Typ RaumListe und speichert diese
     * als Binärdatei ab.
     *
     * @param raumliste übergebener RaumListe vom Typ RaumListe.
     * @throws SaveDataException Fehler beim Schreiben in eine Binärdatei.
     */
    @Override
    public void speichernRaumListe(RaumListe raumliste) throws SaveDataException {
        if (raumliste != null) {
            //Die Klasse ObjectOutputStream ermöglicht das Speichern von Objekten in eineBinärdatei über die Hilfsklasse FileOutput.
            ObjectOutputStream objectOutputStream;
            try {
                objectOutputStream = new ObjectOutputStream(new FileOutputStream(DATEINAME_RAUMLISTE));
                //Schreiben des übergebenen Objektes in Datei
                objectOutputStream.writeObject(raumliste);
                objectOutputStream.close();
            } catch (IOException ioException) {
                ioException.printStackTrace();
                throw new SaveDataException("Fehler beim Speichern der Raumliste: " + ioException.getMessage());
            }
        }
    }

		
```

```java
/**
     * Die Methode implementiert das Auslesen von Objekten vom Typ RaumListe aus einer Binärdatei und gibt diese
     * dann zurück.
     *
     * @return gibt einen RaumListe von Typ RaumListe zurück.
     * @throws RetrieveDataException Fehler beim Auslesen aus der Binärdatei.
     */
    @Override
    public RaumListe auslesenRaumListe() throws RetrieveDataException {
        ObjectInputStream objectInputStream;
        try {
            objectInputStream = new ObjectInputStream(new FileInputStream(DATEINAME_RAUMLISTE));
            //Auslesen der Datei und zuweisung an Objekt (hier müsste ggf. auf den Typ gefrüft werden mittels instanceOf)
            RaumListe raumListe = (RaumListe) objectInputStream.readObject();
            objectInputStream.close();
            return raumListe;
        } catch (IOException | ClassNotFoundException exception) {
            exception.printStackTrace();
            throw new RetrieveDataException("Fehler beim Laden der Raumliste: " + exception.getMessage());
        }
    }
```