# Build Tools - Maven

- [Build Tools - Maven](#build-tools---maven)
- [Einführung in Build-Prozess & Abhängigkeiten](#einführung-in-build-prozess--abhängigkeiten)
  - [Kompilieren und Ausführen einer Java-Datei mittels CLI](#kompilieren-und-ausführen-einer-java-datei-mittels-cli)
  - [Warum Maven?](#warum-maven)
  - [Aufbau Maven-Projekt](#aufbau-maven-projekt)
  - [Pom - Project Object Model](#pom---project-object-model)
  - [**Lifecycle-Phasen**](#lifecycle-phasen)
  - [**Maven-Plugin**](#maven-plugin)
  - [**Goal**](#goal)
- [Installation Maven Mac (Catalina or higher)](#installation-maven-mac-catalina-or-higher)
- [Maven mittels CLI](#maven-mittels-cli)
- [Maven Projekt unter IntelliJ](#maven-projekt-unter-intellij)
  - [Erstellen eines Maven-Projektes](#erstellen-eines-maven-projektes)
  - [Import eines bestehenden Maven-Projektes](#import-eines-bestehenden-maven-projektes)
- [JavaFX - Setup](#javafx---setup)

# Einführung in Build-Prozess & Abhängigkeiten

## Kompilieren und Ausführen einer Java-Datei mittels CLI

- `javac dateiname.java` kompiliert die Datei in eine .class
- `javac -cp verzeichnisname dateiname.java` wenn die abhängige .class Dateien in einem anderen Verzeichnis liegen
- `javap -c dateiname` gibt die .class Datei und deren Methoden aus
- `java dateiname` führt die .class Datei aus
- `java -cp verzeichnisname:. dateiname` führt alle abhängigen .class Dateien im angegebenen Verzeichnis und im gegenwärtigen aus
- `jar cvf library.jar *.class` erzeugt eine .jar Datei, welche  die hinzugefügten .class Dateien beinhaltet
- `jar cvfm dateiname.jar manifest.txt *class` erzeugt eine ausführbare .jar Datei (manifest gibt den Start main() an)
- `java -jar dateiname.jar` führt eine zuvor erstellte .jar Datei aus

## Warum Maven?

- Maven ist ein Software-Tool, das Ihnen hilft, Java-Projekte zu verwalten und Anwendungs-Builds zu automatisieren.
- Sehr schönes Dependency Management. Benötigte Bibliotheken/Frameworks/Plugins werden automatisch heruntergeladen und bei Weitergabe des Projektes direkt in die Projektstruktur eingeordnet.
- Funktioniert auf allen Entwicklungsumgebungen sofern Maven installiert ist und kann sogar mittels CLI verwendet werden.
- Ein Maven-Projekt wird in einer Datei `pom.xml` (*P*roject *O*bject *M*odel) konfiguriert und ein bestehendesd Projekt, kann mittels dieser Datei importiert werden.

## Aufbau Maven-Projekt

- src
    - main
        - java (source code)
- target (kompilierte Dateien, output)
    - classes
    - test-classes

## Pom - Project Object Model

In der pom.xml werden alle Abhängigkeiten, Plugins etc. konfiguriert. Ein so genanntes **Maven Artefakt** wird über die **Koordinaten** definiert. 

- groupId → Gruppierungsbezeichnung
- artficanId → Eindeutige Projektbezeichnung
- version → die Version des Projektes (Snapshot definiert eine ständig ändernde Datei)

In den **properties** werden zum Beispiel die Java-Version (source/ target) oder die Klasse der Main-Methode hinterlegt.

In den **dependencies** werden die Abhängigkeiten definiert, welche von Maven runtergeladen werden, und können zum Beispiel genutzt werden, um Projekte inneinander zu verschachteln oder Github Projekte einbinden zu können. (librarys zum Anbinden mittels API)
* Link zu den Repositories die eingebunden werden können: https://mvnrepository.com 
  
## **Lifecycle-Phasen**

- Lifecycle-Phasen stroßen Goals an, welche aus Plugins stammen
- Auf den Cycle-Phasen (**validate, verify, deploy**) könne Plugins hinterlegt werden
- **compile, test, package und install** sind schon per default mit Plugins hinterlegt
- Die Phasen werden in einer bestimmten Reihenfolge durchlaufen und können auch über die Kommandozeile ausgeführt werden
- mit **package** wird zum Beispiel eine .jar Datei aus .class Dateien generiert und kann mittels **install** in das `~.m2/repository` lokal gespeichert. (install wird eher selten verwendet, nur wenn man die Abhängigkeit öffters benötigt)

## **Maven-Plugin**

- Link zur Doku: [https://maven.apache.org/plugins/index.html](https://maven.apache.org/plugins/index.html)

Maven-Plugins sind Bibliotheken, die zusammengehörende Goals implementieren. (können nachkonfiguriert werden)

Wichtige Plugins sind zum Beispiel: compiler, jar, install, deploy, dependency, javadoc, ...

## **Goal**

Goals sind von Maven-Plugins angebotene ausführbare Kommandos. Goals können bestimmten Lifecycle-Phasen zugeordnet werden und werden dann automatisch zum richtigen Zeitpunkt aufgerufen. Goals können aber auch direkt oder über die Kommandozeile aufgerufen werden. 

- javadoc:javadoc
- javafx:run
- ...

---

# Installation Maven Mac (Catalina or higher)

1. Download Apache Maven und enpacke es in deinem Download-Verzeichnis
2. Lösche die .tar.gz Datei
3. `mv Downloads/apache-maven* /opt/apache-maven` verschieben der Datei
4. `nano ~/.zshenv`
5. `export PATH=$PATH:/opt/apache-maven/bin` in die Datei anfügen und speichern
6. `source ~/.zshenv` aktualisieren der Einstellungen
7. `mvn -version` um die Version zu bekommen und die Installation zu testen
    
    ![Bildschirmfoto 2021-10-03 um 1.07.57 PM.png](01_Bericht_BuildTool_Maven_Bilder/Bildschirmfoto_2021-10-03_um_1.07.57_PM.png)
    

---

# Maven mittels CLI

- `mvn -version`
- Neues Projekt erstellen
    - Verzeichnis erstellen und reinwechseln
    - `mvn archetype:generate -DgroupId=com.mycompany.app -DartifactId=my-app -DarchetypeArtifactId=maven-archetype-quickstart -DarchetypeVersion=1.4 -DinteractiveMode=false`
- `cd my-app` in das erstellte Projektverzeichnis wechseln
    
    ![Bildschirmfoto 2021-10-03 um 1.12.15 PM.png](01_Bericht_BuildTool_Maven_Bilder/Bildschirmfoto_2021-10-03_um_1.12.15_PM.png)
    
- `mvn package` führt alle Phasen bis zum package aus (clean, validate, compile, test, package)
- `java -cp target/my-app-1.0-SNAPSHOT.jar [com.mycompany.app.App](http://com.mycompany.app.App)` um die Java-Datei auszuführen
- `mvn site` Erstellt eine Seite über die Informationen der pom.xml und dient als Doku(target/site)

---

# Maven Projekt unter IntelliJ

## Erstellen eines Maven-Projektes

New Project → Maven → wähle das Java-JDK → Projektname, Speicherort anpassen → Artifact Coordinates anpassen

- GroupId → Gruppierungsbezeichnung (ähnlich den Java-Package-Namen). Normalerweise wird der umgekehrte Domainname der Firma verwendet.
`de.meinefirma.meinprojekt`
- ArticicalId → Eindeutige Projektbezeichnung meist zusammengesetzt aus Name und Version.  `<artifactId>-<version>.<type` zum Beispiel
`test-1.0.0-v.jar`
- Version → Die Version des Projekts, typischerweise im Format: `<Major>.<Minor>.<Bugfix>-<Qualifier>-<Buildnumber>`

![Bildschirmfoto 2021-10-03 um 12.53.08 PM.png](01_Bericht_BuildTool_Maven_Bilder/Bildschirmfoto_2021-10-03_um_12.53.08_PM.png)

→ bestätigen und automatic import wählen

nun kann die pom.xml angepasst werden.

## Import eines bestehenden Maven-Projektes

Import Project → wechsle zum Projektverzeichnis und wähle die pom.xml → eventuelles Anpassen des Imports → open as Project → turst Project

→ nun werden alle dependencies runtergeladen

---

# JavaFX - Setup

- Ein JavaFX Projekt mittels Maven kann in den neueren Versionen unter IntelliJ direkt beim Erstellen eines neuen Projektes ausgewählt werden.

![Bildschirmfoto 2021-10-05 um 5.35.46 PM.png](01_Bericht_BuildTool_Maven_Bilder/Bildschirmfoto_2021-10-05_um_5.35.46_PM.png)

- Es werden die aktuellsten dependencies in der pom.xml eingetragen und man hat ein funktionsfähiges JavaFX Projekt.
- Den Pfad der $HOME-Variable unter Preferences → Preferences → Build, Execution, Deployment → Build Tools → Maven → Runner (Environment variables) eingetragen werden.
    
    ![Bildschirmfoto 2021-10-05 um 6.13.29 PM.png](01_Bericht_BuildTool_Maven_Bilder/Bildschirmfoto_2021-10-05_um_6.13.29_PM.png)
    
- Unter folgender Anleitung ein Projekt erstellt werden, jedoch ist dies mit der aktuellen IntelliJ Version nicht möglich, da keine Archetype von javafx erstellt werden können: [https://openjfx.io/openjfx-docs/#maven](https://openjfx.io/openjfx-docs/#maven)

![Bildschirmfoto 2021-10-05 um 6.32.05 PM.png](01_Bericht_BuildTool_Maven_Bilder/Bildschirmfoto_2021-10-05_um_6.32.05_PM.png)