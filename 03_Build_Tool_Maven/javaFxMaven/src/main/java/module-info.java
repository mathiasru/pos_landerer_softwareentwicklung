module com.itkolleg.javafxmaven {
    requires javafx.controls;
    requires javafx.fxml;


    opens com.itkolleg.javafxmaven to javafx.fxml;
    exports com.itkolleg.javafxmaven;
}