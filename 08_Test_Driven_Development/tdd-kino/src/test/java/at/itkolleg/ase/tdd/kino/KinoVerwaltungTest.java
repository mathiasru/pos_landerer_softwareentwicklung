package at.itkolleg.ase.tdd.kino;

import org.junit.jupiter.api.*;
import org.mockito.Mockito;

import java.time.LocalDate;
import java.util.*;

public class KinoVerwaltungTest {

    KinoSaal kinoSaal; //Mock
    Vorstellung vorstellung; //Mock
    KinoVerwaltung kinoVerwaltung;

    @BeforeEach
    public void setUp() {
        this.kinoSaal = Mockito.mock(KinoSaal.class); //Mocking manuelle Initialisierung
        this.vorstellung = Mockito.mock(Vorstellung.class); //Mocking manuelle Initialisierung
        this.kinoVerwaltung = new KinoVerwaltung();
    }

    //Getter wurden beim Einplanen der Vorstellung getestet, da keinerlei Logik entahlten ist.
    //Ansonsten wäre dies auch zu berücksichtigen

    @Test
    @DisplayName("Vorstellung erfolgreich einplanen")
    public void testEinplanenVorstellungErfolgreich() {
        //Given
        Vorstellung vorstellung = this.vorstellung; //gemockte Vorstellung
        //When
        this.kinoVerwaltung.einplanenVorstellung(vorstellung);
        //Then
        Assertions.assertEquals(1, this.kinoVerwaltung.getVorstellungen().size(), "Falsche Anzahl an eingeplanten Vorstellungen");
        Assertions.assertEquals(vorstellung, this.kinoVerwaltung.getVorstellungen().get(0), "Vorstellung wurde nicht richtig eingeplant");
    }

    @Test
    @DisplayName("Mehrere Vorstellung erfolgreich einplanen")
    public void testMehrereEinplanenVorstellungErfolgreich() {
        //Given
        Vorstellung vorstellung1 = Mockito.mock(Vorstellung.class);
        Vorstellung vorstellung2 = Mockito.mock(Vorstellung.class);
        Vorstellung vorstellung3 = Mockito.mock(Vorstellung.class);
        //When
        this.kinoVerwaltung.einplanenVorstellung(vorstellung1);
        this.kinoVerwaltung.einplanenVorstellung(vorstellung2);
        this.kinoVerwaltung.einplanenVorstellung(vorstellung3);
        //Then
        Assertions.assertEquals(3, this.kinoVerwaltung.getVorstellungen().size(), "Falsche Anzahl an eingeplanten Vorstellungen");
        Assertions.assertEquals(vorstellung1, this.kinoVerwaltung.getVorstellungen().get(0), "Vorstellung 1 wurde nicht richtig eingeplant");
        Assertions.assertEquals(vorstellung2, this.kinoVerwaltung.getVorstellungen().get(1), "Vorstellung 2 wurde nicht richtig eingeplant");
        Assertions.assertEquals(vorstellung3, this.kinoVerwaltung.getVorstellungen().get(2), "Vorstellung 3 wurde nicht richtig eingeplant");
        Exception exception = Assertions.assertThrows(IllegalArgumentException.class, () -> this.kinoVerwaltung.einplanenVorstellung(vorstellung1));
        Assertions.assertEquals(exception.getMessage(), "Test");
    }

    @Test
    @DisplayName("Vorstellung einplanen Fehlgeschlagen, Vorstellung bereits vorhanden")
    public void testEinplanenVorstellungFehlgeschlagen() {
        //Given
        Vorstellung vorstellung = this.vorstellung; //gemockte Vorstellung
        //When
        this.kinoVerwaltung.einplanenVorstellung(vorstellung);
        //Then
        Assertions.assertThrows(IllegalArgumentException.class, () -> this.kinoVerwaltung.einplanenVorstellung(vorstellung), "Vorstellungen noch nicht eingeplant");
    }

    @Test
    @DisplayName("Kaufe Ticket mit korrekten Parametern")
    public void testkaufeTicketMitKorrektenParametern() {
        //Given
        KinoSaal kinoSaal = this.kinoSaal; //gemockte Vorstellung
        Vorstellung vorstellung = this.vorstellung; //gemockte Vorstellung
        Zeitfenster zeitfenster = Zeitfenster.ABEND;
        LocalDate date = LocalDate.of(2022, 3, 16);
        char reihe = 'A';
        int platz = 5;
        float preis = 10.0f;
        //When
        Mockito.when(kinoSaal.getName()).thenReturn("Saal 01");
        Ticket ticket = new Ticket(kinoSaal.getName(), zeitfenster, date, reihe, platz);
        Mockito.when(vorstellung.kaufeTicket(Mockito.any(Character.class), Mockito.any(Integer.class), Mockito.any(Float.class))).thenReturn(ticket);
        //Then
        Assertions.assertEquals(ticket, this.kinoVerwaltung.kaufeTicket(vorstellung, reihe, platz, preis), "Ticket konnte nicht gekauft werden");
        Mockito.verify(kinoSaal).getName(); //Test, ob Methodenaufruf stattfand
        Mockito.verify(vorstellung).kaufeTicket(Mockito.any(Character.class), Mockito.any(Integer.class), Mockito.any(Float.class)); //Test, ob Methodenaufruf stattfand
    }

    @Test
    @DisplayName("Ticket kaufen mit fehlerhaftem Parameter, Sitzplatz existiert nicht")
    public void testKaufeTicketFehlerhafterInputParameterPlatzExistiertNicht() {
        //Given
        Vorstellung vorstellung = this.vorstellung; //gemockte Vorstellung
        //When
        Mockito.when(vorstellung.kaufeTicket(Mockito.any(Character.class), Mockito.any(Integer.class), Mockito.any(Float.class))).thenThrow(IllegalArgumentException.class);
        //Then
        Assertions.assertThrows(IllegalArgumentException.class, () -> this.kinoVerwaltung.kaufeTicket(vorstellung, 'A', 20, 10.0f), "Sitzplatz ist vorhanden");
        Assertions.assertThrows(IllegalArgumentException.class, () -> this.kinoVerwaltung.kaufeTicket(vorstellung, 'D', 5, 10.0f), "Reihe ist vorhanden");
        Mockito.verify(vorstellung, Mockito.atLeast(2)).kaufeTicket(Mockito.any(Character.class), Mockito.any(Integer.class), Mockito.any(Float.class)); //Test Anzahl der Aufrufe
    }

    @Test
    @DisplayName("Ticket kaufen mit fehlerhaftem Parameter, Sitzplatz bereits belegt")
    public void testKaufeTicketFehlerhafterInputParameterPlatzBelegt() {
        //Given
        Vorstellung vorstellung = this.vorstellung; //gemockte Vorstellung
        //When
        Mockito.when(vorstellung.kaufeTicket(Mockito.any(Character.class), Mockito.any(Integer.class), Mockito.any(Float.class))).thenThrow(IllegalStateException.class);
        //Then
        Assertions.assertThrows(IllegalStateException.class, () -> this.kinoVerwaltung.kaufeTicket(vorstellung, 'A', 5, 10.0f), "Sitzplatz noch nicht belegt");
        Mockito.verify(vorstellung).kaufeTicket(Mockito.any(Character.class), Mockito.any(Integer.class), Mockito.any(Float.class)); //Test, ob Methodenaufruf stattfand
    }

    @DisplayName("Ticket kaufen mit fehlerhaftem Parameter, zu wenig Geld")
    @Test
    public void testKaufeTicketFehlerhafterInputParameterZuWenigGeld() {
        //Given
        Vorstellung vorstellung = this.vorstellung;
        //When
        Mockito.when(vorstellung.kaufeTicket(Mockito.any(Character.class), Mockito.any(Integer.class), Mockito.any(Float.class))).thenThrow(IllegalArgumentException.class);
        //Then
        Assertions.assertThrows(IllegalArgumentException.class, () -> this.kinoVerwaltung.kaufeTicket(vorstellung, 'A', 5, 9.0f), "Genug Geld bezahlt");
        Mockito.verify(vorstellung).kaufeTicket(Mockito.any(Character.class), Mockito.any(Integer.class), Mockito.any(Float.class)); //Test, ob Methodenaufruf stattfand
    }

    @TestFactory
    @DisplayName("Dynamischer Ticketkauf Sociable Komponententest") //FactoryTests können die setUp-Methode nicht nutzen
    public Collection<DynamicTest> testKaufeTicketDynamisch() {
        //Given
        //Kinosaal erzeugen
        Map<Character, Integer> reihen = new HashMap<>();
        reihen.put('A', 10);
        reihen.put('B', 10);
        reihen.put('C', 10);
        reihen.put('D', 10);
        reihen.put('E', 10);
        reihen.put('F', 10);
        KinoSaal kinoSaal = new KinoSaal("Saal 01", reihen);

        //Vorstellung erzeugen und einplanen
        Vorstellung vorstellung = new Vorstellung(
                kinoSaal,
                Zeitfenster.ABEND,
                LocalDate.of(2022, 3, 16),
                "Herr der Ringe",
                10.0f);
        this.kinoVerwaltung.einplanenVorstellung(vorstellung);

        List<DynamicTest> testListe = new ArrayList<>(); //Liste der Tests

        for (int i = 0; i < 10000; i++) {
            //Generieren der Testdaten
            Random random = new Random();
            char reihe = (char) (random.nextInt(11) + 65);
            int platz = random.nextInt(20) + 1;
            int geld = random.nextInt(50) + 1;

            try {
                //When
                Ticket ticket = vorstellung.kaufeTicket(reihe, platz, geld);

                //Then
                testListe.add(
                        DynamicTest.dynamicTest(
                                "Erfolgreicher Ticketkauf Reihe:" + reihe + " Platz:" + platz + " Geld:" + geld,
                                () -> Assertions.assertAll("Ticket",
                                        () -> Assertions.assertEquals(vorstellung.getSaal().getName(), ticket.getSaal()),
                                        () -> Assertions.assertEquals(vorstellung.getZeitfenster(), ticket.getZeitfenster()),
                                        () -> Assertions.assertEquals(vorstellung.getDatum(), ticket.getDatum()),
                                        () -> Assertions.assertEquals(reihe, ticket.getReihe()),
                                        () -> Assertions.assertEquals(platz, ticket.getPlatz())
                                )));
            } catch (IllegalArgumentException illegalArgumentException) {
                testListe.add(DynamicTest.dynamicTest(
                        "Nicht existenter Sitzplatz oder zu wenig Geld Reihe:" + reihe + " Platz:" + platz + " Geld:" + geld,
                        () -> Assertions.assertThrows(IllegalArgumentException.class,
                                () -> vorstellung.kaufeTicket(reihe, platz, geld))));
            } catch (IllegalStateException illegalStateException) {
                testListe.add(DynamicTest.dynamicTest(
                        "Belegter Sitzplatz Reihe:" + reihe + " Platz:" + platz,
                        () -> Assertions.assertThrows(IllegalStateException.class,
                                () -> vorstellung.kaufeTicket(reihe, platz, geld))));
            }
        }
        return testListe;
    }

}
