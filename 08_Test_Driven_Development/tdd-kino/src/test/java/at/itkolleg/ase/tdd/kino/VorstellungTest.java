package at.itkolleg.ase.tdd.kino;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.Mockito;

import java.time.LocalDate;

public class VorstellungTest {

    private KinoSaal kinosaal; //Mock
    private Vorstellung vorstellung;
    private Zeitfenster zeitfenster;
    private LocalDate datum;
    private String film;
    private float preis;

    @BeforeEach
    public void setUp() {
        //Given
        this.kinosaal = Mockito.mock(KinoSaal.class); //Mocking manuelle Initialisierung
        this.zeitfenster = Zeitfenster.ABEND;
        this.datum = LocalDate.of(2022, 3, 16);
        this.film = "Herr der Ringe";
        this.preis = 10.0f;
        this.vorstellung = new Vorstellung(this.kinosaal, this.zeitfenster, this.datum, this.film, this.preis);
    }

    //Getter wurden beim Erzeugen der Instanz getestet, da keinerlei Logik entahlten ist.
    //Ansonsten wäre dies auch zu berücksichtigen

    @Test
    @DisplayName("Vorstellung mit korrekten Parametern anlegen")
    public void testVorstelllungAnlegenKorrekteInputParameter() {
        //Given
        KinoSaal kinoSaal = this.kinosaal; //gemockter Kinosaal
        //When
        //Mockito.when(kinoSaal.equals(Mockito.any(Object.class))).thenReturn(true); //Fehler
        Vorstellung vorstellung = new Vorstellung(kinoSaal, this.zeitfenster, this.datum, this.film, this.preis);
        //Then
        Assertions.assertEquals(kinoSaal, vorstellung.getSaal()); //equals sollte gemockt werden (Seiteneffekt)
        Assertions.assertEquals(this.zeitfenster, vorstellung.getZeitfenster());
        Assertions.assertEquals(this.datum, vorstellung.getDatum());
        Assertions.assertEquals(this.film, vorstellung.getFilm());
        Assertions.assertEquals(this.preis, vorstellung.getPreis());
    }

    @Test
    @DisplayName("Vorstellung mit falschen Parametern anlegen")
    public void testVorstellungAnlegenFalscheInputParameter() {
        //Given
        KinoSaal kinoSaal = this.kinosaal; //gemockter Kinosaal
        //When - Then
        Assertions.assertThrows(InvalidConstructionParameterException.class, () -> new Vorstellung(null, this.zeitfenster, this.datum, this.film, this.preis));
        Assertions.assertThrows(InvalidConstructionParameterException.class, () -> new Vorstellung(kinoSaal, null, this.datum, this.film, this.preis));
        Assertions.assertThrows(InvalidConstructionParameterException.class, () -> new Vorstellung(kinoSaal, this.zeitfenster, null, this.film, this.preis));
        Assertions.assertThrows(InvalidConstructionParameterException.class, () -> new Vorstellung(kinoSaal, this.zeitfenster, this.datum, "", this.preis));
        Assertions.assertThrows(InvalidConstructionParameterException.class, () -> new Vorstellung(kinoSaal, this.zeitfenster, this.datum, null, this.preis));
        Assertions.assertThrows(InvalidConstructionParameterException.class, () -> new Vorstellung(kinoSaal, this.zeitfenster, this.datum, this.film, 0));
    }

    @Test
    @DisplayName("Ticket kaufen mit korrekten Parameten")
    public void testKaufeTicketKorrekteInputParameter() {
        //Given
        KinoSaal kinoSaal = this.kinosaal; //gemockter Kinosaal
        char reihe = 'A';
        int platz = 5;
        //When
        Mockito.when(kinoSaal.getName()).thenReturn("Saal 01");
        Mockito.when(kinoSaal.pruefePlatz(Mockito.any(Character.class), Mockito.any(Integer.class))).thenReturn(true);
        Vorstellung vorstellung = new Vorstellung(kinoSaal, this.zeitfenster, this.datum, this.film, this.preis);
        Ticket ticket = new Ticket(kinoSaal.getName(), this.zeitfenster, this.datum, reihe, platz);
        //Then
        Assertions.assertEquals(ticket, vorstellung.kaufeTicket(reihe, platz, this.preis));
        Assertions.assertEquals(1, vorstellung.getTickets().size());
        Mockito.verify(kinoSaal,Mockito.atLeast(2)).getName(); //Test Anzahl der Aufrufe
        Mockito.verify(kinoSaal).pruefePlatz(Mockito.any(Character.class), Mockito.any(Integer.class)); //Test, ob Methodenaufruf stattfand
    }

    @Test
    @DisplayName("Ticket kaufen mit fehlerhaftem Parameter, Sitzplatz existiert nicht")
    public void testKaufeTicketFehlerhafterInputParameterPlatzExistiertNicht() {
        //Given
        KinoSaal kinoSaal = this.kinosaal; //gemockter Kinosaal
        Vorstellung vorstellung = new Vorstellung(kinoSaal, this.zeitfenster, this.datum, this.film, this.preis);
        //When
        Mockito.when(kinoSaal.pruefePlatz(Mockito.any(Character.class), Mockito.any(Integer.class))).thenReturn(false);
        //Then
        Assertions.assertThrows(IllegalArgumentException.class, () -> vorstellung.kaufeTicket('A', 20, this.preis));
        Assertions.assertThrows(IllegalArgumentException.class, () -> vorstellung.kaufeTicket('D', 5, this.preis));
        Mockito.verify(kinoSaal,Mockito.atLeast(2)).pruefePlatz(Mockito.any(Character.class), Mockito.any(Integer.class)); //Test Anzahl der Aufrufe
    }

    @Test
    @DisplayName("Ticket kaufen mit fehlerhaftem Parameter, Sitzplatz bereits belegt")
    public void testKaufeTicketFehlerhafterInputParameterPlatzBelegt() {
        //Given
        KinoSaal kinoSaal = this.kinosaal; //gemockter Kinosaal
        char reihe = 'A';
        int platz = 5;
        Vorstellung vorstellung = new Vorstellung(kinoSaal, this.zeitfenster, this.datum, this.film, this.preis);
        //When
        Mockito.when(kinoSaal.pruefePlatz(Mockito.any(Character.class), Mockito.any(Integer.class))).thenReturn(true);
        vorstellung.kaufeTicket(reihe, platz, this.preis);
        //Then
        Assertions.assertThrows(IllegalStateException.class, () -> vorstellung.kaufeTicket(reihe, platz, this.preis));
        Mockito.verify(kinoSaal).getName(); //Test, ob Methodenaufruf stattfand
    }

    @Test
    @DisplayName("Ticket kaufen mit fehlerhaftem Parameter, zu wenig Geld")
    public void testKaufeTicketFehlerhafterInputParameterZuWenigGeld() {
        //Given
        KinoSaal kinoSaal = this.kinosaal; //gemockter Kinosaal
        char reihe = 'A';
        int platz = 5;
        Vorstellung vorstellung = new Vorstellung(kinoSaal, this.zeitfenster, this.datum, this.film, this.preis);
        //When
        Mockito.when(kinoSaal.pruefePlatz(Mockito.any(Character.class), Mockito.any(Integer.class))).thenReturn(true);
        //Then
        Assertions.assertThrows(IllegalArgumentException.class, () -> vorstellung.kaufeTicket(reihe, platz, 9.0f));
    }

    @ParameterizedTest(name = "Reihe: {0}, Sitz: {1}, Preis: {2}")
    @CsvSource({"A,5,10.0", "A,6,11.0", "A,7,12.0"})
    @DisplayName("Ticket kaufen mit korrekten Parametern, parametrisierter Test")
    public void testKaufeTicketMitKorrektenParametern(char reihe, int platz, float preis) {
        //Given
        KinoSaal kinoSaal = this.kinosaal;
        //When
        Mockito.when(kinoSaal.getName()).thenReturn("Saal 01");
        Mockito.when(kinoSaal.pruefePlatz(Mockito.any(Character.class), Mockito.any(Integer.class))).thenReturn(true);
        Vorstellung vorstellung = new Vorstellung(kinoSaal, this.zeitfenster, this.datum, this.film, this.preis);
        Ticket ticket = new Ticket(kinoSaal.getName(), this.zeitfenster, this.datum, reihe, platz);
        //Then
        Assertions.assertEquals(ticket, vorstellung.kaufeTicket(reihe, platz, preis));
        Assertions.assertEquals(1, vorstellung.getTickets().size());
        Mockito.verify(kinoSaal,Mockito.atLeast(2)).getName(); //Test Anzahl der Aufrufe
        Mockito.verify(kinoSaal).pruefePlatz(Mockito.any(Character.class), Mockito.any(Integer.class)); //Test, ob Methodenaufruf stattfand
    }



}

