package at.itkolleg.ase.tdd.kino;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

public class KinoSaalTest {

    private KinoSaal kinoSaal;
    private String name;
    private Map<Character, Integer> reihen;

    @BeforeEach
    public void setUp() {
        //Given
        this.name = "Saal 01";
        this.reihen = new HashMap<>();
        this.reihen.put('A', 10);
        this.reihen.put('B', 11);
        this.reihen.put('C', 11);
        this.reihen.put('D', 11);
        this.reihen.put('E', 15);
        this.kinoSaal = new KinoSaal(name, reihen);
    }

    //Getter wurden beim Erzeugen der Instanz getestet, da keinerlei Logik entahlten ist.
    //Ansonsten wäre dies auch zu berücksichtigen

    @Test
    @DisplayName("Kinosaal mit korrekten Parametern anlegen")
    public void testKinoSaalAnlegenKorrekteInputParameter() {
        //Given - in setUp
        //When
        KinoSaal kinoSaal = new KinoSaal(this.name, this.reihen);
        //Then
        Assertions.assertEquals("Saal 01", kinoSaal.getName());
        Assertions.assertEquals(5, kinoSaal.getReihen().size()); //auf Anzahl der Einträge prüfen
    }

    @Test
    @DisplayName("Kinosaal mit falschen Parametern anlegen")
    public void testKinoSaalAnlegenFalscheInputParameter() {
        //Given - in setUp
        //When-Then
        Assertions.assertThrows(InvalidConstructionParameterException.class, () -> new KinoSaal(null, this.reihen));
        Assertions.assertThrows(InvalidConstructionParameterException.class, () -> new KinoSaal(this.name, null));
    }

    @Test
    @DisplayName("Kinosaal Getter Name prüfen")
    public void testGetKinoSaalName(){
        //Given - in setup
        //When
        String kinoSaal = this.kinoSaal.getName();
        //Then
        Assertions.assertEquals(this.name, kinoSaal);
    }

    @Test
    @DisplayName("Sitzplatz im Kinosaal prüfen erfolgreich")
    public void testPruefePlatzKorrekteInputParameter() {
        //Given
        KinoSaal kinoSaal = this.kinoSaal;
        //When & Then
        Assertions.assertTrue(kinoSaal.pruefePlatz('A', 10));
        Assertions.assertEquals(true, kinoSaal.pruefePlatz('A', 5)); //andere Schreibweise
    }

    @Test
    @DisplayName("Platz prüfen fehlerhaft")
    public void testPruefePlatzFalscheInputParameter() {
        //Given
        KinoSaal kinoSaal = this.kinoSaal;
        //When & Then
        Assertions.assertFalse(kinoSaal.pruefePlatz('A', 0));
        Assertions.assertEquals(false, kinoSaal.pruefePlatz('A', 11)); //andere Schreibweise
    }

}
