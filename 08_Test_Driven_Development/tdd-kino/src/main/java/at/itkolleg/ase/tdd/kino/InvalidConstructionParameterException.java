package at.itkolleg.ase.tdd.kino;

public class InvalidConstructionParameterException extends RuntimeException{
    public InvalidConstructionParameterException() {
        super("Falscher Konstruktionsparameter");
    }

    public InvalidConstructionParameterException(String message) {
        super(message);
    }
}
