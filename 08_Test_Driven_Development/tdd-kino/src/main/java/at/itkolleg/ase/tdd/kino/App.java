package at.itkolleg.ase.tdd.kino;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

/**
 * Dieses Beispiel stammt aus https://training.cherriz.de/cherriz-training/1.0.0/testen/junit5.html
 */
public class App {
    public static void main(String[] args) {
        //Saal anlegen
        Map<Character, Integer> map = new HashMap<>();
        map.put('A', 10);
        map.put('B', 10);
        map.put('C', 15);
        KinoSaal kinoSaal = new KinoSaal("LadyX", map);

        //Platz prüfen
        System.out.println(kinoSaal.pruefePlatz('A', 11));
        System.out.println(kinoSaal.pruefePlatz('A', 10));
        System.out.println(kinoSaal.pruefePlatz('B', 10));
        System.out.println(kinoSaal.pruefePlatz('C', 14));

        //...
        /*Kinosäle anlegen
          Vorstellungen anlegen
          Vorstellungen über die Kinoverwaltung einplanen
          Tickets für Vorstellungen ausgeben*/
        LocalDate date = LocalDate.of(2022, 3, 16);
        Vorstellung vorstellung = new Vorstellung(kinoSaal, Zeitfenster.ABEND, date, "Spider-Man", 10.0f);

        KinoVerwaltung kinoVerwaltung = new KinoVerwaltung();
        kinoVerwaltung.einplanenVorstellung(vorstellung);

        System.out.println(kinoVerwaltung.kaufeTicket(vorstellung,'A', 10, 10.0f));
        System.out.println(kinoVerwaltung.kaufeTicket(vorstellung,'A', 10, 10.0f));
    }
}
