package at.itkolleg.ase.tdd.kino;

import java.util.Map;

public class KinoSaal {

    private final String name;

    private final Map<Character, Integer> reihen;

    public KinoSaal(String name, Map<Character, Integer> reihen) {
        if(name == null || name.isEmpty()) throw new InvalidConstructionParameterException("Kinosaal-Name darf nicht leer oder null sein");
        if(reihen == null) throw new InvalidConstructionParameterException("Kinoreihen dürfen nicht null sein");
        //hier evtl. auf Anzahl Sitze und Reihenbezeichnung prüfen (Businesslogik)
        this.name = name;
        this.reihen = reihen;
    }

    public String getName() {
        return name;
    }

    public Map<Character, Integer> getReihen() {
        return reihen;
    }

    boolean pruefePlatz(char reihe, int platz) {
        Integer platze = reihen.get(reihe);
        if (platze == null || platz > platze || platz == 0) {
            return false;
        }
        return true;
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof KinoSaal)) return false;
        KinoSaal kinoSaal = (KinoSaal) o;
        return getName().equals(kinoSaal.getName()) && getReihen().equals(kinoSaal.getReihen());
    }

}
