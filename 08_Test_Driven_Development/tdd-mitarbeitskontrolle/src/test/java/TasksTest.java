import domain.*;
import exceptions.DeadlineNotValidException;
import exceptions.TaskDescriptionNotValidException;
import exceptions.TaskPriorityNotValidException;
import exceptions.TaskTitleNotValidException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.time.LocalDateTime;
import java.time.Month;
import java.util.Set;

public class TasksTest {
    // Alle Methoden der Taskklasse sind mit maximaler Code-Coverage zu testen
    // Es sind an passenden Stellen parametrisierte Tests zu verwenden

    private Task task;
    private TaskTitle taskTitle;
    private TaskDescription taskDescription;
    private LocalDateTime deadline;
    private TaskPriority taskPriority;

    @BeforeEach
    public void setUp() {
        this.taskTitle = new TaskTitle("Task 1");
        this.taskDescription = new TaskDescription("Description Task 1");
        this.deadline = LocalDateTime.of(2022, Month.MARCH, 16, 13, 0);
        this.taskPriority = new TaskPriority(1);
        this.task = new Task(this.taskTitle, this.taskDescription, this.deadline, this.taskPriority);
    }

    @Test
    @DisplayName("Teste neuen Task anlegen mit korrekten Parametern")
    public void test_taskAnlegenKorrekteInputParameter() {
        //Given
        Task task = new Task(this.taskTitle, this.taskDescription, this.deadline, this.taskPriority);
        //Then
        Assertions.assertEquals(this.taskTitle, task.getTaskTitle());
        Assertions.assertEquals(this.taskDescription, task.getTaskDescription());
        Assertions.assertEquals(this.deadline, task.getDeadline());
        Assertions.assertEquals(this.taskPriority, task.getTaskPriority());
    }

    @Test
    @DisplayName("Teste neuen Task anlegen mit falschen Parametern")
    public void test_TaskAnlegenFalscheInputParameter_exception() {
        //Given - in setUp
        //When-Then
        Exception exception = Assertions.assertThrows(TaskTitleNotValidException.class,
                () -> new Task(null, this.taskDescription, this.deadline, this.taskPriority));
        Assertions.assertEquals(exception.getMessage(),
                "Task title not valid, must contain at least 2 charachters!");
    }

    @Test
    @DisplayName("Hinzufügen eines gütliten Tags testen ")
    public void test_AddTag() {
        //Given
        Task task = this.task;
        Tag tag = new Tag("Tag 01");
        //When
        task.addTag(tag);
        Set<Tag> tagList = task.getTagList();
        //Then
        Assertions.assertEquals(1, tagList.size());
        Assertions.assertTrue(tagList.contains(tag));
    }

    @ParameterizedTest(name = "Tagname: {0}")
    @CsvSource({"Tag01", "Tag02", "Tag03"})
    public void test_AddTags_Parametrisiert(String tagname) {
        //Given
        Task task = this.task;
        Tag tag = new Tag(tagname);
        //When
        task.addTag(tag);
        Set<Tag> tagList = task.getTagList();
        //Then
        Assertions.assertEquals(1, tagList.size());
        Assertions.assertTrue(tagList.contains(tag));
    }

    @Test
    @DisplayName("Holen einer Tagliste")
    public void test_getTagList() {
        //Given
        Task task = this.task;
        Tag tag1 = new Tag("Tag 01");
        Tag tag2 = new Tag("Tag 02");
        task.addTag(tag1);
        task.addTag(tag2);
        //When
        Set<Tag> tagList = task.getTagList();
        //Then
        Assertions.assertEquals(2, tagList.size());
        Assertions.assertTrue(tagList.contains(tag1));
    }

    @Test
    @DisplayName("Pinnen eines Tasks")
    public void test_pinTask() {
        //Given
        Task task = this.task;
        //When
        task.pinTask();
        // Then
        Assertions.assertTrue(task.isPinned());
    }

    @Test
    @DisplayName("Unpinnen eines Tasks")
    public void test_unpinTask() {
        //Given
        Task task = this.task;
        //When
        task.unpinTask();
        // Then
        Assertions.assertFalse(task.isPinned());
    }

    @Test
    @DisplayName("Ändern des Tagnamens")
    public void test_changeTagName() {
        //Given
        Task task = this.task;
        Tagname tagnameFrom = new Tagname("Tag 01");
        Tagname tagnameTo = new Tagname("New Tag");

        Tag tag = new Tag(tagnameFrom.tagname());
        task.addTag(tag);
        //When
        task.changeTagName(tagnameFrom, tagnameTo);
        Set<Tag> tagList = task.getTagList();
        Tag[] tags = new Tag[1];
        task.getTagList().toArray(tags);
        // Then
        Tag newTag = tags[tags.length - 1];
        Assertions.assertEquals(tagnameTo, newTag.getTagname());
    }

    @Test
    @DisplayName("Taskstatus ändern")
    public void test_setTaskStatusTo() {
        //Given
        Task task = this.task;
        TaskStatus taskStatus = TaskStatus.open;
        //When
        task.setTaskStatusTo(taskStatus);
        // Then
        Assertions.assertEquals(taskStatus, task.getTaskStatus());
    }

    @Test
    @DisplayName("Setzen des Tasktitels")
    public void test_setTaskTitle() {
        //Given
        Task task = this.task;
        TaskTitle taskTitle = new TaskTitle("new Task");
        //When
        task.setTaskTitle(taskTitle);
        //Then
        Assertions.assertEquals(taskTitle, task.getTaskTitle());
    }

    @Test
    @DisplayName("Setzen des Tasktitels, TaskTitleNotValidException")
    public void test_setTaskTitle_exception() {
        //Given
        Task task = this.task;
        //When - Then
        Exception exception = Assertions.assertThrows(TaskTitleNotValidException.class,
                () -> task.setTaskTitle(null));
        Assertions.assertEquals(exception.getMessage(),
                "Task title not valid, must contain at least 2 charachters!");
    }

    @Test
    @DisplayName("Setzen einer Beschreibung")
    public void test_setTaskDescription() {
        //Given
        Task task = this.task;
        TaskDescription taskDescription = new TaskDescription("new Description");
        //When
        task.setTaskDescription(taskDescription);
        //Then
        Assertions.assertEquals(taskDescription, task.getTaskDescription());
    }

    @Test
    @DisplayName("Setzen einer Beschreibung, TaskDescriptionNotValidException")
    public void test_setTaskDescription_exception() {
        //Given
        Task task = this.task;
        //When - Then
        Exception exception = Assertions.assertThrows(TaskDescriptionNotValidException.class,
                () -> task.setTaskDescription(null));
        Assertions.assertEquals(exception.getMessage(),
                "Task description not valid. Must contain at least one character!");
    }

    @Test
    @DisplayName("Setzen der TaskDeadline")
    public void test_setTaskDeadline() {
        //Given
        Task task = this.task;
        LocalDateTime deadline = LocalDateTime.of(2022, Month.MARCH, 17, 13, 0);
        //When
        task.setDeadline(deadline);
        //Then
        Assertions.assertEquals(deadline, task.getDeadline());
    }

    @Test
    @DisplayName("Setzen der TaskDeadline, DeadlineNotValidException")
    public void test_setTaskDeadline_exception() {
        //Given
        Task task = this.task;
        //When - Then
        Exception exception = Assertions.assertThrows(DeadlineNotValidException.class,
                () -> task.setDeadline(null));
        Assertions.assertEquals(exception.getMessage(),
                "Invalid deadline!");
    }

    @Test
    @DisplayName("Setzen der TaskOriority")
    public void test_setTaskPriority() {
        //Given
        Task task = this.task;
        TaskPriority taskPriority = new TaskPriority(2);
        //When
        task.setTaskPriority(taskPriority);
        //Then
        Assertions.assertEquals(taskPriority, task.getTaskPriority());
    }

    @Test
    @DisplayName("Setzen der TaskOriority, TaskPriorityNotValidException")
    public void test_setTaskPriority_exception() {
        //Given
        Task task = this.task;
        //When - Then
        Exception exception = Assertions.assertThrows(TaskPriorityNotValidException.class,
                () -> task.setTaskPriority(null));
        Assertions.assertEquals(exception.getMessage(),
                "Task priority not valid, must be between 1 und 10");
    }

}
