import domain.*;
import exchange.DataImport;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;

public class TaskListTests {
    // Alle Methoden der Taskliste sind mit maximaler Code-Coverage zu testen
    // Es sind an passenden Stellen parametrisierte Tests zu verwenden
    // Die importData-Methode muss mit Mock getestet werden.

    DataImport dataImport;
    private TaskList taskList;
    private Task task;
    private Task gemockterTask;

    @BeforeEach
    private void setUp() {
        this.gemockterTask = Mockito.mock(Task.class);
        this.dataImport = Mockito.mock(DataImport.class);
        this.taskList = new TaskList();

        TaskTitle taskTitle = new TaskTitle("Task 1");
        TaskDescription taskDescription = new TaskDescription("Description Task 1");
        LocalDateTime deadline = LocalDateTime.of(2022, Month.MARCH, 16, 13, 0);
        TaskPriority taskPriority = new TaskPriority(1);
        this.task = new Task(taskTitle, taskDescription, deadline, taskPriority);
    }

    @Test
    @DisplayName("Hinzufügen eines gütliten gemockten Tasks testen ")
    public void test_AddTask() {
        //Given
        TaskList taskList = this.taskList;
        Task task = this.gemockterTask;
        //When
        taskList.addTask(task);
        List<Task> tasks = taskList.getAllTasks();
        //Then
        Assertions.assertEquals(1, tasks.size());
    }

    @Test
    @DisplayName("Hinzufügen eines ungütliten Tasks testen")
    public void test_AddTask_Exception() {
        //Given
        TaskList taskList = this.taskList;
        //When
        taskList.addTask(null);
        List<Task> tasks = taskList.getAllTasks();
        //Then
        Assertions.assertEquals(0, tasks.size());
    }


    @Test
    @DisplayName("Löschen eines Tasks an Index aus  Liste")
    public void test_deleteTaskAtIndex() {
        //Given
        TaskList taskList = this.taskList;
        Task task = this.gemockterTask;
        int index = 0;
        //When
        taskList.addTask(task);
        taskList.addTask(task);
        taskList.deleteTaskAtIndex(index);
        List<Task> tasks = taskList.getAllTasks();
        //Then
        Assertions.assertEquals(1, tasks.size());
    }

    @Test
    @DisplayName("Löschen eines Tasks an falschem Index aus  Liste, Fehlgeschlagen")
    public void test_deleteTaskAtIndex_fehlgeschlagen() {
        //Given
        TaskList taskList = this.taskList;
        Task task = this.gemockterTask;
        int index = 3;
        //When
        taskList.addTask(task);
        taskList.addTask(task);
        taskList.deleteTaskAtIndex(index);
        List<Task> tasks = taskList.getAllTasks();
        //Then
        Assertions.assertEquals(2, tasks.size());
    }

    @Test
    @DisplayName("Holen eines Tasks anhand des Indizes aus Liste")
    public void test_getTaskFromIndex() {
        //Given
        TaskList taskList = this.taskList;
        Task task = this.gemockterTask;
        int index = 0;
        //When
        taskList.addTask(task);
        Task taskFromIndex = taskList.getTaskFromIndex(index);
        //Then
        Assertions.assertEquals(task, taskFromIndex);
    }

    @Test
    @DisplayName("Holen eines Tasks anhand eines falschen Indizes aus Liste, Fehlerhaft")
    public void test_getTaskFromIndex_fehlerhaft() {
        //Given
        TaskList taskList = this.taskList;
        Task task = this.gemockterTask;
        int index = 1;
        //When
        taskList.addTask(task);
        Task taskFromIndex = taskList.getTaskFromIndex(index);
        //Then
        Assertions.assertNull(taskFromIndex);
    }

    @Test
    @DisplayName("Holen einer Liste von Tasks, welche mit einem bestimmten Tag getagged wurden")
    public void test_getAllTasksWithTagTask() {
        //Given
        TaskList taskList = this.taskList;
        Task task = this.task;
        Tag tag = new Tag("Tag 01");
        task.addTag(tag);
        //When
        taskList.addTask(task);
        List<Task> tasks = taskList.getAllTasksWithTag(tag);
        //Then
        Assertions.assertEquals(1, tasks.size());
    }

    @Test
    @DisplayName("Importieren einer List von Importdaten, Erfolgreich")
    public void test_importDate() {
        //Given
        DataImport dataImport = this.dataImport;
        List<Task> taskListDummy = new ArrayList<>();
        taskListDummy.add(this.task);
        //When
        Mockito.when(dataImport.importTasks()).thenReturn(taskListDummy);
        this.taskList.importData(dataImport);
        List<Task> taskList = this.taskList.getAllTasks();
        //Then
        Assertions.assertEquals(1, taskList.size());
        Assertions.assertEquals(this.task, taskList.get(0));
        Mockito.verify(dataImport, Mockito.atLeast(1)).importTasks();
    }
}
