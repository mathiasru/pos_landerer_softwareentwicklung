import domain.*;
import exceptions.LoadTaskListFromFileException;
import exceptions.SaveTaskListToFileException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import persistence.FileManagement;
import persistence.FileManagementImplementation;

import java.time.LocalDateTime;
import java.time.Month;

public class SaveLoadTasklistTests {
    // Alle Methoden der File-Management-Klasse mit maximaler Code-Coverage sind zu testen
    FileManagement fileManagement;
    TaskList taskList;
    Task task;

    @BeforeEach
    public void setUp() {
        this.fileManagement = new FileManagementImplementation();
        this.taskList = new TaskList();

        TaskTitle taskTitle = new TaskTitle("Task 1");
        TaskDescription taskDescription = new TaskDescription("Description Task 1");
        LocalDateTime deadline = LocalDateTime.of(2022, Month.MARCH, 16, 13, 0);
        TaskPriority taskPriority = new TaskPriority(1);
        this.task = new Task(taskTitle, taskDescription, deadline, taskPriority);
        this.taskList.addTask(this.task);
    }

    @Test
    @DisplayName("Laden einer Tasklist aus File, Erfolgreich")
    public void test_saveTaskListToFile() {
        //Given
        FileManagement fileManagement = this.fileManagement;
        TaskList taskList = new TaskList();
        //When
        try {
            fileManagement.saveTaskListToFile(this.taskList);
        } catch (SaveTaskListToFileException e) {
            e.printStackTrace();
        }
        //Tests sollten unabhängig voneonander sein (FIRST-Prinzip)
        try {
            taskList = fileManagement.loadTaskListFromFile();
        } catch (LoadTaskListFromFileException e) {
            e.printStackTrace();
        }
        //Then
        Assertions.assertTrue(taskList.getAllTasks().size() > 0);
    }
    
    @Test
    @DisplayName("Laden einer Tasklist aus File, Exception")
    public void test_saveTaskListToFile_exception() {
        //Given
        FileManagement fileManagement = this.fileManagement;
        //When - Then
        //Hier müsste eine Exception ausgelöst werden
        Assertions.assertThrows(SaveTaskListToFileException.class, () -> fileManagement.saveTaskListToFile(this.taskList));
    }


    @Test
    @DisplayName("Laden einer Tasklist aus File, Erfolgreich")
    public void test_loadTaskListFromFile() {
        //Given
        FileManagement fileManagement = this.fileManagement;
        TaskList taskList = new TaskList();
        //When
        try {
            taskList = fileManagement.loadTaskListFromFile();
        } catch (LoadTaskListFromFileException e) {
            e.printStackTrace();
        }
        //Then
        Assertions.assertTrue(taskList.getAllTasks().size() > 0);
    }

    @Test
    @DisplayName("Laden einer Tasklist aus File, Exception")
    public void test_loadTaskListFromFile_exception() {
        //Given
        FileManagement fileManagement = this.fileManagement;
        //When - Then
        //Hier müsste eine Exception ausgelöst werden
        Assertions.assertThrows(LoadTaskListFromFileException.class, () -> fileManagement.loadTaskListFromFile());
    }


}
