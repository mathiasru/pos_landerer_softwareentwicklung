public class Kommentar {
    public String kommentartex;
    public String autor;
    public int bewertung;
    public int stimmen;

    public Kommentar(String kommentartex, String autor, int bewertung) {
        if (kommentartex == null || autor == null || bewertung < 1 || bewertung > 5){
            throw new InvalidConstructionParameterException();
        }
        this.kommentartex = kommentartex;
        this.autor = autor;
        this.bewertung = bewertung;
    }

    public void zustimmen(){
        this.stimmen++;
    }

    public String getKommentarTex() {
        return kommentartex;
    }

    public int getStimmen() {
        return stimmen;
    }

    public String getAutor() {
        return autor;
    }

    public int getBewertung() {
        return bewertung;
    }
}
