import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

public class KommentarTest {
    public String kommentartext;
    public String autor;
    public int bewertung;

    @BeforeEach
    public void setUp() {
        this.kommentartext = "Das ist ein Kommentartext";
        this.autor = "Mathias Rudig";
        this.bewertung = 3;
    }

    @Test
    public void testAnlegen_korrekteInputParameter_kommentarAnlegen() {
        //Given - siehe setup
        //When
        Kommentar k1 = new Kommentar(this.kommentartext, this.autor, this.bewertung);
        //Then
        Assertions.assertEquals("Das ist ein Kommentartext", k1.getKommentarTex());
        Assertions.assertEquals(0, k1.getStimmen());
        Assertions.assertEquals(3, k1.getBewertung());
        Assertions.assertEquals("Mathias Rudig", k1.getAutor());
    }

    @Test
    public void testAnlegen_falscheInputParameter_kommentarAnlegen() {
        //Given
        String kommentartext = "Das ist ein Kommentartext";
        String autor = "Mathias Rudig";
        int bewertung = 3;
        //WHEN-Then
        Assertions.assertThrows(InvalidConstructionParameterException.class, () -> new Kommentar(null, autor, bewertung));
        Assertions.assertThrows(InvalidConstructionParameterException.class, () -> new Kommentar(kommentartext, null, bewertung));
        Assertions.assertThrows(InvalidConstructionParameterException.class, () -> new Kommentar(kommentartext, autor, 0));
        Assertions.assertThrows(InvalidConstructionParameterException.class, () -> new Kommentar(kommentartext, autor, 6));

    }

    @Test
    public void testAnlegen_falscheInputParameter_kommentarAnlegenV2() {
        //Given
        String kommentartext = "Das ist ein Kommentartext";
        String autor = "Mathias Rudig";
        int bewertung = 3;
        //WHEN-Then
        ArrayList<Throwable> arrayList = new ArrayList<>();
        try {
            Assertions.assertThrows(InvalidConstructionParameterException.class, () -> new Kommentar(null, autor, bewertung));
        } catch (AssertionError e1) {
            arrayList.add(e1);
        }
        try {
            Assertions.assertThrows(InvalidConstructionParameterException.class, () -> new Kommentar(kommentartext, null, bewertung));
        } catch (AssertionError e2) {
            arrayList.add(e2);
        }
        System.out.println(arrayList.size());
        if (arrayList.size() > 0) {
            System.out.println(arrayList);
            Assertions.fail();
        }
    }

    @Test
    public void testKommentarZustimmen() {
        //Given
        String kommentartext = "Das ist ein Kommentartext";
        String autor = "Mathias Rudig";
        int bewertung = 3;
        //When
        Kommentar k1 = new Kommentar(kommentartext, autor, bewertung);
        //Then
        Assertions.assertEquals(0, k1.getStimmen());
        //When
        k1.zustimmen();
        //Then
        Assertions.assertEquals(1, k1.getStimmen());
    }

    /*@AfterAll
    public void tearDown(){
        System.out.println("After All");
    }*/
}
